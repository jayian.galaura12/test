package de.com.limto.limto1.Controls;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class NoSelectionAutoComplete extends androidx.appcompat.widget.AppCompatAutoCompleteTextView {

    public NoSelectionAutoComplete(Context context) {
        super(context);
    }

    public NoSelectionAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoSelectionAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void replaceText(CharSequence text) {
        super.replaceText(text);
    }

    @Override
    public void dismissDropDown() {
        super.dismissDropDown();
    }
}