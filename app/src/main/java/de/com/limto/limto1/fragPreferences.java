package de.com.limto.limto1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.Arrays;

import static de.com.limto.limto1.fragSettings.ENTF;
import static de.com.limto.limto1.fragSettings.GRADE;

public class fragPreferences extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    public final static int fragID = 8;
    public MainActivity _main;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.fragpreferences, rootKey);
        ListPreference listPreference = (ListPreference) findPreference("prefgrade");
        int i = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5) - 1;
        listPreference.setValueIndex(i);
        listPreference.setTitle(getString(R.string.Kontaktgrad) + " " + (i + 1));

        listPreference = (ListPreference) findPreference("prefradius");
        int Entf = getActivity().getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);
        String E = (Entf == 1000 ? getString(R.string.beliebig) : Entf + getString(R.string._km));
        int pos = Arrays.asList(getActivity().getResources().getStringArray(R.array.radius)).indexOf(E);
        listPreference.setValueIndex(pos);
        listPreference.setTitle(getString(R.string.Entfernung) + " " + E);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //IT NEVER GETS IN HERE!
        if (key.equals("prefgrade")) {
            // Set summary to be the user-description for the selected value
            Preference Pref = findPreference(key);
            String grade = sharedPreferences.getString(key, "");
            int i = Arrays.asList(getResources().getStringArray(R.array.grades)).indexOf(grade);
            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putInt(GRADE, i + 1).apply();
            getActivity().getApplicationContext().getSharedPreferences(GRADE, Context.MODE_PRIVATE).edit().putInt(GRADE, i + 1).apply();
            ((MainActivity) getActivity()).lastEndGrad = i + 1;
            Pref.setTitle(getString(R.string.Kontaktgrad) + " " + (i + 1));
        } else if (key.equals("prefradius")) {
            Preference Pref = findPreference(key);
            ListPreference listPreference = (ListPreference) Pref;
            int radius = 1000;
            String r = sharedPreferences.getString(key, "");
            int i = Arrays.asList(getResources().getStringArray(R.array.radius)).indexOf(r);
            if (i >= 0) {
                String entf = r;
                if (!entf.equalsIgnoreCase(getString(R.string.beliebig))) {
                    radius = Integer.parseInt(entf.replace(getString(R.string._km), ""));
                }
                getActivity().getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).edit().putInt(ENTF, radius).apply();
            }
            listPreference.setTitle(getString(R.string.Entfernung) + " " + r);
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }
}