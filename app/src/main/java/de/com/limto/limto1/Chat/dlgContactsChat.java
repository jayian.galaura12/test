package de.com.limto.limto1.Chat;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.com.limto.limto1.AllContactsAdapter;
import de.com.limto.limto1.Constants;
import de.com.limto.limto1.ContactVO;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

public class dlgContactsChat extends DialogFragment {

    //public final static int fragID = 3;
    private static final String TAG = "fragContacts";
    public static final String ASC = " ASC";
    public static final String EQUAL = "equal";
    public static boolean listLoaded = false;
    public static List<ContactVO> contactVOListAndroid;
    public static ArrayList<ContactVO> ArrCopied = new ArrayList<ContactVO>();
    public MainActivity _main;
    RecyclerView rvContacts;
    ProgressBar pb;
    AllContactsAdapter contactAdapter;
    private boolean isvisible;
    private ImageView ivClose;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_Dialog_dlgchat);
        if (_main == null) _main = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dlgcontactschat, container, false);

        rvContacts = (RecyclerView) v.findViewById(R.id.rvContacts);
        final ProgressBar pb = (ProgressBar) v.findViewById(R.id.pb);
        this.pb = pb;
        ivClose = (ImageView) v.findViewById(R.id.ivCloseButton);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgContactsChat.this.dismiss();
            }
        });
        return v;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
        }
        new ListAsyncTask(this).execute();
    }

    private void setAdapter(List<ContactVO> contactVOs) throws Throwable {
        contactAdapter = new AllContactsAdapter(contactVOs, getActivity().getApplicationContext(), this);

        rvContacts.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rvContacts.setAdapter(contactAdapter);

    }

    public List<ContactVO> updateContactsFromAddressbook(AppCompatActivity a) throws CloneNotSupportedException {
        List<ContactVO> listCompare = new ArrayList();
        ContactVO contactVO;
        JSONArray contacts;

        contacts = loadContactsGradeOneFromServer();

        ArrayList<Long> loadedContacts = new ArrayList<>();

        loadedContacts = createContactVOsAndAddMemberData(contacts, listCompare, loadedContacts);

        addDlgChatMembersToList(loadedContacts, listCompare);

        addBlockedUsersToList(loadedContacts, listCompare);

        // updateContactsFromAddressbook(a,true , listCompare);

        return listCompare;
    }

    private void addDlgChatMembersToList(ArrayList<Long> loadedContacts, List<ContactVO> listCompare) {
        if (_main.fragChat != null && _main.fragChat.MemberData.values().size() > 0)
        {
            for (dlgChat.MemberData memberData : _main.fragChat.MemberData.values().toArray(new dlgChat.MemberData[0])) {
                try {
                    if (memberData.getBenutzerid() < 0) continue;
                    ContactVO vo = null;
                    if (!loadedContacts.contains(memberData.getBenutzerid())) {
                        vo = new ContactVO(_main.clsHTTPS.getUser(_main.getBenutzerID(), memberData.getBenutzerid()));
                    }
                    else
                    {
                        vo = getExistingVO(listCompare, memberData.getBenutzerid());
                    }

                    memberData.setKontaktgrad(vo.getKontaktgrad());
                    memberData.setContactVO(vo);
                    listCompare.add(0,vo);
                    loadedContacts.add (vo.getUserID());
                    vo.setMemberData(memberData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private ContactVO getExistingVO(List<ContactVO> listCompare, Long BenutzerID) throws Exception {
        boolean found = false;
        ContactVO vo = null;
        for (int i = 0; i <= listCompare.size(); i++)
        {
            vo = listCompare.get(i);
            if (vo.getUserID() == BenutzerID)
            {
                found = true;
                listCompare.remove(i);
                break;
            }
        }
        if (!found) throw new Exception("Contact not found!");
        return vo;
    }

    private void addBlockedUsersToList(ArrayList<Long> loadedContacts, List<ContactVO> listCompare) {
        if (_main.fragChat != null && _main.fragChat.BlockedUsers.size() > 0)
        {
            for (Long BenutzerID : _main.fragChat.BlockedUsers) {
                try {
                    ContactVO vo = null;
                    if (!loadedContacts.contains(BenutzerID)) {
                        vo = new ContactVO(_main.clsHTTPS.getUser(_main.getBenutzerID(), BenutzerID));
                    }
                    else
                    {
                        vo = getExistingVO(listCompare, BenutzerID);
                    }

                    listCompare.add(listCompare.size(),vo);
                    loadedContacts.add (vo.getUserID());
                    vo.setBlocked(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private ArrayList<Long> createContactVOsAndAddMemberData(JSONArray contacts, List<ContactVO> listCompare, ArrayList<Long> loadedContacts) {
        if (contacts.length() > 0) {
            for (int i = 0; i < contacts.length(); i++) {
                try {
                    JSONObject user = contacts.getJSONObject(i);

                    ContactVO contactVO = new ContactVO(user);
                    if (_main.fragChat != null && _main.fragChat.MemberData.values().size() > 0)
                    {
                        dlgChat.MemberData m = _main.fragChat.MemberData.get(contactVO.getUserID());
                        if (m != null) {
                            m.setKontaktgrad(contactVO.getKontaktgrad());
                            m.setContactVO(contactVO);
                            contactVO.setMemberData(m);
                        }
                    }
                    if (!loadedContacts.contains(contactVO.getUserID())) {
                        listCompare.add(contactVO);
                        loadedContacts.add(contactVO.getUserID());
                    }
                    else
                    {
                        throw new Exception("Double Contact");
                    }
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }
        return loadedContacts;
    }

    private JSONArray loadContactsGradeOneFromServer() {
        {
            try {
                JSONArray contacts = _main.clsHTTPS.getUsers(_main.user.getLong(Constants.id), 1, false);
                return contacts;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    public void updateContactsFromAddressbook(AppCompatActivity a, boolean listLoaded, List<ContactVO> listCompare) throws CloneNotSupportedException {
        List<ContactVO> contactVOList = new ArrayList();
        ContactVO contactVO;

        ContentResolver contentResolver = a.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + ASC);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                contactVO = new ContactVO();
                contactVO.setContactName(name);

                if (hasPhoneNumber > 0) {
                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    while (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        if (phoneNumber.length() == 0) continue;
                        phoneNumber = lib.formatPhoneNumber(phoneNumber);
                        if (phoneNumber.length() == 0 || contactVO.getContactNumber().contains(phoneNumber))
                            continue;
                        contactVO.setContactNumber(phoneNumber);
                    }

                    phoneCursor.close();
                }
                Cursor emailCursor = contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);

                while (emailCursor.moveToNext()) {
                    String emailId = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    emailId = emailId.trim().toLowerCase();
                    if (emailId.length() == 0 || contactVO.getContactEmail().contains(emailId))
                        continue;
                    contactVO.setContactEmail(emailId);
                }

                emailCursor.close();
                boolean hasChildren = false;
                if (listLoaded && listCompare != null) {
                    for (ContactVO c : listCompare) {
                        String EMailLimContact = c.getContactEmail().toLowerCase() + ",";
                        String EMailContact = contactVO.getContactEmail().toLowerCase() + ",";
                        String phoneLimContact = c.getContactNumber() + ",";
                        String phoneContact = contactVO.getContactNumber() + ",";
                        if (phoneContact.contains(phoneLimContact)) {
                            System.out.println(EQUAL);
                        }

                        if ((EMailContact.length() > 3 && EMailLimContact.length() > 3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length() > 3 && phoneLimContact.length() > 3 && phoneContact.contains(phoneLimContact))) {
                            //contactVO.setIsLimindoContact(true);
                            //contactVO.setUserID((c.getUserID()));
                            //contactVO.setContactID(c.getContactID());
                            //contactVO.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            if (!c.isCopied && !contactVO.isCopied) {
                                c.setIsLimindoContact(true);
                            }
                        }
                    }
                }




            }
        }

    }


    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        isvisible = false;
        /*
        if (listLoaded && dlgContactsChat.contactVOListAndroid.size() > 0) {
            _main.mPager.setCurrentItem(fragQuestions.fragID);
            fragQuestions qq = (fragQuestions) _main.fPA.findFragment(fragQuestions.fragID);
            if (qq != null) {
                qq.btnKontakte.performClick();
            }
        } else {
            _main.mPager.setCurrentItem(fragContacts.fragID);
        }

         */
    }

    public void reLoad() {
        new ListAsyncTask(this).execute();
    }

    private static class ListAsyncTask extends AsyncTask<Void, Void, List<ContactVO>> {

        private final dlgContactsChat _dlgContacts;
        private List<ContactVO> listCompare;

        ListAsyncTask(dlgContactsChat dlgContacts) {
            this._dlgContacts = dlgContacts;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            _dlgContacts.pb.setVisibility(View.VISIBLE);
            _dlgContacts.rvContacts.setVisibility(View.GONE);
        }

        @Override
        protected List<ContactVO> doInBackground(Void... integers) {
            try {
                List<ContactVO> res = _dlgContacts.updateContactsFromAddressbook(_dlgContacts._main);
                return res;
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ContactVO> contactVOs) {
            super.onPostExecute(contactVOs);
            try {
                if (_dlgContacts == null || contactVOs == null) return;
                _dlgContacts.setAdapter(contactVOs);
                _dlgContacts.pb.setVisibility(View.GONE);
                _dlgContacts.rvContacts.setVisibility(View.VISIBLE);
                dlgContactsChat.contactVOListAndroid = contactVOs;
                dlgContactsChat.listLoaded = true;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }
    }
}
