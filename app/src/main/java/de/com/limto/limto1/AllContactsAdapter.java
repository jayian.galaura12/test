package de.com.limto.limto1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;

import de.com.limto.limto1.Chat.dlgContactsChat;
import de.com.limto.limto1.lib.LetterTileProvider;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.MainActivity.ERROR_;
import static de.com.limto.limto1.MainActivity.RGLOBAL;
import static de.com.limto.limto1.MainActivity.blnAccessibility;
import static de.com.limto.limto1.clsHTTPS.ACCESSKEY;
import static de.com.limto.limto1.clsHTTPS.Delimiter_A;
import static de.com.limto.limto1.clsHTTPS.ERROR_UNABLE_TO_RESOLVE_HOST;
import static de.com.limto.limto1.clsHTTPS.GET;
import static de.com.limto.limto1.clsHTTPS.MYURLROOT;
import static de.com.limto.limto1.clsHTTPS.getSSLFactory;
import static de.com.limto.limto1.fragQuestions.BENUTZER_ID;
import static de.com.limto.limto1.fragQuestions.ERROR;
import static de.com.limto.limto1.lib.lib.ShowMessage;

public class AllContactsAdapter extends RecyclerView.Adapter<AllContactsAdapter.ContactViewHolder> {

    private static final String TAG = "AllContactsAdapter";
    public static final String XXX = "xxx";
    public static final String MAILTO = "mailto";
    public static final String SMSTO = "smsto:";
    public static final String TEXT_PLAIN = "text/plain";
    public static final String TRUE = "TRUE";
    private final Fragment fragment;
    private final MainActivity _main;
    private final boolean isDlgChat;
    List<ContactVO> contactVOList;
    private Context mContext;
    private Drawable origBackColor = null;
    private boolean isUnconfirmed;
    private boolean isInvited;

    public AllContactsAdapter(List<ContactVO> contactVOList, Context mContext, Fragment fragment) {
        this.contactVOList = contactVOList;
        this.mContext = mContext;
        this.fragment = fragment;
        this._main = (MainActivity) fragment.getActivity();
        this.isDlgChat = this.fragment instanceof dlgContactsChat;
        try {
            for (ContactVO contactVO : contactVOList) {
                if (_main != null && isDlgChat && contactVO.getUserID() == _main.getBenutzerID()) {
                    contactVOList.remove(contactVO);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        isUnconfirmed = this.fragment instanceof fragContacts2 && ((fragContacts2) this.fragment).getUnconfirmed;
        isInvited = this.fragment instanceof fragContacts2 && ((fragContacts2) this.fragment).getInvited;
        if (isInvited) {
            System.out.println(isInvited);
        }
        View view = LayoutInflater.from(mContext).inflate(lib.getLayoutResFromTheme(_main, R.attr.single_contact_view), null);
        return new ContactViewHolder(view);
    }

    public static class GetImageTask extends AsyncTask<Void, Void, Bitmap> {
        private final MainActivity main;
        private final ContactViewHolder h;
        private final ContactVO v;
        private Throwable ex;
        private String FileName;
        private long BenutzerID;
        private String BenutzerName;

        public GetImageTask(ContactViewHolder h, ContactVO v, MainActivity main) {
            this.h = h;
            this.v = v;
            this.main = main;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            FileName = null;
            try {
                String fname;
                String resDownload;
                Bitmap DownImage = null;
                fname = ".JPG";
                BenutzerID = v.getUserID();
                BenutzerName = v.getContactName();
                FileName = "Image" + BenutzerID + fname;
                long ID = main.user.getLong(Constants.id);
                if (main.clsHTTPS.hashImages.containsKey(FileName)) {
                    DownImage = main.clsHTTPS.hashImages.get(FileName);
                    return DownImage;
                }
                HttpsURLConnection conn;
                try {
                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put("FileName", "" + FileName);
                    params.put(BENUTZER_ID, "" + ID);
                    params.put(ACCESSKEY, main.clsHTTPS.accesskey);

                    url = new URL(main.clsHTTPS.buildURI(MYURLROOT + "download", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new clsHTTPS.NullHostNameVerifier());
                    conn = (HttpsURLConnection) url.openConnection();
                    if (main.clsHTTPS.sslSocketFactory == null)
                        main.clsHTTPS.sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, main.clsHTTPS.context)[0];
                    conn.setSSLSocketFactory(main.clsHTTPS.sslSocketFactory);

                    conn.setRequestMethod(GET);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    //conn.setDoOutput(true);
                    //conn.setDoInput(true);
                    conn.setUseCaches(false);

                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    //conn.setRequestProperty(
                    //        "Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty(
                            "Content-Type", "application/octet-stream");

                    conn.setRequestProperty("Accept", "*/*");

                    // InputStream inputStream0 = conn.getInputStream();

                    // Start content wrapper:

                    int responseCode = conn.getResponseCode();

                    // always check HTTP response code first
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String fileName = "";
                        String disposition = conn.getHeaderField("Content-Disposition");
                        String contentType = conn.getContentType();
                        int contentLength = conn.getContentLength();

                        if (disposition != null) {
                            // extracts file name from header field
                            int index = disposition.indexOf("filename=");
                            if (index > 0) {
                                fileName = disposition.substring(index + 9,
                                        disposition.length());
                            }
                        } else {
                            // extracts file name from URL
                            //fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                            // fileURL.length());
                        }

                        System.out.println("Content-Type = " + contentType);
                        System.out.println("Content-Disposition = " + disposition);
                        System.out.println("Content-Length = " + contentLength);
                        System.out.println("fileName = " + fileName);

                        // opens input stream from the HTTP connection
                        InputStream inputStream = conn.getInputStream();

                        DownImage = BitmapFactory.decodeStream(inputStream);
                        inputStream.close();
                        conn.disconnect();
                        resDownload = "";
// Print response
                        //SSLContext context = SSLContext.getInstance("TLS");
                        //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                        //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    } else {
                        int status = responseCode;
                        InputStream iis;
                        boolean err = false;
                        if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                            iis = conn.getErrorStream();
                            err = true;
                        } else {
                            iis = (conn.getInputStream());
                        }
                        Scanner s = new Scanner(iis, clsHTTPS.UTF_8).useDelimiter(Delimiter_A);
                        String result = s.hasNext() ? s.next() : "";
                        if (err) result = ERROR + status + ": " + result;
                        iis.close();
                        conn.disconnect();
                        resDownload = result;// sbline.toString();
                    }
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resDownload = ERROR + e.getMessage();
                } catch (FileNotFoundException e) {
                    resDownload = main.clsHTTPS.context.getString(R.string.file_not_found);
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resDownload = ERROR + e.getMessage();
                }

                if (resDownload == null) resDownload = main.getString(R.string.zZtkeineVerb);

                if (resDownload.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && main.clsHTTPS.handler != null && main.clsHTTPS.context != null) {
                    Message msg = main.clsHTTPS.handler.obtainMessage(Constants.MESSAGE_TOAST);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TOAST, main.getString(R.string.noroutetohost));
                    msg.setData(bundle);
                    main.clsHTTPS.handler.sendMessage(msg);
                }
                if (resDownload.startsWith(ERROR)) throw new
                        Exception(resDownload);
                main.clsHTTPS.hashImages.put(FileName, DownImage);
                return DownImage;

            } catch (Throwable e) {
                if (!e.getMessage().contains("Error: File not found!")) {
                    e.printStackTrace();
                    Log.i(TAG, null, e);
                    this.ex = e;
                    lib.ShowException(TAG, main, ex, "Load Image", false);
                }
            }
            if (FileName != null) main.clsHTTPS.hashImages.put(FileName, main.VNLOGO);
            final Resources res = main.clsHTTPS.context.getResources();
            final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);

            final LetterTileProvider tileProvider = new LetterTileProvider(main.clsHTTPS.context);
            Bitmap letterTile;
            try {
                letterTile = tileProvider.getLetterTile(v.getContactName(), v.getContactName(), tileSize, tileSize, true);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                letterTile = BitmapFactory.decodeResource(res, R.drawable.vn_logo_c02);
            }
            if (FileName != null && letterTile != null)
                main.clsHTTPS.hashImages.put(FileName, letterTile);

            return letterTile;
        }

        @Override
        protected void onPostExecute(Bitmap in) {
            ImageView iv = h.ivProfileImage;
            try {
                if (iv != null) {
                    if (in == null)
                    {
                        final Resources res = main.getResources();
                        final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);

                        final LetterTileProvider tileProvider = new LetterTileProvider(main);
                        final Bitmap letterTile = tileProvider.getLetterTile(BenutzerName, BenutzerName, tileSize, tileSize, true);
                        in =  letterTile;
                        if (in != null) main.clsHTTPS.hashImages.put(FileName, in);
                    }
                    if (in != null && !in.equals(main.VNLOGO) && in.getByteCount() != main.VNLOGO.getByteCount()) {
                        h.ivProfileImage.setImageBitmap(in);
                        h.ivProfileImage.setVisibility(View.VISIBLE);
                        h.ivContactImage.setVisibility(View.GONE);
                    } else {
                        h.ivProfileImage.setVisibility(View.GONE);
                        h.ivContactImage.setVisibility(View.VISIBLE);
                    }
                } else {
                    throw new Exception("ImageView not found!");
                }
            } catch (Throwable ex) {
                Log.e(TAG, null, ex);
            }
        }
    }


    @Override
    public void onBindViewHolder(final ContactViewHolder holder, final int position) {
        final ContactVO contactVO = contactVOList.get(position);
        final boolean isUnconfirmed = this.fragment instanceof fragContacts2 && ((fragContacts2) this.fragment).getUnconfirmed;
        final boolean isInvited = this.fragment instanceof fragContacts2 && ((fragContacts2) this.fragment).getInvited;
        final boolean isDlg = this.fragment instanceof dlgContacts;

        setPopUp(holder, contactVO);

        if (isInvited) {
            System.out.println(isInvited);
        }
        final MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
        //if (m.user == null) return;

        setContactData(holder, contactVO);

        setViews(isDlg, holder, contactVO, m, position);

        setBtnAdd(isInvited, holder, contactVO, position, isUnconfirmed);

        holder.view.setVisibility(View.VISIBLE);

        if (contactVO.getUserID() >= 0 && !isDlgChat) {
            setShowProfile(holder, contactVO);
        }
        else if (!isDlgChat)
        {
            holder.view.findViewById(R.id.llContact).setOnClickListener(null);
        }
        else if (contactVO.getBlocked())
        {
            setUnblock(holder, contactVO);
        }

        if (contactVO.getMemberData() != null)
        {
            setNumberOfMessages(holder, contactVO);
        }

    }

    private void setUnblock(ContactViewHolder holder, ContactVO contactVO) {
        holder.llButtons.setVisibility(View.VISIBLE);
        holder.mOption.setEnabled(true);
        holder.popupnew.getMenu().findItem(R.id.btnDelete).setVisible(false);
        holder.popupnew.getMenu().findItem(R.id.btnSperreAll).setVisible(false);
        MenuItem btnSperre = holder.popupnew.getMenu().findItem(R.id.btnSperre);
        btnSperre.setVisible(true);
        btnSperre.setTitle(_main.getString(R.string.entsperren));
        btnSperre.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (_main.fragChat != null) {
                    do {
                        _main.fragChat.BlockedUsers.remove(contactVO.getUserID());
                        contactVO.setBlocked(false);
                        setDlgChatViews(holder, contactVO);
                    } while (_main.fragChat.BlockedUsers.contains(contactVO.getUserID()));
                    try {
                        _main.fragChat.saveBlocked(_main.fragChat.BenutzerID);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
                holder.llButtons.setVisibility(View.GONE);
                return true;
            }
        });

        holder.popupnew.getMenu().findItem(R.id.btnAdd).setVisible(false);
        holder.popupnew.getMenu().findItem(R.id.btnMessage).setVisible(false);
        holder.view.findViewById(R.id.llContact).setOnClickListener(null);

    }

    private void setBtnAdd(boolean isInvited, ContactViewHolder holder, ContactVO contactVO, int position, boolean isUnconfirmed) {
        boolean invited = isInvited && contactVO.getEinladungenID() > -1;

        if (invited) {
            setBtnAdd_addAndConfirm(holder, contactVO, position);
        } else if (contactVO.getContactID() >= 0 || (isUnconfirmed && contactVO.getContactID2() >= 0)) {
            if (isUnconfirmed && !contactVO.isBestaetigt() && !contactVO.isAbgelehnt) {
                setBtnAdd_confirm(holder, contactVO, position, isUnconfirmed);
            } else {
                setBtnAdd_remove(holder, contactVO, position, isUnconfirmed, false);
            }

        } else if (contactVO.getUserID() >= 0) {
            holder.popupnew.getMenu().findItem(R.id.btnAdd).setVisible(true);
            if (isUnconfirmed && !contactVO.isBestaetigt() && !contactVO.isAbgelehnt && contactVO.getContactID() > -1) {
                setBtnAdd_confirm(holder, contactVO, position, isUnconfirmed);
            } else {
                setBtnAdd_adduser(holder, contactVO, position, isUnconfirmed, false, false);
            }

        } else if (contactVO.getContactEmail().length() > 0 || contactVO.getContactNumber().length() > 0) {
            setBtnAdd_invite(holder, contactVO);

        } else {
            setButtons_invisible(holder);
        }
    }

    private void setViews(boolean isDlg, ContactViewHolder holder, ContactVO contactVO, MainActivity m, int position) {
        if (isDlg) {
            setAddDlgViews(holder, contactVO);
        }
        if (isDlgChat) {
            setDlgChatViews(holder, contactVO);
        }

        setAdminViews(holder, contactVO, m, position);

        if (contactVO.getUserID() >= 0) {
            setIsLimindoUserViews(holder, contactVO, position);
        } else {
            setIsNotLimindoUserViews(holder);
        }

    }

    private void setNumberOfMessages(ContactViewHolder holder, ContactVO contactVO) {
        if (contactVO.getMemberData().getCountMessages() > 0)
        {
            TextView t = new TextView(_main.clsHTTPS.context);
            if (blnAccessibility)
            {
                t.setTextSize(30);
                t.setTypeface(t.getTypeface(), Typeface.BOLD);
            }
            else
            {
                t.setTextSize(20);
                t.setTextColor(Color.BLUE);
            }
            int dp10 = lib.dpToPx(10);
            t.setPadding(dp10, dp10, dp10, dp10);
            t.setText("" + contactVO.getMemberData().getCountMessages());
            ((ViewGroup)holder.view.findViewById(R.id.llContact)).addView(t);
            contactVO.getMemberData().setCountMessages(0);
        }
    }

    private void setShowProfile(ContactViewHolder holder, ContactVO contactVO) {
        holder.view.findViewById(R.id.llContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgProfile fragP = new dlgProfile();
                fragP._main = _main;
                Dialog f = fragP.getDialog();
                fragP.show(_main.fm, "fragProfile");
                try {
                    fragP.setValues(contactVO.getUserID(), contactVO.getContactName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setButtons_invisible(ContactViewHolder holder) {
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setVisible(false);
        holder.popupnew.getMenu().findItem(R.id.btnSperre).setVisible(false);
        holder.mOption.setEnabled(false);
    }

    private void setBtnAdd_invite(ContactViewHolder holder, ContactVO contactVO) {
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setVisible(true);
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setTitle(R.string.einladen);
        holder.popupnew.getMenu().findItem(R.id.btnSperre).setVisible(false);

        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                boolean invited = false;
                invited = inviteContactEMail(contactVO);
                inviteContactWhatsApp(invited, contactVO);
                return invited;
            }
        });
    }

    private void setIsNotLimindoUserViews(ContactViewHolder holder) {
        holder.ivProfileImage.setVisibility(View.GONE);
        holder.ivContactImage.setVisibility(View.VISIBLE);
        holder.popupnew.getMenu().findItem(R.id.btnMessage).setVisible(false);
    }

    private void setIsLimindoUserViews(ContactViewHolder holder, ContactVO contactVO, int position) {
        holder.popupnew.getMenu().findItem(R.id.btnMessage).setVisible(true);
        holder.popupnew.getMenu().findItem(R.id.btnSperre).setVisible(true);
        holder.ivProfileImage.setVisibility(View.GONE);
        holder.ivContactImage.setVisibility(View.VISIBLE);
        try {
            new GetImageTask(holder, contactVO, _main).execute();
        } catch (Throwable e) {
            e.printStackTrace();
            holder.ivProfileImage.setVisibility(View.GONE);
            holder.ivContactImage.setVisibility(View.VISIBLE);
        }
        if (contactVO.getSperreID() >= 0) {
            holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.entsperren);
            setSperreListener(holder, contactVO, position);
        } else {
            if ((!isInvited && !isUnconfirmed) || contactVO.isAbgelehnt || contactVO.isBestaetigt() || (isInvited && contactVO.getEinladungenID() == -1)) {
                holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.sperren);
                setSperreListener(holder, contactVO, position);
            } else if (!isInvited) {
                holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.reject);
                setAblehnen(holder, contactVO, position);
            } else {
                holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.entfernen);
                setAblehnenInvite(holder, contactVO, position);
            }
        }

    }

    private void setAdminViews(ContactViewHolder holder, ContactVO contactVO, MainActivity m, int position) {
        try {
            if (m.user != null && m.user.getBoolean(Constants.administrator) && contactVO.getUserID() >= 0) {
                holder.popupnew.getMenu().findItem(R.id.btnSperreAll).setVisible(true);
                holder.popupnew.getMenu().findItem(R.id.btnDelete).setVisible(true);

                setSperreAllListener(holder, contactVO, position);
                setDeleteListener(holder, contactVO, position);
            } else {
                holder.popupnew.getMenu().findItem(R.id.btnDelete).setVisible(false);
                holder.popupnew.getMenu().findItem(R.id.btnSperreAll).setVisible(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setDlgChatViews(ContactViewHolder holder, ContactVO contactVO) {
        holder.llButtons.setVisibility(View.GONE);
        holder.chkAdd.setVisibility(View.GONE);
        holder.view.findViewById(R.id.llContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DialogFragment) AllContactsAdapter.this.fragment).dismiss();
                if (contactVO.getMemberData() != null)
                {
                    contactVO.getMemberData().setCountMessages(0);
                }
                _main.showFragChat(false, contactVO.getUserID(), contactVO.getContactName(), true, false);
            }
        });
    }

    private void setAddDlgViews(ContactViewHolder holder, ContactVO contactVO) {
        holder.llButtons.setVisibility(View.GONE);
        holder.chkAdd.setVisibility(View.VISIBLE);
        holder.chkAdd.setChecked(contactVO.getChecked());
        holder.chkAdd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                contactVO.setChecked(isChecked);
            }
        });
    }

    private void setContactData(ContactViewHolder holder, ContactVO contactVO) {
        if (contactVO.getContactImage() != 0) {
            holder.ivContactImage.setImageResource(contactVO.getContactImage());
        } else {
            holder.ivContactImage.setImageResource(R.drawable.ic_contact_invite);
        }
        holder.tvContactName.setText(contactVO.getContactName() + " " + contactVO.getOnlineState().getStateString());

        holder.tvPhoneNumber.setText(contactVO.getContactNumber());
        holder.tvEmail.setText(contactVO.getContactEmail());
        if (contactVO.isOnline()) {
            holder.view.setBackgroundResource(R.color.colorOnline);
            if (contactVO.isLimindoContact()) {
                holder.view.setBackgroundResource(R.color.isLimindoContactOnline);
            } else if (!contactVO.isBestaetigt() && !isInvited && contactVO.isLimindoUser() && (_main.clsHTTPS.AdminID == null || _main.clsHTTPS.AdminID == 0)) {
                holder.tvEmail.setText(XXX);
                holder.tvPhoneNumber.setText(XXX);
            }
        } else if (contactVO.isLimindoContact()) {
            holder.view.setBackgroundResource(R.color.isLimindoContact);
        } else {
            if (!contactVO.isBestaetigt() && contactVO.isLimindoUser() && (_main.clsHTTPS.AdminID == null || _main.clsHTTPS.AdminID == 0)) {
                holder.tvEmail.setText(XXX);
                holder.tvPhoneNumber.setText(XXX);
            }
            holder.view.setBackgroundResource(R.color.isLimindoUser);//(Color.parseColor("#dddddd"));
        }

    }

    private void setPopUp(final ContactViewHolder holder, final ContactVO contactVO) {
        holder.popupnew = new PopupMenu(mContext, holder.mOption);
        //inflating menu from xml resource
        holder.popupnew.inflate(R.menu.options_menu_contacts);
        holder.popupnew.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.btnDelete:

                        return true;
                    case R.id.btnSperreAll:
                        //handle menu2 click
                        return true;
                    case R.id.btnSperre:
                        //handle menu2 click
                        return true;
                    case R.id.btnAdd:
                        //handle menu2 click
                        return true;
                    case R.id.btnMessage:
                        _main.showFragChat(false, contactVO.getUserID(), contactVO.getContactName(), true, false);
                        return true;
                    default:
                        return false;
                }
            }
        });
        holder.mOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //adding click listener

                //displaying the popup
                holder.popupnew.show();

            }
        });
        holder.mOption.setEnabled(true);

    }

    private void inviteContactWhatsApp(boolean invited, ContactVO contactVO) {
        if (contactVO.getContactNumber().length() > 0) {
            String number = contactVO.getContactNumber();
            number = number.replaceFirst("\\+", "");
            String message = fragment.getActivity().getString(R.string.plsInstall);
            try {
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                sendIntent.putExtra("jid", number + "@s.whatsapp.net"); //phone number without "+" prefix
                sendIntent.setPackage("com.whatsapp");
                MainActivity.dontStop++;
                fragment.getActivity().startActivityForResult(sendIntent, RGLOBAL);
                if (!invited) {
                    try {
                        _main.clsHTTPS.Invite(_main.user.getLong(Constants.id), contactVO.getContactEmail(), lib.formatPhoneNumber(contactVO.getContactNumber()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {

            }

        }
    }

    private boolean inviteContactEMail(ContactVO contactVO) {
        if (contactVO.getContactEmail().length() > 0) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    MAILTO, contactVO.getContactEmail(), null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.LIMTO));
            emailIntent.putExtra(Intent.EXTRA_TEXT, mContext.getString(R.string.plsInstall));
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{contactVO.getContactEmail()});
            MainActivity.dontStop++;
            fragment.getActivity().startActivityForResult(Intent.createChooser(emailIntent, mContext.getString(R.string.sendMail)),RGLOBAL);
            try {
                _main.clsHTTPS.Invite(_main.user.getLong(Constants.id), contactVO.getContactEmail(), lib.formatPhoneNumber(contactVO.getContactNumber()));
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    private void setBtnAdd_confirm(final ContactViewHolder holder, final ContactVO contactVO, final int position, final boolean isUnconfirmed) {
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setTitle(R.string.confirm);
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    String res = m.clsHTTPS.confirmContact(m.user.getLong(Constants.id), contactVO.getContactID());
                    if (res.equals(TRUE)) {
                        contactVO.setContactID(-1);
                        contactVO.setContactImage(android.R.drawable.ic_input_add);
                        contactVO.setBestaetigt(true);
                        holder.ivContactImage.setImageResource(contactVO.getContactImage());
                        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
                        AllContactsAdapter.this.onBindViewHolder(holder, position);
                        if (contactVO.getContactID2() >= 0) {
                            setBtnAdd_remove(holder, contactVO, position, isUnconfirmed, false);
                        } else {
                            setBtnAdd_adduser(holder, contactVO, position, isUnconfirmed, true, true);
                        }
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }

                return true;
            }
        });
    }
     /*   holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                if (m.user == null) return;
                try {
                    String res = m.clsHTTPS.confirmContact(m.user.getLong(Constants.id), contactVO.getContactID());
                    if (res.equals(TRUE)) {
                        contactVO.setContactID(-1);
                        contactVO.setContactImage(android.R.drawable.ic_input_add);
                        contactVO.setBestaetigt(true);
                        holder.ivContactImage.setImageResource(contactVO.getContactImage());
                        holder.btnAdd.setOnClickListener(null);
                        AllContactsAdapter.this.onBindViewHolder(holder, position);
                        if (contactVO.getContactID2() >= 0) {
                            setRemove(holder, contactVO, position, isUnconfirmed, false);
                        } else {
                            setAdden(holder, contactVO, position, isUnconfirmed, true, true);
                        }
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }
        });


    }
    */


    private void setSperreListener(final ContactViewHolder holder, final ContactVO contactVO, final int position) {
        holder.popupnew.getMenu().findItem(R.id.btnSperre).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                if (contactVO.getSperreID() >= 0) {
                    try {
                        boolean res = m.clsHTTPS.removeSperre(m.user.getLong(Constants.id), contactVO.getSperreID());
                        if (res) {
                            contactVO.setSperreID(-1);
                            holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.sperren);
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, null, ex);
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        int res = m.clsHTTPS.addSperre(m.user.getLong(Constants.id), contactVO.getUserID());
                        if (res >= 0) {
                            contactVO.setSperreID(res);
                            holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.entsperren);
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, null, ex);
                        ex.printStackTrace();
                    }
                }
                return false;
            }
        });
    }

    private void setSperreAllListener(final ContactViewHolder holder, final ContactVO contactVO, final int position) {
        holder.popupnew.getMenu().findItem(R.id.btnSperreAll).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    lib.ShowMessagePosNeg(m, new SpannableString(m.getString(R.string.SperreAll)), m.getString(R.string.sperrenplus), m.getString(R.string.sperrenplus), m.getString(R.string.cancel), false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                Long res = m.clsHTTPS.setGesperrt(m.user.getLong(Constants.id), null, null, contactVO.getUserID(), true);
                                if (res > -1) {
                                    holder.view.setVisibility(View.GONE);
                                    //holder.btnSperre.setText(m.getString(R.string.sperren));
                                }
                            } catch (Throwable ex) {
                                Log.e(TAG, null, ex);
                                ex.printStackTrace();
                            }
                        }
                    }, null, null);

                } catch (Throwable ex) {
                    Log.e(TAG, null, ex);
                    ex.printStackTrace();
                }
                return false;
            }

        });
    }

    public void onMenuItemDelete() {

    }

    private void setDeleteListener(final ContactViewHolder holder, final ContactVO contactVO, final int position) {

        holder.popupnew.getMenu().findItem(R.id.btnDelete).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    lib.ShowMessagePosNeg(m,
                            new SpannableString(String.format(m.getString(R.string.reallyDeleteForeignAccount), contactVO.getContactName())),
                            m.getString(R.string.KontoLoeschen), m.getString(R.string.delete), m.getString(R.string.cancel), false, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    try {
                                        String res = m.clsHTTPS.deleteAccount(true, false, m.getBenutzerID(), contactVO.getUserID());
                                        if (res.startsWith(ERROR_)) {
                                            ShowMessage(m, res.replace(ERROR_, m.getString(R.string.Error_)), m.getString(R.string.Error));
                                        } else if (res.equalsIgnoreCase(TRUE)) {
                                            holder.view.setVisibility(View.GONE);

                                            //holder.btnSperre.setText(m.getString(R.string.sperren));
                                        }
                                    } catch (Throwable ex) {
                                        Log.e(TAG, null, ex);
                                        ex.printStackTrace();
                                    }
                                }
                            }, null, null);

                } catch (Throwable ex) {
                    Log.e(TAG, null, ex);
                    ex.printStackTrace();
                }

                return false;
            }
        });

    }

    private void setBtnAdd_adduser(final ContactViewHolder holder, final ContactVO contactVO, final int position, final boolean isUnconfirmed, final boolean showDialog, final boolean confirm) {
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setTitle(R.string.adden);
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                addUser(holder, contactVO, position, isUnconfirmed, confirm);
                return false;
            }
        });
        if (showDialog) addUser(holder, contactVO, position, isUnconfirmed, confirm);
    }


    private void addUser(final ContactViewHolder holder, final ContactVO contactVO, final int position, final boolean isUnconfirmed, final boolean confirm) {
        final MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
        final long ID = contactVO.getUserID();
        if (m.user == null) return;
        try {
            AlertDialog dlg = lib.ShowMessageYesNo(m, String.format(m.getString(R.string.adduser), contactVO.getContactName()), m.getString(R.string.adden), false, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    long UserID = 0;
                    try {
                        UserID = m.user.getLong(Constants.id);
                        long KontaktID = m.clsHTTPS.addContact(UserID, ID, confirm);
                        contactVO.setContactImage(android.R.drawable.ic_menu_call);
                        if (!isUnconfirmed) contactVO.setContactID(KontaktID);
                        else contactVO.setContactID2(KontaktID);
                        holder.ivContactImage.setImageResource(contactVO.getContactImage());
                        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
                        AllContactsAdapter.this.onBindViewHolder(holder, position);
                        if (isUnconfirmed) {
                            boolean not = false;
                            for (ContactVO c : contactVOList) {
                                if (!(c.isAbgelehnt || c.isBestaetigt())) {
                                    not = true;
                                    break;
                                }
                            }
                            if (!not) m._getUnconfirmed();
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }, null, null);
            dlg.show();
        } catch (Throwable e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
        }
    }

    private void setBtnAdd_addAndConfirm(final ContactViewHolder holder, final ContactVO contactVO, final int position) {
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setTitle(R.string.adden);


        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final MainActivity main = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    AlertDialog dlg = lib.ShowMessageYesNo(main, String.format(main.getString(R.string.adduser), contactVO.getContactName()), main.getString(R.string.adden), false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            addUserAndConfirmAndDeleteInvited(main, contactVO, holder, position);
                        }
                    }, null, null);
                    dlg.show();
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return false;
            }
        });

    }

    private void addUserAndConfirmAndDeleteInvited(MainActivity m, ContactVO contactVO, ContactViewHolder holder, int position) {
        long UserID = 0;
        try {
            UserID = m.user.getLong(Constants.id);
            final long ContactUserID = contactVO.getUserID();
            long KontaktID = m.clsHTTPS.addContact(UserID, ContactUserID, false);
            m.clsHTTPS.confirmContact(UserID, KontaktID);
            contactVO.setContactImage(android.R.drawable.ic_menu_call);
            contactVO.setContactID(KontaktID);
            holder.ivContactImage.setImageResource(contactVO.getContactImage());
            holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
            String res = m.clsHTTPS.deleteInvited(m.user.getLong(Constants.id), contactVO.getEinladungenID());
            contactVO.setEinladungenID(-1);
            AllContactsAdapter.this.onBindViewHolder(holder, position);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private void setAblehnen(final ContactViewHolder holder, final ContactVO contactVO, final int position) {
        holder.popupnew.getMenu().findItem(R.id.btnSperre).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                final MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    AlertDialog dlg = lib.ShowMessageYesNo(_main.clsHTTPS.context,
                            _main.clsHTTPS.context.getString(R.string.reallyrefusecontact),
                            _main.clsHTTPS.context.getString(R.string.refuse), false, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        boolean res = m.clsHTTPS.removeContact(m.user.getLong(Constants.id), contactVO.getContactID());
                                        contactVO.setContactImage(android.R.drawable.ic_delete
                                        );
                                        contactVO.setContactID(-1);
                                        holder.ivContactImage.setImageResource(contactVO.getContactImage());
                                        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
                                        contactVO.isAbgelehnt = true;
                                        AllContactsAdapter.this.contactVOList.remove(contactVO);
                                        //AllContactsAdapter.this.onBindViewHolder(holder, position);
                                        AllContactsAdapter.this.notifyDataSetChanged();
                                        AlertDialog dlg = lib.ShowMessageYesNo(_main.clsHTTPS.context,
                                                _main.clsHTTPS.context.getString(R.string.alsoblockcontact),
                                                _main.clsHTTPS.context.getString(R.string.block), false, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        try {
                                                            int res = m.clsHTTPS.addSperre(m.user.getLong(Constants.id), contactVO.getUserID());
                                                            if (res >= 0) {
                                                                contactVO.setSperreID(res);
                                                                holder.popupnew.getMenu().findItem(R.id.btnSperre).setTitle(R.string.entsperren);

                                                            }
                                                        } catch (Exception ex) {
                                                            Log.e(TAG, null, ex);
                                                            ex.printStackTrace();
                                                        }
                                                    }
                                                }, null, null);
                                    } catch (Throwable e) {
                                        e.printStackTrace();
                                        Log.e(TAG, null, e);
                                    }
                                }
                            }, null, null);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                try {
                    boolean res = m.clsHTTPS.removeContact(m.user.getLong(Constants.id), contactVO.getContactID());
                    contactVO.setContactImage(android.R.drawable.ic_delete
                    );
                    contactVO.setContactID(-1);
                    holder.ivContactImage.setImageResource(contactVO.getContactImage());
                    holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
                    contactVO.isAbgelehnt = true;
                    AllContactsAdapter.this.contactVOList.remove(contactVO);
                    //AllContactsAdapter.this.onBindViewHolder(holder, position);
                    AllContactsAdapter.this.notifyDataSetChanged();
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return false;
            }
        });

    }

    private void setAblehnenInvite(final ContactViewHolder holder, final ContactVO contactVO, final int position) {
        holder.popupnew.getMenu().findItem(R.id.btnSperre).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    String res = m.clsHTTPS.deleteInvited(m.user.getLong(Constants.id), contactVO.getEinladungenID());
                    contactVO.setContactImage(android.R.drawable.ic_delete
                    );
                    contactVO.setEinladungenID(-1);
                    holder.ivContactImage.setImageResource(contactVO.getContactImage());
                    holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
                    //contactVO.isAbgelehnt = true;
                    AllContactsAdapter.this.onBindViewHolder(holder, position);
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return false;
            }
        });

    }

    private void setBtnAdd_remove(final ContactViewHolder holder, final ContactVO contactVO, final int position, final boolean isUnconfirmed, final boolean setSperre) {
        holder.popupnew.getMenu().findItem(R.id.btnAdd).setTitle(R.string.entfernen);

        holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                MainActivity m = (MainActivity) AllContactsAdapter.this.fragment.getActivity();
                try {
                    boolean res = m.clsHTTPS.removeContact(m.user.getLong(Constants.id), isUnconfirmed ? contactVO.getContactID2() : contactVO.getContactID());
                    contactVO.setContactImage(android.R.drawable.ic_delete
                    );
                    if (isUnconfirmed) contactVO.setContactID2(-1);
                    else contactVO.setContactID(-1);
                    holder.ivContactImage.setImageResource(contactVO.getContactImage());
                    holder.popupnew.getMenu().findItem(R.id.btnAdd).setOnMenuItemClickListener(null);
                    contactVO.isAbgelehnt |= setSperre;
                    AllContactsAdapter.this.onBindViewHolder(holder, position);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        if (contactVOList == null) return 0;
        return contactVOList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        View mOption;
        CheckBox chkAdd;
        LinearLayout llButtons;
        View view;
        ImageView ivContactImage;
        ImageView ivProfileImage;
        TextView tvContactName;
        TextView tvPhoneNumber;
        TextView tvEmail;
        PopupMenu popupnew;

        ContactViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ivContactImage = (ImageView) itemView.findViewById(R.id.ivContactImage);
            ivProfileImage = (ImageView) itemView.findViewById(R.id.ivProfileImage);
            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvPhoneNumber = (TextView) itemView.findViewById(R.id.tvPhoneNumber);
            tvEmail = (TextView) itemView.findViewById(R.id.tvEmail);
            mOption = (View) itemView.findViewById(R.id.btnoption);
            llButtons = (LinearLayout) itemView.findViewById(R.id.llButtons);
            chkAdd = itemView.findViewById(R.id.chkAdd);
        }
    }

}