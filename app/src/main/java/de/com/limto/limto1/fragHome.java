package de.com.limto.limto1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import org.json.JSONArray;

import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.fragQuestions.ENTF;
import static de.com.limto.limto1.lib.lib.TAG;

public class fragHome extends Fragment {
    public final static int fragID = 0;
    MainActivity _main;
    private int neueFragen;
    private int neueAntworten;
    private Button btnOwnQuestions;
    private Button btnOwnQuestionsAnswered;
    private Button btnOwnQuestionsWithNewAnswers;
    private Button btnQuestionsOfContacts;
    private Button btnNewQuestionsOfContacts;
    private Button btnQuestionsOfContactsAnswered;
    private Button btnQuestionsOfContactsWithNewAnswers;
    private Button btnAccount;
    private Button btnNewAnswersForOwnQuestions;
    private int questionsOfContacts;
    private int questionsOfContactsAnswered;
    private int questionsOfContactsWithNewAnswers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (_main == null)_main = (MainActivity) getActivity();
        View view = inflater.inflate(lib.getLayoutResFromTheme(_main, R.attr.fraghome), container, false);
        lib.setFontsBold(view);
        return view;
    }

    @Override

    public void onViewCreated(android.view.View view, android.os.Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (_main == null)_main = (MainActivity) getActivity();
        btnOwnQuestions = (Button) view.findViewById(R.id.btnOwnQuestions);
        btnOwnQuestionsAnswered = (Button) view.findViewById(R.id.btnOwnQuestionsAnswered);
        btnOwnQuestionsWithNewAnswers = (Button) view.findViewById(R.id.btnOwnQuestionsWithNewAnswers);
        btnNewAnswersForOwnQuestions = (Button) view.findViewById(R.id.btnNewAnswersForOwnQuestions);
        btnQuestionsOfContacts = (Button) view.findViewById(R.id.btnQuestionsContacts);
        btnQuestionsOfContacts.setBackgroundColor(lib.getColorResFromThme(getContext(), R.attr.colorContacts));
        btnNewQuestionsOfContacts = (Button) view.findViewById(R.id.btnNewQuestionsOfContacts);
        btnNewQuestionsOfContacts.setBackgroundColor(lib.getColorResFromThme(getContext(), R.attr.colorContacts));
        btnQuestionsOfContactsAnswered = (Button) view.findViewById(R.id.btnQuestionsOfContactsAnswered);
        btnQuestionsOfContactsAnswered.setBackgroundColor(lib.getColorResFromThme(getContext(), R.attr.colorContacts));
        btnQuestionsOfContactsWithNewAnswers = (Button) view.findViewById(R.id.btnQuestionsWithNewAnswersOfContacts);
        btnQuestionsOfContactsWithNewAnswers.setBackgroundColor(lib.getColorResFromThme(getContext(), R.attr.colorContacts));
        btnAccount = (Button) view.findViewById(R.id.btnAccount);

        btnOwnQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(true, false, false);
            }
        });

        btnOwnQuestionsAnswered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(true, false, false);
            }
        });

        btnOwnQuestionsWithNewAnswers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(true, false, false);
            }
        });

        btnNewAnswersForOwnQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(true, false, false);
            }
        });
        btnQuestionsOfContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(false, true, false);
            }
        });

        btnNewQuestionsOfContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(false, true, false);
            }
        });


        btnQuestionsOfContactsAnswered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(false, true, false);
            }
        });

        btnQuestionsOfContactsWithNewAnswers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadQuestions(false, true, false);
            }
        });
        getNew();

        btnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(_main, btnAccount);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.mnupopupaccount, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        _main.onOptionsItemSelected(item);
                        return true;
                    }
                });

                popup.show(); //showing popup menu
            }

        });
    }

    public void setvalues(int neueAntworten, int neueFragen, int neueAntwortenOther) throws Exception {
        final int Grad = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(fragSettings.GRADE, 5);
        int Entf = getActivity().getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);

        JSONArray jsonquestionsall = _main.clsHTTPS.getCountQuestions(_main,0, Grad, Entf, false, false, false, null, false, false, null);
        JSONArray jsonquestionsanswered = _main.clsHTTPS.getCountQuestions(_main,0, Grad, Entf, false, false, false, null, true, false, null);
        JSONArray jsonquestionswithnewanswers = _main.clsHTTPS.getCountQuestions(_main,0, Grad, Entf, false, false, false, null, true, true, _main.getLastLogoffDate());
        JSONArray jsonnewquestions = _main.clsHTTPS.getCountQuestions(_main,0, Grad, Entf, false, false, false, null, false, false, _main.getLastLogoffDate());
        this.neueFragen = 0;
        for (int i = 1; i < jsonnewquestions.length(); i++)
        {
            this.neueFragen += jsonnewquestions.getJSONObject(i).optInt("count");
        }

        this.questionsOfContacts = 0;
        for (int i = 1; i < jsonquestionsall.length(); i++)
        {
            this.questionsOfContacts += jsonquestionsall.getJSONObject(i).optInt("count");
        }

        this.questionsOfContactsAnswered = 0;

        for (int i = 1; i < jsonquestionsanswered.length(); i++)
        {
            this.questionsOfContactsAnswered += jsonquestionsanswered.getJSONObject(i).optInt("count");
        }

        this.questionsOfContactsWithNewAnswers = 0;

        for (int i = 1; i < jsonquestionswithnewanswers.length(); i++)
        {
            this.questionsOfContactsWithNewAnswers += jsonquestionswithnewanswers.getJSONObject(i).optInt("count");
        }


        btnOwnQuestions.setText(jsonquestionsall.getJSONObject(0).getInt("count") + " " + getString(R.string.ownQuestions));
        btnOwnQuestionsAnswered.setText (jsonquestionsanswered.getJSONObject(0).getInt("count") + " " + getString(R.string.ownQuestionsAnswered));
        btnOwnQuestionsWithNewAnswers.setText((jsonquestionswithnewanswers.getJSONObject(0).getInt("count") + " " + getString(R.string.ownQuestionsWithNewAnswers)));
        btnNewAnswersForOwnQuestions.setText(this.neueAntworten + " " + getString(R.string.newAnswersForOwnQuestions));
        btnQuestionsOfContacts.setText(this.questionsOfContacts + " " + getString(R.string.QuestionsOfContacts));
        btnNewQuestionsOfContacts.setText(this.neueFragen + " " + getString(R.string.newQuestions));
        btnQuestionsOfContactsAnswered.setText(this.questionsOfContactsAnswered + " " + getString(R.string.QuestionsOfContactsAnswered)); ;
        btnQuestionsOfContactsWithNewAnswers.setText(this.questionsOfContactsWithNewAnswers + " " + getString(R.string.QuestionsWithNewAnswersOfContacts));

    }

    public void getNew() {
        if (_main.user != null) {
            neueFragen = _main.user.optInt("neuefragen");
            neueAntworten = _main.user.optInt("neueantworten");
            int neueAntwortenOther = _main.getNewAnswersOther();
            try {
                setvalues(neueAntworten, neueFragen, neueAntwortenOther);
            } catch (Exception e) {
                e.printStackTrace();
            }
            _main.setNewAnswers(neueAntworten);
            _main.setNewQuestions(neueFragen);

        } else {
            _main.mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getNew();
                }
            }, 1000);
        }
    }

    public void loadQuestions(boolean eigene, boolean kontakte, boolean dontmove) {
        if (_main == null || _main.mPager == null) return;
        if (!dontmove) _main.mPager.setCurrentItem(fragQuestions.fragID);
        if (_main.readMessage != null) return;
        Fragment f = _main.fPA.findFragment(fragQuestions.fragID);
        if (f != null) {
            try {
                fragQuestions fragQ = (fragQuestions) f;
                ((fragQuestions) f)._main = _main;
                //((fragQuestions) f).init(true, -1, -1, null, null, null, null, false);
                if (eigene) {
                    fragQ.btnEigene.performClick();
                }
                if (kontakte) fragQ.btnKontakte.performClick();

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }
    }
}
