package de.com.limto.limto1;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;

import androidx.appcompat.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.loader.app.LoaderManager;

import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import de.com.limto.limto1.Errors.dbSqlite;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;
import de.com.limto.limto1.logger.LogWrapper;

import static android.Manifest.permission.READ_CONTACTS;
import static android.content.Context.MODE_PRIVATE;
import static de.com.limto.limto1.MainActivity.dontStop;
import static de.com.limto.limto1.clsHTTPS.MYURLROOT;

/**
 * A login screen that offers login via email/password.
 */
public class dlgLogin extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    //public final  static int fragID = 0;
    public static final String EMAIL = "email";
    public static final String RUFNUMMER = "rufnummer";
    public static final String VORNAME = "vorname";
    public static final String NAME = "name";
    public static final String PLZ = "plz";
    public static final String BENUTZERNAME = "benutzername";
    //public static final String recapStr = "6LeQgVsUAAAAAE-j55Hsfrz4PXY0m0rmEkKhOLBF";
    public static final String recapStr = "6LdVr5AUAAAAACswc0VKu-Y54KzOAFgLwNE2889A";
    public static final String ERROR = "Error:";
    public static final String ERROR_MESSAGE = "Error message: ";
    public static final String UNKNOWN_TYPE_OF_ERROR = "Unknown type of error: ";
    public static final String SECRET = "secret";
    public static final String KEY = "KEY";
    public static final String EMAIL1 = "Email";
    public static final String BENUTZERNAME1 = "Benutzername";
    public static final String regexRufnummer = "^\\+\\d+$";
    public static final String DESC = " DESC";
    public static final int RC_SIGN_IN = 1234;
    static final String regexnumbers = ".*[0-9]+.*";
    static final String regexlower = ".*[a-z]+.*";
    static final String regexupper = ".*[A-Z]+.*";
    static final String regexspecial = ".*[^A-Za-z0-9]+.*";
    static final String regexspaces = ".*\\s+.*";
    static final String regexcount = "^.{8,}$";
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    private static final String TAG = "fragLogin";
    public static String Laendervorwahl = "+49";
    public EditText mPasswordView;
    public EditText mBenutzername;
    public EditText mPasswordView2;
    public MainActivity _main;
    public LoginMode mode = LoginMode.register;
    public Context context;
    public LimindoService mService;
    public boolean initialised;
    public boolean showlogin;
    public GoogleSignInAccount account;
    public View mSignInGoogle;
    public Bitmap profileImage = null;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    UserLoginTask mAuthTask = null;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private View mProgressView;
    private View mLoginFormView;
    private EditText mName;
    private EditText mVorname;
    private EditText mRufnummer;
    private EditText mPLZ;
    private EditText mNewUserName;
    private Button mEmailSignInButton;
    private Button mEmailUpdateButton;
    private Button mEmailSignInNewButton;
    private LinearLayout mllSignIn;
    private FragmentManager fm;
    private boolean callback;
    private Bundle savedInstanceState;
    private boolean blnDoLogin;
    private TextView mOptional;
    private TextView tvKennwortVergessen;
    private TextView tvBenutzernameVergessen;
    private GoogleSignInClient mGoogleSignInClient;
    private View mSignInGoogle0;
    private EditText mAdresse;
    private boolean loadaccount;
    private boolean initviews;
    private View mllSignInOptional;
    private View mllForgotPassword;
    private dbSqlite db;
    private boolean firstLogin;
    private TextInputLayout mPWLayout2;
    private TextInputLayout mPWLayout;
    private boolean _isvisible;

    public dlgLogin() {
        _main = (MainActivity) getActivity();
        context = _main;
    }

    public boolean isvisible() {
        return _isvisible;
    }

    public boolean isvisibleatall() {
        return isAdded() && (_isvisible || isVisible()) && !isHidden();
    }


    public void setMode(final LoginMode mode) throws Throwable {

        if (mode != null) this.mode = mode;
        // profileImage = null;
        if (account == null) {
            setModeNewLogin();
        } else {
            setModeEditAccount();
        }

        mllForgotPassword.setVisibility(View.VISIBLE);

        if (mode == LoginMode.login) {
            setModeLoginTitleAndActionListener();
        } else {
            mPasswordView.setOnEditorActionListener(null);
        }

        if (mode == LoginMode.register) {
            setModeRegister();
        } else {
           setModeOtherThanRegister();
        }

        if (mode == LoginMode.edtdata) {
            setModeEditData();
        } else {
            mNewUserName.setVisibility(View.GONE);
        }

        if (mode == LoginMode.login) {
            setModeLoginVisibility();
            setModeLoginListeners();
        } else {
            setModeOtherThanLogin();
            dontShowSignInViews();
        }

        mSignInGoogle.setVisibility(View.GONE);

        if (mode == LoginMode.edtdata || mode == LoginMode.register) {
            setModeEditRegister();
        } else {
            mEmailUpdateButton.setVisibility(View.GONE);
        }

        if (mode == LoginMode.edtdata) {
            try {
                setUserDataIntoTextViews();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        setClickListeners();
    }

    private void dontShowSignInViews() {
        mllSignIn.setVisibility(View.GONE);
        mSignInGoogle0.setVisibility(View.GONE);
    }

    private void setClickListeners() {
        tvKennwortVergessen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mEmailView.setError(null);
                mBenutzername.setError(null);
                if (_main != null && _main.clsHTTPS != null && mEmailView.getText().length() > 0) {
                    verifyCaptchaAndSendNewPassword();
                }
            }
        });

        tvBenutzernameVergessen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mEmailView.setError(null);
                mBenutzername.setError(null);
                if (_main != null && _main.clsHTTPS != null && mEmailView.getText().length() > 0) {
                    verifyCaptchaAndSendUsername();
                }
            }
        });
    }

    private void verifyCaptchaAndSendUsername() {
        SafetyNet.getClient(_main).verifyWithRecaptcha(recapStr)
                .addOnSuccessListener(_main, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                        if (!response.getTokenResult().isEmpty()) {
                            String res = _main.clsHTTPS.getUsernames(mEmailView.getText().toString(), response.getTokenResult());
                            if (res.startsWith(ERROR))
                                mEmailView.setError(res.replace(ERROR, getString(R.string.fehler_)));
                            else
                                lib.ShowMessage(dlgLogin.this.getContext(), String.format(getString(R.string.sendusernames), res), "");
                        }
                    }
                })
                .addOnFailureListener(_main, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mEmailView.setError(getString(R.string.captchanotverified));
                        if (e instanceof ApiException) {
                            ApiException apiException = (ApiException) e;
                            Log.d(TAG, ERROR_MESSAGE +
                                    CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                        } else {
                            Log.d(TAG, UNKNOWN_TYPE_OF_ERROR + e.getMessage());
                        }
                    }
                });
    }

    private void verifyCaptchaAndSendNewPassword() {
        SafetyNet.getClient(_main).verifyWithRecaptcha(recapStr)
                .addOnSuccessListener(_main, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                    @Override
                    public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                        if (!response.getTokenResult().isEmpty()) {
                            String res = _main.clsHTTPS.createnewpw(mEmailView.getText().toString(), mBenutzername.getText().toString(), response.getTokenResult());
                            if (res.startsWith(ERROR))
                                mBenutzername.setError(res.replace(ERROR, getString(R.string.Fehler_)));
                            else
                                lib.ShowMessage(dlgLogin.this.getContext(), String.format(getString(R.string.sendnewpw), res), "");
                        }
                    }
                })
                .addOnFailureListener(_main, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        try {
                            mBenutzername.setError(getString(R.string.captchanotverified));
                            if (e instanceof ApiException) {
                                ApiException apiException = (ApiException) e;
                                Log.d(TAG, ERROR_MESSAGE +
                                        CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                            } else {
                                Log.d(TAG, UNKNOWN_TYPE_OF_ERROR + e.getMessage());
                            }
                        } catch (Throwable ex) {
                            Log.e(TAG, null, ex);
                        }
                    }
                });
    }

    private void setModeEditRegister() {
        mEmailUpdateButton.setVisibility(View.VISIBLE);
        if (mode == LoginMode.register) {
            mSignInGoogle.setVisibility(View.VISIBLE);
        }
        mEmailUpdateButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (mode == LoginMode.register) {
                        String Rufnummer = mRufnummer.getText().toString();
                        if (Rufnummer.equalsIgnoreCase(context.getString(R.string.leer)) || Rufnummer.length() == 0) {
                            Rufnummer = Laendervorwahl;
                        }

                        final EditText edtInput = new EditText(context);
                        edtInput.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                String res = edtInput.getText().toString();
                                boolean valid = isRufnummerValid(res);
                                if (!valid) {
                                    edtInput.setError(dlgLogin.this.getString(R.string.invalid_phonenumber)); //(context.getString(R.string.invalidPhoneNumber));
                                } else {
                                    edtInput.setError(null);
                                }
                            }
                        });
                        AlertDialog dlg = lib.getInputBox(context, "", getString(R.string.EnterPhone), Rufnummer, false, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    if (edtInput.getError() == null)
                                        mRufnummer.setText(edtInput.getText());
                                    attemptLogin(true, true, false, null, false);
                                } catch (Throwable throwable) {
                                    Log.e(TAG, null, throwable);
                                    throwable.printStackTrace();
                                }
                            }
                        }, null, edtInput);
                    } else {
                        attemptLogin(true, true, false, null, false);
                    }
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            }
        });
        if (mode == LoginMode.register) {
            mEmailUpdateButton.setText(R.string.register);

        } else {
            mEmailUpdateButton.setText(R.string.edit);
        }
    }

    private void setModeLoginListeners() {
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    attemptLogin(true, false, false, null, false);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            }
        });
        mEmailSignInNewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgLogin.this.mode = LoginMode.register;
                try {
                    setMode(dlgLogin.this.mode);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
    }

    private void setModeOtherThanLogin() {
        mName.setVisibility(View.VISIBLE);
        mVorname.setVisibility(View.VISIBLE);
        mRufnummer.setVisibility(View.VISIBLE);
        mPLZ.setVisibility(View.VISIBLE);
        mAdresse.setVisibility(View.VISIBLE);


        try {
            TelephonyManager tMgr = (TelephonyManager) _main.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            } else {
                mRufnummer.setText(tMgr.getLine1Number());
            }

        } catch (Throwable e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
        }
    }

    private void setModeLoginVisibility() {
        mName.setVisibility(View.GONE);
        mVorname.setVisibility(View.GONE);
        mRufnummer.setVisibility(View.GONE);
        mPLZ.setVisibility(View.GONE);
        mOptional.setVisibility(View.GONE);
        mAdresse.setVisibility(View.GONE);
        mllSignIn.setVisibility(View.VISIBLE);
        mSignInGoogle0.setVisibility(View.VISIBLE);
    }

    private void setModeEditData() {
        mOptional.setVisibility(View.VISIBLE);
        mNewUserName.setVisibility(View.VISIBLE);
        mllSignInOptional.setVisibility(View.VISIBLE);
        getDialog().setTitle(R.string.Aendern);
        mBenutzername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (mNewUserName.getText().length() == 0) {
                        mNewUserName.setText(mBenutzername.getText().toString());
                    }
                }
            }
        });
    }

    private void setModeOtherThanRegister() {
        mPasswordView2.setVisibility(View.GONE);
        mPWLayout2.setPasswordVisibilityToggleEnabled(false);
    }

    private void setModeRegister() {
        getDialog().setTitle(R.string.Registrieren);
        if (account == null) {
            mPasswordView2.setVisibility(View.VISIBLE);
        }
        mOptional.setVisibility(View.VISIBLE);
        mllSignInOptional.setVisibility(View.GONE);
        mllForgotPassword.setVisibility(View.GONE);
        mPWLayout2.setPasswordVisibilityToggleEnabled(true);
    }

    private void setModeEditAccount() {
        mPWLayout.setPasswordVisibilityToggleEnabled(false);
        mPWLayout.setVisibility(View.GONE);
        mPWLayout2.setVisibility(View.GONE);
    }

    private void setModeNewLogin() {
        mPasswordView.setVisibility(View.VISIBLE);
        mPWLayout.setVisibility(View.VISIBLE);
        mPWLayout2.setVisibility(View.VISIBLE);
        mPWLayout.setPasswordVisibilityToggleEnabled(true);
    }

    private void setModeLoginTitleAndActionListener() {
        getDialog().setTitle(R.string.Anmelden);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    try {
                        attemptLogin(true, false, false, null, false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (dontStop > 0) dontStop--;
        if (dontStop == 0) LimindoService.dontStop = false;
    }

    public void Login() {
        try {
            attemptLogin(true, false, false, null, true);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
    }

    public void Register() {
        dlgLogin.this.mode = LoginMode.register;
        try {
            setMode(dlgLogin.this.mode);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void updateUI(final GoogleSignInAccount account) {
        if (!this.isAdded() || this.isDetached() || !(initialised || initviews)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateUI(account);
                }
            }, 500);
            return;
        }
        try {
            this.account = account;
            if (account != null) {

                if (this.initialised || initviews) {
                    if (mPasswordView != null) {
                        mPasswordView.setVisibility(View.GONE);
                        mPWLayout.setPasswordVisibilityToggleEnabled(false);
                    }
                    if (mPasswordView2 != null) {
                        mPasswordView2.setVisibility(View.GONE);
                        mPWLayout2.setPasswordVisibilityToggleEnabled(false);
                    }
                    this.tvBenutzernameVergessen.setVisibility(View.GONE);
                    this.tvKennwortVergessen.setVisibility(View.GONE);
                    this.mBenutzername.setEnabled(false);
                    this.mEmailView.setEnabled(false);
                    this.mEmailView.setText(account.getEmail());
                    this.mName.setText(account.getFamilyName());
                    this.mBenutzername.setText(account.getDisplayName());
                    this.mVorname.setText(account.getGivenName());
                    mPasswordView.setText(account.getId());
                    mPasswordView2.setText(account.getId());
                    if (account.getPhotoUrl() != null)
                        new LoadProfileImage(profileImage).execute(account.getPhotoUrl().toString());
                    else profileImage = null;
                    setGooglePlusButtonText((SignInButton) mSignInGoogle, getString(R.string.google_sign_out));
                    setGooglePlusButtonText((SignInButton) mSignInGoogle0, getString(R.string.google_sign_out));
                    mSignInGoogle0.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            _main.signoutgoogle();
                        }
                    });
                    mSignInGoogle.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            _main.signoutgoogle();
                        }
                    });

                } else this.loadaccount = true;
            } else {
                if (this.initialised || initviews) {
                    this.tvBenutzernameVergessen.setVisibility(View.VISIBLE);
                    this.tvKennwortVergessen.setVisibility(View.VISIBLE);
                    this.mBenutzername.setEnabled(true);
                    this.mEmailView.setEnabled(true);
                    setGooglePlusButtonText((SignInButton) mSignInGoogle, getString(R.string.google_sign_up));
                    setGooglePlusButtonText((SignInButton) mSignInGoogle0, getString(R.string.google_sign_up));
                    mSignInGoogle0.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            _main.signingoogle();

                        }
                    });
                    mSignInGoogle.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            _main.signingoogle();
                        }
                    });
                    if (mode == LoginMode.login) {
                        if (mPasswordView != null) {
                            mPasswordView.setVisibility(View.VISIBLE);
                            mPWLayout.setPasswordVisibilityToggleEnabled(true);
                            mPWLayout.setVisibility(View.VISIBLE);
                        }
                        if (mPasswordView2 != null) {
                            mPasswordView2.setVisibility(View.GONE);
                            mPWLayout2.setPasswordVisibilityToggleEnabled(false);
                        }
                    } else if (mode == LoginMode.register) {
                        if (mPasswordView != null) {
                            mPasswordView.setVisibility(View.VISIBLE);
                            mPWLayout.setPasswordVisibilityToggleEnabled(true);
                            mPWLayout.setVisibility(View.VISIBLE);
                        }
                        if (mPasswordView2 != null) {
                            mPasswordView2.setVisibility(View.VISIBLE);
                            mPWLayout2.setPasswordVisibilityToggleEnabled(true);
                            mPWLayout2.setVisibility(View.VISIBLE);
                        }
                    }
                } else this.loadaccount = true;
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            Log.e(TAG, null, ex);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initialised = false;
        View v = null;
        try {
            v = inflater.inflate(R.layout.dlglogin, container, false);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View v = view;
        try {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            Laendervorwahl = getString(R.string.intPrefix);
            _main = (MainActivity) getActivity();
            context = _main;

            mEmailView = (AutoCompleteTextView) v.findViewById(R.id.email);
            populateAutoComplete();

            mPasswordView = (EditText) v.findViewById(R.id.password);
            mPasswordView2 = (EditText) v.findViewById(R.id.password2);
            mPWLayout2 = v.findViewById(R.id.pwLayout2);
            mPWLayout = v.findViewById(R.id.pwLayout);
            mBenutzername = (EditText) v.findViewById(R.id.benutzername);
            mNewUserName = (EditText) v.findViewById(R.id.neuerbenutzername);
            mName = (EditText) v.findViewById(R.id.name);
            mVorname = (EditText) v.findViewById(R.id.vorname);
            mRufnummer = (EditText) v.findViewById(R.id.rufnummer);
            mPLZ = (EditText) v.findViewById(R.id.plz);
            mAdresse = (EditText) v.findViewById(R.id.Adresse);
            mOptional = (TextView) v.findViewById(R.id.txtOptional);
            /*
            try
            {
                Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                Account[] accounts = AccountManager.get(getContext()).getAccounts();
                for (Account account : accounts)
                {
                    if (emailPattern.matcher(account.name).matches())
                    {
                        String possibleEmail = account.name;
                        if (possibleEmail.length() > 3)
                        {
                            if (!showlogin) mEmailView.setText(possibleEmail);
                            break;
                        }
                    }
                }
            }
            catch (Throwable e)
            {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
            */
            tvKennwortVergessen = (TextView) v.findViewById(R.id.tvVergessenPW);
            tvBenutzernameVergessen = (TextView) v.findViewById(R.id.tvVergessenBen);
            mEmailSignInButton = (Button) v.findViewById(R.id.email_sign_in_button);
            mEmailSignInNewButton = (Button) v.findViewById(R.id.email_sign_in_new_button);
            mllSignIn = (LinearLayout) v.findViewById(R.id.llsign_in);
            mllSignInOptional = v.findViewById(R.id.llsign_in_optional);
            mllForgotPassword = v.findViewById(R.id.llvergessen);
            mEmailUpdateButton = (Button) v.findViewById(R.id.email_update_button);
            mSignInGoogle = v.findViewById(R.id.sign_in_button);
            mSignInGoogle0 = v.findViewById(R.id.sign_in_button0);
            mLoginFormView = v.findViewById(R.id.login_form);
            mProgressView = v.findViewById(R.id.login_progress);
            initviews = true;
            /*if (loadaccount) {
                loadaccount =false;
                updateUI(account);
            }
            else if(_main!=null && _main.account!=null && account == null)
            {
                updateUI(_main.account);
            }
            */
            setMode(mode);
            setUserDataIntoTextViews();
            setGooglePlusButtonText((SignInButton) mSignInGoogle, getString(R.string.google_sign_up));
            setGooglePlusButtonText((SignInButton) mSignInGoogle0, getString(R.string.google_sign_up));
            mSignInGoogle0.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    _main.signingoogle();

                }
            });
            mSignInGoogle.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    _main.signingoogle();
                }
            });

            initialised = true;
            if (blnDoLogin) {
                blnDoLogin = false;
                doLogin(fm, callback, savedInstanceState);
            }
            mAdresse.addTextChangedListener(new TextWatcher() {
                int beforeCursorPosition;
                String text;

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    text = charSequence.toString();
                    beforeCursorPosition = i;
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (mAdresse.getLineCount() > 4) {
                        mAdresse.setText(text);
                        mAdresse.setSelection(beforeCursorPosition);
                    }
                }
            });

        } catch (Throwable ex) {
            lib.ShowException(TAG, getContext(), ex, true);
        }
        _isvisible = true;

    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        _isvisible = false;
    }

    public void initdoLogin(FragmentManager fm, boolean callback, Bundle savedInstanceState) {
        this.fm = fm;
        this.callback = callback;
        this.savedInstanceState = savedInstanceState;
        this.blnDoLogin = true;
    }

    public void doLogin(FragmentManager fm, boolean callback, Bundle savedinstancestate) {
        if (_main != null && _main.user == null) {
            if (_main.mService != null && _main.mService.getState() != LimindoService.STATE_NONE && _main.mService.getState() != LimindoService.STATE_ERROR && _main.mService.user != null) {
                _main.user = _main.mService.user;
                if (_main.mnuUpload != null) _main.mnuUpload.setEnabled(true);
                try {
                    this.setUserDataIntoTextViews();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                if (showlogin) {
                    try {
                        logon(fm, callback, savedinstancestate);
                    } catch (Throwable e) {
                        e.printStackTrace();
                        if (mPasswordView != null) mPasswordView.setError(e.getMessage());
                    }
                } else {
                    if (!_main.getUnconfirmed && !_main.getInvited)
                        _main.mPager.setCurrentItem(fragQuestions.fragID);
                }
            } else {
                try {
                    logon(fm, callback, savedinstancestate);
                } catch (Throwable e) {
                    e.printStackTrace();
                    if (mPasswordView != null) mPasswordView.setError(e.getMessage());
                }
            }
        } else if (showlogin) {
            try {
                logon(fm, callback, savedinstancestate);
            } catch (Throwable e) {
                e.printStackTrace();
                if (mPasswordView != null) mPasswordView.setError(e.getMessage());
            }
        }

    }

    public void logon(FragmentManager fm, boolean callback, Bundle savedinstancestate) throws Throwable {
        SharedPreferences prefs = this.context.getSharedPreferences(SECRET, MODE_PRIVATE);
        String AccessKey = prefs.getString(KEY, null);
        String Email = prefs.getString(EMAIL1, null);
        String Benutzername = prefs.getString(BENUTZERNAME1, null);
        if (showlogin) {
            AccessKey = null;
            Email = _main.loginEMail;
            Benutzername = _main.loginBenutzername;
            firstLogin = true;
            showlogin = false;
        }
        if (mode != LoginMode.none) {
            if (Email != null) mEmailView.setText(Email);
            if (Benutzername != null) mBenutzername.setText(Benutzername);
            if (mAuthTask == null && AccessKey != null && Email != null && Benutzername != null && _main.user == null) {
                mAuthTask = new UserLoginTask(this, null, false, Email, Benutzername, null, null, null, null, null, null, null, AccessKey, callback, savedinstancestate, false);
                showProgress(true);
                mAuthTask.execute((Void) null);
            } else {
                if (fm == null) throw new Exception("Neues Anmelden nicht möglich!");
                if (Benutzername == null && Email == null) this.mode = LoginMode.login;
                else this.mode = LoginMode.login;

                setMode(mode);
                //this.show(fm, "fragLogin");
            }
        } else {
            if (mAuthTask == null && AccessKey != null && Email != null && Benutzername != null && (mService == null || mService.user == null)) {
                mAuthTask = new UserLoginTask(this, null, false, Email, Benutzername, null, null, null, null, null, null, null, AccessKey, callback, savedinstancestate, false);
                showProgress(true);
                mAuthTask.execute((Void) null);
            } else {
                throw new Exception("Keine Neuanmeldung möglich");
            }
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _main = ((MainActivity) getActivity());
        db = _main.db;
        if (db == null && !(Log.getLogNode() instanceof LogWrapper)) {
            try {
                Context c = _main.getApplicationContext();
                /*db = new dbSqlite(c);
                db.createDataBase();
                _main.db = db;
                _main.initializeLogging(0,null,db);
                */
                Log.e(TAG, "Logging started");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }


    }

    protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        // Find the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(buttonText);
                return;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        /*
        if (_main.isTablet || _main.isTV) {
            Point s = lib.getScreenSize(_main);
            getDialog().getWindow().setLayout((int) (s.x * .9), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        */
        if (_main != null) // && _main.isTablet == false && _main.isTV == false)
            getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //if (_main!=null) updateUI(_main.account);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, dlgLogin.this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (_main.checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(boolean relogin, final boolean update, final boolean callback, final Bundle savedinstancestate, final boolean registeronfailure) throws Throwable {
        if (mAuthTask != null) {
            return;
        }
        Log.e("fragLogin", "attempt login");
        if (_main != null && _main.user != null && !relogin) return;
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();
        if (mBenutzername.getText().length() == 0)
            mBenutzername.setText(mEmailView.getText().toString());
        final String benutzername = mBenutzername.getText().toString();
        String neuerbenutzername = mNewUserName.getText().toString();
        if (neuerbenutzername.length() == 0) neuerbenutzername = benutzername;
        final String name = mName.getText().toString();
        final String vorname = mVorname.getText().toString();
        final String rufnummer = mRufnummer.getText().toString();
        final String plz = mPLZ.getText().toString();
        final String adresse = mAdresse.getText().toString();
        if (!update) neuerbenutzername = "";
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (account == null && mode != LoginMode.login && mode != LoginMode.edtdata && (TextUtils.isEmpty(password) || !isPasswordValid(password))) {
            mPasswordView.setError(getString(R.string.error_invalid_password2));
            focusView = mPasswordView;
            cancel = true;
        }

        if (account == null && mode == LoginMode.register && !mPasswordView.getText().toString().equals(mPasswordView2.getText().toString())) {
            mPasswordView2.setError(getString(R.string.error_pw_unequal));
            if (focusView != mPasswordView) focusView = mPasswordView2;
            cancel = true;
        }

        if (mode == LoginMode.register && !isRufnummerValid(mRufnummer.getText().toString())) {
            mRufnummer.setError(getString(R.string.error_invalid_phonenumber));
            focusView = mRufnummer;
            cancel = false;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (mode == LoginMode.register) {
                if (account == null) {
                    final String finalNeuerbenutzername = neuerbenutzername;
                    SafetyNet.getClient(this.getActivity()).verifyWithRecaptcha(recapStr)
                            .addOnSuccessListener(this.getActivity(), new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
                                @Override
                                public void onSuccess(SafetyNetApi.RecaptchaTokenResponse response) {
                                    if (!response.getTokenResult().isEmpty()) {
                                        //handleSiteVerify(response.getTokenResult());
                                        showProgress(true);
                                        mAuthTask = new UserLoginTask(dlgLogin.this, response.getTokenResult(), update, email, benutzername, finalNeuerbenutzername, password, name, vorname, rufnummer, plz, adresse, null, callback, savedinstancestate, false);
                                        mAuthTask.execute((Void) null);
                                    }
                                }
                            })
                            .addOnFailureListener(this.getActivity(), new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    if (e instanceof ApiException) {
                                        ApiException apiException = (ApiException) e;
                                        Log.d(TAG, ERROR_MESSAGE +
                                                CommonStatusCodes.getStatusCodeString(apiException.getStatusCode()));
                                    } else {
                                        Log.d(TAG, UNKNOWN_TYPE_OF_ERROR + e.getMessage());
                                    }
                                }
                            });
                } else {
                    mAuthTask = new UserLoginTask(this, "Google", update, email, benutzername, neuerbenutzername, account.getId(), name, vorname, rufnummer, plz, adresse, null, callback, savedinstancestate, registeronfailure);
                    mAuthTask.execute((Void) null);
                }
            } else {
                showProgress(true);
                mAuthTask = new UserLoginTask(this, account == null ? null : "Google", update, email, benutzername, neuerbenutzername, password, name, vorname, rufnummer, plz, adresse, null, callback, savedinstancestate, registeronfailure);
                mAuthTask.execute((Void) null);
            }
        }
    }

    private boolean isRufnummerValid(String s) {
        String ss = lib.formatPhoneNumber(s);

        return ss.length() > 7 && ss.matches(regexRufnummer);
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        boolean rescount = password.matches(regexcount);
        boolean resnumbers = password.matches(regexnumbers);
        boolean reslower = password.matches(regexlower);
        boolean resupper = password.matches(regexupper);
        boolean resspecial = password.matches(regexspecial);
        boolean resspaces = password.matches(regexspaces);

        if (!rescount) {
            lib.ShowMessage(context, getString(R.string.passwordcount), getString(R.string.password));
            return false;
        }
        if (resspaces) {
            lib.ShowMessage(context, getString(R.string.passwordnospaces), getString(R.string.password));
            return false;
        }
        int matches = 0;
        if (resnumbers) matches++;
        if (reslower) matches++;
        if (resupper) matches++;
        if (resspecial) matches++;

        if (matches < 3) {
            lib.ShowMessage(context, getString(R.string.wrongpassword), getString(R.string.password));
            return false;
        }

        return true;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (!isAdded() || getActivity() == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public androidx.loader.content.Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new androidx.loader.content.CursorLoader(_main,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + DESC);
    }

    @Override
    public void onLoadFinished(androidx.loader.content.Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(androidx.loader.content.Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(_main,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private void loadQuestions(boolean dontMove) {
        if (_main == null || _main.mPager == null) return;
        if (!dontMove) _main.mPager.setCurrentItem(fragQuestions.fragID);
        Fragment f = _main.fPA.findFragment(fragQuestions.fragID);
        if (f != null) {
            try {
                ((fragQuestions) f)._main = _main;
                ((fragQuestions) f).initFragQuestions(true, -1, -1, null, null, null, 0l, false);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }
        f = _main.fPA.findFragment(fragQuestion.fragID);
        if (f != null) {
            try {
                ((fragQuestion) f).setDate();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }

        f = _main.fPA.findFragment(fragHome.fragID);
        if (f != null) {
            try {
                ((fragHome) f).getNew();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }

    }

    private void setUserDataIntoTextViews() throws Throwable {


        if (_main == null && getActivity() != null) _main = (MainActivity) getActivity();
        if (mEmailView == null || _main == null || _main.user == null || showlogin) return;
        if (!_main.user.isNull(EMAIL)) mEmailView.setText(_main.user.getString(EMAIL));
        if (!_main.user.isNull(RUFNUMMER)) mRufnummer.setText(_main.user.getString(RUFNUMMER));
        if (!_main.user.isNull(VORNAME))
            dlgLogin.this.mVorname.setText(_main.user.getString(VORNAME));
        if (!_main.user.isNull(NAME)) dlgLogin.this.mName.setText(_main.user.getString(NAME));
        if (!_main.user.isNull(PLZ)) dlgLogin.this.mPLZ.setText(_main.user.getString(PLZ));
        if (!_main.user.isNull("adresse"))
            dlgLogin.this.mAdresse.setText(_main.user.getString("adresse"));
        if (!_main.user.isNull(BENUTZERNAME))
            dlgLogin.this.mBenutzername.setText(_main.user.getString(BENUTZERNAME));
        if (!_main.user.isNull(BENUTZERNAME))
            dlgLogin.this.mNewUserName.setText(_main.user.getString(BENUTZERNAME));
        // this.profileImage = null;

    }


    enum LoginMode {
        login, firstlogin, register, edtdata, none
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public static class UserLoginTask extends AsyncTask<Void, Void, String> {

        static final String UPDATE = "update";
        static final String NAME = "Name";
        static final String VORNAME = "Vorname";
        static final String NICK = "Nick";
        static final String TEST = "test";
        static final String NEUER_BENUTZERNAME = "NeuerBenutzername";
        static final String E_MAIL = "EMail";
        static final String NEUE_E_MAIL = "NeueEMail";
        static final String RUFNUMMER = "Rufnummer";
        static final String PLZ = "PLZ";
        static final String KENNWORT = "Kennwort";
        static final String TOKEN_RESULT = "TokenResult";
        static final String BREITENGRAD = "Breitengrad";
        static final String LAENGENGRAD = "Laengengrad";
        static final String DATUM_POSITION_AKTUALISIERUNG = "DatumPositionAktualisierung";
        static final String LOCALE = "locale";
        static final String DEAKTIVIERT = "deaktiviert";
        static final String DUPLICATE_ENTRY = "Duplicate entry";
        static final String ADRESSE = "Adresse";
        private final String mEmail;
        private final boolean mUpdate;
        private final boolean mCallback;
        private final Bundle mSavedInstanceState;
        private final String mTokenResult;
        private final String mAdresse;
        private final boolean mRegisteronfailure;
        private final dlgLogin fragLogin;
        private final Context context;
        private final String mPassword;
        private final String mName;
        private final String mVorname;
        private final String mPLZ;
        private final String mAccesskey;
        private final String mNeuerBenutzername;
        private MainActivity _main;
        private LimindoService mService;
        private String mBenutzername;
        private String mPhoneNumber;

        UserLoginTask(dlgLogin fragLogin, String tokenResult, boolean update, String email, String benutzername, String neuerbenutzername, String password, String Name, String Vorname, String Rufnummer, String PLZ, String Adresse, String AccessKey, boolean callback, Bundle savedinstancestate, boolean registeronfailure) {
            mUpdate = update;
            mEmail = email;
            mBenutzername = benutzername;
            mNeuerBenutzername = neuerbenutzername;
            mPassword = password;
            mAccesskey = AccessKey;
            mName = Name;
            mVorname = Vorname;
            mPhoneNumber = Rufnummer;
            mPLZ = PLZ;
            mAdresse = Adresse;
            mTokenResult = tokenResult;
            this.fragLogin = fragLogin;
            if (mPhoneNumber != null && mPhoneNumber.length() > 3) {
                mPhoneNumber = lib.formatPhoneNumber(mPhoneNumber);
                fragLogin.mRufnummer.setText(mPhoneNumber);
            }
            this.mCallback = callback;
            this.mSavedInstanceState = savedinstancestate;
            this.mRegisteronfailure = registeronfailure;
            this._main = fragLogin._main;
            this.mService = fragLogin.mService;
            this.context = fragLogin.context;
            Log.e("UserLoginTask", "TokenResult: " + mTokenResult);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (_main == null || (_main.mService == null && mService == null))
                throw new IllegalArgumentException();

            try {
                if (_main.user != null && _main.user.getLong(Constants.id) == 57) return;
            } catch (JSONException e) {
                e.printStackTrace();
            }

            stopservice();
        }

        private void stopservice() {
            if (_main.mService != null) {
                try {
                    _main.mService.stop(false);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            } else {
                try {
                    mService.stop(false);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            LimindoService.Ping rr = null;
            // First test, if the server is online
            try {
                rr = LimindoService.ping(new URL(MYURLROOT), context);
            } catch (Throwable e) {
                e.printStackTrace();
                return ERROR + ":" + e.getMessage();
            }

            // Fehlermeldung wenn der Server innerhalb von 10 Sekunden nicht antwortet
            try {
                if ((rr != null ? rr.dns : 100000) > 10000)
                    return ERROR + ":" + _main.getString(R.string.notonline);
            } catch (Throwable ex) {
                return ERROR;
            }

            try {
                // Anmeldung am Server
                clsHTTPS https = (_main != null ? _main.clsHTTPS : mService.clsHTTPS);
                Log.e("UserLoginTask", "TRUE");
                Map<String, String> p = new java.util.HashMap<String, String>();
                String res = null;
                // Anmeldung mit Accesskey
                if (mAccesskey != null) {
                    res = https.Logon(mAccesskey, mBenutzername, mEmail, MainActivity.getOnlineState(https.context));
                } else {
                    // Anmeldung mit EMail, Benutzername und Kennwort
                    p.put(UPDATE, "" + mUpdate);
                    p.put(NAME, mName);
                    p.put(VORNAME, mVorname);
                    p.put(NICK, TEST);
                    p.put(BENUTZERNAME1, mBenutzername);
                    p.put(NEUER_BENUTZERNAME, mNeuerBenutzername);
                    p.put(E_MAIL, mEmail);
                    p.put(NEUE_E_MAIL, mEmail);
                    if (mPhoneNumber == null || mPhoneNumber.length() == 0)
                        mPhoneNumber = context.getString(R.string.leer);
                    p.put(RUFNUMMER, mPhoneNumber);
                    p.put(PLZ, mPLZ);
                    p.put(ADRESSE, mAdresse);
                    p.put(KENNWORT, mPassword);
                    if (mTokenResult != null) p.put(TOKEN_RESULT, mTokenResult);
                    if (mPassword.length() < 8)
                        throw new RuntimeException(fragLogin.getString(R.string.passwordtoshort));
                    double breit = 0.0d;
                    double lang = 0.0d;
                    Date dt = Calendar.getInstance().getTime();
                    if (_main == null) _main = (MainActivity) fragLogin.getActivity();
                    if (_main.locCoarse() != null) {
                        breit = _main.locCoarse().getLatitude();
                        lang = _main.locCoarse().getLongitude();
                        dt = new Date(_main.locCoarse().getTime());
                    }
                    // Aktuelle Position übergeben
                    p.put(BREITENGRAD, "" + breit);
                    p.put(LAENGENGRAD, "" + lang);
                    p.put(DATUM_POSITION_AKTUALISIERUNG, https.fmtDateTime.format(dt));
                    p.put(LOCALE, Locale.getDefault().toString());
                    if (_main.user != null) {
                        p.put("BenutzerID", "" + _main.user.getLong(Constants.id));
                        _main.user = null;
                    }
                    //p.put("login", "test");
                    //Login per REST durchführen
                    res = https.Login(p);
                    // Als Ergebnis erhält man bei Erfolg ein JSONObjekt mit den Benutzerdaten.

                }
                return res;
                //Thread.sleep(2000);
            } catch (Throwable e) {
                Log.e(TAG, null, e);
                return ERROR + e.getClass().toString() + ":" + e.getMessage();
            }
/*
            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return "" + pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return "" + true; */
        }

        @Override
        protected void onPostExecute(final String success) {
            try {
                // Anzeigen das das Login durchgeführt wurde
                fragLogin.mAuthTask = null;
                /*
                try {
                    if (_main.user != null && _main.user.getLong(Constants.id) == 57) stopservice();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                 */
                fragLogin.showProgress(false);
                // Wenn fragLogin von Main aus gestartet wurde
                if (fragLogin.isAdded() && fragLogin.getActivity() != null && _main != null) {
                    // Es ist kein Fehler aufgetreten
                    if (success != null && !success.startsWith(ERROR)) {
                        // Es wurde keine E-Mail geschickt
                        if (!success.equals(E_MAIL)) {
                            fragLogin.mBenutzername.setError(fragLogin.getString(R.string.userlogon));
                            fragLogin.mPasswordView.requestFocus();
                            if (mAccesskey == null) {
                                // Es wurde ein neuer Accesskey generiert
                                reloginWithAccesskey(success);
                            } else {
                                // Es wurde eine Neuanmeldung für den USER durchgeführt und der USER wurde bestimmt
                                updateDataWithUserData(success);
                                // Überprüfen ob Einladungen oder unbestätigkte Kontakte vorliegen
                                boolean res;
                                res = _main.getInvited();
                                if (!res) res = _main._getUnconfirmed();
                                if (!res) {
                                    // Beim ersten Anmelden Kontakliste anzeigen
                                    boolean invisibleRegistration = false;
                                    invisibleRegistration = fragLogin.mode == LoginMode.none
                                            && fragLogin.mEmailUpdateButton.getVisibility() == View.VISIBLE
                                            && fragLogin.mEmailUpdateButton.getText().toString().equalsIgnoreCase(fragLogin.getString(R.string.register));
                                    if (fragLogin.mode == LoginMode.register
                                            || fragLogin.firstLogin
                                            || (invisibleRegistration)) {
                                        try {
                                            //_main.mPager.setCurrentItem(fragContacts.fragID);
                                            fragLogin.loadQuestions(true);
                                            dlgContacts dlgContacts = new dlgContacts();
                                            dlgContacts._main = _main;
                                            if (fragLogin.fm == null) fragLogin.fm = _main.fm;
                                            dlgContacts.show(fragLogin.fm, "dlgContacts");
                                        } catch (Throwable ex) {
                                            lib.ShowException(TAG, fragLogin.getContext(), ex, true);
                                        }
                                    }
                                    // Ansonsten eigene Fragen laden
                                    else {
                                        if (_main.readMessage != null) {
                                            fragLogin.loadQuestions(false);
                                        } else {
                                            fragLogin.loadQuestions(true);
                                            _main.mPager.setCurrentItem(fragHome.fragID);
                                            _main.getSupportActionBar().setTitle(fragLogin.getString(R.string.mnuHome));
                                        }
                                    }
                                    // Liegen Einladungen oder unbestätigte Kontakte vor Fragen laden aber nicht zum Fragenfenster wechseln
                                    // weil eventuell die Kontakte angezeigt werden.
                                } else {
                                    fragLogin.loadQuestions(true);
                                }
                                fragLogin.firstLogin = false;
                                try {
                                    fragLogin.dismiss();
                                } catch (Throwable ex) {
                                    Log.e(TAG, null, ex);
                                }
                            }
                        } else {
                            EMailMustBeConfirmed();
                        }
                    } else {
                        // Es ist ein Fehler aufgetreten
                        processError(success);

                    }
                    //Die Anmeldung wird unsichtbar durchgeführt
                } else if (mService != null) {
                    if (success != null && !success.startsWith(ERROR)) {
                        if (mAccesskey == null) {
                            invisibleLoginNotSuccessful();
                        } else {
                            invisibleLoginSuccessfulSetData(success);
                        }
                    } else {
                        invisibleLoginError(success);
                    }
                }
            } catch (Throwable ex) {
                Log.e(TAG, null, ex);
            }

        }

        private void invisibleLoginError(String success) {
            String result = success.replace(ERROR, "");
            if (result.contains(DUPLICATE_ENTRY)) {
                if (result.contains(BENUTZERNAME1)) {
                    result = fragLogin.getString(R.string.nameschonvorhanden);
                } else {
                    result = fragLogin.getString(R.string.doubleentry);
                }
            } else if (result.contains("Failed to connect to")) {
                result = fragLogin.getString(R.string.zZtkeineVerb);
            }
            result = result.replace(ERROR, "");
            try {
                mService.sendMessageToNotification(result, false, false);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
            mService.mState = LimindoService.STATE_ERROR;
            mService.mErrMsg = result;
            mService.updateUserInterfaceTitle(false, false);
        }

        private void invisibleLoginSuccessfulSetData(String success) {
            try {
                mService.user = new JSONArray(success).getJSONObject(0);
                mService.clsHTTPS.setAccesskey(mAccesskey);
                String fname;
                fname = ".JPG";
                Long BenutzerID = mService.user.getLong(Constants.id);
                String BenutzerName = mService.user.getString(BENUTZERNAME);
                try {
                    Bitmap in = mService.clsHTTPS.downloadProfileImage(context, BenutzerID, BenutzerID, BenutzerName);
                    if (in != null) {
                        mService.user.put("Image", in);
                    }
                } catch (Throwable e) {
                    Log.i(TAG, null, e);
                }
                mService.connectService(true, false, true);
                if (_main != null) {
                    _main.user = mService.user;
                    if (_main.mnuUpload != null) _main.mnuUpload.setEnabled(true);
                    _main.deaktiviert = _main.user.getBoolean(DEAKTIVIERT);
                }
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }

        private void invisibleLoginNotSuccessful() {
            try {
                mService.sendMessageToNotification(fragLogin.getString(R.string.nosecretkey), false, false);
                mService.mState = LimindoService.STATE_ERROR;
                mService.mErrMsg = fragLogin.getString(R.string.nosecretkey);
                mService.updateUserInterfaceTitle(false, false);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }

        private void processError(String success) {
            String result = success.replace(ERROR, "");
            if (result.contains(DUPLICATE_ENTRY)) {
                if (result.contains(BENUTZERNAME1)) {
                    result = fragLogin.getString(R.string.nameschonvorhanden);
                } else {
                    result = fragLogin.getString(R.string.doubleentry);
                }
            }
            if (mRegisteronfailure) {
                fragLogin.Register();
            } else {
                fragLogin.mBenutzername.setError(fragLogin.getString(R.string.Fehler) + ": " + result);
                Log.e(TAG, result);
                fragLogin.mBenutzername.requestFocus();
            }
        }

        private void EMailMustBeConfirmed() {
            fragLogin.mBenutzername.setError(fragLogin.getString(R.string.EMailSent));
            fragLogin.mBenutzername.requestFocus();
            try {
                if (_main != null) {
                    _main.user = null;
                    if (_main.mService != null) {
                        _main.mService.user = null;
                        _main.mService.stop(false);
                    }
                    //_main.clsHTTPS.accesskey = null;
                }
                fragLogin.firstLogin = true;
                fragLogin.setMode(LoginMode.login);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        private void updateDataWithUserData(String success) {
            try {
                // MainActivity updaten
                _main.user = new JSONArray(success).getJSONObject(0);
                _main.clsHTTPS.setAccesskey(mAccesskey);
                if (_main.mnuUpload != null) _main.mnuUpload.setEnabled(true);
                _main.clsHTTPS.AdminID = null;
                String fname;
                fname = ".JPG";
                Long BenutzerID = _main.user.getLong(Constants.id);
                String BenutzerName = _main.user.getString(BENUTZERNAME);
                _main.setBenutzerID(BenutzerID);
                Bitmap in = null;
                // Profilbild updaten
                try {
                    in = _main.clsHTTPS.downloadProfileImage(context, BenutzerID, BenutzerID, BenutzerName);
                } catch (Throwable ex) {
                    Log.i(TAG, null, ex);
                    ex.printStackTrace();
                }
                if (in != null) {
                    _main.user.put(Constants.Image, in);
                }

                if (_main.user != null) {
                    if (_main.user.getBoolean(Constants.administrator)) {
                        _main.clsHTTPS.AdminID = _main.user.getLong(Constants.id);
                    } else {
                        _main.clsHTTPS.AdminID = null;
                    }
                }
                _main.deaktiviert = _main.user.getBoolean(DEAKTIVIERT);
                //if (_main.user.getLong(Constants.id) == 55 || _main.user.getLong(Constants.id) == 57)
                //    _main.showAnswers = true;
                if (fragLogin.mService == null) fragLogin.mService = _main.mService;
                if (mService == null) mService = fragLogin.mService;
                if (fragLogin.mService != null) {
                    fragLogin.mService.user = _main.user;
                    fragLogin.mService.showAnswers = _main.showAnswers;
                    fragLogin.mService.clsHTTPS.setAccesskey(mAccesskey);
                    fragLogin.mService.connectService(true, false, true);
                }
                if (mCallback) {
                    _main.init2(mSavedInstanceState);
                } else {
                    _main.connectService(true, LimindoService.alivecount > 0, true);
                }

                //Wenn von Google ein neues Bild geladen wurde dieses hochladen falls noch keines vorhanden ist
                if (fragLogin.mode == LoginMode.register && fragLogin.profileImage != null) {
                    updateProfileImage(in, BenutzerID, fname);
                }
                // fragLogin updaten
                fragLogin.setUserDataIntoTextViews();


            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                if (fragLogin.isAdded() && fragLogin.getContext() != null)
                    lib.ShowException(TAG, fragLogin.getContext(), e, false);
            }
        }

        private void updateProfileImage(Bitmap in, Long BenutzerID, String fname) {
            if (in == null) {
                try {
                    in = _main.clsHTTPS.downloadImage(fragLogin.getContext(), "Image" + BenutzerID + fname, BenutzerID);
                } catch (Throwable ex) {
                    Log.i(TAG, null, ex);
                    in = null;
                }
            }
            if (in == null) {
                lib.ShowMessage(fragLogin.getContext(), fragLogin.getString(R.string.UploadingProfileImage), "");
                try {
                    final ByteArrayOutputStream out = new ByteArrayOutputStream();
                    final Bitmap bmp = fragLogin.profileImage;
                    myRunnableCompressBM myRunnable = new myRunnableCompressBM(out, bmp);
                    _main.runOnUiThread(myRunnable);
                    synchronized (myRunnable) {
                        while (myRunnable.needWait) myRunnable.wait();
                    }
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(out.toByteArray());
                    fname = ".JPG";
                    fname = "Image" + BenutzerID + fname;
                    _main.clsHTTPS.upload(fragLogin.getContext(), inputStream, fname, BenutzerID, null);
                    _main.user.put(Constants.Image, bmp);
                    if (_main.clsHTTPS.hashImages.containsKey(fname)) {
                        _main.clsHTTPS.hashImages.replace(fname, bmp);
                    } else {
                        _main.clsHTTPS.hashImages.put(fname, bmp);
                    }
                } catch (Throwable ex) {
                    lib.ShowException(TAG, fragLogin.getContext(), ex, false);
                }
            }
        }

        private void reloginWithAccesskey(String success) {
            if (mNeuerBenutzername != null && mNeuerBenutzername.length() >= 8)
                mBenutzername = mNeuerBenutzername;
            SharedPreferences.Editor prefs = _main.getSharedPreferences(SECRET, MODE_PRIVATE).edit();
            prefs.putString(KEY, success);
            prefs.putString(EMAIL1, mEmail);
            prefs.putString(BENUTZERNAME1, mBenutzername);
            // Accesskey speichern
            prefs.apply();
            // Neuanmeldung mit Accesskey und eventuellem TokenResult "Google" durchführen
            fragLogin.mAuthTask = new UserLoginTask(fragLogin, mTokenResult != null && mTokenResult.equals("Google") ? mTokenResult : null, false, mEmail, mBenutzername, null, null, null, null, null, null, null, success, mCallback, mSavedInstanceState, false);
            fragLogin.showProgress(true);
            fragLogin.mAuthTask.execute((Void) null);
        }

        @Override
        protected void onCancelled() {
            fragLogin.mAuthTask = null;
            fragLogin.showProgress(false);
        }

        // Asynchrones komprimieren einer Bitmap
        public static class myRunnableCompressBM implements Runnable {
            public boolean needWait;
            private OutputStream out;
            private Bitmap bmp;

            public myRunnableCompressBM(OutputStream pout, Bitmap pbmp) {
                this.out = pout;
                this.bmp = pbmp;
                this.needWait = true;
            }

            @Override
            public void run() {
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                synchronized (this) {
                    this.needWait = false;
                    this.notify();
                }

            }
        }


    }

    /**
     * Background Async task to load user profile picture from url
     */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        Bitmap bmImage;
        private Throwable ex;

        public LoadProfileImage(Bitmap bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... uri) {
            String url = uri[0];
            Bitmap mIcon11 = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
                };

                // Install the all-trusting trust manager
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Throwable e) {
                Log.e("Error", e.getMessage());
                this.ex = e;
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            if (result != null) {


                Bitmap resized = Bitmap.createScaledBitmap(result, 200, 200, true);
                dlgLogin.this.profileImage = (ImageHelper.getRoundedCornerBitmap(getContext(), resized, 250, 200, 200, false, false, false, false));

            } else {
                dlgLogin.this.profileImage = bmImage;
                if (this.ex != null) {
                    lib.ShowException(TAG, dlgLogin.this.getContext(), ex, false);
                }
            }
        }
    }
}

