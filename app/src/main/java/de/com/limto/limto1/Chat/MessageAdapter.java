package de.com.limto.limto1.Chat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.UUID;

import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.lib.lib;

import static de.com.limto.limto1.Controls.Question.sdfdateshort;
import static de.com.limto.limto1.Controls.Question.sdfday;
import static de.com.limto.limto1.Controls.Question.sdftime;
import static de.com.limto.limto1.MainActivity.BENUTZERIDEMPFAENGER;
import static de.com.limto.limto1.MainActivity.BENUTZER_ID;
import static de.com.limto.limto1.MainActivity.blnAccessibility;
import static de.com.limto.limto1.clsHTTPS.TRUE;


public class MessageAdapter extends BaseAdapter {
    public static String TAG = "MessageAdapter";
    public MainActivity _main;
    HashMap<Long, LinkedHashMap<UUID, Message>> messages = new HashMap<Long, LinkedHashMap<UUID, Message>>();
    dlgChat context;
    private HashMap<Long, LinkedHashMap<UUID, Message>> messagesbak = null;
    private String strMessages;
    private Long BenutzerID;


    MessageAdapter(dlgChat f) {
        this.context = f;
        _main = (MainActivity) f.getActivity();
    }

//ToDo überarbeiten!
    public Message insertObject(JSONObject o) throws Throwable {

        if (o != null) {

            Message mm = createNewMessageFromO(o);

            if (mm.statusChanged()) {
                Message found = setMessageStatus(mm, o);
                return found;
            } else {
                Message m = SelectMemberButtonAndInsertMessageIfNotExists(mm, o);
                notifyDataSetChanged();
                return m;
            }

        } else {
            throw new Exception("Object null");
        }

    }

    private Message SelectMemberButtonAndInsertMessageIfNotExists(Message mm, JSONObject o) throws Throwable {
        try {
            if ((!(context.isAdded() && (context.isVisible()||context.isvisible)))) {
                context.selectRadioButton(mm.getBenutzerID());
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        Message m = hasuuid(mm.getUUID(), mm.getBenutzerID());
        if (m != null && (!(context.isAdded() && (context.isVisible()||context.isvisible)))) {
            try {
                this.context.selectRadioButton(m.getMemberData().getBenutzerid());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                lib.ShowException(TAG, context.getContext(), throwable, m.getMemberData().toString(), false);
            }
        } else {
            m = insertMessageFromObject(o,m);
        }
        return m;
    }

    private Message createNewMessageFromO(JSONObject o) throws Throwable {
        Message mm;
        try {
            mm = new Message(_main, context, o, false);
        } catch ( Throwable ex)
        {
            lib.setStatusAndLog(context.getContext(), TAG, "*** insertObject Error " + ex.getMessage());
            throw ex;
        }
        if (mm.getBenutzerID() == context.BenutzerID)
        {
            mm.setBelongsToCurrentuser(true);
        }
        if (mm.getReceivedByServer())
        {
            System.out.println("ReceivedByServer");
        }
        return mm;
    }

    private Message setMessageStatus(Message mm, JSONObject o) {
        Message found = null;
        if (mm.getUUID() != null) {

            Message m = getMessages(mm.getBenutzerID()).get(mm.getUUID());
            {
                if (m != null) {
                    if (!o.optString("Message", "").contains("⸙")) {
                        try {

                            if (mm.getSendMessages()) m.setSendMessages(true);

                            if (mm.getReceivedByServer()) {
                                m.setReceivedByServer(mm.getReceivedByServer());
                                if (m.getText().toString().equals(context.editText.getText().toString())) {
                                    context.editText.getText().clear();
                                }
                            }
                            if (mm.getSentToReceiver())
                                m.setSentToReceiver(mm.getSentToReceiver());
                            if (mm.getReadByReceiver()) {
                                if (!m.getSentToReceiver()) m.setSentToReceiver(true);
                                m.setReadByReceiver(mm.getReadByReceiver());
                            }
                        } catch (Throwable ex) {
                            lib.ShowException(TAG, _main, ex, new Gson().toJson(m) + "\n" + o.toString(), false);
                        }
                        notifyDataSetChanged();
                        found = m;
                        if (context.getBenutzerIDEmpfaenger() == null || context.getBenutzerIDEmpfaenger() < 0) {
                            context.setNameEmpfaenger(mm.getBenutzerName());
                            context.setBenutzerIDEmpfaenger(mm.getBenutzerID(), false);
                        }
                    }

                }
            }
        }
        return found;
    }

    private LinkedHashMap<UUID, Message> getMessages(long benutzerID) {
        LinkedHashMap<UUID, Message> mm = messages.get(benutzerID);
        if (mm == null) {
            mm = new LinkedHashMap<>();
            messages.put(benutzerID, mm);
        }
        return mm;
    }
    private Message insertMessageFromObject(JSONObject o, Message mm) throws Throwable {
        if (_main == null || _main.user == null) {
            Exception ex = new Exception("main or user == null");
            mm.setError(ex);
            throw (ex);
        }
        if (mm == null) mm = new Message(_main, context, o, false);
        Message res = null;
        res = context.onMessage(mm); //(mm.getUUID(), mm.getBenutzerID(), mm.getBenutzerName(), new SpannableString(mm.getText()), dlgChat.getRandomColor(), mm.getTime(), false);
        String uuid = o.optString("uuid");
        o.put("read", true);
        try {
            String msg = o.getString("Message");
            lib.setStatusAndLog(_main, TAG, "*** trysendMessageRead: " + msg);
            boolean success = trysendMessageRead(o, 0, false, res);
        } catch (Throwable e) {
            e.printStackTrace();
            lib.ShowException(TAG, _main, e, true);
        }
        return res;
    }

    public boolean trysendMessageRead(final JSONObject o, final int i, final boolean shown, final Message mm) throws Throwable {
        UUID uuid;
        uuid = UUID.fromString(o.optString("uuid", null));
        try {
            String Message = o.getString("Message");
            if (Message.contains("⸙")) return false;
            if (o.has("sendMessages")) {
                lib.setStatusAndLog(_main, TAG, "*** trysendMessageRead sendMessages " + Message);
                return false;
            }
            lib.setStatusAndLog(_main, TAG, "*** trysendMessageRead called: " + Message);
            boolean isShown = (context.isAdded() || context.isvisible || context.isVisible()) && !context.isHidden();
            if (!shown && !isShown) {
                dlgChat.pendingRead.add(o);
                context.savePendingRead(_main.getBenutzerID());
                lib.setStatusAndLog(_main, TAG, "*** trysendMessageRead added to pendingread " + Message);
                return false;
            }

            if (uuid == null) throw new Exception("uuid not found!");

            long BenutzerIDEmpfaenger = o.getLong(BENUTZERIDEMPFAENGER);
            long BenutzerID = o.getLong(BENUTZER_ID);

            if (BenutzerID < 0) {
                lib.ShowMessage(_main, _main.getString(R.string.invalid_userid), _main.getString(R.string.Error));
                return false;
            }


            String rres = _main.clsHTTPS.sendMessage(BenutzerID, Message, uuid, true, _main);

            if (!rres.equalsIgnoreCase(TRUE) && i < 6) {
                _main.mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            boolean res = trysendMessageRead(o, i + 1, shown, mm);
                        } catch (Throwable e) {
                            e.printStackTrace();
                            lib.ShowException(TAG, _main, e, true);
                        }
                    }
                }, 100);
                return false;
            } else if (!rres.equalsIgnoreCase(TRUE)) {
                lib.ShowMessage(_main, _main.getString(R.string.readcouldnotbesent) + "\n" + rres, _main.getString(R.string.Error));
                return false;
            } else {
                setSentToServer(mm, uuid,o);
                return true;
            }
        } catch (Throwable ex)
        {
            ex.printStackTrace();
            setError (mm,ex,uuid, o);
            return false;
        }
    }

    private boolean setError(Message mm, Throwable ex, UUID uuid, JSONObject o) {
        if (mm == null)
        {
            mm = getMessage(o);
            if (mm == null)
            {
                Message finalMm = mm;
                _main.mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            setError(finalMm, ex, uuid, o);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 500);
            }
            return false;
        }
        mm.setError(ex);
        return true;
    }

    private boolean setSentToServer(Message mm, UUID uuid, JSONObject o) throws Exception {
        if (mm == null)
        {
            mm = getMessage(o);
            if (mm == null)
            {
                Message finalMm = mm;
                _main.mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            setSentToServer(finalMm, uuid, o);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 500);
            }
            return false;
        }
        mm.setSentToServer(true);
        boolean res = dlgChat.pendingRead.remove(o);

        return true;
    }


    public void add(boolean self, Message message, boolean notify) {
        LinkedHashMap<UUID, Message> messages = getMessages(message.getMemberData().getBenutzerid());
        if (messages.containsKey(message.getUUID()) && self)
        {
            message.setUUID(UUID.randomUUID()); //UUID.fromString(message.getUUID().toString() + "SELF"));
            message.setSelf(false);
        }
        if (!messages.containsKey(message.getUUID())) {
            messages.put(message.getUUID(), message);
        }
        if (notify) notifyDataSetChanged();
    }


    public Long getBenutzerID() {
        this.BenutzerID = context.getBenutzerIDEmpfaenger();
        return BenutzerID;
    }

    public LinkedHashMap<UUID, Message> getCurrentMessages() {
        return getMessages(getBenutzerID());
    }



    @Override
    public int getCount() {
        return getMessages(getBenutzerID()).size();
    }

    @Override
    public Object getItem(int i) {
        return getCurrentMessages().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View convertView, final ViewGroup viewGroup) {
        LayoutInflater messageInflater = getLayoutInflater();
        try {
            MessageViewHolder holder = new MessageViewHolder();
            SpannableString ss = null;
            if (context == null) {
                return convertView = createView(messageInflater);
            } else {
                if (_main == null) _main = (MainActivity) context.getActivity();
                Message message = getCurrentMessages().get(getCurrentMessages().keySet().toArray(new UUID[0])[i]);
                if (message == null) return convertView = createView(messageInflater);
                if (message.getMemberData() == null)
                {
                    message.setMemberData(context.MemberData.get(message.getBenutzerID()));
                }
                setMemberData(holder,message);

                String strTime = getMessageTime(message);

                if (message.getText().toString().startsWith("(JSONObject)"))
                {
                    ss = lib.getClickableSpanFromMessage(_main.clsHTTPS, _main.getBenutzerID(), _main, message.getText().toString(), message.isBelongsToCurrentUser() && !blnAccessibility);
                }


                if (message.isBelongsToCurrentUser()) {
                    convertView = createOwnMessage(message,ss, messageInflater, holder, strTime);
                } else {
                    convertView = createTheirMessage(messageInflater, holder, ss, message, strTime);
                }

                return convertView;
            }
        } catch (Throwable ex) {
            lib.ShowException(TAG, _main, ex, "MessageAdapter.getView message: ", false);
        }
        if (convertView == null) convertView = messageInflater.inflate(lib.getLayoutResFromTheme(_main, R.attr.myMessage), null);
        return convertView;
    }

    private View createTheirMessage(LayoutInflater messageInflater, MessageViewHolder holder, SpannableString ss, Message message, String strTime) {
        View convertView = messageInflater.inflate(R.layout.their_message, null);
        holder.avatar = (View) convertView.findViewById(R.id.avatar);
        holder.name = (TextView) convertView.findViewById(R.id.name);
        holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);
        convertView.setTag(holder);

        setTimeAndHooks(strTime, message, holder);


        holder.messageBody.setMovementMethod(LinkMovementMethod.getInstance());

        setHolderText(holder,ss, message);

        setAvatar(holder,message);

        return convertView;
    }

    private void setAvatar(MessageViewHolder holder, Message message) {

        GradientDrawable drawable = (GradientDrawable) holder.avatar.getBackground();

        int backgroundColor;
        try {
            backgroundColor = Color.parseColor(message.getMemberData().getColor());
        } catch (Exception e) {
            e.printStackTrace();
            backgroundColor = Color.parseColor(dlgChat.getRandomColor());
        }

        drawable.setColor(backgroundColor);

        if (_main != null) {
            try {
                Bitmap I = (_main.clsHTTPS.downloadProfileImage(_main, message.getMemberData().getBenutzerid(), this.BenutzerID, message.getMemberData().getName()));
                RoundedBitmapDrawable d = null;
                if (I != null)
                    d = RoundedBitmapDrawableFactory.create(context.getResources(), I);
                if (d != null) {
                    d.setCircular(true);
                    holder.avatar.setBackground(d);
                }
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        }

    }

    private View createView(LayoutInflater messageInflater)
    {
        View convertView = messageInflater.inflate(lib.getLayoutResFromTheme(_main, R.attr.myMessage), null);
        return convertView;
    }

    private View createOwnMessage(Message message, SpannableString ss, LayoutInflater messageInflater, MessageViewHolder holder, String strTime) {
        View convertView = createView(messageInflater);
        holder.messageBody = (TextView) convertView.findViewById(R.id.message_body);
        convertView.setTag(holder);

        setHolderText (holder,ss, message);

        holder.name = convertView.findViewById(R.id.namereceiver);

        setTimeAndHooks(strTime, message, holder);

        return convertView;
    }

    private void setTimeAndHooks(String strTime, Message message, MessageViewHolder holder) {
        StringBuilder sbName = new StringBuilder(strTime);

        if (message.getSentToServer()) {
            sbName.append("\u2713");
        }
        if (message.getReceivedByServer()) {
            sbName.append("\u2713");
        }
        if (message.getSentToReceiver()) {
            sbName.append("\u2713");
        }
        if (message.getReadByReceiver()) {
            sbName.append("\u2713");
        }
        holder.name.setText(sbName.toString());
    }

    private void setHolderText(MessageViewHolder holder, SpannableString ss, Message message) {
        if (ss != null)
        {
            holder.messageBody.setText(ss);
            holder.messageBody.setMovementMethod(LinkMovementMethod.getInstance());
        }
        else {
            holder.messageBody.setText(message.getText());
            holder.messageBody.setMovementMethod(LinkMovementMethod.getInstance());
        }
        if (message.getError() != null)
        {
            holder.messageBody.setError(message.getError().getMessage());
        }
    }

    private String getMessageTime(Message message) {
        if (message.getTime() != null) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY,0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.MILLISECOND,0);
            if (message.getTime().before(c.getTime()))
            {
                c.add(Calendar.DATE,-6);
                if (message.getTime().before(c.getTime()))
                {
                    return sdfdateshort.format(message.getTime());
                }
                else {
                    return sdfday.format(message.getTime());
                }
            }
            else
            {
                return sdftime.format(message.getTime());
            }
        }
        return "";
    }

    private void setMemberData(MessageViewHolder holder, Message message) {
        holder.setMemberData(message.getMemberData());
    }

    private LayoutInflater getLayoutInflater() {
        LayoutInflater I = null;
        int counter = 0;
        while (I == null && counter < 100) {
            try {
                I = context.getLayoutInflater();
            } catch (Throwable ex) {
                I = null;
                counter++;
            }
        }
        return I;
    }

    public void clearmessages(Long benutzerIDEmpfaenger) {
        if (benutzerIDEmpfaenger != null) {
            getMessages(benutzerIDEmpfaenger).clear();
        } else {
            messages.clear();
        }
    }

    public void saveMessages(boolean clear, long userid) throws Throwable {
        try {
            if (messages == null || _main == null) throw new Exception("messages or main is null");
            if (messages.size() == 0) return;
            String strMessages = new Gson().toJson(messages);
            _main.getPreferences(Context.MODE_PRIVATE).edit().putString("Messages" + userid, strMessages).apply();
            _main.getPreferences(Context.MODE_PRIVATE).edit().putBoolean("blnMessages", false).apply();
            if (clear) {
                clearmessages(null);
                notifyDataSetChanged();
            }
        } catch (Throwable ex)
        {
            ex.printStackTrace();
        }
    }

    public int size(Long benutzerIDEmpfaenger) throws Exception {
        if (benutzerIDEmpfaenger != null && benutzerIDEmpfaenger > -1) {
            return getMessages(benutzerIDEmpfaenger).size();
        } else {
            return getCurrentMessages().size();
        }
    }

    public void backupMessages() {
        messagesbak = messages;
    }

    public void restoreMessagesFromBackup() throws Exception {
        if (messagesbak == null) throw new Exception("messagesbak null");
        messages = messagesbak;
        notifyDataSetChanged();
    }

    public void loadMessages(boolean clear, long userid) {
        try {
            if (clear) {
                messages.clear();
                strMessages = null;
            }
            if (strMessages == null && messages.size() == 0) {
                strMessages = _main.getPreferences(Context.MODE_PRIVATE).getString("Messages" + userid, null);
                if (strMessages != null) {
                    Object o = new Gson().fromJson(strMessages, new TypeToken<HashMap<Long, LinkedHashMap<UUID, Message>>>() {
                    }.getType());
                    HashMap<Long, LinkedHashMap<UUID, Message>> m = (HashMap<Long, LinkedHashMap<UUID, Message>>) o;
                    messages = m;
                    notifyDataSetChanged();
                }
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            lib.ShowException(TAG, context.getContext(), ex, false);
        }
    }

    public Message hasuuid(UUID uuid, Long BenutzerID) {
        if (uuid != null) {
            Message m = getMessages(BenutzerID).get(uuid);
            {
                if (m != null && !m.isBelongsToCurrentUser()) {
                    return m;
                }
            }
        }
        return null;

    }

    public void remove(Message messagewrite) throws Exception {
        if (messagewrite == null || messagewrite.getMemberData() == null) return;
        LinkedHashMap<UUID, Message> msg = getMessages(messagewrite.getMemberData().getBenutzerid());
        if (msg.containsKey(messagewrite.getUUID()))
        {
            Message m = msg.remove(messagewrite.getUUID());
            if (m == null) throw new Exception(messagewrite.getUUID().toString() + " not found!");
        }
        else
        {
            throw new Exception(messagewrite.getUUID().toString() + " not found!");
        }
    }

    public Message getMessage(JSONObject o) {
        UUID uuid = UUID.fromString(o.optString("uuid", ""));
        long BenutzerID = o.optLong(BENUTZER_ID);
        return hasuuid(uuid, BenutzerID);
    }
}

class MessageViewHolder {
    public View avatar;
    public TextView name;
    public TextView messageBody;
    private dlgChat.MemberData memberData;

    public void setMemberData(dlgChat.MemberData memberData) {
        this.memberData = memberData;
    }

    public dlgChat.MemberData getMemberData() {
        return memberData;
    }
}