package de.com.limto.limto1;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

import de.com.limto.limto1.Errors.SQLiteDataActivity;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static android.content.Context.MODE_PRIVATE;
import static de.com.limto.limto1.MainActivity.RUFNUMMER;
import static de.com.limto.limto1.dlgLogin.regexcount;
import static de.com.limto.limto1.dlgLogin.regexlower;
import static de.com.limto.limto1.dlgLogin.regexnumbers;
import static de.com.limto.limto1.dlgLogin.regexspaces;
import static de.com.limto.limto1.dlgLogin.regexspecial;
import static de.com.limto.limto1.dlgLogin.regexupper;

/**
 * A login screen that offers login via email/password.
 */
public class fragChangePW extends DialogFragment {
    //public final  static int fragID = 0;
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    private static final String TAG = "fragChangePW";
    public static final String EMAIL = "email";
    public static final String BENUTZERNAME = "benutzername";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private ChangePWTask mAuthTask = null;

    // UI references.
    private TextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView mBenutzername;
    private EditText mPasswordView2;
    private LinearLayout mllSignIn;
    public MainActivity _main;
    public Context context;
    public LimindoService mService;
    public boolean initialized;
    private FragmentManager fm;
    private boolean callback;
    private Bundle savedInstanceState;
    private EditText mOldPasswordView;
    private Button mChangePasswordButton;


    public fragChangePW() {
        _main = (MainActivity) getActivity();
        context = _main;
    }

    public void init() throws Throwable {
        getDialog().setTitle(R.string.KennwortAendern);
        mPasswordView2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    try {
                        changepw();
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                    return true;
                }
                return false;
            }
        });
        mChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    changepw();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            }
        });
        try {
            setViews();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initialized = false;
        View v = null;
        try {
            _main = (MainActivity) getActivity();
            context = _main;
            v = inflater.inflate(R.layout.fragchangepw, container, false);
            mEmailView = (TextView) v.findViewById(R.id.email);
            mOldPasswordView = (EditText) v.findViewById(R.id.oldpassword);
            mPasswordView = (EditText) v.findViewById(R.id.password);
            mPasswordView2 = (EditText) v.findViewById(R.id.password2);
            mBenutzername = (TextView) v.findViewById(R.id.benutzername);
            mllSignIn = (LinearLayout) v.findViewById(R.id.llsign_in);
            mLoginFormView = v.findViewById(R.id.login_form);
            mProgressView = v.findViewById(R.id.login_progress);
            mChangePasswordButton = (Button) v.findViewById(R.id.change_password_button);

            init();
            initialized = true;
            return v;
        } catch (Throwable ex) {
            return v;
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _main = ((MainActivity) getActivity());

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void changepw() throws Throwable {
        if (mAuthTask != null) {
            return;
        }
        if (_main == null || _main.user == null) return;
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String password = mPasswordView.getText().toString();
        final String oldPassword = mOldPasswordView.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(oldPassword)) {
            mOldPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mOldPasswordView;
            cancel = true;
        }

        if (!mPasswordView.getText().toString().equals(mPasswordView2.getText().toString())) {
            mPasswordView2.setError(getString(R.string.error_pw_unequal));
            if (focusView != mPasswordView) focusView = mPasswordView2;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            new ChangePWTask(mEmailView.getText().toString(),mBenutzername.getText().toString(), oldPassword, password,null).execute();


        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        boolean rescount = password.matches(regexcount);
        boolean resnumbers = password.matches(regexnumbers);
        boolean reslower = password.matches(regexlower);
        boolean resupper = password.matches(regexupper);
        boolean resspecial = password.matches(regexspecial);
        boolean resspaces = password.matches(regexspaces);

        if (!rescount) {
            lib.ShowMessage(context, getString(R.string.passwordcount), getString(R.string.password));
            return false;
        }
        if (resspaces) {
            lib.ShowMessage(context, getString(R.string.passwordnospaces), getString(R.string.password));
            return false;
        }
        int matches = 0;
        if (resnumbers) matches++;
        if (reslower) matches++;
        if (resupper) matches++;
        if (resspecial) matches++;

        if (matches < 3) {
            lib.ShowMessage(context, getString(R.string.wrongpassword), getString(R.string.password));
            return false;
        }

        return true;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (!isAdded() || getActivity() == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }




    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class ChangePWTask extends AsyncTask<Void, Void, String> {

        public static final String ERROR = "Error:";
        public static final String E_MAIL = "EMail";
        public static final String SECRET = "secret";
        public static final String KEY = "KEY";
        public static final String EMAIL = "Email";
        public static final String BENUTZERNAME = "Benutzername";
        public static final String DUPLICATE_ENTRY = "Duplicate entry";
        private final String mNewPassword;
        private final String mPassword;
        private final String mEMail;
        private String mBenutzername;
        private final String mAccesskey;

        ChangePWTask(String EMail, String Benutzername, String password, String newPassword, String AccessKey) {
            mEMail = EMail;
            mBenutzername = Benutzername;
            mPassword = password;
            mNewPassword = newPassword;
            mAccesskey = AccessKey;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (_main==null || (_main.mService == null && mService == null)) throw new IllegalArgumentException();
            if (mAccesskey != null) {
                if (_main != null && _main.mService != null) {
                    try {
                        _main.mService.stop(false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                } else if (mService != null) {
                    try {
                        mService.stop(false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                }
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {

                clsHTTPS https = (_main != null ? _main.clsHTTPS : mService.clsHTTPS);
                Map<String, String> p = new java.util.HashMap<String, String>();
                String res = null;
                if (mAccesskey != null) {
                    res = https.Logon(mAccesskey, mBenutzername, mEMail, _main.onlineState);
                } else {
                    res = https.changepw(_main.user.getLong((Constants.id)), mPassword, mNewPassword);

                }
                return res;
                //Thread.sleep(2000);
            } catch (Throwable e) {
                Log.e(TAG, null, e);
                return ERROR + e.getClass().toString() + ":" + e.getMessage();
            }
/*
            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return "" + pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return "" + true; */
        }

        @Override
        protected void onPostExecute(final String success) {
            mAuthTask = null;
            showProgress(false);
            if (isAdded() && getActivity() != null && _main != null) {
                if (success != null && !success.startsWith(ERROR)) {
                    if (!success.equals(E_MAIL)) {
                        mPasswordView.setError(getString(R.string.userlogon));
                        mPasswordView.requestFocus();
                        if (mAccesskey == null) {
                            SharedPreferences.Editor prefs = _main.getSharedPreferences(SECRET, MODE_PRIVATE).edit();
                            prefs.putString(KEY, success);
                            prefs.putString(EMAIL, mEMail);
                            prefs.putString(BENUTZERNAME, mBenutzername);

                            prefs.commit();
                            mAuthTask = new ChangePWTask(mEMail, mBenutzername, null, null, success);
                            showProgress(true);
                            mAuthTask.execute((Void) null);
                        } else {
                            try {
                                _main.user = new JSONArray(success).getJSONObject(0);
                                _main.clsHTTPS.setAccesskey(mAccesskey);
                                String fname;
                                fname = ".JPG";
                                Long BenutzerID = _main.user.getLong(Constants.id);
                                String BenutzerName = _main.user.getString(BENUTZERNAME);
                                try{
                                    Bitmap in = _main.clsHTTPS.downloadProfileImage(context, BenutzerID, BenutzerID, BenutzerName);
                                    if (in != null) {
                                        _main.user.put("Image",in);
                                    }
                                }
                                catch (Throwable ex)
                                {
                                    Log.i(TAG,null,ex);
                                }
                                if (_main.user.getLong(Constants.id) == 55 || _main.user.getLong(Constants.id) == 57)
                                    _main.showAnswers = true;
                                if (mService == null) mService = _main.mService;
                                if (mService != null) {
                                    mService.user = _main.user;
                                    mService.showAnswers = _main.showAnswers;
                                    mService.clsHTTPS.setAccesskey(mAccesskey);
                                    mService.connectService(true, false, true);
                                }
                                setViews();
                                _main.connectService(true, LimindoService.alivecount > 0, true);
                            } catch (Throwable e) {
                                e.printStackTrace();
                                Log.e(TAG, null, e);
                                lib.ShowException(TAG, getContext(), e, false);
                            }
                            boolean getUnconfirmed = false;
                            if (_main.clsHTTPS != null) {
                                String res = null;
                                try {
                                    res = _main.clsHTTPS.getUnconfirmed(_main.user.getLong(Constants.id));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    JSONArray rres = null;
                                    if (res!=null && res.length()>0) rres = new JSONArray(res);
                                    if (rres != null && rres.length() > 0) {
                                        getUnconfirmed = true;
                                        _main.getUnconfirmed = true;
                                        Fragment frag = _main.fPA.findFragment(fragContacts2.fragID);
                                        if (frag != null) {
                                            if (_main.mPager.getCurrentItem() == fragContacts2.fragID
                                                    || _main.mPager.getCurrentItem() == fragContacts.fragID
                                                    || _main.mPager.getCurrentItem() == SQLiteDataActivity.fragID) {
                                                fragContacts2 f = (fragContacts2) frag;
                                                f.refresh();
                                            }
                                        }
                                        _main.mPager.setCurrentItem(fragContacts2.fragID);

                                    }
                                    else
                                    {
                                        res = _main.clsHTTPS.getInvited(_main.user.getLong(Constants.id),null,null);
                                        rres = null;
                                        if (res!=null && res.length()>0) rres = new JSONArray(res);
                                        if (rres != null && rres.length() > 0) {
                                            _main.getInvited = true;
                                            _main.ArrInvited = rres;
                                            Fragment frag = _main.fPA.findFragment(fragContacts2.fragID);
                                            if (frag != null) {
                                                if (_main.mPager.getCurrentItem() == fragContacts2.fragID
                                                        || _main.mPager.getCurrentItem() == fragContacts.fragID
                                                        || _main.mPager.getCurrentItem() == SQLiteDataActivity.fragID) {
                                                    fragContacts2 f = (fragContacts2) frag;
                                                    f.refresh();
                                                }
                                            }
                                            _main.mPager.setCurrentItem(fragContacts2.fragID);

                                        }
                                        else {
                                            res = _main.clsHTTPS.getInvited(_main.user.getLong(Constants.id), _main.user.getString(MainActivity.EMAIL), _main.user.getString(RUFNUMMER));
                                            rres = null;
                                            if (res!=null && res.length()>0) rres = new JSONArray(res);
                                            if (rres != null && rres.length() > 0) {
                                                _main.getInvited = true;
                                                _main.ArrInvited = rres;
                                                Fragment frag = _main.fPA.findFragment(fragContacts2.fragID);
                                                if (frag != null) {
                                                    if (_main.mPager.getCurrentItem() == fragContacts2.fragID
                                                            || _main.mPager.getCurrentItem() == fragContacts.fragID
                                                            || _main.mPager.getCurrentItem() == SQLiteDataActivity.fragID) {
                                                        fragContacts2 f = (fragContacts2) frag;
                                                        f.refresh();
                                                    }
                                                }
                                                _main.mPager.setCurrentItem(fragContacts2.fragID);

                                            }
                                        }
                                    }
                                } catch (Throwable ex) {
                                    Log.e(TAG, res);
                                }

                            }
                            if (!getUnconfirmed && !_main.getInvited) loadQuestions(false);
                            else loadQuestions(true);

                            dismiss();
                        }
                    } else {
                        mPasswordView.setError(getString(R.string.EMailSent));
                        mPasswordView.requestFocus();
                    }
                } else {
                    String result = success.replace(ERROR, "");
                    if (result.contains(DUPLICATE_ENTRY)) {
                        if (result.contains(BENUTZERNAME)) {
                            result = getString(R.string.nameschonvorhanden);
                        } else {
                            result = getString(R.string.doubleentry);
                        }
                    }
                    mPasswordView.setError(getString(R.string.Fehler) + ": " + result);
                    Log.e(TAG, result);
                    mPasswordView.requestFocus();
                }
            } else if (mService != null) {
                if (success != null && !success.startsWith(ERROR)) {
                    if (mAccesskey == null) {
                        try {
                            mService.sendMessageToNotification(getString(R.string.nosecretkey), false, false);
                            mService.mState = LimindoService.STATE_ERROR;
                            mService.mErrMsg = getString(R.string.nosecretkey);
                            mService.updateUserInterfaceTitle(false, false);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            Log.e(TAG, null, throwable);
                        }
                    } else {
                        try {
                            mService.user = new JSONArray(success).getJSONObject(0);
                            mService.clsHTTPS.setAccesskey(mAccesskey);
                            String fname;
                            fname = ".JPG";
                            Long BenutzerID = mService.user.getLong(Constants.id);
                            String BenutzerName = mService.user.getString(BENUTZERNAME);

                            try
                            {
                                Bitmap in = mService.clsHTTPS.downloadProfileImage(context, BenutzerID, BenutzerID, BenutzerName);
                                if (in != null) {
                                    mService.user.put("Image",in);
                                }
                            }
                            catch (Throwable ex)
                            {
                                Log.i(TAG,null,ex);
                            }


                            mService.connectService(true, false, true);
                            if (_main != null) _main.user = mService.user;
                        } catch (Throwable e) {
                            e.printStackTrace();
                            Log.e(TAG, null, e);
                        }
                    }
                } else {
                    String result = success.replace(ERROR, "");
                    if (result.contains(DUPLICATE_ENTRY)) {
                        if (result.contains(BENUTZERNAME)) {
                            result = getString(R.string.nameschonvorhanden);
                        } else {
                            result = getString(R.string.doubleentry);
                        }
                    }
                    try {
                        mService.sendMessageToNotification(result, false, false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                    mService.mState = LimindoService.STATE_ERROR;
                    mService.mErrMsg = result;
                    mService.updateUserInterfaceTitle(false, false);
                }
            }

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void loadQuestions(boolean dontMove) {
        if (_main == null || _main.mPager == null) return;
        if (!dontMove) _main.mPager.setCurrentItem(fragQuestions.fragID);
        if (_main.readMessage != null) return;
        Fragment f = _main.fPA.findFragment(fragQuestions.fragID);
        if (f != null) {
            try {
                ((fragQuestions) f)._main = _main;
                ((fragQuestions) f).initFragQuestions(true, -1, -1, null, null, null, null, false);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }
        f = _main.fPA.findFragment(fragQuestion.fragID);
        if (f != null) {
            try {
                ((fragQuestion) f).setDate();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        }

    }

    private void setViews() throws JSONException {
        if (_main == null && getActivity() != null) _main = (MainActivity) getActivity();
        if (mEmailView == null || _main == null || _main.user == null) return;
        if (!_main.user.isNull(EMAIL)) mEmailView.setText(_main.user.getString(EMAIL));
        if (!_main.user.isNull(BENUTZERNAME))
            fragChangePW.this.mBenutzername.setText(_main.user.getString(BENUTZERNAME));

    }
}

