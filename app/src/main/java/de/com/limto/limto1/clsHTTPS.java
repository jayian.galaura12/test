package de.com.limto.limto1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.security.KeyStore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import de.com.limto.limto1.Chat.dlgChat;
import de.com.limto.limto1.Controls.QuestionsAdapter;
import de.com.limto.limto1.Errors.Path;
import de.com.limto.limto1.lib.LetterTileProvider;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static android.content.Context.MODE_PRIVATE;
import static de.com.limto.limto1.Controls.QuestionsAdapter.BENUTZER_FRAGEN_ID;
import static de.com.limto.limto1.Controls.QuestionsAdapter.FRAGE_ID;
import static de.com.limto.limto1.MainActivity.BENUTZERIDEMPFAENGER;
import static de.com.limto.limto1.MainActivity.BENUTZERNAME;
import static de.com.limto.limto1.fragQuestions.ARCHIVIERT;
import static de.com.limto.limto1.fragQuestions.AUS;
import static de.com.limto.limto1.fragQuestions.FRAG_QUESTIONS_GET_QUESTIONS;
import static de.com.limto.limto1.fragQuestions.GELOESCHT;
import static de.com.limto.limto1.fragQuestions.RADIUS_KM;
import static de.com.limto.limto1.fragQuestions.SEARCH;
import static de.com.limto.limto1.fragQuestions.TIME_QUERY;
import static de.com.limto.limto1.fragSettings.ALL;
import static de.com.limto.limto1.fragSettings.LANG;
import static de.com.limto.limto1.fragSettings.SYSTEM;
import static de.com.limto.limto1.lib.lib.debugMode;

public class clsHTTPS {
    private static final String MYDOMAIN = "v15849.1blu.de"; //""cafe.parkwiese.de"; //"nataliehm.dnshome.de";
    private static final String MYPORT = "62114";
    public static final String ERROR_BENUTZER_NICHT_ANGEMELDET = "Error:Benutzer nicht angemeldet!";
    public static final String ERROR_UNABLE_TO_RESOLVE_HOST = "Error: Unable to resolve host";
    public static final String ERROR = "Error: ";
    public static final String ACCESSKEY = "Accesskey";
    public static final String GET_USER = "getUser";
    public static final String POST = "POST";
    public static final String UTF_8 = "UTF-8";
    public static final String Delimiter_A = "\\A";
    public static final String ERROR1 = "Error:";
    public static final String BENUTZER_ID = "BenutzerID";
    public static final String KONTAKT_ID = "KontaktID";
    public static final String E_MAIL = "EMail";
    public static final String TELEFONNUMMER = "Telefonnummer";
    public static final String INVITE = "Invite";
    public static final String GET_INVITED = "getInvited";
    public static final String KONTAKTGRAD = "Kontaktgrad";
    public static final String UNCONFIRMED = "unconfirmed";
    public static final String GETUSERS = "getusers";
    public static final String DELETE_INVITED = "deleteInvited";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String ERROR_NO_SERVICE = "Error: No Service";
    public static final String ERROR_MAIN_NULL = "Error: main==null";
    public static final String SEND_MESSAGE = "sendMessage";
    private static final String ERROR_NOT_CONNECTED = "Error: Not connected";
    private static final String TAG = "clsHTTPS";
    private static final String DONTSAVELOGOFFDATE = "dontsavelogoffdate";
    public static final String ONLINE_STATE = "onlineState";
    public static String MYURLROOT = "https://" + MYDOMAIN + ":" + MYPORT + "/LimindoWeb/rest/test/";
    public static final String GET = "GET";
    public static final String MYURL = MYURLROOT + "query";
    public static final String MYURLTALK = "wss://" + MYDOMAIN + ":" + MYPORT + "/LimindoWeb/talk";
    public static final String GET_ANSWERS = "getAnswers";
    public static final String MYURLAnswers = MYURLROOT + GET_ANSWERS;
    public static final String MYURLQuestion = MYURLROOT + "Question";
    public static final String MYURLAnswer = MYURLROOT + "Answer";
    private static final String MYURLQuestions = MYURLROOT + "getQuestions";
    private static final String MYURLCountQuestions = MYURLROOT + "getCountQuestionsStored";
    private static final String MYURLLogon = MYURLROOT + "Logon";
    private static final String MYURLLogin = MYURLROOT + "Login";
    private static final String MYURLGetUnconfirmed = MYURLROOT + "getUnconfirmed";
    private static final String MYURLConfirm = MYURLROOT + "Confirm";
    private static final String MYURLCreateNewPW = MYURLROOT + "CreateNewPW";
    private static final String MYURLGetUsernames = MYURLROOT + "getUserNames";
    private static final String MYURLChangePW = MYURLROOT + "ChangePW";
    private static final String MYURLGetServerTime = MYURLROOT + "getServerTime";
    private static final String MYURLDel_Deact_Account = MYURLROOT + "Del_Deact_Account";

    static final String NO_KEYSTORE = "";
    static final String UNAUTH_KEYSTORE = "unauthclient.bks"; // Doesn't exist in server trust store, should fail authentication.
    public static final String TEST = "=test";
    public static final String ADMIN_ID = "AdminID";
    public static final String LOGON = "logon";
    public static int AUTH_KEYSTORE = R.raw.authclient;// "authclient.bks"; // Exists in server trust store, should pass authentication.
    static int TRUSTSTORE = R.raw.clienttrust; //"clienttrust.bks";
    private static final String CLIENT_PWD = "hlkLÖ&$)=K78340j";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public final static SimpleDateFormat fmtDateTime = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    static final SimpleDateFormat fmtDate = new SimpleDateFormat(YYYY_MM_DD);

    public Context context = null;
    public MainActivity main;
    public String accesskey;
    public Handler handler;
    public Long AdminID;
    public static long maxNetworkTimeout = 10000;

    public clsHTTPS(Context context, Handler handler, Long AdminID) {

        this.context = context;
        this.setHandler(handler);
        this.AdminID = AdminID;

    }

    public static void main(String[] args) throws Exception {


    }

    public static Uri buildURI(String url, Map<String, String> params, String startparam) {

        //  build url with parameters.
        if (startparam != null) url += "?" + startparam + TEST;
        Uri.Builder builder = Uri.parse(url).buildUpon();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }


        return builder.build();
    }

    public static Uri buildPathURI(String url, Map<String, String> params, String startparam) {

        //  build url with parameters.
        if (startparam != null) url += "?" + startparam + TEST;
        Uri.Builder builder = Uri.parse(url).buildUpon();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.appendPath(entry.getValue());
        }


        return builder.build();
    }

    private String resconnect0;
    public SSLSocketFactory sslSocketFactory = null;

    public String connect(final Map<String, String> params, final String startparam) {
        return connect(params, startparam, false);
    }

    public String connect(final Map<String, String> params, final String startparam, boolean re) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    if (clsHTTPS.this.AdminID != null) params.put(ADMIN_ID, "" + AdminID);
                    url = new URL(buildURI(MYURL, params, startparam).toString());
                    if (url.toString().length() > 4096) {
                        resconnect0 = ERROR + "HTTP-Header size exceeded!";
                    } else {
                        HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                        if (sslSocketFactory == null)
                            sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                        conn.setSSLSocketFactory(sslSocketFactory);

                        conn.setRequestMethod(POST);
                        //Map<String, List<String>> headers = conn.getHeaderFields();
                        conn.setDoOutput(true);
                        conn.setUseCaches(false);
                        // Print response
                        //SSLContext context = SSLContext.getInstance("TLS");
                        //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                        //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                        int status = conn.getResponseCode();
                        InputStream is;
                        boolean err = false;
                        if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                            is = conn.getErrorStream();
                            err = true;
                        } else {
                            is = (conn.getInputStream());
                        }
                        Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                        String result = s.hasNext() ? s.next() : "";
                        if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                        is.close();
                        conn.disconnect();

                        resconnect0 = result;
                    }// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resconnect0 = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resconnect0 = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resconnect0 = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resconnect0 == null) resconnect0 = context.getString(R.string.zZtkeineVerb);
        if (!re && (resconnect0.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET) && !startparam.equalsIgnoreCase(LOGON))) {
            relogin();
            return connect(params, startparam, true);
        }
        if (resconnect0.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }

        if (resconnect0.startsWith(ERROR1)) {
            lib.ShowException(TAG, context, new Exception(resconnect0), startparam, false);
        }
        return resconnect0;

    }

    public String getAnswersTalk(final Map<String, String> params, final QuestionsAdapter.Group g, final MainActivity main) {
        try {
            URL url;
            params.put(ACCESSKEY, accesskey);
            url = new URL(buildURI(MYURLAnswers, params, GET_ANSWERS).toString());
            if (main != null) {
                LimindoService s = main.mService;
                if (s != null) {
                    main.groups.put(g.getBenutzerFragenID(), g);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (main.groups.containsKey(g.getBenutzerFragenID())) {
                                main.groups.remove(g);
                                System.out.println(g.question.Frage + " " + g.question.ID + " " + params.toString()); // "getAnswersTalkHandler remove");

                            }
                        }
                    }, 5000);
                    boolean res = s.sendMessage(url.toString());
                    // if (Debug.isDebuggerConnected()) lib.ShowMessage(main,g.question.Frage + " " + g.question.ID, "getAnswersTalk");
                    if (res) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } else {
                    return ERROR_NO_SERVICE;
                }
            } else {
                return ERROR_MAIN_NULL;
            }
        } catch (Throwable ex) {
            Log.e("getAnswersTalk", null, ex);
            return ERROR1 + ex.getMessage();
        }
    }

    public String getFragenTalk(final Map<String, String> params, MainActivity main) {
        try {
            URL url;
            params.put(ACCESSKEY, accesskey);
            url = new URL(buildURI(MYURLAnswers, params, "findFragen").toString());
            if (main != null) {
                LimindoService s = main.mService;
                if (s != null) {
                    s.sendMessage(url.toString());
                    return TRUE;
                } else {
                    return ERROR_NO_SERVICE;
                }
            }
            return ERROR_MAIN_NULL;
        } catch (Throwable ex) {
            Log.e("getFragenTalk", null, ex);
            return ERROR1 + ex.getMessage();
        }
    }


    String resconnect1;

    public String getAnswers(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLAnswers, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resconnect1 = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resconnect1 = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resconnect1 = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resconnect1 = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resconnect1 == null) resconnect1 = context.getString(R.string.Error_zZtKeineVerb);
        if (!re && (resconnect1.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return getAnswers(params, true);
        }
        if (resconnect1.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resconnect1;

    }

    //String resconnect;
    String resquestion;

    public String Question(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLQuestion, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resquestion = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resquestion = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resquestion = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resquestion = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resquestion == null) resquestion = context.getString(R.string.zZtkeineVerb);
        if (!re && (resquestion.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return Question(params, true);
        }
        if (resquestion.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resquestion;

    }


    String resbewertungen;

    public String Bewertungen(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLROOT + "getBewertungen", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resbewertungen = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resbewertungen = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resbewertungen = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resbewertungen = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resbewertungen == null) resbewertungen = context.getString(R.string.zZtkeineVerb);
        if (!re && (resbewertungen.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return Bewertungen(params, true);
        }
        if (resbewertungen.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resbewertungen;

    }

    String resmelden;

    public String Melden(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLROOT + "Melden", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resmelden = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resmelden = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resmelden = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resmelden = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resmelden == null) resmelden = context.getString(R.string.zZtkeineVerb);
        if (!re && (resmelden.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return Bewertungen(params, true);
        }
        if (resmelden.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resmelden;

    }

    String ressperren;

    public String Sperren(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLROOT + "setGesperrt", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    ressperren = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    ressperren = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    ressperren = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            ressperren = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (ressperren == null) ressperren = context.getString(R.string.zZtkeineVerb);
        if (!re && (ressperren.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return Bewertungen(params, true);
        }
        if (ressperren.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return ressperren;

    }

    String resgetuser;

    public String getUser(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLROOT + GET_USER, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resgetuser = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resgetuser = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resgetuser = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resgetuser = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resgetuser == null) resgetuser = context.getString(R.string.zZtkeineVerb);
        if (!re && (resgetuser.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return getUser(params, true);
        }
        if (resgetuser.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resgetuser;

    }

    String resanswer;

    public String Answer(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLAnswer, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resanswer = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resanswer = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resanswer = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resanswer = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resanswer == null) resanswer = context.getString(R.string.zZtkeineVerb);
        if (!re && (resanswer.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return Answer(params, true);
        }
        if (resanswer.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resanswer;

    }

    String resgetquestions;

    public String getQuestions(final Map<String, String> params, boolean re) {
        resgetquestions = null;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLQuestions, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resgetquestions = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resgetquestions = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resgetquestions = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout + 60000);
            if (t.isAlive()) {
                t.interrupt();
                resgetquestions = ERROR + "Timeout!";
            }
        } catch (InterruptedException e) {
            resgetquestions = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resgetquestions == null) resgetquestions = context.getString(R.string.zZtkeineVerb);
        if (!re && (resgetquestions.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return getQuestions(params, true);
        }
        if (resgetquestions.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resgetquestions;

    }

    public JSONArray getCountQuestions(MainActivity main, int startGrad, int endGrad, int Entf, boolean archiviert, boolean geloescht, boolean ausgeblendet, String search, boolean beantwortet, boolean neueantworten, java.util.Date DatumNeu) throws Exception {
        if (main.user == null) return null;
        clsHTTPS https = this;
        HashMap<String, String> p = new java.util.HashMap<String, String>();
        String res = null;

        p.put(BENUTZER_ID, "" + main.user.getLong(Constants.id));
        p.put(RADIUS_KM, "" + Entf);
        if (main.locCoarse() != null) {
            p.put("Laengengrad", String.valueOf(main.locCoarse().getLongitude()));
            p.put("Breitengrad", String.valueOf(main.locCoarse().getLatitude()));
        }
        p.put(KONTAKTGRAD, "" + endGrad);
        p.put(ARCHIVIERT, "" + archiviert);
        p.put(GELOESCHT, "" + geloescht);
        p.put(AUS, "" + ausgeblendet);
        if (search != null && search.length() > 0) {
            search = StringEscapeUtils.escapeJava(search);
            search = search.replace("\\", "\\\\");
            search = search.replace("'", "\\'");
            p.put(SEARCH, search);
        }
        p.put("beantwortet", "" + beantwortet);
        p.put("neueantworten", "" + neueantworten);
        p.put("lang", main.Lang);
        if (DatumNeu != null) p.put("DatumNeu", fmtDateTime.format(DatumNeu));

        //for (int i = 0; i <= endGrad; i++) {
        //p.remove("Kontaktgrad");
        //p.put("Kontaktgrad", "" + endGrad);
        JSONArray json = null;
        int retries = 0;
        Long TimeTotal;
        long TimeStart = Calendar.getInstance().getTimeInMillis();
        while (json == null && retries <= 10) {
            res = https.getCountQuestions(p, false);//https.connect(p, "getquestionsp");
            TimeTotal = Calendar.getInstance().getTimeInMillis() - TimeStart;
            System.out.println(TIME_QUERY + TimeTotal);
            TimeStart = Calendar.getInstance().getTimeInMillis();
            if (res.startsWith(ERROR)) throw new Exception(res);
            try {
                json = new JSONArray(res);
            } catch (Throwable ex) {
                Log.e(FRAG_QUESTIONS_GET_QUESTIONS, res, ex);
                if (retries >= 10) throw ex;
                Thread.sleep(100);
            }
            retries++;
        }
        return json;
    }

    String resgetcountquestions;

    public String getCountQuestions(final Map<String, String> params, boolean re) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLCountQuestions, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) {
                        result = ERROR + status + ": " + result;
                    }
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resgetcountquestions = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resgetcountquestions = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resgetcountquestions = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resgetcountquestions = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resgetcountquestions == null)
            resgetcountquestions = context.getString(R.string.zZtkeineVerb);
        if (!re && (resgetcountquestions.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
            Log.e(TAG, null, new Error(resgetquestions));
            relogin();
            return getCountQuestions(params, true);
        }
        if (resgetcountquestions.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resgetcountquestions;

    }

    private String relogin() {
        if (context == null) return null;
        SharedPreferences prefs = context.getSharedPreferences("secret", MODE_PRIVATE);
        String AccessKey = prefs.getString("KEY", null);
        String Email = prefs.getString("Email", null);
        String Benutzername = prefs.getString("Benutzername", null);

        String res = null;

        res = Logon(AccessKey, Benutzername, Email, main != null ? main.onlineState : MainActivity.getOnlineState(context));
        return res;
    }

    public JSONObject question(Long ID, Long FrageID, Long BenutzerID, String EMail, String BenutzerName, String Frage, Date DatumStart, Date DatumEnde, int Kontaktgrad, int RadiusKM, Double Laengengrad, Double Breitengrad, Boolean oeffentlich, Integer Fehlergrad, Boolean emergency) throws Throwable {
        // TODO Auto-generated method stub
        Map<String, String> map = new HashMap<>();
        map.put("Frage", StringEscapeUtils.escapeJava(Frage));
        map.put(BENUTZER_ID, "" + BenutzerID);
        if (debugMode()) {
            if (context.getSharedPreferences("test", MODE_PRIVATE).getBoolean("test", false)) {
                if (fragContacts2.contactVOListLimindo != null) {
                    int randomNum = 0;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        randomNum = ThreadLocalRandom.current().nextInt(0, fragContacts2.contactVOListLimindo.size());
                    } else {
                        Random r = new Random();
                        randomNum = r.nextInt(fragContacts2.contactVOListLimindo.size());
                    }
                    ContactVO testVO = fragContacts2.contactVOListLimindo.get(randomNum);
                    map.put("BenutzerIDTest", "" + testVO.getUserID());
                    BenutzerName = testVO.getContactName();
                }
            }
        }

        map.put("BenutzerName", BenutzerName);
        map.put(E_MAIL, EMail);
        map.put("DatumStart", fmtDateTime.format(DatumStart));
        map.put("DatumEnde", fmtDateTime.format(DatumEnde));
        map.put(KONTAKTGRAD, "" + Kontaktgrad);
        map.put("RadiusKM", "" + RadiusKM);
        map.put("oeffentlich", oeffentlich.toString());
        if (Laengengrad != null) map.put("Laengengrad", "" + Laengengrad);
        if (Breitengrad != null) map.put("Breitengrad", "" + Breitengrad);
        if (Fehlergrad != null) map.put("Fehlergrad", "" + Fehlergrad);
        if (emergency != null) map.put("emergency", "" + emergency);
        map.put("ID", "" + ID);
        map.put("FrageID", "" + FrageID);
        String Lang;
        Lang = context.getApplicationContext().getSharedPreferences(LANG, Context.MODE_PRIVATE).getString(LANG, ALL);
        if (Lang.equalsIgnoreCase(SYSTEM) || Lang.equalsIgnoreCase(ALL))
            Lang = Locale.getDefault().toString();
        map.put("Locale", Lang);
        String res = Question(map, false);
        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }
        JSONObject BenutzerFrageID = new JSONObject(res);
        return BenutzerFrageID;
    }

    public JSONObject answer(Long BenutzerFragenID, Long BenutzerIDFrage, Boolean oeffentlich, Long BenutzerID, String EMail, String BenutzerName, String Antwort,
                             Long FrageID, int Kontaktgrad, Double Breitengrad, Double Laengengrad, Boolean reoeffentlich, boolean update) throws Throwable {
        return answer(-1l, 0l, BenutzerFragenID, BenutzerIDFrage, oeffentlich, BenutzerID, null, null, EMail, BenutzerName, Antwort, FrageID, Kontaktgrad, Breitengrad, Laengengrad, reoeffentlich, update);
    }

    public JSONObject answer(Long ParentFrageAnwortenID, long reAntwortID, Long BenutzerFragenID, Long BenutzerIDFrage, Boolean oeffentlich, Long BenutzerID, Long BenutzerIDSender, Long BenutzerIDEmpfaenger, String EMail, String BenutzerName, String Antwort, Long FrageID, int Kontaktgrad, Double Breitengrad, Double Laengengrad, Boolean reoeffentlich, boolean update) throws Throwable {

        // TODO Auto-generated method stub
        Map<String, String> map = new HashMap<>();
        map.put("FrageID", "" + FrageID);
        map.put("BenutzerFragenID", "" + BenutzerFragenID);
        map.put(KONTAKTGRAD, "" + Kontaktgrad);
        map.put(BENUTZER_ID, "" + BenutzerID);
        if (ParentFrageAnwortenID != null && ParentFrageAnwortenID > -1)
            map.put("ParentFrageAntwortenID", "" + ParentFrageAnwortenID);
        if (reAntwortID > 0) map.put("reAntwortID", "" + reAntwortID);
        if (BenutzerIDSender != null) map.put("BenutzerIDSender", "" + BenutzerIDSender);
        if (BenutzerIDEmpfaenger != null)
            map.put("BenutzerIDEmpfaenger", "" + BenutzerIDEmpfaenger);
        if (oeffentlich != null) map.put("oeffentlich", oeffentlich.toString());
        if (debugMode()) {
            if (context.getSharedPreferences("test", MODE_PRIVATE).getBoolean("test", false)) {
                if (fragContacts2.contactVOListLimindo != null) {
                    int randomNum = 0;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        randomNum = ThreadLocalRandom.current().nextInt(0, fragContacts2.contactVOListLimindo.size());
                    } else {
                        Random r = new Random();
                        randomNum = r.nextInt(fragContacts2.contactVOListLimindo.size());
                    }
                    ContactVO testVO = fragContacts2.contactVOListLimindo.get(randomNum);
                    map.put("BenutzerIDTest", "" + testVO.getUserID());
                    BenutzerName = testVO.getContactName();
                }
            }
        }
        map.put("BenutzerIDFrage", "" + BenutzerIDFrage);
        map.put(E_MAIL, EMail);
        map.put("BenutzerName", "" + BenutzerName);
        Antwort = StringEscapeUtils.escapeJava(Antwort);
        if (Antwort.length() > 1024) Antwort = Antwort.substring(0, 1023);
        map.put("Antwort", Antwort);
        if (Breitengrad != null) map.put("Breitengrad", "" + Breitengrad);
        if (Laengengrad != null) map.put("Laengengrad", "" + Laengengrad);
        if (reoeffentlich != null) map.put("reoeffentlich", "" + reoeffentlich);
        map.put("update", "" + update);
        String res = Answer(map, false);
        if (res.startsWith(ERROR1)) {
            res = res.replace("Error", "Fehler");
            throw new Exception(res);
        }
        return new JSONObject(res);
    }

    public JSONArray getUsers(long BenutzerID, int KontaktGrad, boolean getUnconfirmed) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(KONTAKTGRAD, "" + KontaktGrad);
        map.put(BENUTZER_ID, "" + BenutzerID);
        map.put(UNCONFIRMED, "" + getUnconfirmed);
        String res = connect(map, GETUSERS);
        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }
        if (res == null || res.length() == 0) {
            return new JSONArray();
        }
        return new JSONArray(res);

    }

    private String getUsersAsString(long BenutzerID, int KontaktGrad) throws Exception {
        StringBuilder res = new StringBuilder();
        JSONArray contacts = getUsers(BenutzerID, KontaktGrad, false);
        if (contacts.length() > 0) {
            for (int i = 0; i < contacts.length(); i++) {
                JSONObject user = contacts.getJSONObject(i);

                String BenutzerName = user.getString("benutzername");
                long ID = user.getLong(Constants.id);
                res.append(BenutzerName + " " + ID + "\n");

            }
        }
        return res.toString();
    }

    public static Object[] getSSLFactory(int jksFile, Context context) throws Exception {
        // Create key store
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        KeyManager[] kmfs = null;
        if (jksFile >= 0) {
            keyStore.load(context.getResources().openRawResource(R.raw.client), CLIENT_PWD.toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(
                    KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, CLIENT_PWD.toCharArray());
            kmfs = kmf.getKeyManagers();
        }

        // create trust store (validates the self-signed server!)
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(context.getResources().openRawResource(R.raw.client), CLIENT_PWD.toCharArray());
        TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        trustFactory.init(trustStore);
        //'Server Openssl?'

        SSLContext sslContext = SSLContext.getInstance("TLS");
        TrustManager[] trustManagers = trustFactory.getTrustManagers();
        sslContext.init(kmfs, trustManagers, null);
        //return new Tls12SocketFactory(sslContext.getSocketFactory());
        //SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslContext.getSocketFactory());
        //return NoSSLv3Factory;

        return new Object[]{sslContext.getSocketFactory(), trustManagers};
    }

    public int addContact(long userID, long id, boolean bestaetigt) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + userID);
        map.put("KontaktBenutzerID", "" + id);
        map.put("bestaetigt", "" + bestaetigt);

        String res = connect(map, "addcontact");

        if (res.startsWith(ERROR1)) {
            //lib.ShowMessage(context, res.replace(ERROR1, ""), context.getString(R.string.Error));
            throw new Exception(res);
        }
        int KontaktID = Integer.parseInt(res);
        return KontaktID;

    }

    public int addSperre(long userID, long gesperrterBenutzerID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + userID);
        map.put("GesperrterBenutzerID", "" + gesperrterBenutzerID);

        String res = connect(map, "addsperre");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }
        int SperreID = Integer.parseInt(res);
        return SperreID;

    }

    public boolean removeContact(long userid, long contactID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(KONTAKT_ID, "" + contactID);
        map.put(BENUTZER_ID, "" + userid);

        String res = connect(map, "removecontact");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }
        return Boolean.parseBoolean(res);
    }

    public boolean removeSperre(long id, int sperreID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("SperreID", "" + sperreID);
        map.put(BENUTZER_ID, "" + id);

        String res = connect(map, "removesperre");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }
        return Boolean.parseBoolean(res);

    }

    public boolean deleteQuestion(Long id, Long BenutzerID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("ID", "" + id);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = connect(map, "deletequestion");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return true;
    }

    public boolean archiveQuestion(Long id, Long BenutzerID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("ID", "" + id);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = connect(map, "archivequestion");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return true;
    }

    public boolean hideQuestion(Long id, Long BenutzerID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("ID", "" + id);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = connect(map, "hidequestion");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return true;
    }

    public boolean showQuestion(Long id, Long BenutzerID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("ID", "" + id);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = connect(map, "showquestion");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return true;
    }

    public int getKontaktgrad(int Kontaktgradmax, Long BenutzerID, Long KontaktBenutzerID) throws Exception {
        if (BenutzerID == KontaktBenutzerID) return 0;
        Map<String, String> map = new HashMap<>();
        map.put("Kontaktgradmax", "" + Kontaktgradmax);
        map.put(BENUTZER_ID, "" + BenutzerID);
        map.put("KontaktBenutzerID", "" + KontaktBenutzerID);
        String res = connect(map, "getkontaktgrad");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return Integer.parseInt(res);
    }

    public boolean deleteAnswer(Long id, Long benutzerID, Long BenutzerIDFrage, Long BenutzerFragenID, Long FrageID, int Kontaktgrad) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("ID", "" + id);
        map.put(BENUTZER_ID, "" + benutzerID);
        map.put("BenutzerIDFrage", "" + BenutzerIDFrage);
        map.put("BenutzerFragenID", "" + BenutzerFragenID);
        map.put(KONTAKTGRAD, "" + Kontaktgrad);
        map.put("FrageID", "" + FrageID);
        String res = connect(map, "deleteanswer");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return true;
    }

    public int valueAnswer(Long BenutzerID, Long FrageAntwortenID, Long benutzerIDAntwort, int Bewertung, Long BenutzerIDFrage, Long BenutzerFragenID, Long FrageID, int Kontaktgrad) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("FrageAntwortenID", "" + FrageAntwortenID);
        map.put(BENUTZER_ID, "" + BenutzerID);
        map.put("BenutzerIDAntwort", "" + benutzerIDAntwort);
        map.put("Bewertung", "" + Bewertung);
        map.put("BenutzerIDFrage", "" + BenutzerIDFrage);
        map.put("BenutzerFragenID", "" + BenutzerFragenID);
        map.put(KONTAKTGRAD, "" + Kontaktgrad);
        map.put("FrageID", "" + FrageID);
        String res = connect(map, "valueanswer");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return Integer.parseInt(res);
    }

    public int getAnswerValue(Long BenutzerID, Long FrageAntwortenID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("FrageAntwortenID", "" + FrageAntwortenID);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = connect(map, "getanswervalue");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return Integer.parseInt(res);
    }

    public JSONObject getUser(Long BenutzerID, Long getID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("ID", "" + getID);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = getUser(map, false);

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return new JSONObject(res);
    }

    public void reset() {
        this.sslSocketFactory = null;
    }

    public void logoff(long BenutzerID, boolean dontSaveLogoffDate) throws Throwable {
        Date dt = getServerDate(BenutzerID);
        //if (dt != null && context != null)
        //    context.getSharedPreferences("settings", MODE_PRIVATE).edit().putString("DateLastLogoff", clsHTTPS.fmtDateTime.format(dt)).commit();

        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + BenutzerID);
        map.put(DONTSAVELOGOFFDATE, "" + dontSaveLogoffDate);
        String res = connect(map, "logoff");


    }

    public void setOnline(long BenutzerID, MainActivity.OnlineState onlineState) throws Throwable {
        Date dt = getServerDate(BenutzerID);
        //if (dt != null && context != null)
        //    context.getSharedPreferences("settings", MODE_PRIVATE).edit().putString("DateLastLogoff", clsHTTPS.fmtDateTime.format(dt)).commit();

        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + BenutzerID);
        map.put(ONLINE_STATE, "" + onlineState.state);
        String res = connect(map, "online");
        context.getSharedPreferences("Main", MODE_PRIVATE).edit().putInt(ONLINE_STATE, onlineState.state).apply();
        if (main != null) {
            main.setOnlineStateQuestions(BenutzerID, onlineState);
        }

    }


    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public boolean InsertError(long BenutzerID, Throwable ex, String CodeLoc, String comment, Date date) {
        StringWriter errors = new StringWriter();
        if (ex != null) ex.printStackTrace(new PrintWriter(errors));
        String err = errors.toString();
        try {
            return InsertError(BenutzerID, err, CodeLoc, comment, date);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return false;
        }
    }

    public boolean InsertError(long BenutzerID, String err, String CodeLoc, String comment, Date date) throws Throwable {
        try {
            if (err.length() > 2048) err = err.substring(0, 2047);
            if (comment.length() > 1024) comment = comment.substring(0, 1023);
            Map<String, String> map = new HashMap<>();
            map.put(BENUTZER_ID, "" + BenutzerID);
            map.put("ex", "" + err);
            map.put("CodeLoc", "" + CodeLoc);
            map.put("comment", "" + comment);
            map.put("Date", clsHTTPS.fmtDateTime.format(date));
            String res = connect(map, "inserterror");

            if (res.startsWith(ERROR1)) {
                android.util.Log.e("InsertError", res);
                return false;
            }

            return true;
        } catch (Throwable ex) {
            ex.printStackTrace();
            return false;
        }

    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public Handler getHandler() {
        return handler;
    }

    String resserverdate;

    public Date getServerDate(long BenutzerID) {
        Date dt = null;
        int retries = 0;
        final Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + BenutzerID);

        while (dt == null && retries < 10) {
            String res;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        URL url;
                        map.put(ACCESSKEY, accesskey);
                        url = new URL(buildURI(MYURLGetServerTime, map, null).toString());
                        HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                        if (sslSocketFactory == null)
                            sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                        conn.setSSLSocketFactory(sslSocketFactory);

                        conn.setRequestMethod(POST);
                        //Map<String, List<String>> headers = conn.getHeaderFields();
                        conn.setDoOutput(true);
                        conn.setUseCaches(false);
                        // Print response
                        //SSLContext context = SSLContext.getInstance("TLS");
                        //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                        //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                        int status = conn.getResponseCode();
                        InputStream is;
                        boolean err = false;
                        if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                            is = conn.getErrorStream();
                            err = true;
                        } else {
                            is = (conn.getInputStream());
                        }
                        Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                        String result = s.hasNext() ? s.next() : "";
                        if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                        is.close();
                        conn.disconnect();
                        resserverdate = result;// sbline.toString();
                    } catch (SSLHandshakeException | SocketException e) {
                        //Log.e(TAG, null,,e)
                        resserverdate = ERROR + e.getMessage();
                    } catch (Throwable e) {
                        //Log.e(TAG, null,,e)
                        resserverdate = ERROR + e.getMessage();
                    }

                }
            });
            t.start();
            try {
                t.join(5000);
            } catch (InterruptedException e) {
                resserverdate = ERROR + e.getMessage();
                //Log.e(TAG, null,,e)

            }
            if (resserverdate == null) resserverdate = context.getString(R.string.zZtkeineVerb);
            if ((resserverdate.startsWith(ERROR_BENUTZER_NICHT_ANGEMELDET))) {
                Log.e(TAG, null, new Error(resgetquestions));
                relogin();
            }
            if (resserverdate.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
                Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
            res = resserverdate;

            if (res.startsWith(ERROR1)) {
                Log.e("InsertError", res);
                return null;
            }

            try {
                dt = fmtDateTime.parse(res);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            retries++;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return dt;


    }

    public JSONArray getQuestion(Long BenutzerID, Long benutzerFragenID) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("BenutzerFragenID", "" + benutzerFragenID);
        map.put(BENUTZER_ID, "" + BenutzerID);
        String res = connect(map, "getquestion");

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return new JSONArray(res);

    }

    public Integer getBewertungen(Long BenutzerID, Long BenutzerIDBewertungen, Date Datum) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + BenutzerID);
        map.put("BenutzerIDBewertungen", "" + BenutzerIDBewertungen);
        if (Datum != null) map.put("DateStart", "" + fmtDateTime.format(Datum));
        String res = Bewertungen(map, false);

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return Integer.parseInt(res);

    }

    public Long setGemeldet(Long BenutzerID, Long FrageAntwortenID, Long BenutzerFragenID, Boolean Value) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + BenutzerID);
        if (FrageAntwortenID != null) map.put("FrageAntwortenID", "" + FrageAntwortenID);
        if (BenutzerFragenID != null) map.put("BenutzerFragenID", "" + BenutzerFragenID);
        map.put("Value", "" + Value);
        String res = Melden(map, false);

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return Long.parseLong(res);

    }

    public Long setGesperrt(Long BenutzerID, Long AntwortID, Long FrageID, Long gesperrterBenutzerID, boolean Value) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(BENUTZER_ID, "" + BenutzerID);
        if (AntwortID != null) map.put("AntwortID", "" + AntwortID);
        if (FrageID != null) map.put("FrageID", "" + FrageID);
        if (gesperrterBenutzerID != null)
            map.put("gesperrterBenutzerID", "" + gesperrterBenutzerID);
        map.put("gesperrt", "" + Value);
        String res = Sperren(map, false);

        if (res.startsWith(ERROR1)) {
            throw new Exception(res);
        }

        return Long.parseLong(res);

    }

    String reslogon;

    public String Logon(final String mAccesskey, final String mBenutzername, final String mEmail, final MainActivity.OnlineState onlineState) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put("key", mAccesskey);
                    params.put("Benutzername", mBenutzername);
                    params.put("Email", mEmail);
                    params.put(ONLINE_STATE, "" + onlineState.state);
                    //params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLLogon, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    reslogon = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    reslogon = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    reslogon = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(60000 + maxNetworkTimeout);
        } catch (InterruptedException e) {
            reslogon = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (reslogon == null) reslogon = context.getString(R.string.zZtkeineVerb);

        if (reslogon.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return reslogon;
    }

    String reslogin;

    public String Login(final Map<String, String> params) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLLogin, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    reslogin = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    reslogin = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    reslogin = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            reslogin = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (reslogin == null) reslogin = context.getString(R.string.zZtkeineVerb);

        if (reslogin.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return reslogin;
    }

    String resunconfirmed;

    public String getUnconfirmed(final Long BenutzerID) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLGetUnconfirmed, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resunconfirmed = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resunconfirmed = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resunconfirmed = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resunconfirmed = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resunconfirmed == null) resunconfirmed = context.getString(R.string.zZtkeineVerb);

        if (resunconfirmed.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resunconfirmed;
    }

    String resinvited;

    public String getInvited(final Long BenutzerID, final String EMail, final String Telefonnummer) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    if (EMail != null) params.put(E_MAIL, EMail);
                    if (Telefonnummer != null && Telefonnummer.length() > 5)
                        params.put(TELEFONNUMMER, Telefonnummer);
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLROOT + GET_INVITED, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resinvited = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resinvited = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resinvited = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resinvited = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resinvited == null) resinvited = context.getString(R.string.zZtkeineVerb);

        if (resinvited.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resinvited;
    }

    String resdeleteInvited;

    public String deleteInvited(final Long BenutzerID, final long ID) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put("ID", "" + ID);
                    params.put(ACCESSKEY, accesskey);
                    url = new URL(buildURI(MYURLROOT + DELETE_INVITED, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resdeleteInvited = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resdeleteInvited = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resdeleteInvited = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resdeleteInvited = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resdeleteInvited == null) resdeleteInvited = context.getString(R.string.zZtkeineVerb);

        if (resdeleteInvited.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resdeleteInvited;
    }

    String resconfirm;

    public String confirmContact(final long BenutzerID, final long KontaktID) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);
                    params.put(KONTAKT_ID, "" + KontaktID);
                    url = new URL(buildURI(MYURLConfirm, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resconfirm = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resconfirm = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resconfirm = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resconfirm = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resconfirm == null) resconfirm = context.getString(R.string.zZtkeineVerb);

        if (resconfirm.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resconfirm;
    }


    String resInvite;

    public String Invite(final long BenutzerID, final String EMail, final String Telefonnummer) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);
                    params.put(E_MAIL, EMail);
                    params.put(TELEFONNUMMER, Telefonnummer);
                    url = new URL(buildURI(MYURLROOT + INVITE, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resInvite = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resInvite = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resInvite = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resInvite = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resInvite == null) resInvite = context.getString(R.string.zZtkeineVerb);

        if (resInvite.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resInvite;
    }

    String resTaetigk;

    public String setTaetigkeitsschwerpunkt(final long BenutzerID, final String Taetigkeitsschwerpunkt) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);
                    params.put("Taetigkeitsschwerpunkt", Taetigkeitsschwerpunkt);
                    url = new URL(buildURI(MYURLROOT + "setTaetigkeitsschwerpunkt", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resTaetigk = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resTaetigk = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resTaetigk = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resTaetigk = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resTaetigk == null) resTaetigk = context.getString(R.string.zZtkeineVerb);

        if (resTaetigk.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resTaetigk;
    }


    String rescreatenewpw;

    public String createnewpw(final String EMail, final String Benutzername, final String tokenResult) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(E_MAIL, EMail);
                    params.put("Benutzername", Benutzername);
                    params.put("Token", tokenResult);
                    url = new URL(buildURI(MYURLCreateNewPW, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    rescreatenewpw = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    rescreatenewpw = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    rescreatenewpw = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            rescreatenewpw = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (rescreatenewpw == null) rescreatenewpw = context.getString(R.string.zZtkeineVerb);

        if (rescreatenewpw.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return rescreatenewpw;

    }

    String reschangepw;

    public String changepw(final long BenutzerID, final String oldPassword, final String password) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put("AltesKennwort", oldPassword);
                    params.put("NeuesKennwort", password);
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);

                    url = new URL(buildURI(MYURLChangePW, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    reschangepw = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    reschangepw = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    reschangepw = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            reschangepw = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (reschangepw == null) reschangepw = context.getString(R.string.zZtkeineVerb);

        if (reschangepw.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return reschangepw;

    }

    String resdelete;

    public String deleteAccount(final boolean delete, final boolean activate, final Long BenutzerID, final Long foreignID) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put("Delete", "" + delete);
                    params.put("Activate", "" + activate);
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put("foreign_id", "" + foreignID);
                    params.put(ACCESSKEY, accesskey);

                    url = new URL(buildURI(MYURLDel_Deact_Account, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resdelete = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resdelete = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resdelete = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resdelete = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resdelete == null) resdelete = context.getString(R.string.zZtkeineVerb);

        if (resdelete.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resdelete;


    }

    public String getAnswers(Long benutzerID, int kontaktgrad, Long frageID, Long benutzerFragenid) {
        HashMap<String, String> p = new HashMap<String, String>();
        p.put(BENUTZER_ID, "" + benutzerID);
        p.put(KONTAKTGRAD, "" + kontaktgrad);
        p.put(FRAGE_ID, "" + frageID);
        p.put(BENUTZER_FRAGEN_ID, "" + benutzerFragenid);

        //for (int i = 0; i <= Folder.Kontaktgrad; i++)
        //{
        //p.remove("Kontakgrad");
        //p.put("Kontaktgrad", "" + i);
        return getAnswers(p, false);


    }


    public static class UploadTask extends AsyncTask<String, Void, String> {

        private final Context context;
        private final InputStream is;
        private final String FileName;
        private final Long BenutzerID;
        private final Handler handler;
        private final clsHTTPS clsHTTPS;
        private final dlgChat recallDlgChat;
        private SSLSocketFactory sslSocketFactory;
        private final String accesskey;

        public UploadTask(Context context,
                          final InputStream is,
                          final String FileName,
                          final Long BenutzerID,
                          final Handler handler,
                          final SSLSocketFactory sslSocketFactory,
                          final String accesskey,
                          final clsHTTPS clsHTTPS,
                          final dlgChat recallDlgChat) {
            this.context = context;
            dialog = new ProgressDialog(context);
            this.is = is;
            this.FileName = FileName;
            this.BenutzerID = BenutzerID;
            this.handler = handler;
            this.sslSocketFactory = sslSocketFactory;
            this.accesskey = accesskey;
            this.clsHTTPS = clsHTTPS;
            this.recallDlgChat = recallDlgChat;
        }

        /**
         * progress dialog to show user that the backup is processing.
         */
        private ProgressDialog dialog;

        /**
         * application context.
         */

        protected void onPreExecute() {
            this.dialog.setMessage(context.getString(R.string.uploading));
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(String resUpload) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }

            if (resUpload == null) resUpload = context.getString(R.string.zZtkeineVerb);

            if (resUpload != null && handler != null && context != null) {
                Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                //bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
                bundle.putString(Constants.TOAST, resUpload);
                msg.setData(bundle);
                handler.sendMessage(msg);
                msg = handler.obtainMessage(Constants.MESSAGE_UPLOADCOMPLETE);
                handler.sendMessage(msg);
            }
            if (resUpload.equalsIgnoreCase("SUCCESS") && recallDlgChat != null) {
                try {
                    recallDlgChat.sendAttachmentMessage();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            clsHTTPS.sslSocketFactory = sslSocketFactory;
            //return resUpload;


        }

        protected String doInBackground(final String... args) {
            String resUpload;
            resUpload = uploadFile(FileName, BenutzerID, accesskey);
            return resUpload;
        }

        private String uploadFile(String FileName, Long BenutzerID, String accesskey) {
            String resUpload = null;
            try {
                URL url;
                Map<String, String> params = new HashMap<>();
                params.put("FileName", "" + FileName);
                params.put(BENUTZER_ID, "" + BenutzerID);
                params.put(ACCESSKEY, accesskey);

                url = new URL(buildURI(MYURLROOT + "upload", params, null).toString());
                HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                if (sslSocketFactory == null)
                    sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                conn.setSSLSocketFactory(sslSocketFactory);

                conn.setRequestMethod(POST);
                //Map<String, List<String>> headers = conn.getHeaderFields();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);

                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Cache-Control", "no-cache");
                //conn.setRequestProperty(
                //        "Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty(
                        "Content-Type", "application/octet-stream");

                conn.setRequestProperty("Accept", "*/*");


                // Start content wrapper:

                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());

                //request.writeBytes(twoHyphens + boundary + crlf);
                //request.writeBytes("Content-Disposition: form-data; name=\"" +
                //        attachmentName + "\";filename=\"" +
                //        attachmentFileName + "\"" + crlf);
                //request.writeBytes(crlf);

                // Convert Bitmap to ByteBuffer:

                int nRead = -1;
                byte[] data = new byte[4096];

                while ((nRead = is.read(data, 0, data.length)) != -1) {
                    request.write(data, 0, nRead);
                }

                //request.writeBytes(crlf);
                //request.writeBytes(twoHyphens + boundary +
                //        twoHyphens + crlf);

                //Flush output buffer:

                request.flush();
                request.close();
                is.close();


// Print response
                //SSLContext context = SSLContext.getInstance("TLS");
                //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                int status = conn.getResponseCode();
                InputStream iis;
                boolean err = false;
                if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                    iis = conn.getErrorStream();
                    err = true;
                } else {
                    iis = (conn.getInputStream());
                }
                Scanner s = new Scanner(iis, UTF_8).useDelimiter(Delimiter_A);
                String result = s.hasNext() ? s.next() : "";
                if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                iis.close();
                conn.disconnect();
                resUpload = result;// sbline.toString();
            } catch (SSLHandshakeException | SocketException e) {
                //Log.e(TAG, null,,e)
                resUpload = ERROR + e.getMessage();
            } catch (Throwable e) {
                //Log.e(TAG, null,,e)
                resUpload = ERROR + e.getMessage();
            }
            return resUpload;
        }

    }


    public String upload(final Context context, final InputStream is, final String FileName, final Long BenutzerID, dlgChat recallDlgChat) throws Throwable {
        new UploadTask(context, is, FileName, BenutzerID, handler, sslSocketFactory, accesskey, this, recallDlgChat).execute("");

        return "";
    }

    String resDownloadImage;
    Bitmap DownImage = null;
    public ConcurrentHashMap<String, Bitmap> hashImages = new ConcurrentHashMap<>();

    public Bitmap downloadProfileImage(final Context context, final Long id, final Long BenutzerID, String BenutzerName) throws Throwable {
        String fname = ".JPG";
        String FileName = "Image" + id + fname;
        DownImage = downloadImage(context, FileName, BenutzerID);
        if (DownImage == null && BenutzerName != null) {
            final Resources res = context.getResources();
            final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);

            final LetterTileProvider tileProvider = new LetterTileProvider(context);
            final Bitmap letterTile = tileProvider.getLetterTile(BenutzerName, BenutzerName, tileSize, tileSize, true);
            DownImage = letterTile;
            if (DownImage != null) hashImages.put(FileName, DownImage);
        }

        return DownImage;
    }

    public Bitmap downloadImage(final Context context, final String FileName, final Long BenutzerID) throws Exception {
        if (hashImages.containsKey(FileName)) return hashImages.get(FileName);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                HttpsURLConnection conn;
                try {
                    DownImage = null;
                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put("FileName", "" + FileName);
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);

                    url = new URL(buildURI(MYURLROOT + "download", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(GET);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    //conn.setDoOutput(true);
                    //conn.setDoInput(true);
                    conn.setUseCaches(false);

                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    //conn.setRequestProperty(
                    //        "Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty(
                            "Content-Type", "application/octet-stream");

                    conn.setRequestProperty("Accept", "*/*");

                    // InputStream inputStream0 = conn.getInputStream();

                    // Start content wrapper:

                    int responseCode = conn.getResponseCode();

                    // always check HTTP response code first
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String fileName = "";
                        String disposition = conn.getHeaderField("Content-Disposition");
                        String contentType = conn.getContentType();
                        int contentLength = conn.getContentLength();

                        if (disposition != null) {
                            // extracts file name from header field
                            int index = disposition.indexOf("filename=");
                            if (index > 0) {
                                fileName = disposition.substring(index + 9,
                                        disposition.length());
                            }
                        } else {
                            // extracts file name from URL
                            //fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                            // fileURL.length());
                        }

                        System.out.println("Content-Type = " + contentType);
                        System.out.println("Content-Disposition = " + disposition);
                        System.out.println("Content-Length = " + contentLength);
                        System.out.println("fileName = " + fileName);

                        // opens input stream from the HTTP connection
                        InputStream inputStream = conn.getInputStream();

                        DownImage = BitmapFactory.decodeStream(inputStream);
                        inputStream.close();
                        conn.disconnect();
                        resDownloadImage = "";
// Print response
                        //SSLContext context = SSLContext.getInstance("TLS");
                        //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                        //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    } else {
                        int status = responseCode;
                        InputStream iis;
                        boolean err = false;
                        if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                            iis = conn.getErrorStream();
                            err = true;
                        } else {
                            iis = (conn.getInputStream());
                        }
                        Scanner s = new Scanner(iis, UTF_8).useDelimiter(Delimiter_A);
                        String result = s.hasNext() ? s.next() : "";
                        if (err) result = ERROR + status + ": " + result;
                        iis.close();
                        conn.disconnect();
                        resDownloadImage = result;// sbline.toString();
                    }
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resDownloadImage = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resDownloadImage = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resDownloadImage = ERROR + e.getMessage();
        }
        if (resDownloadImage == null)
            resDownloadImage = context.getString(R.string.zZtkeineVerb);

        if (resDownloadImage.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        if (resDownloadImage.startsWith(ERROR)) throw new

                Exception(resDownloadImage);
        if (DownImage != null) hashImages.put(FileName, DownImage);
        return DownImage;
    }

    private String resDownload;

    public File downloadFile(final Context context, final String FileName, final Long BenutzerID, boolean blnRun) throws Exception {
        File file = null;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                HttpsURLConnection conn;
                try {
                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put("FileName", "" + FileName);
                    params.put(BENUTZER_ID, "" + BenutzerID);
                    params.put(ACCESSKEY, accesskey);

                    url = new URL(buildURI(MYURLROOT + "download", params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(GET);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    //conn.setDoOutput(true);
                    //conn.setDoInput(true);
                    conn.setUseCaches(false);

                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    //conn.setRequestProperty(
                    //        "Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty(
                            "Content-Type", "application/octet-stream");

                    conn.setRequestProperty("Accept", "*/*");

                    // InputStream inputStream0 = conn.getInputStream();

                    // Start content wrapper:

                    int responseCode = conn.getResponseCode();

                    // always check HTTP response code first
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String fileName = "";
                        String disposition = conn.getHeaderField("Content-Disposition");
                        String contentType = conn.getContentType();
                        int contentLength = conn.getContentLength();

                        if (disposition != null) {
                            // extracts file name from header field
                            int index = disposition.indexOf("filename=");
                            if (index > 0) {
                                fileName = disposition.substring(index + 9,
                                        disposition.length());
                            }
                        } else {
                            // extracts file name from URL
                            //fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                            // fileURL.length());
                        }

                        System.out.println("Content-Type = " + contentType);
                        System.out.println("Content-Disposition = " + disposition);
                        System.out.println("Content-Length = " + contentLength);
                        System.out.println("fileName = " + fileName);

                        // opens input stream from the HTTP connection
                        InputStream inputStream = conn.getInputStream();
                        File FilesDir = context.getFilesDir();
                        File PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS); //Path.combine(FilesDir.getPath(), "f");
                        String strPATH = Path.combine(PATH.getPath(), FileName);
                        File file = new File(strPATH);
                        if (!file.exists()) {
                            OutputStream output = new FileOutputStream(strPATH);

                            byte data[] = new byte[1024];

                            long total = 0;

                            int count;
                            while ((count = inputStream.read(data)) != -1) {
                                total += count;
                                // publishing the progress....
                                // After this onProgressUpdate will be called

                                // writing data to file
                                output.write(data, 0, count);
                            }

                            // flushing output
                            output.flush();

                            // closing streams
                            output.close();


                            inputStream.close();
                            conn.disconnect();
                        }
                        resDownload = strPATH;

// Print response
                        //SSLContext context = SSLContext.getInstance("TLS");
                        //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                        //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    } else {
                        int status = responseCode;
                        InputStream iis;
                        boolean err = false;
                        if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                            iis = conn.getErrorStream();
                            err = true;
                        } else {
                            iis = (conn.getInputStream());
                        }
                        Scanner s = new Scanner(iis, UTF_8).useDelimiter(Delimiter_A);
                        String result = s.hasNext() ? s.next() : "";
                        if (err) result = ERROR + status + ": " + result;
                        iis.close();
                        conn.disconnect();
                        resDownload = result;// sbline.toString();
                    }
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resDownload = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resDownload = ERROR + e.getMessage();
                }

            }
        });
        if (blnRun) {
            t.run();
        } else {
            t.start();
            try {
                t.join(maxNetworkTimeout);
            } catch (InterruptedException e) {
                resDownload = ERROR + e.getMessage();
            }
        }
        if (resDownload == null) resDownload = context.getString(R.string.zZtkeineVerb);

        if (resDownload.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        if (resDownload.startsWith(ERROR)) throw new Exception(resDownload);
        return new File(resDownload);

    }

    String resgetusernames;

    public String getUsernames(final String EMail, final String tokenResult) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url;
                    Map<String, String> params = new HashMap<>();
                    params.put(E_MAIL, EMail);
                    params.put("Token", tokenResult);
                    url = new URL(buildURI(MYURLGetUsernames, params, null).toString());
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    if (sslSocketFactory == null)
                        sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, context)[0];
                    conn.setSSLSocketFactory(sslSocketFactory);

                    conn.setRequestMethod(POST);
                    //Map<String, List<String>> headers = conn.getHeaderFields();
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    // Print response
                    //SSLContext context = SSLContext.getInstance("TLS");
                    //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                    //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                    int status = conn.getResponseCode();
                    InputStream is;
                    boolean err = false;
                    if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                        is = conn.getErrorStream();
                        err = true;
                    } else {
                        is = (conn.getInputStream());
                    }
                    Scanner s = new Scanner(is, UTF_8).useDelimiter(Delimiter_A);
                    String result = s.hasNext() ? s.next() : "";
                    if (err) result = ERROR + status + ": " + result;
                    /*
                    StringBuilder sbline = new StringBuilder();
                    int i;
                    while((i = ir.read())!=-1) {

                        // int to character
                        char c = (char)i;
                        sbline.append(c);
                        // print char
                        //System.out.println("Character Read: "+c);
                    }
                    ir.close();
                    */
                    is.close();
                    conn.disconnect();
                    resgetusernames = result;// sbline.toString();
                } catch (SSLHandshakeException | SocketException e) {
                    //Log.e(TAG, null,,e)
                    resgetusernames = ERROR + e.getMessage();
                } catch (Throwable e) {
                    //Log.e(TAG, null,,e)
                    resgetusernames = ERROR + e.getMessage();
                }

            }
        });
        t.start();
        try {
            t.join(maxNetworkTimeout);
        } catch (InterruptedException e) {
            resgetusernames = ERROR + e.getMessage();
            //Log.e(TAG, null,,e)

        }
        if (resgetusernames == null) resgetusernames = context.getString(R.string.zZtkeineVerb);

        if (resgetusernames.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && handler != null && context != null) {
            Message msg = handler.obtainMessage(Constants.MESSAGE_TOAST);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.TOAST, context.getString(R.string.noroutetohost));
            msg.setData(bundle);
            handler.sendMessage(msg);
        }
        return resgetusernames;

    }

    public String getFragen2Talk(HashMap<String, String> params, MainActivity main) {
        try {
            URL url;
            params.put(ACCESSKEY, accesskey);
            url = new URL(buildURI(MYURLAnswers, params, "findFragen2").toString());
            if (main != null) {
                LimindoService s = main.mService;
                if (s != null) {
                    s.sendMessage(url.toString());
                    return TRUE;
                } else {
                    return ERROR_NO_SERVICE;
                }
            }
            return ERROR_MAIN_NULL;
        } catch (Throwable ex) {
            Log.e("getFragen2Talk", null, ex);
            return ERROR1 + ex.getMessage();
        }
    }

    public String sendMessage(Long BenutzerIDEmpfaenger, String Message, UUID uuid, boolean read, MainActivity main) throws JSONException {
        HashMap<String, String> p = new java.util.HashMap<String, String>();
        if (main == null || main.user == null) return null;
        String Name = main.user.getString(BENUTZERNAME);
        long BenutzerID = main.user.getLong(Constants.id);
        if (Message.startsWith("selfxx")) BenutzerIDEmpfaenger = BenutzerID;
        p.put(BENUTZERIDEMPFAENGER, "" + BenutzerIDEmpfaenger);
        p.put(BENUTZER_ID, "" + BenutzerID);
        p.put(BENUTZERNAME, Name);
        p.put("message", "" + Message);
        p.put("uuid", uuid.toString());
        p.put("read", String.valueOf(read));

        return _sendMessage(p, main);
    }


    private String _sendMessage(HashMap<String, String> params, MainActivity main) {
        try {
            URL url;
            params.put(ACCESSKEY, accesskey);
            url = new URL(buildURI(MYURLAnswers, params, SEND_MESSAGE).toString());
            if (main != null) {
                LimindoService s = main.mService;
                if (s != null) {
                    if (s.sendMessage(url.toString())) {
                        return TRUE;
                    } else {
                        return ERROR_NOT_CONNECTED;
                    }
                } else {
                    return ERROR_NO_SERVICE;
                }
            }
            return ERROR_MAIN_NULL;
        } catch (Throwable ex) {
            Log.e(SEND_MESSAGE, null, ex);
            return ERROR1 + ex.getMessage();
        }
    }


    public static class NullHostNameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String s, SSLSession sslSession) {
            return s.equalsIgnoreCase(MYDOMAIN);
        }
    }


}

