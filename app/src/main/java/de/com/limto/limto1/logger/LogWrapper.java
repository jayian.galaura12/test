/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.com.limto.limto1.logger;


import android.database.Cursor;

import java.util.Calendar;

import de.com.limto.limto1.Errors.dbSqlite;
import de.com.limto.limto1.LimindoService;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.clsHTTPS;
import de.com.limto.limto1.lib.lib;

import static de.com.limto.limto1.logger.Log.ERROR;

/**
 * Helper class which wraps Android's native Log utility in the Logger interface.  This way
 * normal DDMS output can be one of the many targets receiving and outputting logs simultaneously.
 */
public class LogWrapper implements LogNode {

    private static final String TAG = "LogWrapper";
    private clsHTTPS mdb;
    private dbSqlite mdbsqlite;
    // For piping:  The next node to receive Log data after this one has done its work.
    private LogNode mNext;
    private long benutzerID;

    public LogWrapper(long BenutzerID, clsHTTPS db, dbSqlite dbsqlite) {

        mdb = db;
        mdbsqlite = dbsqlite;
        benutzerID = BenutzerID;
        try {
            if (db != null && BenutzerID > -1) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Cursor cursor = mdbsqlite.query("SELECT * FROM Errors WHERE exported = 0");
                            while (cursor.moveToNext()) {
                                String CodeLoc = cursor.getString(cursor.getColumnIndex("CodeLoc"));
                                String comment = cursor.getString(cursor.getColumnIndex("comment"));
                                String search = CodeLoc + comment;
                                //if (search.contains(MainActivity.MAINACTIVITY_UN_CAUGHT) || search.contains(LimindoService.UNCAUGHT_EXCEPTION_HANDLER)) {
                                mdb.InsertError(benutzerID, cursor.getString(cursor.getColumnIndex("ex")), CodeLoc, comment, clsHTTPS.fmtDateTime.parse(cursor.getString(cursor.getColumnIndex("date"))));
                                mdbsqlite.DataBase.execSQL("UPDATE Errors SET exported = 1 WHERE ID = " + cursor.getInt(cursor.getColumnIndex("ID")));
                                //}
                            }
                            ;
                            cursor.close();
                            cursor = mdbsqlite.query("SELECT COUNT(*) FROM Errors");
                            if (cursor.moveToFirst()) {
                                int count = cursor.getInt(0);
                                if (count > 5000) {
                                    mdbsqlite.DataBase.execSQL("Delete from Errors WHERE ID IN (SELECT ID FROM Errors LIMIT " + (count - 4000) + ")");
                                }
                            }
                            cursor.close();
                        } catch (Throwable e) {
                            e.printStackTrace();
                            android.util.Log.e(TAG, "create", e);
                        }
                    }
                }).start();
            }
        } catch (Throwable e) {
            e.printStackTrace();
            android.util.Log.e(TAG, "create", e);
        }
    }

    public String getErrors(int timeMinutes) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MINUTE, -timeMinutes);
            String sql = "SELECT * FROM Errors WHERE date > '" + clsHTTPS.fmtDateTime.format(cal.getTime()) + "'";
            Cursor cursor = mdbsqlite.query(sql);
            StringBuilder sb = new StringBuilder();
            while (cursor.moveToNext()) {
                if (!cursor.isClosed()) {
                    for (int i = 0; i < cursor.getColumnCount(); i++) {
                        sb.append(cursor.getColumnName(i) + ": ");
                        String s = cursor.getString(i);
                        if (s.length() > 100) s = s.substring(0, 99);
                        sb.append(s);
                        sb.append(System.getProperty("line.separator"));
                    }
                    sb.append(System.getProperty("line.separator"));
                }
            }
            ;
            cursor.close();
            return sb.toString();

        } catch (Throwable e) {
            e.printStackTrace();
            android.util.Log.e(TAG, "create", e);
        }
        return null;
    }

    /**
     * Returns the next LogNode in the linked list.
     */
    public LogNode getNext() {
        return mNext;
    }

    /**
     * Sets the LogNode data will be sent to..
     */
    public void setNext(LogNode node) {
        mNext = node;
    }

    /**
     * Prints data out to the console using Android's native log mechanism.
     *
     * @param priority Log level of the data being logged.  Verbose, Error, etc.
     * @param tag Tag for for the log data.  Can be used to organize log statements.
     * @param msg The actual message to be logged. The actual message to be logged.
     * @param tr If an exception was thrown, this can be sent along for the logging facilities
     * to extract and print useful information.
     */
    String gStatus = "";
    StringBuilder gStatusCache = new StringBuilder();
    public String lastError = null;

    @Override
    public void println(int priority, String tag, String msg, Throwable tr) {
        try {
            // There actually are log methods that don't take a msg parameter.  For now,
            // if that's the case, just convert null to the empty string and move on.
            String useMsg = msg;
            if (useMsg == null) {
                useMsg = "";
            }
            long res = 0;

            WriteMessageToDBSqlite(tr, priority, tag, msg, useMsg, res);


            setgStatusAndAppendToStatusCache(priority, tag, msg);


            // If an exeption was provided, convert that exception to a usable string and attach
            // it to the end of the msg method.
            if (tr != null) {
                msg += "\n" + android.util.Log.getStackTraceString(tr);
            }

            // This is functionally identical to Log.x(tag, useMsg);
            // For instance, if priority were Log.VERBOSE, this would be the same as Log.v(tag, useMsg)
            android.util.Log.println(priority, tag, useMsg);


            // If this isn't the last node in the chain, move things along.
            if (mNext != null) {
                mNext.println(priority, tag, msg, tr);
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            android.util.Log.e(tag, msg, ex);
        }
    }

    private void WriteMessageToDBSqlite(Throwable tr, int priority, String tag, String msg, String useMsg, long res) {
        if (mdbsqlite != null) {
            if (priority >= ERROR) try {
                mdbsqlite.InsertError(tr, tag, useMsg + "\ngStatus: " + gStatus + gStatusCache.toString(), res);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                android.util.Log.e(tag, msg, throwable);
            }
            if (msg == null && tr != null) msg = tr.getMessage();
            if (mdbsqlite.mContext instanceof MainActivity) {
                if (msg != null && msg.length() < 40 && !(msg.contains("1234")))
                    lib.ShowMessageDebug(mdbsqlite.mContext, gStatus, "LogWrapper");
            }
            lastError = mdbsqlite.lastError;
        }
    }

    private void setgStatusAndAppendToStatusCache(int priority, String tag, String msg) {
        gStatus = priority + ":" + tag + ": " + msg;
        gStatusCache.append("\n" + gStatus);

        do {
            if (gStatusCache.length() < 800) break;
            int found = gStatusCache.indexOf("\n");
            if (found < 0) break;
            gStatusCache.delete(0, found + 1);
        } while (gStatusCache.length() > 800);

    }

    public long getBenutzerID() {
        return benutzerID;
    }

    public void setBenutzerID(long benutzerID) {
        this.benutzerID = benutzerID;
    }

    public void setClsHTTPS(clsHTTPS clsHTTPS) {
        this.mdb = clsHTTPS;
    }

    public clsHTTPS getClsHTTPS() {
        return mdb;
    }

    public void setDBSQLite(dbSqlite dbsqLite) {
        this.mdbsqlite = dbsqLite;
    }

    public dbSqlite getDBSQLite() {
        return mdbsqlite;
    }
}
