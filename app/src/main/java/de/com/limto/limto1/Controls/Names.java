package de.com.limto.limto1.Controls;

import static de.com.limto.limto1.fragQuestion.VOTE;

public class Names {
    public boolean vote;
    private String name;
    public String getName()
    {
        return  name;
    }
    public Names(String frage) {
        name = frage;
        if (frage.startsWith(VOTE))
        {
            name = frage.substring(VOTE.length());
            this.vote = true;
        }
    }
}
