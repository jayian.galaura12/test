package de.com.limto.limto1;

import androidx.appcompat.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

public class dlgContacts extends DialogFragment {

    //public final static int fragID = 3;
    private static final String TAG = "fragContacts";
    private static final String ASC = " ASC";
    private static final String EQUAL = "equal";
    private static boolean listLoaded = false;
    private static List<ContactVO> contactVOListAndroid;
    private static ArrayList<ContactVO> ArrCopied = new ArrayList<ContactVO>();
    public MainActivity _main;
    RecyclerView rvContacts;
    ProgressBar pb;
    AllContactsAdapter contactAdapter;
    private boolean isvisible;
    private Button btnOK;
    private Button btnCancel;
    private static List<ContactVO> contactVOListAndroidAll;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (_main == null) _main = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dlgcontacts, container, false);

        rvContacts = (RecyclerView) v.findViewById(R.id.rvContacts);
        final ProgressBar pb = (ProgressBar) v.findViewById(R.id.pb);
        this.pb = pb;
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        btnOK = (Button) view.findViewById(R.id.btnOK);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listLoaded && contactVOListAndroid != null) {
                    for (ContactVO vo : contactVOListAndroid) {
                        if (vo.getChecked()) {
                            final long ID = vo.getUserID();
                            if (_main.user == null) return;
                            long UserID = 0;
                            try {
                                UserID = _main.user.getLong(Constants.id);
                                long KontaktID = _main.clsHTTPS.addContact(UserID, ID, false);
                                vo.setContactImage(android.R.drawable.ic_menu_call);
                                vo.setContactID(KontaktID);
                            } catch (Throwable ex) {
                                ex.printStackTrace();
                                lib.ShowException(TAG, getContext(),ex, null, false);
                                //return;
                            }
                        }
                    }
                    dismiss();
                    try {
                        dlgInviteAllContacts();
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                try {
                    dlgInviteAllContacts();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
        new ListAsyncTask(this).execute();
    }

    private void dlgInviteAllContacts() throws Throwable {
        lib.ShowMessageYesNo(getContext(), getString(R.string.invite_all_contacts_by_email), getString(R.string.invite), false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                inviteAllContacts();
            }
        }, null, null);
    }

    private void inviteAllContacts() {

       final List<ContactVO> l = contactVOListAndroidAll;
        if (l != null && l.size() > 0) {
            ArrayList<String> emails = new ArrayList<>();
            for (final ContactVO c : l) {
                if (!c.isLimindoContact() && !c.isLimindoUser() && c.getContactEmail() != null && c.getContactEmail().length() > 5 && lib.isValidEmailAddress(c.getContactEmail())) {
                    emails.add(c.getContactEmail());
                }
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (final ContactVO c : l) {
                        if (!c.isLimindoContact() && !c.isLimindoUser() && c.getContactEmail() != null && c.getContactEmail().length() > 5 && lib.isValidEmailAddress(c.getContactEmail())) {
                            try {
                                _main.clsHTTPS.Invite(_main.getBenutzerID(), c.getContactEmail(), lib.formatPhoneNumber(c.getContactNumber()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }).start();

            if (emails.size() > 0) {
                try {
                    lib.sendMails(_main, emails);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }


            }
        }

    }


    private void setAdapter(List<ContactVO> contactVOs) throws Throwable {
        contactAdapter = new AllContactsAdapter(contactVOs, getActivity().getApplicationContext(), this);
        rvContacts.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rvContacts.setAdapter(contactAdapter);
    }

    public List<ContactVO> getcontactVOList(AppCompatActivity a) throws Exception {
        List<ContactVO> contactVOList = new ArrayList();
        List<ContactVO> listCompare = new ArrayList();
        contactVOListAndroidAll = new ArrayList<>();

        ContactVO contactVO;
        boolean getInvited = _main.getInvited;
        JSONArray contacts;
        if (getInvited) {
            contacts = _main.ArrInvited;
        } else {
            try {
                contacts = _main.clsHTTPS.getUsers(_main.user.getLong(Constants.id), 0, false);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
        if (contacts.length() > 0) {
            for (int i = 0; i < contacts.length(); i++) {
                try {
                    JSONObject user = contacts.getJSONObject(i);

                    contactVO = new ContactVO(user);
                    listCompare.add(contactVO);
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            }
        }

        ContentResolver contentResolver = a.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + ASC);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                contactVO = new ContactVO();
                contactVO.setContactName(name);

                if (hasPhoneNumber > 0) {
                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    while (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        if (phoneNumber.length() == 0) continue;
                        phoneNumber = lib.formatPhoneNumber(phoneNumber);
                        if (phoneNumber.length() == 0 || contactVO.getContactNumber().contains(phoneNumber))
                            continue;
                        contactVO.setContactNumber(phoneNumber);
                    }

                    phoneCursor.close();
                }
                Cursor emailCursor = contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);

                while (emailCursor.moveToNext()) {
                    String emailId = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    emailId = emailId.trim().toLowerCase();
                    if (emailId.length() == 0 || contactVO.getContactEmail().contains(emailId))
                        continue;
                    contactVO.setContactEmail(emailId);
                }

                emailCursor.close();
                boolean hasChildren = false;
                if (listCompare != null) {
                    for (ContactVO c : listCompare) {
                        String EMailLimContact = c.getContactEmail().toLowerCase() + ",";
                        String EMailContact = contactVO.getContactEmail().toLowerCase() + ",";
                        String phoneLimContact = c.getContactNumber() + ",";
                        String phoneContact = contactVO.getContactNumber() + ",";
                        if (phoneContact.contains(phoneLimContact)) {
                            System.out.println(EQUAL);
                        }

                        if ((EMailContact.length() > 3 && EMailLimContact.length() > 3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length() > 3 && phoneLimContact.length() > 3 && phoneContact.contains(phoneLimContact))) {
                            //contactVO.setIsLimindoContact(true);
                            //contactVO.setUserID((c.getUserID()));
                            //contactVO.setContactID(c.getContactID());
                            //contactVO.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            if (!c.isCopied && !contactVO.isCopied) {
                                c.setIsLimindoContact(true);
                                ContactVO cc = contactVO.copy();
                                contactVO.copycount++;
                                cc.isCopied = true;
                                //c.isCopied = true;
                                contactVO.setIsLimindoContact(true);
                                cc.setIsLimindoContact(true);
                                cc.setUserID((c.getUserID()));
                                cc.setContactID(c.getContactID());
                                cc.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                                hasChildren = true;
                                cc.setChecked(true);
                                contactVOList.add(cc);
                            }
                        }
                    }
                }


                if (!hasChildren) contactVOListAndroidAll.add(contactVO);

            }
        }
        return contactVOList;
    }

    private List<ContactVO> refreshList(boolean listLoaded, List<ContactVO> listCompare, List<ContactVO> contactVOList) throws Exception {
        ContactVO contactVO;
        if (_main.user == null) return listCompare;
        if (contactVOList.size() > 0) {
            for (int i = 0; i < contactVOList.size(); i++) {

                contactVO = contactVOList.get(i);

                if (listLoaded && listCompare != null) {
                    if (contactVO.isCopied) continue;
                    for (ContactVO c : listCompare) {
                        String EMailLimContact = c.getContactEmail().toLowerCase() + ",";
                        String EMailContact = contactVO.getContactEmail().toLowerCase() + ",";
                        String phoneLimContact = c.getContactNumber() + ",";
                        String phoneContact = contactVO.getContactNumber() + ",";
                        if (phoneContact.contains(phoneLimContact)) {
                            System.out.println(EQUAL);
                        }
                        if ((EMailContact.length() > 3 && EMailLimContact.length() > 3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length() > 3 && phoneLimContact.length() > 3 && phoneContact.contains(phoneLimContact))) {

                            c.setIsLimindoContact(true);
                            //contactVO.setIsLimindoContact(true);
                            //contactVO.setUserID((c.getUserID()));
                            //contactVO.setContactID(c.getContactID());
                            //contactVO.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            if (c.isCopied) continue;
                            ContactVO cc = contactVO.copy();
                            contactVO.copycount++;
                            cc.isCopied = true;
                            //c.isCopied = true;
                            contactVO.setIsLimindoContact(true);
                            cc.setIsLimindoContact(true);
                            cc.setUserID((c.getUserID()));
                            cc.setContactID(c.getContactID());
                            cc.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            contactVOList.add(i + 1, cc);
                            i++;
                            if (contactVO.copycount < 2) ArrCopied.add(contactVO);
                        }
                    }
                }


            }
        }
        if (listLoaded && listCompare != null) {
            for (ContactVO c : ArrCopied) {
                contactVOList.remove(c);
            }
            ArrCopied.clear();
        }
        return contactVOList;

    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        isvisible = false;
        if (listLoaded && dlgContacts.contactVOListAndroid.size() > 0) {
            _main.mPager.setCurrentItem(fragQuestions.fragID);
            fragQuestions qq = (fragQuestions) _main.fPA.findFragment(fragQuestions.fragID);
            if (qq != null) {
                qq.btnKontakte.performClick();
            }
        } else {
            _main.mPager.setCurrentItem(fragContacts.fragID);
        }
    }

    public void reLoad() {
        new ListAsyncTask(this).execute();
    }

    private static class ListAsyncTask extends AsyncTask<Void, Void, List<ContactVO>> {

        private final dlgContacts _dlgContacts;
        private List<ContactVO> listCompare;
        private Exception ex;

        ListAsyncTask(dlgContacts dlgContacts) {
            this._dlgContacts = dlgContacts;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            _dlgContacts.pb.setVisibility(View.VISIBLE);
            _dlgContacts.rvContacts.setVisibility(View.GONE);
        }

        @Override
        protected List<ContactVO> doInBackground(Void... integers) {
            try {
                return _dlgContacts.getcontactVOList(_dlgContacts._main);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                this.ex = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ContactVO> contactVOs) {
            super.onPostExecute(contactVOs);
            try {
                _dlgContacts.pb.setVisibility(View.GONE);
                if (_dlgContacts == null || contactVOs == null) {
                    if (this.ex == null)
                    {
                        return;
                    }
                    else
                    {
                        lib.ShowException(TAG, _dlgContacts.getContext(),this.ex,false);
                        AlertDialog dlg = lib.ShowMessageYesNo(_dlgContacts.getContext(),
                                _dlgContacts.getString(R.string.ErrorGettingUsersRepeat),
                                _dlgContacts.getString(R.string.Error), false,
                                new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new ListAsyncTask(_dlgContacts).execute();
                            }
                        }, null, null);
                        return;
                    }
                }
                _dlgContacts.setAdapter(contactVOs);
                _dlgContacts.rvContacts.setVisibility(View.VISIBLE);
                dlgContacts.contactVOListAndroid = contactVOs;
                dlgContacts.listLoaded = true;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }
    }
}