package de.com.limto.limto1.Errors;

/**
 * Created by jhmgbl on 23.09.17.
 */

import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.fragQuestions;
import de.com.limto.limto1.lib.lib;


public class SQLiteDataActivity extends Fragment
{
    public final static int fragID = 7;
    private static final String TAG = "SQLiteDataActivity";
    dbSqlite sqLiteHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    ListAdapter listAdapter ;
    ListView LISTVIEW;
    boolean blndata;

    ArrayList<String> ID_Array = new ArrayList<>();
    ArrayList<String> date_Array = new ArrayList<>();
    ArrayList<String> ex_Array = new ArrayList<>();
    ArrayList<String> CodeLoc_Array = new ArrayList<>();
    ArrayList<String> comment_Array = new ArrayList<>();
    ArrayList<String> exported_Array = new ArrayList<>();
    public MainActivity _main;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _main = (MainActivity)getActivity();
        if (_main != null && _main.mService != null) sqLiteHelper = _main.mService.db;
        this.setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View v = inflater.inflate(R.layout.activity_sqlite_data,container,false);

        LISTVIEW = (ListView) v.findViewById(R.id.listView1);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater)
    {
        menuInflater.inflate(R.menu.errors2, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        try {
            switch (item.getItemId()) {
                case R.id.menu_gotolast:
                    if (LISTVIEW != null && listAdapter != null)
                        LISTVIEW.setSelection(listAdapter.getCount() - 1);
                    return true;
                case R.id.menu_uncaught:
                    ShowSQLiteDBdata(true, false, null);
                    return true;
                case R.id.menu_exceptions:
                    ShowSQLiteDBdata(false, true, null);
                    return true;
                case R.id.menu_finderrors:
                    final EditText input = new EditText(getContext());
                    lib.getInputBox(getContext(), getString(R.string.finderrors), getString(R.string.entersearch), "", false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                ShowSQLiteDBdata(false, false, input.getText().toString());
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                    }, null, input);

                    return true;
                case R.id.menu_deleteerrors:
                    if (sqLiteHelper != null && sqLiteHelper.DataBase != null) {
                        sqLiteHelper.DataBase.execSQL("DELETE FROM Errors");
                        ShowSQLiteDBdata(false, false, null);
                    }
                    return true;
                case R.id.menu_exporterrors:
                    if (sqLiteHelper != null) try {

                    } catch (Throwable throwable) {
                        Log.e("SQLiteDataActivity","exportErrors",throwable);
                        throwable.printStackTrace();
                    }
                    return true;
                case R.id.menu_close:
                    ((MainActivity)getActivity()).mPager.setCurrentItem(fragQuestions.fragID);
                    return true;


            }
        }
        catch (Throwable e)
        {
            Log.e("Errors","menu",e);
        }
        return super.onOptionsItemSelected(item);
    }

    public void init(dbSqlite db) throws Throwable
    {
        sqLiteHelper = db;
        ShowSQLiteDBdata(false, false, null) ;

    }

    @Override
    public void onResume() {

        try {
            ShowSQLiteDBdata(false, false, null);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG,"ShowSQLiteDBdata", throwable);
        }
        super.onResume();
    }

    @Override
    public void onPause() {

        if (cursor!= null) {
            cursor.close();
            cursor = null;
        }
        super.onPause();
    }

    @Override
    public void onStop() {

        if (cursor!= null) {
            cursor.close();
            cursor=null;
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {

        if (sqLiteHelper!= null) {
            sqLiteHelper.close();
            sqLiteHelper = null;
        }
        if (cursor!= null) {
            cursor.close();
            cursor=null;
        }
        super.onDestroy();
    }

    private void ShowSQLiteDBdata(boolean uncaught, boolean exception, String search) throws Throwable
    {

        if (LISTVIEW == null) return;
        //sqLiteDatabase = sqLiteHelper.getWritableDatabase();
        if (sqLiteHelper==null) sqLiteHelper = ((MainActivity)getActivity()).mService.db;
        if (sqLiteHelper == null) return;
        String where = "";
        if (uncaught) where = " WHERE CodeLoc LIKE '%uncaught%' OR comment LIKE '%uncaught%'";
        if (exception) where = " WHERE ex != 'null'";
        if (search != null) where = " WHERE comment LIKE '%" + search + "%'";
        cursor = sqLiteHelper.query((blndata?"SELECT * FROM Data":"SELECT * FROM Errors" + where));

        listAdapter = new de.com.limto.limto1.Errors.ListAdapter(SQLiteDataActivity.this, cursor);
        /*
                ID_Array,
                date_Array,
                ex_Array,
                CodeLoc_Array,
                comment_Array,
                exported_Array
        );
        */
        LISTVIEW.setAdapter(listAdapter);

        //cursor.close();
    }

    public void refresh() throws Throwable
    {
        ShowSQLiteDBdata(false, false, null);
    }
}



