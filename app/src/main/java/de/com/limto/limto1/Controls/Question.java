package de.com.limto.limto1.Controls;


import android.content.Context;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.MonthDay;
import java.util.ArrayList;
import java.util.Date;

import de.com.limto.limto1.Constants;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;

import static de.com.limto.limto1.clsHTTPS.ONLINE_STATE;
import static de.com.limto.limto1.fragAnswer.NO;
import static de.com.limto.limto1.fragAnswer.UNDEFINED;
import static de.com.limto.limto1.fragAnswer.YES;
import static de.com.limto.limto1.fragQuestion.VOTE;

public class Question extends Object {
    public static final String FRAGE = "frage";
    public static final String BENUTZERID = "benutzerid";
    public static final String KONTAKTGRAD = "kontaktgrad";
    public static final String DATUMSTART = "datumstart";
    public static final String DATUMENDE = "datumende";
    public static final String RADIUSKM = "radiuskm";
    public static final String BENUTZERNAME = "benutzername";
    public static final String EMAIL = "email";
    public static final String FRAGEID = "frageid";
    public static final String BREITENGRAD = "breitengrad";
    public static final String LAENGENGRAD = "laengengrad";
    public static final String ONLINE = "online";
    public static final String DATUMNEU = "datumneu";
    public static final String FEHLERGRAD = "fehlergrad";
    public static final String EMERGENCY = "emergency";
    public static final String BEWERTUNGEN = "bewertungen";
    public static final String FRAGEN = "fragen";
    public static final String ANTWORTEN = "antworten";
    public static final String AKTIVITAETSINDEX = "aktivitaetsindex";
    public static final String BESTAENDIGKEIT = "bestaendigkeit";
    public static final String WERTUNG = "wertung";
    public static final String GEMELDET = "gemeldet";
    public static final String OEFFENTLICH = "oeffentlich";
    public static final String ANTWORTZEIT = "antwortzeit";
    public static final String LOCALE = "locale";
    private final Context context;
    public String Locale = "de_DE";
    public Integer Aktivitaetsindex=null;
    public Integer Bestaendigkeit=null;
    public Integer Wertung=null;
    public Integer Antwortzeit = null;
    public boolean gemeldet = false;
    public Boolean emergency;
    public Integer Fehlergrad;
    public Boolean oeffentlich;
    public String EMail;
    public String Benutzername;
    public Long ID;
    public boolean fetched;
    public boolean expanded;
    public boolean online;
    public ArrayList<Answer> items = new java.util.ArrayList<>();

    public String Frage;
    public Long BenutzerID;
    public int Kontaktgrad;
    public Date DatumStart;
    public Date DatumEnde;
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String MM_DD_HH_MM_SS = "MM-dd HH:mm:ss";
    public static SimpleDateFormat sdf = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS);
    public static SimpleDateFormat sdfdate = new SimpleDateFormat(MM_DD_HH_MM_SS);
    public static SimpleDateFormat sdftime = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat sdfday = new SimpleDateFormat("E HH:mm");
    public static SimpleDateFormat sdfdateshort = new SimpleDateFormat("MM-dd");;
    public int RadiusKM;
    public Double Laengengrad;
    public Double Breitengrad;
    public Long FrageID;
    public int Grad;
    public Date DatumNeu;
    public boolean selected;
    public boolean aus = false;
    public Integer Bewertungen = null;
    public Integer Fragen = null;
    public Integer Antworten = null;
    public MainActivity.OnlineState onlineState = MainActivity.OnlineState.offline;

    public Question(Context context, Long ID, Long FrageID, String Frage, Long BenutzerID, String BenutzerName, String EMail, int Grad, Date DatumStart, Date DatumEnde, int RadiusKM, Double Laengengrad, Double Breitengrad, boolean online, boolean oeffentlich, Integer fehlergrad, boolean emergency, MainActivity.OnlineState onlineState) {
        this.context = context;
        this.ID = ID;
        this.FrageID = FrageID;
        this.Frage = Frage;
        this.BenutzerID = BenutzerID;
        this.Benutzername = BenutzerName;
        this.EMail = EMail;
        this.Kontaktgrad = Grad;
        this.DatumStart = DatumStart;
        this.DatumEnde = DatumEnde;
        this.RadiusKM = RadiusKM;
        this.Laengengrad = Laengengrad;
        this.Breitengrad = Breitengrad;
        this.online = online;
        this.DatumNeu = null;
        this.oeffentlich = oeffentlich;
        this.Fehlergrad = fehlergrad;
        this.emergency = emergency;
        this.onlineState = onlineState;
    }


    public Question(Context context, JSONObject json) throws JSONException, ParseException {
        this.context = context;
        this.ID = json.getLong(Constants.id);
        this.Frage = StringEscapeUtils.unescapeJava(json.getString(FRAGE));
        this.BenutzerID = json.getLong(BENUTZERID);
        this.Kontaktgrad = json.getInt(KONTAKTGRAD);
        this.DatumStart = sdf.parse(json.getString(DATUMSTART));
        this.DatumEnde = sdf.parse(json.getString(DATUMENDE));
        this.RadiusKM = json.getInt(RADIUSKM);
        this.Benutzername = json.getString(BENUTZERNAME);
        this.EMail = json.getString(EMAIL);
        this.FrageID = json.getLong(FRAGEID);
        this.Locale = json.optString(LOCALE, java.util.Locale.getDefault().toString());
        if (json.isNull(BREITENGRAD)) this.Breitengrad = null; else this.Breitengrad = json.getDouble(BREITENGRAD);
        if (json.isNull(LAENGENGRAD)) this.Laengengrad = null; else this.Laengengrad = json.getDouble(LAENGENGRAD);
        if (json.isNull(ONLINE)) this.online = false; else this.online = json.getBoolean(ONLINE);
        if (json.isNull(DATUMNEU)) this.DatumNeu = null; else this.DatumNeu = sdf.parse(json.getString(DATUMNEU));
        if (json.isNull(FEHLERGRAD)) this.Fehlergrad = null; else this.Fehlergrad = Integer.parseInt(json.getString(FEHLERGRAD));
        try {
            if (json.isNull(EMERGENCY)) this.emergency = null;
            else this.emergency = json.getInt(EMERGENCY) != 0;
        }
        catch (JSONException ex)
        {
            if (json.isNull(EMERGENCY)) this.emergency = null;
            else this.emergency = json.getBoolean(EMERGENCY);
        }
        if (json.isNull(BEWERTUNGEN)) this.Bewertungen = null; else this.Bewertungen = json.getInt(BEWERTUNGEN);
        if (json.isNull(FRAGEN)) this.Fragen = null; else this.Fragen = json.getInt(FRAGEN);
        if (json.isNull(ANTWORTEN)) this.Antworten = null; else this.Antworten = json.getInt(ANTWORTEN);
        if (json.isNull(AKTIVITAETSINDEX)) this.Aktivitaetsindex = null; else this.Aktivitaetsindex = json.getInt(AKTIVITAETSINDEX);
        if (json.isNull(BESTAENDIGKEIT)) this.Bestaendigkeit = null; else this.Bestaendigkeit = json.getInt(BESTAENDIGKEIT);
        if (json.isNull(WERTUNG)) this.Wertung = null; else this.Wertung = json.getInt(WERTUNG);
        try {
            if (json.isNull(GEMELDET)) this.gemeldet = false;
            else this.gemeldet = json.getInt(GEMELDET) != 0;
        }
        catch (JSONException ex)
        {
            if (json.isNull(GEMELDET)) this.gemeldet = false;
            else this.gemeldet = json.getBoolean(GEMELDET);
        }
        if (json.isNull(OEFFENTLICH)) {
            this.oeffentlich = null;
        } else {
            try
            {
                this.oeffentlich = (json.getInt(OEFFENTLICH)!=0);
            }
            catch (JSONException ex)
            {
                this.oeffentlich = (json.getBoolean(OEFFENTLICH));
            }
        }
        if (json.isNull(ANTWORTZEIT)) this.Antwortzeit = null; else this.Antwortzeit = json.getInt(ANTWORTZEIT);
        if (json.isNull(ONLINE_STATE)) this.onlineState = this.online ? MainActivity.OnlineState.visible : MainActivity.OnlineState.offline;
        else this.onlineState = MainActivity.OnlineState.getOnlineState(json.getInt(ONLINE_STATE));
    }

    public Answer findAnswer(long FrageAntwortenID)
    {
        for (Answer a : items)
        {
            if (a.ID == FrageAntwortenID)
            {
                return a;
            }
        }
        return null;
    }

    public void update(JSONObject json) throws JSONException, ParseException
    {
        this.ID = json.getLong(Constants.id);
        this.Frage = StringEscapeUtils.unescapeJava(json.getString(FRAGE));
        this.BenutzerID = json.getLong(BENUTZERID);
        this.Kontaktgrad = json.getInt(KONTAKTGRAD);
        this.DatumStart = sdf.parse(json.getString(DATUMSTART));
        this.DatumEnde = sdf.parse(json.getString(DATUMENDE));
        this.RadiusKM = json.getInt(RADIUSKM);
        this.Benutzername = json.getString(BENUTZERNAME);
        this.EMail = json.getString(EMAIL);
        this.FrageID = json.getLong(FRAGEID);
        if (json.isNull(BREITENGRAD)) this.Breitengrad = null; else this.Breitengrad = json.getDouble(BREITENGRAD);
        if (json.isNull(LAENGENGRAD)) this.Laengengrad = null; else this.Laengengrad = json.getDouble(LAENGENGRAD);
        if (json.isNull(ONLINE)) this.online = false; else this.online = json.getBoolean(ONLINE);
        if (json.isNull(FEHLERGRAD)) this.Fehlergrad = null; else this.Fehlergrad = Integer.parseInt(json.getString(FEHLERGRAD));
        try {
            if (json.isNull(EMERGENCY)) this.emergency = null;
            else this.emergency = json.getInt(EMERGENCY) != 0;
        }
        catch (JSONException ex)
        {
            if (json.isNull(EMERGENCY)) this.emergency = null;
            else this.emergency = json.getBoolean(EMERGENCY);
        }
        if (json.isNull(BEWERTUNGEN)) this.Bewertungen = null; else this.Bewertungen = json.getInt(BEWERTUNGEN);
        if (json.isNull(FRAGEN)) this.Fragen = null; else this.Fragen = json.getInt(FRAGEN);
        if (json.isNull(ANTWORTEN)) this.Antworten = null; else this.Antworten = json.getInt(ANTWORTEN);
        if (json.isNull(AKTIVITAETSINDEX)) this.Aktivitaetsindex = null; else this.Aktivitaetsindex = json.getInt(AKTIVITAETSINDEX);
        if (json.isNull(BESTAENDIGKEIT)) this.Bestaendigkeit = null; else this.Bestaendigkeit = json.getInt(BESTAENDIGKEIT);
        if (json.isNull(WERTUNG)) this.Wertung = null; else this.Wertung = json.getInt(WERTUNG);
        try {
            if (json.isNull(GEMELDET)) this.gemeldet = false;
            else this.gemeldet = json.getInt(GEMELDET) != 0;
        }
        catch (JSONException ex)
        {
            if (json.isNull(GEMELDET)) this.gemeldet = false;
            else this.gemeldet = json.getBoolean(GEMELDET);
        }
        if (json.isNull(OEFFENTLICH)) {
            this.oeffentlich = null;
        } else {
            try
            {
                this.oeffentlich = (json.getInt(OEFFENTLICH)!=0);
            }
            catch (JSONException ex)
            {
                this.oeffentlich = (json.getBoolean(OEFFENTLICH));
            }
        }
        if (json.isNull(ANTWORTZEIT)) this.Antwortzeit = null; else this.Antwortzeit = json.getInt(ANTWORTZEIT);
    }

    public void updateUser(JSONObject user) throws JSONException {
        if (user.isNull(ANTWORTZEIT)) this.Antwortzeit = null; else this.Antwortzeit = user.getInt(ANTWORTZEIT);
        if (user.isNull(AKTIVITAETSINDEX)) this.Aktivitaetsindex = null; else this.Aktivitaetsindex = user.getInt(AKTIVITAETSINDEX);
        if (user.isNull(BESTAENDIGKEIT)) this.Bestaendigkeit = null; else this.Bestaendigkeit = user.getInt(BESTAENDIGKEIT);
        if (user.isNull(WERTUNG)) this.Wertung = null; else this.Wertung = user.getInt(WERTUNG);
        if (user.isNull(BEWERTUNGEN)) this.Bewertungen = null; else this.Bewertungen = user.getInt(BEWERTUNGEN);
        if (user.isNull(FRAGEN)) this.Fragen = null; else this.Fragen = user.getInt(FRAGEN);
        if (user.isNull(ANTWORTEN)) this.Antworten = null; else this.Antworten = user.getInt(ANTWORTEN);
    }

    public boolean isVOTE() {
       return Frage.startsWith(VOTE);
    }

    public String getFrage(boolean blnEdit)
    {
        int pos = 0;
        if (!blnEdit && isVOTE()) {
            pos = VOTE.length();
            String strVote;
            if (this.fetched) {
                String yes = "" + hasAnswers(YES);
                String no = "" + hasAnswers(NO);
                String undefined = "" + hasAnswers(UNDEFINED);
                strVote = context.getString(R.string.voteresult, yes, no, undefined);
            }
            else
            {
                strVote = context.getString(R.string.openforresult);
            }
            return Frage.substring(pos) + "<br>" + strVote;
        }
        else
        {
            return Frage;
        }
    }

    public int hasAnswers(String txt) {
        int count = 0;
        for (Answer aa : items)
        {
            if(aa.Antwort.startsWith(txt)) count ++;
        }
        return count;
    }

    public boolean isSystemMessage() {
        return (this.FrageID == 1l && this.ID == 1l);
    }
}