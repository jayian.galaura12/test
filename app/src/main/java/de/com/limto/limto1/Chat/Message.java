package de.com.limto.limto1.Chat;

import android.graphics.Bitmap;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextUtils;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.lib.lib;

import static de.com.limto.limto1.MainActivity.BENUTZERIDEMPFAENGER;
import static de.com.limto.limto1.MainActivity.BENUTZERNAME;
import static de.com.limto.limto1.MainActivity.BENUTZER_ID;
import static de.com.limto.limto1.MainActivity.TIME;

public class Message {
    private static final String TAG = "Message";
    private boolean sendMessages;
    private transient MainActivity _main;
    private transient dlgChat context;
    private SpannedString text;
    private dlgChat.MemberData memberData;
    private boolean belongsToCurrentUser;
    private Date time = Calendar.getInstance().getTime();
    private boolean sentToServer;
    private boolean receivedByServer;
    private boolean sentToReceiver;
    private boolean readByReceiver;
    private UUID uuid;
    private Bitmap thumb;
    private long benutzerIDEmpfaenger;
    private long benutzerID;
    private String benutzerName = "empty name";
    private Throwable error;


    public Message(MainActivity main, dlgChat dlgChat,SpannableString text, dlgChat.MemberData data, Date time, UUID uuid, boolean belongsToCurrentUser) {
        this._main = main;
        this.context = dlgChat;
        this.setText(text);
        this.memberData = data;
        this.belongsToCurrentUser = belongsToCurrentUser;
        this.time = time;
        this.uuid = uuid;
    }

    public Message(MainActivity main, dlgChat dlgChat, JSONObject o, boolean belongsToCurrentUser) throws Throwable {
        this._main = main;
        this.context = dlgChat;
        UUID uuid = UUID.randomUUID();
        uuid = UUID.fromString(o.optString("uuid", uuid.toString()));
        this.uuid = uuid;
        long BenutzerIDEmpfaenger = o.getLong(BENUTZERIDEMPFAENGER);
        this.setBenutzerIDEmpfaenger(BenutzerIDEmpfaenger);
        long BenutzerID = o.getLong(BENUTZER_ID);
        if (BenutzerID < 0) {
            lib.ShowMessage(_main, _main.getString(R.string.invalid_userid), _main.getString(R.string.Error));
            throw new Exception(_main.getString(R.string.invalid_userid));
        }
        Date time = Calendar.getInstance().getTime();
        Long lngTime = o.optLong(TIME, 0);
        if (lngTime == 0) {
            lngTime = time.getTime();
        }
        time = new Date(lngTime);

        getBenutzerNameFromBenutzerID (BenutzerID);

        String Message = o.getString("Message");
        sentToReceiver = o.optBoolean("sent", false);
        readByReceiver = o.optBoolean("read", false);
        receivedByServer = o.optString("Message").equalsIgnoreCase("*received*");
        sendMessages = o.optBoolean("sendMessages", false);
        if (sendMessages)
        {
            lib.setStatusAndLog(_main, TAG, "### sendMessages = " + sendMessages + " " + o.toString());
        }
        if (BenutzerID == context.BenutzerID) {
            BenutzerID = BenutzerIDEmpfaenger;
        }
        this.setBenutzerID(BenutzerID);
        this.setText(new SpannableString(Message));

        setData(BenutzerID, BenutzerIDEmpfaenger, dlgChat, Message);

        this.belongsToCurrentUser = belongsToCurrentUser;
        this.time = time;
    }

    private void setData(long BenutzerID, long BenutzerIDEmpfaenger, dlgChat dlgChat, String Message) throws Throwable {

        de.com.limto.limto1.Chat.dlgChat.MemberData data;
        String color = de.com.limto.limto1.Chat.dlgChat.getRandomColor();

        if (!belongsToCurrentUser) {
            if ((!context.MemberData.containsKey(BenutzerID))) {
                data = context.newChatMember(this, uuid, BenutzerID, getBenutzerName(), new SpannableString(Message), color, time, belongsToCurrentUser, false);
                //if (data == null) throw new Exception ("Could not create new ChatMember!"); //Grade > 1
            } else {
                data = dlgChat.MemberData.get(BenutzerID);
                data.setNameEmpfaenger(getBenutzerName());
                data.setBenutzeridEmpfaenger(BenutzerID);
            }
        }
        else
        {
            if (!dlgChat.MemberData.containsKey(BenutzerID)) {
                data = new dlgChat.MemberData(getBenutzerName(), dlgChat.NameEmpfaenger, color, BenutzerID, BenutzerIDEmpfaenger, null);
                dlgChat.MemberData.put(BenutzerID, data);
                context.insertMember(data);
            } else {
                data = new dlgChat.MemberData(getBenutzerName(), dlgChat.NameEmpfaenger, color, BenutzerID, BenutzerIDEmpfaenger, null);
                data.setNameEmpfaenger(dlgChat.NameEmpfaenger);
                data.setBenutzeridEmpfaenger(BenutzerIDEmpfaenger);
            }
        }
        this.memberData = data;

    }

    private void getBenutzerNameFromBenutzerID(long benutzerID) {
        try {
            Long id = _main.getBenutzerID();
            JSONObject user = _main.clsHTTPS.getUser(id, benutzerID);
            String name = user.getString(BENUTZERNAME);
            this.setBenutzerName(name);
        }
        catch (Throwable ex)
        {
            lib.setStatusAndLog(_main, TAG, "*** getUser " + ex.getMessage());
        }
    }

    public CharSequence getText() {
        return text;
    }

    public dlgChat.MemberData getMemberData() {
        return memberData;
    }

    public boolean isBelongsToCurrentUser() {
        return belongsToCurrentUser;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setSentToServer(boolean sentToServer) throws Exception {
        //if (sentToServer && this.sentToServer) throw new Exception("Message sent is already set");
        notBelongsToCurrentUser(sentToServer);
        this.sentToServer = sentToServer;
    }

    private void notBelongsToCurrentUser(boolean sent) {
        if (!belongsToCurrentUser) {
            lib.setStatusAndLog(_main, TAG, "### Message does not belong to current user BenName=" + benutzerName + " send " + sendMessages + " BenIDEmpf=" + benutzerIDEmpfaenger + " " + this.toString() );
            System.out.println(sendMessages);
        }
    }

    public boolean getSentToServer()
    {
        return sentToServer;
    }

    public boolean getSentToReceiver()
    {
        return sentToReceiver;
    }

    public void setSentToReceiver(boolean sentToReceiver) throws Exception {
        //if (received && this.received) throw new Exception("Message ist already set to received!");
        notBelongsToCurrentUser(sentToReceiver);
        this.sentToReceiver = sentToReceiver;
    }

    public boolean getReadByReceiver()
    {
        return readByReceiver;
    }

    public void setReadByReceiver(boolean readByReceiver) throws Exception {
        //if (seen && this.seen) throw new Exception("Message is already set to seen!");
        notBelongsToCurrentUser(readByReceiver);
        this.readByReceiver = readByReceiver;
    }

    public void setUUID(UUID uuid)
    {
        this.uuid = uuid;
    }

    public UUID getUUID()
    {
        return uuid;
    }

    public void setText(SpannableString ss) {
        this.text = (SpannedString) TextUtils.concat(ss,"");
    }

    public void setThumb(Bitmap thumb) {
        this.thumb = thumb;
    }

    public Bitmap getThumb() {
        return thumb;
    }

    public void setMemberData(dlgChat.MemberData memberdata) {
        this.memberData = memberdata;
    }


    public void setSelf(boolean self) {
        this.belongsToCurrentUser = self;
    }

    public boolean getSelf() {
        return belongsToCurrentUser;
    }

    public void addMessage(SpannableString message) {
        this.text = (SpannedString) TextUtils.concat(this.text, message);
    }

    public void setReceivedByServer(boolean receivedByServer) throws Exception {
        notBelongsToCurrentUser(receivedByServer);
        this.receivedByServer = receivedByServer;
    }

    public boolean getReceivedByServer() {
        return receivedByServer;
    }

    public boolean statusChanged() {
        return readByReceiver || sentToReceiver || receivedByServer;
    }

    public void setBenutzerIDEmpfaenger(long benutzerIDEmpfaenger) {
        this.benutzerIDEmpfaenger = benutzerIDEmpfaenger;
    }

    public long getBenutzerIDEmpfaenger() {
        return benutzerIDEmpfaenger;
    }

    public void setBenutzerID(long benutzerID) {
        this.benutzerID = benutzerID;
    }

    public long getBenutzerID() {
        return benutzerID;
    }

    public void setBenutzerName(String benutzerName) {
        this.benutzerName = benutzerName;
    }

    public String getBenutzerName() {
        if (benutzerName.equals("empty name")) getBenutzerNameFromBenutzerID (benutzerID);
        return benutzerName;
    }

    public void setBelongsToCurrentuser(boolean belongsToCurrentuser) {
        this.belongsToCurrentUser = belongsToCurrentuser;
    }

    public boolean getBelongsToCurrentuser() {
        return belongsToCurrentUser;
    }

    public boolean getSendMessages() {

        return sendMessages;
    }

    public void setSendMessages(boolean sendMessages) {
        this.sendMessages = sendMessages;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public Throwable getError() {
        return error;
    }
}
