package de.com.limto.limto1.Controls;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.StrictMode;

import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;

import de.com.limto.limto1.Constants;
import de.com.limto.limto1.LimindoService;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.clsHTTPS;
import de.com.limto.limto1.fragAnswer;
import de.com.limto.limto1.fragQuestions;
import de.com.limto.limto1.lib.LetterTileProvider;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.LimindoService.HandlerUnbound.DIE_IDENTITÄT_KONNTE_NICHT_ÜBERPRÜFT_WERDEN;
import static de.com.limto.limto1.LimindoService.STATE_ERROR;
import static de.com.limto.limto1.LimindoService.STATE_NONE;
import static de.com.limto.limto1.MainActivity.HandlerBound.BENUTZER_NICHT_ANGEMELDET;
import static de.com.limto.limto1.MainActivity.blnAccessibility;
import static de.com.limto.limto1.clsHTTPS.ACCESSKEY;
import static de.com.limto.limto1.clsHTTPS.Delimiter_A;
import static de.com.limto.limto1.clsHTTPS.ERROR_UNABLE_TO_RESOLVE_HOST;
import static de.com.limto.limto1.clsHTTPS.FALSE;
import static de.com.limto.limto1.clsHTTPS.GET;
import static de.com.limto.limto1.clsHTTPS.MYURLROOT;
import static de.com.limto.limto1.clsHTTPS.getSSLFactory;

//import android.runtime.*;
//import com.facebook.*;
//import com.facebook.android.Facebook;
//import com.sromku.simple.*;

public class QuestionsAdapter extends BaseExpandableListAdapter {
    private static final String TAG = "QuestionsAdapter";
    public static final String TRUE = "TRUE";
    public static final String ERROR = "Error:";
    public static final String BENUTZER_ID = "BenutzerID";
    public static final String KONTAKTGRAD = "Kontaktgrad";
    public static final String FRAGE_ID = "FrageID";
    public static final String BENUTZER_FRAGEN_ID = "BenutzerFragenID";
    public static final String GET_FOLDER_ITEMS = "getFolderItems: ";
    public static final String ID_2 = "id2";
    public static final String FOLDER_IS_NULL = "Folder is null";
    private final Fragment fragment;
    private final MainActivity _main;
    private View.OnLongClickListener onGroupClickListener = null;
    private View.OnClickListener onGroupClickListenerAll = null;
    private View.OnClickListener onGroupClickListenerAntwort = null;
    public Context context;
    public fragQuestions myApp;
    //private ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 3);

    public static int requestCode = 9997;
    public List<Question> rows;
    public ExpandableListView lv = null;
    private float text1Size;
    public static int[] QuestionColors;
    public static int[] LevelColors;
    public static char[] digits = new String("⓪❶❷❸❹❺❻❼❽❾").toCharArray();
    private View.OnClickListener imgQuestionClicklistener = null;
    private View.OnClickListener imgIndicatorClickListener;
    private View.OnLongClickListener imgIndicatorLongClickListener;
    public boolean blnDontSetCollapsed;
    private boolean SMWasExpanded;

    public Context getContext() {
        return context != null ? context : _main;
    }

    ;

    public class GetAnswersForQuestionTask extends AsyncTask<Void, Void, String> {
        private final View groupview;
        private final Question q;
        private final int groupposition;
        private final Group group;
        private Throwable ex;

        public GetAnswersForQuestionTask(Group g) {
            this.groupview = g.groupview;
            this.q = g.question;
            this.groupposition = g.groupposition;
            this.group = g;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String res;
            try {
                int i = 0;
                boolean rres = false;
                do {
                    rres = getAnswersForQuestion(q, groupposition);
                    i++;
                    if (!rres) Thread.sleep(200);
                } while (!rres && i <= 10);

                if (i >= 10) {
                    throw new Exception("user == null");
                }
                res = TRUE;

            } catch (Throwable e) {
                e.printStackTrace();
                res = e.getMessage();
                Log.e(TAG, null, e);
                this.ex = e;
            }
            return res;
        }

        @Override
        protected void onPostExecute(final String success) {
            boolean blnsuccess = success.equals((TRUE));
            String ErrorMessage = null;
            if (!blnsuccess) ErrorMessage = success;
            processGroup(blnsuccess, ErrorMessage, this.group, ex);
        }
    }

    public void processGroup(boolean blnsuccess, String errorMessage, Group group, Throwable ex) {
        View groupview = group.groupview;
        Question q = group.question;
        int groupposition = group.groupposition;
        try {
            if (blnsuccess && q.fetched) {
                //Show Number of Answers in groupview and set indicators
                if (q.ID > -1) {
                    int count = -1;
                    long userid = -1;
                    userid = getUserID();
                    int countFremde = getCountFremdeAntworten(q, userid);
                    ;

                    count = q.items.size();
                    ImageView iv = (ImageView) groupview.findViewById(R.id.imgQuestion);
                    ImageView ivIndicator = groupview.findViewById(R.id.imgIndicator);

                    setTextViewGradToCount(groupview, q, count);
                    setIndicatorToCount(count, ivIndicator, groupposition);
                    setimgQuestionAndGetUserImage(q, iv, groupview);
                    TextView textViewDate = setTxtDate(q, groupview);


                    lib.Ref<Integer> colorText = new lib.Ref<>(0);
                    lib.Ref<Boolean> bold = new lib.Ref<>(false);
                    ;
                    setDesignOnDateAndError(textViewDate, q, iv, ivIndicator, groupposition, bold, colorText);

                    redesignTextViewDate(groupview, textViewDate, q, bold, colorText);

                } else {
                    setTextViewAndIndicatorForReportedQuestions(groupview, groupposition, q);

                    statistics();
                }
            } else {
                if (errorMessage == null || errorMessage.length() == 0)
                    errorMessage = _main.getString(R.string.notFetched);
                setTextViewGradError(groupview, errorMessage);
                if (ex != null) {
                    Log.e(TAG, null, ex);
                    lib.ShowException(TAG, context, ex, false);
                }
            }
        } catch (Throwable eex) {
            Log.e(TAG, null, eex);
        }
    }

    private void setDesignOnDateAndError(TextView textViewDate, Question q, ImageView iv, ImageView ivIndicator, int groupposition, lib.Ref<Boolean> bold, lib.Ref<Integer> colorText) {

        boolean blnNeueAntwort = hasNewAnswers(q);
        boolean blnNeueFrage = q.DatumStart.after(_main.getLastLogoffDate());

        //Es handelt sich um eine Fehlermeldung
        if (q.Fehlergrad != null) {
            setTextAndImageViewsError(textViewDate, q, iv, ivIndicator, groupposition, bold, colorText);
        } else if (blnNeueAntwort) {
            colorText.set(setNewAnswer(q.isSystemMessage(), iv, ivIndicator, groupposition, R.color.colorErrorNeueAntwort));
            if (q.isSystemMessage() && !lv.isGroupExpanded(groupposition)) {
                //lv.expandGroup(groupposition);
            }
            bold.set(true);
        } else if (blnNeueFrage) {
            colorText.set(setNewQuestion(q, ivIndicator, iv, groupposition, R.color.colorNeueFrage));
            bold.set(true);
        } else { //Alte Frage
            iv.setBackgroundResource(0);
            colorText.set(context.getResources().getColor(R.color.colorDate));
        }
    }

    private void setTextViewGradError(View groupview, String errorMessage) {
        TextView textView = (TextView) groupview.findViewById(R.id.txtGrad);
        textView.setError(errorMessage);
    }

    private void setTextViewAndIndicatorForReportedQuestions(View groupview, int groupposition, Question q) {
        TextView textView = (TextView) groupview.findViewById(R.id.txtGrad);
        ImageView ivIndicator = groupview.findViewById(R.id.imgIndicator);
        textView.setText(q.items.size() + _main.getString(R.string.answers));
        if (q.items.size() > 0) {
            if (lv.isGroupExpanded(groupposition)) {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open)));
            } else {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
            }
        } else {
            ivIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_unbeantwortetefrage)));
        }
    }

    private void redesignTextViewDate(View groupview, TextView textViewDate, Question q, lib.Ref<Boolean> bold, lib.Ref<Integer> colorText) {
        setTextViewGradError(groupview, null);
        String stxtDate = textViewDate.getText().toString();
        int color = (q.Grad >= 0 ? LevelColors[q.Grad] : ContextCompat.getColor(context, R.color.colorGradMinussolid));
        stxtDate = (q.Grad >= 6 ? digits[q.Grad] : "■") + stxtDate;
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(stxtDate);
        wordtoSpan.setSpan(new ForegroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(colorText.get()), 0, wordtoSpan.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (bold.get() || blnAccessibility)
            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, wordtoSpan.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        //wordtoSpan.setSpan(new BackgroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        textViewDate.setText(wordtoSpan);
    }

    private void setTextAndImageViewsError(TextView textViewDate, Question q, ImageView iv, ImageView ivIndicator, int groupposition, lib.Ref<Boolean> bold, lib.Ref<Integer> colorText) {
        colorText.set(setFehlerGrad(textViewDate, q, iv));
        boolean neueAntwort = q.DatumNeu != null && q.DatumNeu.after(_main.getLastLogoffDate());
        boolean neueFrage = q.DatumStart.after(_main.getLastLogoffDate());
        if (neueAntwort) {
            colorText.set(setErrorNewAnswer(q.isSystemMessage(), iv, ivIndicator, groupposition));
            bold.set(true);
        } else if (neueFrage) {
            colorText.set(setErrorNewQuestion(q, ivIndicator, iv, groupposition));
            bold.set(true);
        }
    }

    private int setErrorNewQuestion(Question q, ImageView ivIndicator, ImageView iv, int groupposition) {
        int colorText = setNewQuestion(q, ivIndicator, iv, groupposition, R.color.colorErrorNeueFrage);
        return colorText;
    }

    private int setNewQuestion(Question q, ImageView ivIndicator, ImageView iv, int groupposition, int ivBackGroundResource) {
        int colorText = (context.getResources().getColor(R.color.colorNeueFrage));
        iv.setBackgroundResource(ivBackGroundResource);
        setIvIndicatorNewQuestion(q, ivIndicator, groupposition);
        return colorText;
    }

    private void setIvIndicatorNewQuestion(Question q, ImageView ivIndicator, int groupposition) {
        if (q.items.size() == 0) {
            ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragen));
        } else if (lv.isGroupExpanded(groupposition)) {
            ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
        } else {
            ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
        }

    }

    private int setErrorNewAnswer(boolean isSystemMessage, ImageView iv, ImageView ivIndicator, int groupposition) {
        return setNewAnswer(isSystemMessage, iv, ivIndicator, groupposition, R.color.colorErrorNeueAntwort);
    }

    private int setNewAnswer(boolean isSystemMessage, ImageView iv, ImageView ivIndicator, int groupposition, int resBackground) {
        int colorText = (context.getResources().getColor(R.color.colorNeueAntwort));
        iv.setBackgroundResource(resBackground);
        setIvIndicatorNewAnswer(isSystemMessage, ivIndicator, groupposition);
        if (isSystemMessage && !lv.isGroupExpanded(groupposition) && !SMWasExpanded) {
            lv.expandGroup(groupposition);
            SMWasExpanded = true;
        }
        return colorText;
    }

    private void setIvIndicatorNewAnswer(boolean isSystemMessage, ImageView ivIndicator, int groupposition) {
        if (lv.isGroupExpanded(groupposition)) {
            ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten_open));
        } else {
            if (isSystemMessage) {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.exclamation));
            } else {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten));
            }
        }

    }

    private int setFehlerGrad(TextView textView, Question q, ImageView iv) {
        textView.setText("{" + q.Fehlergrad + "}" + textView.getText());
        int colorText = (context.getResources().getColor(R.color.colorError));
        iv.setBackgroundResource(R.color.colorError);
        return colorText;
    }

    private void setimgQuestionAndGetUserImage(Question q, ImageView iv, View groupview) {
        if (q.emergency != null && q.emergency) {
            iv.setImageResource(R.drawable.sos);
        } else if (q.gemeldet) {
            iv.setImageResource(R.drawable.vn_logo_a03);
        } else {
            iv.setImageResource(R.drawable.vn_logo_c02);
        }
        new GetImageTask(groupview, q, null, _main).execute();
    }

    private TextView setTxtDate(Question q, View groupview) {
        Date dt = q.DatumStart;
        TextView textView = (TextView) groupview.findViewById(R.id.txtDate);
        textView.setText(((q.Grad < 0) ? q.ID : "") + (((q.oeffentlich != null) && q.oeffentlich) ? _main.getString(R.string.__public) : _main.getString(R.string.__private)) + dtFormatDayTime.format(dt));
        return textView;
    }

    private boolean hasNewAnswers(Question q) {
        boolean blnNeueAntwort = false;
        Date lastDate = null;
        if (q.isSystemMessage()) {
            try {
                lastDate = _main.getLastSystemDate();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            lastDate = _main.getLastLogoffDate();
        }
        if (lastDate == null) return true;
        for (Answer a : q.items) {
            if (a.Datum.after(lastDate)) {
                blnNeueAntwort = true;
                break;
            }
        }
        return blnNeueAntwort;
    }

    private void setIndicatorToCount(int count, ImageView ivIndicator, int groupposition) {
        if (count > 0) {
            if (lv.isGroupExpanded(groupposition)) {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open)));
            } else {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
            }
        } else {
            ivIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_unbeantwortetefrage)));
        }

    }

    private void setTextViewGradToCount(View groupview, Question q, int count) {
        TextView textView = (TextView) groupview.findViewById(R.id.txtGrad);
        String grad = _main.getString(R.string.grade) + q.Grad + "(" + q.Kontaktgrad + ")";
        int lGrad = grad.length();
        grad += " " + count + " " + _main.getString(R.string.answers);
        int colorGrad = q.Grad >= 0 ? QuestionColors[q.Grad] : ContextCompat.getColor(_main, R.color.colorGradMinus);
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(grad);
        wordtoSpan.setSpan(new BackgroundColorSpan(colorGrad), 0, lGrad, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(wordtoSpan);
    }

    private int getCountFremdeAntworten(Question q, long userid) {
        int count = -1;
        for (Answer a : q.items) {
            try {
                if (count == -1) count = 0;
                if (a.BenutzerID != userid && (a.BenutzerIDSender == null || a.BenutzerIDSender != userid)) {
                    count++;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    private long getUserID() throws Exception {
        long userid = -1;
        if (_main.mService != null) userid = _main.mService.getUserID();
        if (userid < 0 && _main != null) {
            userid = _main.getBenutzerID();
        }
        return userid;
    }

    public static class GetImageTask extends AsyncTask<Void, Void, Bitmap> {
        private final View view;
        private final Question q;
        private final Answer a;
        private final MainActivity main;
        private Throwable ex;
        private String FileName = null;
        private String BenutzerName;

        public GetImageTask(View view, Question q, Answer a, MainActivity main) {
            this.view = view;
            this.q = q;
            this.a = a;
            this.main = main;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {

            try {
                if (main == null || main.user == null) {
                    this.ex = new Exception("GetImage main or user is null!");
                } else {
                    String fname;
                    String resDownload;
                    Bitmap DownImage = null;
                    fname = ".JPG";
                    Long BenutzerID = a != null ? a.BenutzerID : q.BenutzerID;
                    BenutzerName = a != null ? a.BenutzerName : q.Benutzername;
                    FileName = "Image" + BenutzerID + fname;
                    long ID = main.user.getLong(Constants.id);
                    if (main.clsHTTPS.hashImages.containsKey(FileName)) {
                        DownImage = main.clsHTTPS.hashImages.get(FileName);
                        return DownImage;
                    }
                    HttpsURLConnection conn;
                    for (int i = 0; i <= 1; i++) {
                        try {
                            URL url;
                            Map<String, String> params = new HashMap<>();
                            params.put("FileName", "" + FileName);
                            params.put(BENUTZER_ID, "" + ID);
                            params.put(ACCESSKEY, main.clsHTTPS.accesskey);

                            url = new URL(main.clsHTTPS.buildURI(MYURLROOT + "download", params, null).toString());
                            HttpsURLConnection.setDefaultHostnameVerifier(new clsHTTPS.NullHostNameVerifier());
                            conn = (HttpsURLConnection) url.openConnection();
                            if (main.clsHTTPS.sslSocketFactory == null)
                                main.clsHTTPS.sslSocketFactory = (SSLSocketFactory) getSSLFactory(0, main.clsHTTPS.context)[0];
                            conn.setSSLSocketFactory(main.clsHTTPS.sslSocketFactory);

                            conn.setRequestMethod(GET);
                            //Map<String, List<String>> headers = conn.getHeaderFields();
                            //conn.setDoOutput(true);
                            //conn.setDoInput(true);
                            conn.setUseCaches(false);

                            conn.setRequestProperty("Connection", "Keep-Alive");
                            conn.setRequestProperty("Cache-Control", "no-cache");
                            //conn.setRequestProperty(
                            //        "Content-Type", "multipart/form-data;boundary=" + boundary);
                            conn.setRequestProperty(
                                    "Content-Type", "application/octet-stream");

                            conn.setRequestProperty("Accept", "*/*");

                            // InputStream inputStream0 = conn.getInputStream();

                            // Start content wrapper:

                            int responseCode = conn.getResponseCode();

                            // always check HTTP response code first
                            if (responseCode == HttpURLConnection.HTTP_OK) {
                                String fileName = "";
                                String disposition = conn.getHeaderField("Content-Disposition");
                                String contentType = conn.getContentType();
                                int contentLength = conn.getContentLength();

                                if (disposition != null) {
                                    // extracts file name from header field
                                    int index = disposition.indexOf("filename=");
                                    if (index > 0) {
                                        fileName = disposition.substring(index + 9,
                                                disposition.length());
                                    }
                                } else {
                                    // extracts file name from URL
                                    //fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                                    // fileURL.length());
                                }

                                System.out.println("Content-Type = " + contentType);
                                System.out.println("Content-Disposition = " + disposition);
                                System.out.println("Content-Length = " + contentLength);
                                System.out.println("fileName = " + fileName);

                                // opens input stream from the HTTP connection
                                InputStream inputStream = conn.getInputStream();

                                DownImage = BitmapFactory.decodeStream(inputStream);
                                inputStream.close();
                                conn.disconnect();
                                resDownload = "";
// Print response
                                //SSLContext context = SSLContext.getInstance("TLS");
                                //context.init(null, new X509TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
                                //HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
                            } else {
                                int status = responseCode;
                                InputStream iis;
                                boolean err = false;
                                if (("" + status).startsWith("4") || ("" + status).startsWith("5")) {
                                    iis = conn.getErrorStream();
                                    err = true;
                                } else {
                                    iis = (conn.getInputStream());
                                }
                                Scanner s = new Scanner(iis, clsHTTPS.UTF_8).useDelimiter(Delimiter_A);
                                String result = s.hasNext() ? s.next() : "";
                                if (err) result = ERROR + status + ": " + result;
                                iis.close();
                                conn.disconnect();
                                resDownload = result;// sbline.toString();
                            }
                        } catch (SSLHandshakeException | SocketException e) {
                            //Log.e(TAG, null,,e)
                            resDownload = ERROR + e.getMessage();
                        } catch (Throwable e) {
                            //Log.e(TAG, null,,e)
                            resDownload = ERROR + e.getMessage();
                        }


                        if (resDownload == null)
                            resDownload = main.getString(R.string.zZtkeineVerb);

                        if (resDownload.startsWith(ERROR_UNABLE_TO_RESOLVE_HOST) && main.clsHTTPS.handler != null && main.clsHTTPS.context != null) {
                            Message msg = main.clsHTTPS.handler.obtainMessage(Constants.MESSAGE_TOAST);
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.TOAST, main.getString(R.string.noroutetohost));
                            msg.setData(bundle);
                            main.clsHTTPS.handler.sendMessage(msg);
                        }
                        if (resDownload.startsWith(ERROR)) {
                            if (resDownload.contains(BENUTZER_NICHT_ANGEMELDET) || resDownload.contains(DIE_IDENTITÄT_KONNTE_NICHT_ÜBERPRÜFT_WERDEN)) {
                                /*Message msg = main.clsHTTPS.handler.obtainMessage(Constants.MESSAGE_RELOGIN);
                                main.clsHTTPS.handler.sendMessage(msg);
                                continue;
                                */
                                lib.setStatusAndLog(main, TAG, resDownload);
                            }
                            throw new
                                    Exception(resDownload);
                        }
                        main.clsHTTPS.hashImages.put(FileName, DownImage);
                        return DownImage;
                    }

                }
            } catch (Throwable e) {
                if (!e.getMessage().contains("Error: File not found!")) {
                    e.printStackTrace();
                    Log.i(TAG, null, e);
                    this.ex = e;
                    lib.ShowException(TAG, main, ex, "Load Image", false);
                    return null;
                }
            }
            //if (FileName != null) main.clsHTTPS.hashImages.put(FileName, main.VNLOGO);

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap in) {
            ImageView iv = null;
            TextView tv = null;
            if (a == null) {
                iv = (ImageView) view.findViewById(R.id.imgQuestion);
                tv = view.findViewById(R.id.txtQuestion);
            } else {
                iv = (ImageView) view.findViewById(R.id.imgAnswer);
                tv = view.findViewById(R.id.txtAntwort);
            }
            try {
                if (iv != null) {
                    if (in == null) {
                        final Resources res = main.getResources();
                        final int tileSize = res.getDimensionPixelSize(R.dimen.letter_tile_size);

                        final LetterTileProvider tileProvider = new LetterTileProvider(main);
                        final Bitmap letterTile = tileProvider.getLetterTile(BenutzerName, BenutzerName, tileSize, tileSize, true);
                        in = letterTile;
                        if (in != null) main.clsHTTPS.hashImages.put(FileName, in);
                    }

                    if (in != null) {
                        iv.setImageBitmap(in);
                    }
                    if (this.ex != null) {
                        tv.setError(ex.getMessage());
                    }
                } else {
                    throw new Exception("ImageView not found!");
                }
            } catch (Throwable ex) {
                Log.i(TAG, null, ex);
            }
        }
    }


    public class Group {
        public View groupview;
        public Question question;
        public int groupposition;

        public Group(View groupview, Question q, int groupposition) {
            this.groupposition = groupposition;
            this.groupview = groupview;
            this.question = q;
        }

        public Long getBenutzerFragenID() {
            return question.ID;
        }
    }

    public String GetFolderItemsTaskTalk(View groupview, Question q, int groupposition) {
        Group g = new Group(groupview, q, groupposition);
        String res;
        try {
            int i = 0;
            boolean rres = false;
            do {
                    /*
                    g.question.items.clear();
                    g.question.fetched = false;
                    if (_main.groups.containsKey(g.question.ID))
                    {
                        _main.groups.remove(_main.groups.get(g.question.ID));
                    }
                    */
                rres = getAnswersForQuestionTalk(g, i);
                i++;
                if (!rres) Thread.sleep(200);
            } while (!rres && i <= 10);

            if (i >= 10) {
                throw new Exception("user == null");
            }
            res = TRUE;

        } catch (Throwable e) {
            e.printStackTrace();
            res = "Error: " + e.getMessage();
            Log.e(TAG, null, e);
            lib.ShowException(TAG, context, e, false);
        }
        return res;
    }


    //private SynchronizationContext SyncContext;
    public QuestionsAdapter(final Fragment context, final List<Question> List) {
        //setThreadPolicy();
        this.fragment = context;
        this.context = context.getContext();
        dtFormatDay = new SimpleDateFormat(this.context.getString(R.string.dtFormatDay));
        dtFormatDayTime = new SimpleDateFormat(this.context.getString(R.string.dtFormatDayTime));
        //this.staticContext = this.context;
        myApp = (fragQuestions) fragment;
        _main = (MainActivity) myApp.getActivity();
        if (this.context == null) return;
        {
            QuestionColors = new int[]{ContextCompat.getColor(this.context, R.color.level0),
                    ContextCompat.getColor(this.context, R.color.level1),
                    ContextCompat.getColor(this.context, R.color.level2),
                    ContextCompat.getColor(this.context, R.color.level3),
                    ContextCompat.getColor(this.context, R.color.level4),
                    ContextCompat.getColor(this.context, R.color.level5)};
            LevelColors = new int[]{ContextCompat.getColor(this.context, R.color.level0solid),
                    ContextCompat.getColor(this.context, R.color.level1solid),
                    ContextCompat.getColor(this.context, R.color.level2solid),
                    ContextCompat.getColor(this.context, R.color.level3solid),
                    ContextCompat.getColor(this.context, R.color.level4solid),
                    ContextCompat.getColor(this.context, R.color.level5solid)};
            this.rows = List;
            statistics();
            lv = ((fragQuestions) context).lv;
            //lv.setGroupIndicator(R.drawable.selector);
            lv.setOnScrollListener(onScrollListener);
            lv.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
                @Override
                public void onGroupCollapse(int i) {
                    if (blnDontSetCollapsed) return;
                    if (i < rows.size()) {
                        Question Folder = rows.get(i);
                        Folder.expanded = false;
                        String Name = Folder.Frage;
                    }
                    boolean blnRemoved = false;


                }
            });

            imgQuestionClicklistener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((View) v.getParent()).performLongClick();
                    return;
                }
            };

            imgIndicatorClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int groupID = (int) v.getTag();
                    if (groupID < rows.size()) {
                        Question q = rows.get(groupID);
                        if (lv.isGroupExpanded(groupID)) {
                            lv.collapseGroup(groupID);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                ((ImageView) v).setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage), _main.getTheme()));
                            } else {
                                ((ImageView) v).setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
                            }
                        } else if (q.items != null && q.items.size() > 0) {
                            lv.expandGroup(groupID);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                ((ImageView) v).setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open), _main.getTheme()));
                            } else {
                                ((ImageView) v).setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open)));
                            }
                        } else {
                            ((View) v.getParent()).performLongClick();
                        }
                    }
                }
            };

            imgIndicatorLongClickListener = new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ((View) v.getParent()).performLongClick();
                    return true;
                }
            };

            onGroupClickListener = new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int i = (int) view.getTag();
                    if (i < rows.size()) {
                        Question q = rows.get(i);
                        _main.currentQuestion = q;
                        q.selected ^= true;
                        myApp.lastGroupPosition = -1;
                        if (q.selected) {
                            if (!(view instanceof ImageView) && view.getParent().getParent().getParent() instanceof LinearLayout) {
                                ((View) view.getParent().getParent().getParent()).setBackgroundResource(R.color.colorQuestionSelected);
                            } else if (!(view instanceof ImageView) && view.getParent().getParent() instanceof LinearLayout) {
                                ((View) view.getParent().getParent()).setBackgroundResource(R.color.colorQuestionSelected);
                            } else {
                                ((View) view.getParent()).setBackgroundResource(R.color.colorQuestionSelected);

                            }

                        } else if (q.DatumEnde.before(Calendar.getInstance().getTime())) {

                            if (!(view instanceof ImageView) && view.getParent().getParent().getParent() instanceof LinearLayout) {
                                ((View) view.getParent().getParent().getParent()).setBackgroundResource(R.color.colorExpired);
                            } else if (!(view instanceof ImageView) && view.getParent().getParent() instanceof LinearLayout) {
                                ((View) view.getParent().getParent()).setBackgroundResource(R.color.colorExpired);
                            } else {
                                ((View) view.getParent()).setBackgroundResource(R.color.colorExpired);
                            }

                        } else {

                            if (!(view instanceof ImageView) && view.getParent().getParent().getParent() instanceof LinearLayout) {
                                ((View) view.getParent().getParent().getParent()).setBackgroundResource(R.color.colorBGFrage); //(q.Grad >= 0 ? QuestionColors[q.Grad] : ContextCompat.getColor(QuestionsAdapter.this.context, R.color.colorGradMinus));
                            } else if (!(view instanceof ImageView) && view.getParent().getParent() instanceof LinearLayout) {
                                ((View) view.getParent().getParent()).setBackgroundResource(R.color.colorBGFrage); //(q.Grad >= 0 ? QuestionColors[q.Grad] : ContextCompat.getColor(QuestionsAdapter.this.context, R.color.colorGradMinus));
                            } else {
                                ((View) view.getParent()).setBackgroundResource(R.color.colorBGFrage); //(q.Grad >= 0 ? QuestionColors[q.Grad] : ContextCompat.getColor(QuestionsAdapter.this.context, R.color.colorGradMinus));
                            }

                        }
                        if (_main.fPA != null) {
                            fragAnswer fragAnswer = (fragAnswer) _main.fPA.findFragment(de.com.limto.limto1.fragAnswer.fragID);
                            if (fragAnswer != null) fragAnswer.init();
                        }
                        //if (q.selected) return true;
                    } else {
                        try {
                            boolean blnMore;
                            if (_main.lastLimit < -1) blnMore = false;
                            else blnMore = true;

                            myApp.initFragQuestions(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                        } catch (Exception e) {
                            Log.e(TAG, null, e);
                            e.printStackTrace();
                        }

                    }

                    return true;
                }
/*
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                //int i = (int) view.getTag();
                if (i < rows.size()) {
                    Question q = rows.get(i);
                    _main.currentQuestion = q;
                    q.selected ^= true;
                    myApp.lastGroupPosition = -1;
                    if (q.selected) {
                        view.setBackgroundColor(QuestionsAdapter.this.context.getResources().getColor(R.color.colorQuestionSelected));
                    } else if (q.DatumEnde.before(Calendar.getInstance().getTime())) {
                        view.setBackgroundColor(Color.DKGRAY);
                    } else {
                        view.setBackgroundColor(Color.BLACK);
                    }
                    if (_main.fPA != null) {
                        fragAnswer fragAnswer = (fragAnswer) _main.fPA.findFragment(de.org.Limto.limindo2.fragAnswer.fragID);
                        if (fragAnswer != null) fragAnswer.init();
                    }
                    if (q.selected) return true;
                } else {
                    try {
                        boolean blnMore;
                        if (_main.lastLimit < -1) blnMore = false;
                        else blnMore = true;

                        myApp.init(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                    } catch (Exception e) {
                        Log.e(TAG, null, e);
                        e.printStackTrace();
                    }

                }

                return false;
            }*/
            };

            onGroupClickListenerAll = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int i = (int) view.getTag();
                    if (i < rows.size()) {
                        if (i >= 0) {
                            Question q = rows.get(i);
                            _main.currentQuestion = q;
                            _main.loadcurrentquestion = false;
                            _main.currentgroupid = i;
                            _main.currentAnswer = null;
                            Answer a = null;
                            _main.currentAnswerAnswer = a;

                            _main.mPager.setCurrentItem(fragAnswer.fragID);
                            if (_main.fPA != null) {
                                fragAnswer fA = (fragAnswer) _main.fPA.findFragment(fragAnswer.fragID);
                                if (fA != null) {
                                    fA.init();
                                }
                            }
                        }
                        //if (q.selected) return true;
                    } else {
                        try {
                            boolean blnMore;
                            if (_main.lastLimit < -1) blnMore = false;
                            else blnMore = true;

                            myApp.initFragQuestions(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                        } catch (Exception e) {
                            Log.e(TAG, null, e);
                            e.printStackTrace();
                        }

                    }

                }
/*
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                //int i = (int) view.getTag();
                if (i < rows.size()) {
                    Question q = rows.get(i);
                    _main.currentQuestion = q;
                    q.selected ^= true;
                    myApp.lastGroupPosition = -1;
                    if (q.selected) {
                        view.setBackgroundColor(QuestionsAdapter.this.context.getResources().getColor(R.color.colorQuestionSelected));
                    } else if (q.DatumEnde.before(Calendar.getInstance().getTime())) {
                        view.setBackgroundColor(Color.DKGRAY);
                    } else {
                        view.setBackgroundColor(Color.BLACK);
                    }
                    if (_main.fPA != null) {
                        fragAnswer fragAnswer = (fragAnswer) _main.fPA.findFragment(de.org.Limto.limindo2.fragAnswer.fragID);
                        if (fragAnswer != null) fragAnswer.init();
                    }
                    if (q.selected) return true;
                } else {
                    try {
                        boolean blnMore;
                        if (_main.lastLimit < -1) blnMore = false;
                        else blnMore = true;

                        myApp.init(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                    } catch (Exception e) {
                        Log.e(TAG, null, e);
                        e.printStackTrace();
                    }

                }

                return false;
            }*/
            };


            onGroupClickListenerAntwort = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int[] Position = (int[]) view.getTag();
                    int groupPosition = Position[0];
                    int childPosition = Position[1];
                    if (groupPosition < rows.size()) {
                        if (groupPosition >= 0) {
                            Question q = rows.get(groupPosition);
                            if (q.isVOTE()) return;
                            _main.currentQuestion = q;
                            _main.loadcurrentquestion = false;
                            _main.currentgroupid = groupPosition;
                            _main.currentchildid = childPosition;
                            _main.currentAnswer = null;
                            Answer a = null;
                            if (childPosition >= 0) a = q.items.get(childPosition);
                            _main.currentAnswerAnswer = a;

                            _main.mPager.setCurrentItem(fragAnswer.fragID);
                            if (_main.fPA != null) {
                                fragAnswer fA = (fragAnswer) _main.fPA.findFragment(fragAnswer.fragID);
                                if (fA != null) {
                                    fA.init();
                                }
                            }
                        }
                        //if (q.selected) return true;
                    } else {
                        try {
                            boolean blnMore;
                            if (_main.lastLimit < -1) blnMore = false;
                            else blnMore = true;

                            myApp.initFragQuestions(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                        } catch (Exception e) {
                            Log.e(TAG, null, e);
                            e.printStackTrace();
                        }

                    }

                }
            };
        }

        //lv.setOnGroupClickListener(onGroupClickListener);

        lv.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener()
        {
            @Override
            public void onGroupExpand(int groupPosition)
            {
                Question Folder = null;
                if (groupPosition < rows.size())
                {
                    Folder = rows.get(groupPosition);
                    Folder.expanded = true;
                    myApp.lastGroupPosition = groupPosition;
                }
                //lib.ShowToast(HBCIAdapter.this.context, "Expanding group " + groupPosition + " " + rows.get(groupPosition).Name);
            }
        });


        //((_MainActivity)context).lv.setOverScrollMode(View.OVER_SCROLL_NEVER);
        //SyncContext = (SynchronizationContext.Current != null) ? SynchronizationContext.Current : new SynchronizationContext();
    }

    // Indexes are used for IDs:

    @Override
    protected void finalize() throws Throwable {
        //executor.shutdown();
        super.finalize();
    }

    ;

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }
    //---------------------------------------------------------------------------------------
    // Group methods:

    @Override
    public long getGroupId(int groupPosition) {
        // The index of the group is used as its ID:
        return groupPosition;
    }

    // Return the number of produce ("vegetables", "fruitimport com.microsoft.live.LiveOperationException;s", "herbs") objects:
    private int _lastGroupCount;

    @Override
    public int getGroupCount() {
        _lastGroupCount = rows.size();
        return _lastGroupCount;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        // Recycle a previous view if provided:
        //lib.LastgroupPosition= groupPosition;
        View view = convertView;
        lib.Ref<Boolean> blnNew = new lib.Ref<Boolean>(false);
        // If no recycled view, inflate a new view as a simple expandable list item 1:
        View v = null;
        if ((v = returnViewOnInvalidGroupposition(groupPosition)) != null) return v;
        view = createNewViewIfNecessary(view, blnNew);

        if (groupPosition < 0) return view;

        if (groupPosition < rows.size()) {
            view.setTag(groupPosition);
            Question question = rows.get(groupPosition);
            if (question.ID > -1) {
                LinearLayout llText = (LinearLayout) view.findViewById(R.id.llFrageText);
                llText.setTag(groupPosition);
                setSelectedExpired(view, question);

                ImageView iv = (ImageView) view.findViewById(R.id.imgQuestion);
                ImageView ivIndicator = (ImageView) view.findViewById(R.id.imgIndicator);
                ImageView imgMenu = view.findViewById(R.id.imgMenu);

                IncreaseIVSizeForTabletAndTV(iv, ivIndicator, blnNew);
                ivIndicator.setBackgroundResource(android.R.color.transparent);

                TextView tvFrage = setTextQuestion(view, groupPosition, question, blnNew.get());
                TextView tvBenutzername = setTVBenutzername(view, question, groupPosition);
                TextView tvDate = setDate(view, question, groupPosition);

                setIVPicture(view, iv, question);
                setIVProperties(iv, ivIndicator, imgMenu, groupPosition);

                String stxtDate = tvDate.getText().toString();
                Date dt = question.DatumNeu;

                setImageViewsAndTVDate(question, dt, iv, ivIndicator, lv, tvDate, groupPosition, stxtDate);

                TextView tvDist = setTextDist(view, groupPosition, question);
                TextView tvGrad = setTextGrad(view, groupPosition, question);
                getAnswers(view, question, groupPosition, tvFrage);

            } else { // id < 0
                TextView tvFrage = (TextView) view.findViewById(R.id.txtQuestion);
                setFrageText(blnNew, question, tvFrage);
                new GetAnswersForQuestionTask(new Group(view, question, groupPosition)).execute();
            }

            view.setVisibility(View.VISIBLE);

            // if groupcount is less than position load next 50 questions
            if (this.getGroupCount() - 1 <= groupPosition && _main.lastLimit >= 0 && _main.lastEndGrad != 0) {
                boolean blnMore;
                blnMore = true;
                try {
                    ((fragQuestions) fragment).initFragQuestions(true, true, -1, -1, null, null, null, _main.lastLimit + 50, blnMore);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //if (question.isSystemMessage()) lv.expandGroup(groupPosition);
            return view;
        } else {
            Log.e(TAG, "groupPosition " + groupPosition + " not found! LastGroupCount: " + _lastGroupCount);
            //return null;
            view.setVisibility(View.GONE);
        }

        return view;

        //if (blnNew) textView.setTextSize(lib.convertFromDp(context.getApplicationContext(), textView.getTextSize()));

    }

    private void setFrageText(lib.Ref<Boolean> blnNew, Question question, TextView tvFrage) {
        if (blnNew.get() && (_main.isTablet || _main.isTV))
            tvFrage.setTextSize(TypedValue.COMPLEX_UNIT_PX, tvFrage.getTextSize() * 1.5f);
        Spanned Frage;
        if (question.isVOTE()) {
            Frage = lib.fromHtml("<b>" + _main.getString(R.string.vote) + "</b> " + question.getFrage(false));
            tvFrage.setText(Frage);
        } else {
            tvFrage.setText(question.getFrage(false));
        }

    }

    private void IncreaseIVSizeForTabletAndTV(ImageView iv, ImageView ivIndicator, lib.Ref<Boolean> blnNew) {
        if (blnNew.get() && (_main.isTablet || _main.isTV)) {
            iv.getLayoutParams().width = lib.dpToPx(50);
            iv.getLayoutParams().height = lib.dpToPx(50);
            float size = lib.getDimensionFromTheme(context, R.attr.indicatorsizetablet);
            int sizepx = (int) size;
            ivIndicator.getLayoutParams().width = sizepx;
            ivIndicator.getLayoutParams().height = sizepx;
        }
    }

    private View createNewViewIfNecessary(View view, lib.Ref<Boolean> blnNew) {
        if (view == null || view.findViewById(R.id.more) != null) {
            LayoutInflater tempVar = this.fragment.getActivity().getLayoutInflater();
            view = tempVar.inflate(lib.getLayoutResFromTheme(context, R.attr.view_fragen), null);

            blnNew.set(true);

        }
        return view;
    }

    private View returnViewOnInvalidGroupposition(int groupPosition) {
        if (groupPosition >= rows.size()) {
            LayoutInflater tempVar = LayoutInflater.from(context);
            View view = tempVar.inflate(R.layout.view_fragenmehr, null);
            return view;
        }
        return null;
    }

    private void setIVProperties(ImageView iv, ImageView ivIndicator, ImageView imgMenu, int groupPosition) {
        iv.setTag(groupPosition);
        ivIndicator.setTag(groupPosition);
        ivIndicator.setOnClickListener(this.imgIndicatorClickListener);
        ivIndicator.setOnLongClickListener(this.imgIndicatorLongClickListener);
        iv.setLongClickable(true);
        iv.setOnLongClickListener(this.onGroupClickListener);
        iv.setOnClickListener(this.imgQuestionClicklistener);
        imgMenu.setOnClickListener(imgQuestionClicklistener);

    }

    private void getAnswers(View view, Question question, int groupPosition, TextView tvFrage) {
        if (this._main != null && this._main.mService != null &&
                this._main.mService.getState() == LimindoService.STATE_CONNECTED &&
                this._main.mHandler == _main.mService.mHandler) {
            String res = GetFolderItemsTaskTalk(view, question, groupPosition);
            if (!res.equals(TRUE)) {
                tvFrage.setError(res.replace(ERROR, "Fehler:"));
            } else {
                statistics();
            }
        } else {
            if (_main != null) {
                _main.bindService0();
                if (_main.mService == null) Log.e("GetFolderItems", "Service null");
                else if (_main.mService.getState() != LimindoService.STATE_CONNECTED)
                    Log.e("GetFolderItems", "Service not connected State" + _main.mService.getState());
                else Log.e("GetFolderItems", "Wrong Handler");
            }
            new GetAnswersForQuestionTask(new Group(view, question, groupPosition)).execute();
        }
    }

    private void statistics() {
        if (myApp.btnEigene.isChecked()) {
            /*
            _main.setOwnQuestions(this.rows.size());
            _main.mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int EigeneFragenBeantwortet = 0;
                    int EigeneFragenUnbeantwortet = 0;
                    int EigeneFragenNotFetched = 0;
                    for (Question q : rows)
                    {
                        if (q.fetched)
                        {
                            if (q.items.size()>0)
                            {
                                EigeneFragenBeantwortet++;
                            }
                            else
                            {
                                EigeneFragenUnbeantwortet ++;
                            }
                        }
                        else
                        {
                            EigeneFragenNotFetched ++;
                        }
                    }
                    _main.setOwnQuestionsAnswered(EigeneFragenBeantwortet);
                    _main.setOwnQuestionsUnanswered(EigeneFragenUnbeantwortet);
                    _main.setOwnQuestionsNotFetched(EigeneFragenNotFetched);


                }
            }, 500);

             */
        }


    }


    private void setImageViewsAndTVDate(Question question, Date dt, ImageView iv, ImageView ivIndicator, ExpandableListView lv, TextView tvDate, int groupPosition, String stxtDate) {
        int colorText = -1;
        boolean bold = false;
        if (question.Fehlergrad != null) {
            tvDate.setText("{" + question.Fehlergrad + "}" + tvDate.getText());
            colorText = (context.getResources().getColor(R.color.colorError));
            iv.setBackgroundResource(R.color.colorError);
            if (dt != null && dt.after(_main.getLastLogoffDate())) {
                colorText = (context.getResources().getColor(R.color.colorNeueAntwort));
                bold = true;
                iv.setBackgroundResource(R.color.colorErrorNeueAntwort);
                if (lv.isGroupExpanded(groupPosition)) {
                    ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten_open));
                } else {
                    ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten));
                }


            } else if (question.DatumStart.after(_main.getLastLogoffDate())) {


                colorText = (context.getResources().getColor(R.color.colorNeueFrage));
                bold = true;
                iv.setBackgroundResource(R.color.colorErrorNeueFrage);
                if (question.items.size() == 0) {
                    ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragen));
                } else if (lv.isGroupExpanded(groupPosition)) {
                    ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
                } else {
                    ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
                }
            }
        } else if (dt != null && dt.after(_main.getLastLogoffDate())) {
            colorText = (context.getResources().getColor(R.color.colorNeueAntwort));
            iv.setBackgroundResource(R.color.colorNeueAntwort);
            bold = true;
            if (lv.isGroupExpanded(groupPosition)) {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten_open));
            } else {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten));
            }

        } else if (question.DatumStart.after(_main.getLastLogoffDate())) {
            colorText = (context.getResources().getColor(R.color.colorNeueFrage));
            iv.setBackgroundResource(R.color.colorNeueFrage);
            if (question.items.size() == 0) {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragen));
            } else if (lv.isGroupExpanded(groupPosition)) {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
            } else {
                ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
            }
            bold = true;
        } else {
            colorText = (context.getResources().getColor(R.color.colorDate));
            //iv.setBackgroundColor(((ColorDrawable)view.getBackground()).getColor());
            iv.setBackground(null);
        }

        int color = (question.Grad >= 0 ? LevelColors[question.Grad] : ContextCompat.getColor(context, R.color.colorGradMinussolid));
        stxtDate = (question.Grad >= 6 ? digits[question.Grad] : "■") + stxtDate;
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(stxtDate);
        wordtoSpan.setSpan(new ForegroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(colorText), 1, wordtoSpan.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (bold || blnAccessibility) {
            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD), 1, wordtoSpan.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        //wordtoSpan.setSpan(new BackgroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvDate.setText(wordtoSpan);

    }

    private TextView setTextGrad(View view, int groupPosition, Question question) {
        TextView textView = (TextView) view.findViewById(R.id.txtGrad);
        //textView.setBackgroundColor(question.Grad >= 0 ? QuestionColors[question.Grad] : ContextCompat.getColor(context, R.color.colorGradMinus));
        textView.setTag(groupPosition);
        textView.setOnClickListener(this.onGroupClickListenerAll);
        textView.setOnLongClickListener(this.onGroupClickListener);
        String grad = _main.getString(R.string.grade) + question.Grad + "(" + question.Kontaktgrad + ")";
        int lGrad = grad.length();
        //grad += " " + question.Locale;
        int colorGrad = question.Grad >= 0 ? QuestionColors[question.Grad] : ContextCompat.getColor(context, R.color.colorGradMinus);
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(grad);
        wordtoSpan.setSpan(new BackgroundColorSpan(colorGrad), 0, lGrad, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        //wordtoSpan.setSpan(new ForegroundColorSpan(colorText), 1, wordtoSpan.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(wordtoSpan);
        return textView;
    }

    private TextView setTextDist(View view, int groupPosition, Question question) {
        Location Loc = ((MainActivity) fragment.getActivity()).locCoarse();
        TextView textView = (TextView) view.findViewById(R.id.txtDistance);
        textView.setTag(groupPosition);
        textView.setOnClickListener(this.onGroupClickListenerAll);
        textView.setOnLongClickListener(this.onGroupClickListener);
        if (question.Breitengrad != null && question.Laengengrad != null && Loc != null) {
            long dist = Math.round(lib.distanceInKm(question.Breitengrad, question.Laengengrad, Loc.getLatitude(), Loc.getLongitude()));
            textView.setText(dist + _main.getString(R.string.km));
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
        return textView;
    }

    private void setIVPicture(View view, ImageView iv, Question question) {
        if (question.emergency != null && question.emergency) {
            iv.setImageResource(R.drawable.sos);
        } else if (question.gemeldet) {
            iv.setImageResource(R.drawable.vn_logo_a03);
        } else {
            iv.setImageResource(R.drawable.vn_logo_c02);
            new GetImageTask(view, question, null, _main).execute();
        }

    }

    private TextView setDate(View view, Question question, int groupPosition) {
        Date dt = question.DatumStart;
        TextView textView = (TextView) view.findViewById(R.id.txtDate);
        textView.setTag(groupPosition);
        textView.setOnClickListener(this.onGroupClickListenerAll);
        textView.setOnLongClickListener(this.onGroupClickListener);
        textView.setText((question.Grad < 0 ? question.ID : "") + (question.oeffentlich != null && question.oeffentlich ? _main.getString(R.string.__public) : _main.getString(R.string.__private)) + dtFormatDayTime.format(dt));
        return textView;

    }

    private TextView setTVBenutzername(View view, Question question, int groupPosition) {
        String BenutzerName = question.Benutzername;
        TextView textView = (TextView) view.findViewById(R.id.txtBenutzername);
        textView.setTag(groupPosition);
        textView.setOnClickListener(this.onGroupClickListenerAll);
        textView.setOnLongClickListener(this.onGroupClickListener);
        Integer Bewertungen = null;
        Bewertungen = question.Bewertungen;
        if (Bewertungen != null) BenutzerName += " *" + Bewertungen + "*";
        BenutzerName += " " + question.onlineState.getStateString();
        textView.setText(BenutzerName);
        if (question.online && question.onlineState.state > MainActivity.OnlineState.invisible.state)
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorUserOnline));
        else textView.setTextColor(context.getResources().getColor(R.color.colorUser));
        return textView;
    }

    private TextView setTextQuestion(View view, int groupPosition, Question question, boolean blnNew) {
        TextView textView = (TextView) view.findViewById(R.id.txtQuestion);
        textView.setTag(groupPosition);
        textView.setOnClickListener(this.onGroupClickListenerAll);
        textView.setOnLongClickListener(this.onGroupClickListener);
        textView.setTextColor(ContextCompat.getColor(context, R.color.colorGroupText));//(question.Grad >= 0 ? QuestionColors[question.Grad] : ContextCompat.getColor(context,R.color.color.GradMinus));
        if (question.emergency != null && question.emergency)
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorEmergency));
        if (blnNew && (_main.isTablet || _main.isTV))
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textView.getTextSize() * 1.5f);
        String text = question.getFrage(false);
        int link = text.toLowerCase().indexOf("<a href=");
        Spanned Frage;
        if (question.isVOTE()) {
            Frage = lib.fromHtml("<b>" + _main.getString(R.string.vote) + "</b> " + question.getFrage(false));
        } else {
            Frage = new SpannableString(text);
        }
        if (link > -1) {
            String html = text.substring(link, text.toLowerCase().indexOf("</a>") + 4);
            text = text.replace(html, "");
            SpannableStringBuilder sb = new SpannableStringBuilder();
            if (question.isVOTE()) {
                sb.append(lib.fromHtml("<b>" + _main.getString(R.string.vote) + "</b> "));
            } else {
                //Frage = new SpannableString(text);
            }
            sb.append(text);
            Spanned url;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                url = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
            } else {
                url = Html.fromHtml(html);
            }
            sb.insert(link, url);
            textView.setAutoLinkMask(0);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setText(sb, TextView.BufferType.SPANNABLE);
            textView.setOnClickListener(null);
        } else {
            textView.setText(Frage);
        }
        return textView;
    }

    private void setSelectedExpired(View view, Question question) {
        if (question.selected) {
            view.setBackgroundColor(context.getResources().getColor(R.color.colorQuestionSelected));
        } else if (question.DatumEnde.before(Calendar.getInstance().getTime())) {
            view.setBackgroundResource(R.color.colorExpired);
        } else {
            view.setBackgroundResource(R.color.colorBGFrage);
            //view.setBackgroundColor(question.Grad >= 0 ? QuestionColors[question.Grad] : ContextCompat.getColor(context, R.color.colorGradMinus));
        }

    }

    private void setImageViews(View view, boolean blnNew) {

    }

    @Override
    public Object getGroup(int groupPosition) {
        if (groupPosition < rows.size()) return rows.get(groupPosition);
        return null;
    }

    //---------------------------------------------------------------------------------------
    // Child methods:

    @Override
    public long getChildId(int groupPosition, int childPosition) {        // The index of the child is used as its ID:
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // Return the number of children (produce item objects) in the group (produce object):
        if (groupPosition < rows.size()) {
            Question Folder = rows.get(groupPosition);
            try {
                getAnswersForQuestion(Folder, groupPosition);
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                lib.ShowException(TAG, context, e, "GetFolderItems", false);
            }
            int res = Folder.items.size();
            //System.out.println(res);
            return res;
        } else {
            return 0;
        }
    }


    private static class ItemParams {
        public Answer item;
        public ImageView img;
        public View view;

        public ItemParams(Answer item, ImageView img, View view) {
            this.item = item;
            this.img = img;
            this.view = view;
        }
    }

    private static class ItemParamsSet {
        public ImageView IView;
        public Bitmap img;
        public View view;

        public ItemParamsSet(ImageView IView, Bitmap img, View view) {
            this.IView = IView;
            this.img = img;
            this.view = view;
        }
    }

    public Cursor ServiceCursor;
    public int CountVisible = 0;
    /*
        private OnLongClickListener ImgOnLongClickListener = new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                View vv = (View) v.getParent().getParent();
                if (vv.getTag() != null) {
                    ViewHolder holder = (ViewHolder) (vv.getTag());
                    final Answer Answer = holder.item;
                    myApp.setLastItem(Answer);
                    myApp.lastGroupPosition = holder.groupPosition;
                }
                return true;
            }
        };
QuestionColors = new int[]{ContextCompat.getColor(this.context, R.color.level0),
                    ContextCompat.getColor(this.context, R.color.level1),
                    ContextCompat.getColor(this.context, R.color.level2),
                    ContextCompat.getColor(this.context, R.color.level3),
                    ContextCompat.getColor(this.context, R.color.level4),
                    ContextCompat.getColor(this.context, R.color.level5)};

        private View.OnClickListener ImgClick = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                View vv = (View) v.getParent().getParent();
                if (vv.getTag() != null) {
                    ViewHolder holder = (ViewHolder) (vv.getTag());
                    Answer Answer = holder.item;
                    myApp.setLastItem(Answer);
                    myApp.lastGroupPosition = holder.groupPosition;

                }

            }
        };
        */
    public SimpleDateFormat dtFormatDay;
    public SimpleDateFormat dtFormatDayTime;
    public static NumberFormat numberFormatter = NumberFormat.getCurrencyInstance(Locale.getDefault());


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        // Recycle a previous view if provided:
        lib.setgstatus("GetChildview Start");
        View view = convertView;
        boolean isNewView = false;
        if (view == null) {
            lib.setgstatus("GetChildview Inflate");
            view = LayoutInflater.from(context).inflate(lib.getLayoutResFromTheme(context, R.attr.view_antworten), null);
            isNewView = true;
        }
        if (rows.size() <= groupPosition) {
            return view;
        }
        Question Folder = rows.get(groupPosition);
        try {
            getAnswersForQuestion(Folder, groupPosition);
        } catch (Throwable e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
        }
        if (Folder.items.size() <= childPosition) {
            return view;
        }
        final Answer answer = Folder.items.get(childPosition);
        // If no recycled view, inflate a new view as a simple expandable list item 2:
        if (answer.Grad == -10) {
            try {
                answer.Grad = _main.clsHTTPS.getKontaktgrad(5, _main.user.getLong(Constants.id), answer.BenutzerID);
            } catch (Exception e) {
                e.printStackTrace();
                lib.ShowException(TAG, context, e, "GetKontaktgrad", false);
            }
        }
        try {

            lib.setgstatus("GetChildview ImageView");
            //if (isNewView) view.setMinimumHeight((int) lib.convertFromDp(context.getApplicationContext(), view.getHeight()));

            boolean ItemExists = false;
            //view.setBackgroundColor(item.Grad >= 0 ? QuestionColors[item.Grad] : ContextCompat.getColor(context, R.color.colorGradMinus));

            int offset = 0;
            if (_main.isTablet || _main.isTV) offset = 10;
            try {
                LinearLayout llAntwort = view.findViewById(R.id.llAntwort);
                ViewGroup.LayoutParams p = llAntwort.getLayoutParams();
                if (answer.ParentFrageAntwortenID != null) {
                    if (answer.reAntwortID == 0) {
                        llAntwort.setPadding(lib.dpToPx(75 + offset), llAntwort.getPaddingTop(), llAntwort.getPaddingRight(), llAntwort.getPaddingBottom());
                    } else {
                        llAntwort.setPadding(lib.dpToPx(75 + offset + answer.level * 20), llAntwort.getPaddingTop(), llAntwort.getPaddingRight(), llAntwort.getPaddingBottom());
                    }

                } else {
                    llAntwort.setPadding(lib.dpToPx(55 + offset), llAntwort.getPaddingTop(), llAntwort.getPaddingRight(), llAntwort.getPaddingBottom());
                }
                //float size = text1Size * .7f;

                setSelected(answer, view);

                TextView txtDate = setTxtDate(answer, view, groupPosition, childPosition);

                ImageView iv = (ImageView) view.findViewById(R.id.imgAnswer);
                ImageView imgMenu = view.findViewById(R.id.imgMenu);

                setImageViewsAntwort(answer, view, isNewView, iv, imgMenu);

                setTxtSender(view, groupPosition, childPosition, answer);

                setTxtEntfernung(view, groupPosition, childPosition, answer);

                setTxtAntwort(view, groupPosition, childPosition, answer, isNewView, offset);

                setTxtGrad(view, groupPosition, childPosition, answer);

                lib.setgstatus("GetChildview Finished");
                view.setTag(answer);
                answer.setView(view);
            } catch (Throwable ex) {
                lib.ShowException(TAG, context, ex, TAG, false);
                ex.printStackTrace();
            }
            /*
            if (this.getGroupCount() - 1 <= groupPosition && _main.lastLimit >= 0)
            {
                boolean blnMore;
                if (_main.lastLimit < 0) blnMore = false;
                else blnMore = true;

                ((fragQuestions) fragment).init(true, true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
            }
            */
            return view;
        } catch (Throwable ex) {
            lib.ShowException(TAG, context, ex, false);
            return view;
        }

    }

    private void setSelected(Answer answer, View view) {
        if (answer.selected) {
            view.setBackgroundColor(context.getResources().getColor(R.color.colorQuestionSelected));
        } else {
            view.setBackgroundResource(R.color.colorBGFrage);
        }
    }

    private void setTxtGrad(View view, int groupPosition, int childPosition, Answer item) {
        TextView txtValue = (TextView) view.findViewById(R.id.txtGrad);
        txtValue.setTag(new int[]{groupPosition, childPosition});
        txtValue.setOnClickListener(this.onGroupClickListenerAntwort);
        String grad = (item.ParentFrageAntwortenID != null
                && !(item.oeffentlich != null && item.oeffentlich)
                ? _main.getString(R.string.privat)
                : item.Grad >= 0 ? _main.getString(R.string.grade) + item.Grad : _main.getString(R.string.nograde));
                /*if (item.Fragen != null) grad += " F" + item.Fragen + "";
                if (item.Antworten != null) grad += " A" + item.Antworten + "";*/
        if (item.Bewertung > 0) grad += " [" + item.Bewertung + "]";
                /*if (item.Antwortzeit != null)
                    grad += " " + DateUtils.formatElapsedTime(item.Antwortzeit);
                */
        txtValue.setBackgroundColor(item.Grad >= 0 ? QuestionColors[item.Grad] : ContextCompat.getColor(context, R.color.colorGradMinus));
        txtValue.setText(grad);
        //txtValue.setTextSize(TypedValue.COMPLEX_UNIT_PX,size);
        txtValue.setTextColor(ContextCompat.getColor(context, R.color.colorGrad));
        //txtValue.setBackgroundColor(color);

    }

    private void setTxtAntwort(View view, int groupPosition, int childPosition, Answer item, boolean isNewView, int offset) {
        TextView txtAntwort = (TextView) view.findViewById(R.id.txtAntwort);
        txtAntwort.setTag(new int[]{groupPosition, childPosition});
        txtAntwort.setOnClickListener(this.onGroupClickListenerAntwort);
        if (isNewView && (_main.isTablet || _main.isTV)) {
            txtAntwort.setTextSize(TypedValue.COMPLEX_UNIT_PX, txtAntwort.getTextSize() * 1.5f);
        }
        if (item.ParentFrageAntwortenID != null) {
            if (item.reAntwortID == 0) {
                item.padding = 75 + offset;
                //txtUsage.setPadding(lib.dpToPx(75 + offset), txtUsage.getPaddingTop(), txtUsage.getPaddingRight(), txtUsage.getPaddingBottom());
            } else {
                //txtUsage.setPadding(lib.dpToPx(75 + offset + item.level * 20), txtUsage.getPaddingTop(), txtUsage.getPaddingRight(), txtUsage.getPaddingBottom());
                item.padding = 95 + offset;
            }

        } else {
            // txtUsage.setPadding(lib.dpToPx(55 + offset), txtUsage.getPaddingTop(), txtUsage.getPaddingRight(), txtUsage.getPaddingBottom());
            item.padding = 55 + offset;
        }

        //if (_main.isWatch) txtUsage.setTextSize(TypedValue.COMPLEX_UNIT_PX,txtUsage.getTextSize() * .7f);

        if (item.Antwort != null) txtAntwort.setText((item.getAnswer()));
        else txtAntwort.setText("");

    }

    private void setTxtEntfernung(View view, int groupPosition, int childPosition, Answer item) {
        TextView txtGV = (TextView) view.findViewById(R.id.txtEntfernung);
        //txtGV.setTextSize(TypedValue.COMPLEX_UNIT_PX,size);
        txtGV.setText(R.string.x);
        txtGV.setTag(new int[]{groupPosition, childPosition});
        txtGV.setOnClickListener(this.onGroupClickListenerAntwort);

        Location loc = ((MainActivity) fragment.getActivity()).locCoarse();
        if (loc != null && item.Breitengrad != null && item.Laengengrad != null) {
            long dist = Math.round(lib.distanceInKm(loc.getLatitude(), loc.getLongitude(), item.Breitengrad, item.Laengengrad));
            txtGV.setText(dist + _main.getString(R.string.km));
        }

    }

    private void setTxtSender(View view, int groupPosition, int childPosition, Answer item) {
        TextView txtSender = (TextView) view.findViewById(R.id.txtBenutzername);
        txtSender.setTag(new int[]{groupPosition, childPosition});
        txtSender.setOnClickListener(this.onGroupClickListenerAntwort);

        String BenutzerName = item.BenutzerName != null ? item.BenutzerName : item.EMail;
        Integer Bewertungen = null;
        Bewertungen = item.Bewertungen;
        if (Bewertungen != null) BenutzerName += " *" + Bewertungen + "*";
        BenutzerName += " " + item.onlineState.getStateString();
        txtSender.setText(BenutzerName);

        if (item.online && item.onlineState.state > MainActivity.OnlineState.invisible.state)
            txtSender.setTextColor(ContextCompat.getColor(context, R.color.colorUserOnline));
        else txtSender.setTextColor(context.getResources().getColor(R.color.colorUser));
        //txtSender.setTextSize(TypedValue.COMPLEX_UNIT_PX,size);

    }

    private TextView setTxtDate(Answer item, View view, int groupPosition, int childPosition) {

        TextView txtDate = (TextView) view.findViewById(R.id.txtDate);
        txtDate.setTag(new int[]{groupPosition, childPosition});
        txtDate.setOnClickListener(this.onGroupClickListenerAntwort);


        String stxtDate = null;
        if (item.Datum != null) {
            stxtDate = (dtFormatDayTime.format(item.Datum));
        } else {
            stxtDate = ("");
        }
        int colorText;
        boolean bold = false;
        if (item.Datum.after(_main.getLastLogoffDate())) {
            colorText = (context.getResources().getColor(R.color.colorNeueAntwort));
            bold = true;
        } else {
            colorText = (ContextCompat.getColor(context, R.color.colorDatum));
        }
        int color = (item.Grad >= 0 ? LevelColors[item.Grad] : ContextCompat.getColor(context, R.color.colorGradMinussolid));
        stxtDate = (item.Grad >= 6 ? digits[item.Grad] : "■") + stxtDate;
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(stxtDate);
        wordtoSpan.setSpan(new ForegroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(colorText), 1, stxtDate.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (bold || blnAccessibility)
            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD), 1, stxtDate.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //wordtoSpan.setSpan(new BackgroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtDate.setText(wordtoSpan);
        if (item.gemeldet) txtDate.setText(item.ID + "!!!" + txtDate.getText());
        //txtDate.setTextSize(TypedValue.COMPLEX_UNIT_PX,size);
        return txtDate;
    }

    private void setImageViewsAntwort(Answer item, View view, boolean isNewView, ImageView iv, ImageView imgMenu) {
        if (isNewView && (_main.isTablet || _main.isTV)) {
            iv.getLayoutParams().width = lib.dpToPx(40);
            iv.getLayoutParams().height = lib.dpToPx(40);
        }
        if (item.emergency != null && item.emergency) {
            iv.setImageResource(R.drawable.sos);
        } else if (item.gemeldet) {
            iv.setImageResource(R.drawable.vn_logo_a03);
        } else {
            iv.setImageResource(R.drawable.vn_logo_c02);
            new GetImageTask(view, null, item, _main).execute();
        }

        iv.setOnClickListener(imgQuestionClicklistener);
        imgMenu.setOnClickListener(imgQuestionClicklistener);

    }


    public String getS(int resid) {
        return context.getString(resid);
    }

    private boolean getFolderItemsLock = false;

    private boolean getAnswersForQuestion(final Question Folder, final int GroupPosition) throws Throwable {
        return getAnswersForQuestion(Folder, GroupPosition, 0);
    }

    private boolean getAnswersForQuestionTalk(final Group g, final int retries) throws Throwable {
        Question Folder = g.question;
        if (Folder != null && !Folder.fetched) {
            if (Folder.ID > -1) {
                boolean changed = false;
                CheckApplicationVariables();

                //Folder.fetched = true;

                lib.LastgroupPosition = g.groupposition;
                try {
                    //if (Folder.fetched == false) {
                    //lib.BMList = new java.util.ArrayList<HBCIListItem>();
                    String res = callAnswersTalk(g);
                    if (res.startsWith(ERROR)) {
                        processError(res, Folder);
                    } else if (res.equals(FALSE)) {
                        processFailure(res, Folder, g);
                    }
                } catch (Throwable e) {
                    processErrorAndThrow(e, Folder);
                }
            } else { //if Folder.ID < 0
                new GetAnswersForQuestionTask(g).execute();
            }
        } else if (Folder != null) { //And Folder is fetched
            //Show number af Answers fetched
            processGroup(true, null, g, null);
        } else {
            Log.e(TAG, GET_FOLDER_ITEMS + FOLDER_IS_NULL);
        }
        //Folder.items = lib.BMList;
        return true;
    }

    private void processErrorAndThrow(Throwable e, Question Folder) throws Throwable {
        e.printStackTrace();
        Folder.fetched = false;
        lib.ShowException(TAG, context, e, false);
        throw e;
    }

    private void showSpecialQuestion(Group g) {
        Question Folder = g.question;
        TextView textView = (TextView) g.groupview.findViewById(R.id.txtGrad);
        textView.setText(Folder.items.size() + _main.getString(R.string.answers));
        ImageView imgIndicator = g.groupview.findViewById(R.id.imgIndicator);
        if (Folder.items.size() > 0) {
            if (lv.isGroupExpanded(g.groupposition)) {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open)));
            } else {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
            }
        } else {
            imgIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_unbeantwortetefrage)));
        }
    }

   /* void showQuestion(Group g) {
        int count = -1;
        long userid = -1;
        if (_main.mService != null) userid = _main.mService.getUserID();
        if (_main.user != null) try {
            userid = _main.user.getLong(Constants.id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (Answer a : g.question.items) {
            try {
                if (count == -1) count = 0;
                if (a.BenutzerID != userid && (a.BenutzerIDSender == null || a.BenutzerIDSender != userid)) {
                    count++;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        int countFremde = count;
        count = - 1;
        if (count == -1) count = g.question.items.size();
        View groupview = g.groupview;
        Question q = g.question;
        TextView textView = (TextView) groupview.findViewById(R.id.txtGrad);
        ImageView iv = (ImageView) groupview.findViewById(R.id.imgQuestion);
        ImageView imgIndicator = groupview.findViewById(R.id.imgIndicator);
        String grad = _main.getString(R.string.grade) + q.Grad + "(" + q.Kontaktgrad + ")";
        int lGrad = grad.length();
        grad += " " + count + " " + _main.getString(R.string.answers);
        int colorGrad = q.Grad >= 0 ? QuestionColors[q.Grad] : ContextCompat.getColor(_main, R.color.colorGradMinus);
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(grad);
        wordtoSpan.setSpan(new BackgroundColorSpan(colorGrad), 0, lGrad, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (count > 0) {
            if (lv.isGroupExpanded(g.groupposition)) {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open)));
            } else {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
            }
        } else {
            imgIndicator.setImageDrawable(context.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_unbeantwortetefrage)));
        }

                /*if (q.Fragen != null) grad += " F" + q.Fragen + "";
                if (q.Antworten != null) grad += " A" + q.Antworten + "";
                if (q.Antwortzeit != null)
                    grad += " " + DateUtils.formatElapsedTime(q.Antwortzeit);
                */
   /*
        textView.setText(wordtoSpan);
        //QuestionsAdapter.this.notifyDataSetChanged();
        Date dt = q.DatumStart;
        textView = (TextView) groupview.findViewById(R.id.txtDate);
        textView.setText(((q.Grad < 0) ? q.ID : "")
                + (((q.oeffentlich != null) && q.oeffentlich)
                ? _main.getString(R.string.__public)
                : _main.getString(R.string.__private))
                + dtFormatDayTime.format(dt));
        //textView.setText(dtFormatDayTime.format(dt));
        boolean blnNeueAntwort = false;
        for (Answer a : q.items) {
            if (a.Datum.after(_main.getLastLogoffDate())) {
                blnNeueAntwort = true;
                break;
            }
        }
        dt = q.DatumNeu;
        if (q.emergency != null && q.emergency) {
            iv.setImageResource(R.drawable.sos);
        } else if (q.gemeldet) {
            iv.setImageResource(R.drawable.vn_logo_a03);
        } else {
            iv.setImageResource(R.drawable.vn_logo_c02);
            new GetImageTask(groupview, q, null, _main).execute();
        }
        int colorText;
        new GetImageTask(groupview, q, null, _main).execute();
        boolean bold = false;
        if (q.Fehlergrad != null) {
            textView.setText("{" + q.Fehlergrad + "}" + textView.getText());
            colorText = (context.getResources().getColor(R.color.colorError));
            iv.setBackgroundResource(R.color.colorError);
            if (dt != null && dt.after(_main.getLastLogoffDate())) {
                colorText = (context.getResources().getColor(R.color.colorNeueAntwort));
                iv.setBackgroundResource(R.color.colorErrorNeueAntwort);
                if (lv.isGroupExpanded(g.groupposition)) {
                    imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten_open));
                } else {
                    imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten));
                }
                bold = true;
            } else if (q.DatumStart.after(_main.getLastLogoffDate())) {
                colorText = (context.getResources().getColor(R.color.colorNeueFrage));
                iv.setBackgroundResource(R.color.colorErrorNeueFrage);
                if (q.items.size() == 0)
                {
                    imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragen));
                }
                else if (lv.isGroupExpanded(g.groupposition)) {
                    imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
                } else {
                    imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
                }
                bold = true;
            }
        } else if (blnNeueAntwort) {
            iv.setBackgroundResource(R.color.colorNeueAntwort);
            if (lv.isGroupExpanded(g.groupposition)) {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten_open));
            } else {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neueantworten));
            }

            colorText = (context.getResources().getColor(R.color.colorNeueAntwort));
            bold = true;
        } else if (q.DatumStart.after(_main.getLastLogoffDate())) {
            iv.setBackgroundResource(R.color.colorNeueFrage);
            if (q.items.size() == 0)
            {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragen));
            }
            else if (lv.isGroupExpanded(g.groupposition)) {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
            } else {
                imgIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
            }colorText = (context.getResources().getColor(R.color.colorNeueFrage));
            bold = true;
        } else {
            colorText = (context.getResources().getColor(R.color.colorDate));
        }
        textView.setError(null);
        String stxtDate = textView.getText().toString();
        int color = (q.Grad >= 0 ? LevelColors[q.Grad] : ContextCompat.getColor(context, R.color.colorGradMinussolid));
        stxtDate = (q.Grad >= 6 ? digits[q.Grad] : "■") + stxtDate;
        wordtoSpan = new SpannableStringBuilder(stxtDate);
        wordtoSpan.setSpan(new ForegroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(colorText), 1, stxtDate.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (bold || blnAccessibility)
            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD), 1, stxtDate.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        //wordtoSpan.setSpan(new BackgroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(wordtoSpan);

    }

    */

    private void processFailure(String res, Question Folder, Group g) throws Throwable {
        Folder.fetched = false;
        Log.e(TAG, GET_FOLDER_ITEMS + res);
        if (_main.mService != null && (_main.mService.mState == STATE_NONE || _main.mService.mState == STATE_ERROR)) {
            _main.mService.relogin(true);
        }
        new GetAnswersForQuestionTask(g).execute();
    }

    private void processError(String res, Question Folder) throws Throwable {
        //    Folder.items.clear();
        Folder.fetched = false;
        Log.e(TAG, GET_FOLDER_ITEMS + res);
        if (_main.mService != null && (_main.mService.mState == STATE_NONE || _main.mService.mState == STATE_ERROR)) {
            _main.mService.relogin(true);
        }
        throw new Exception(res);
    }

    private String callAnswersTalk(Group g) throws JSONException {
        Question Folder = g.question;
        clsHTTPS https = this.myApp._main.clsHTTPS;
        HashMap<String, String> p = new HashMap<String, String>();
        String res = null;
        Long BenutzerID = this.myApp._main.user.getLong(Constants.id);
        int Kontaktgrad = Folder.Kontaktgrad;
        //if ((Folder.oeffentlich == null || !Folder.oeffentlich) && BenutzerID != Folder.BenutzerID && !_main.showAnswers)
        //    Kontaktgrad = 0;
        p.put(BENUTZER_ID, "" + BenutzerID);
        p.put(KONTAKTGRAD, "" + Kontaktgrad);
        p.put(FRAGE_ID, "" + Folder.FrageID);
        p.put(BENUTZER_FRAGEN_ID, "" + Folder.ID);

        //for (int i = 0; i <= Folder.Kontaktgrad; i++)
        //{
        //p.remove("Kontakgrad");
        //p.put("Kontaktgrad", "" + i);
        res = https.getAnswersTalk(p, g, _main);
        return res;
    }

    private void CheckApplicationVariables() throws Exception {
        if (myApp == null) throw new Exception("myApp null");
        if (myApp._main == null) throw new Exception("_main == null");
        if (myApp._main.user == null)
            myApp._main.user = (myApp._main.mService != null ? myApp._main.mService.user : null);
        if (myApp._main.user == null) {
            throw new Exception("user == null");
        }
    }

    private boolean getAnswersForQuestion(final Question question, final int GroupPosition, final int retries) throws Throwable {
        // JMHBCIApplication app = (JMHBCIApplication) context.getApplication();
        if (question != null && question.fetched == false) {
            question.fetched = true;
            boolean changed = false;
            if (myApp == null) throw new Exception("myApp null");
            if (myApp._main == null) throw new Exception("_main == null");
            if (myApp._main.user == null)
                myApp._main.user = (myApp._main.mService != null ? myApp._main.mService.user : null);
            if (myApp._main.user == null) {
                throw new Exception("user == null");
            }


            lib.LastgroupPosition = GroupPosition;
            try {
                //if (Folder.fetched == false) {
                //lib.BMList = new java.util.ArrayList<HBCIListItem>();
                question.items = new ArrayList<Answer>();
                //app.latchExpand = new CountDownLatch(1);
                clsHTTPS https = this.myApp._main.clsHTTPS;
                String res = null;
                if (question != null) {
                    Long BenutzerID = this.myApp._main.user.getLong(Constants.id);
                    int Kontaktgrad = question.Kontaktgrad;
                    //if ((Folder.oeffentlich == null || !Folder.oeffentlich) && BenutzerID != Folder.BenutzerID && !_main.showAnswers)
                    //    Kontaktgrad = 0;
                    res = https.getAnswers(BenutzerID, Kontaktgrad, question.FrageID, question.ID);

                    addAnswersToQuestion(question, res);


                } else {
                    Log.e(TAG, GET_FOLDER_ITEMS + FOLDER_IS_NULL);
                }
                /*
                if (Folder.DatumNeu != null) {
                    Folder.DatumNeu = null;
                    this.notifyDataSetChanged();
                }
                */

                //}
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, "getFolderItems", e);
                lib.ShowException(TAG, context, e, false);
                throw e;
            }
        }
        //Folder.items = lib.BMList;
        return true;
    }

    public void addAnswersToQuestion(Question question, String res) throws Throwable {
        ArrayList<Long> ids = new ArrayList<>();
        ArrayList<Long> ids2 = new ArrayList<>();
        if (res.startsWith(ERROR)) {
            //    Folder.items.clear();
            question.fetched = false;
            Log.e(TAG, GET_FOLDER_ITEMS + res);
            throw new Exception(res);
        } else {
            JSONArray j = new JSONArray(res);
            for (int ii = 0; ii < j.length(); ii++) {
                JSONObject o = j.getJSONObject(ii);
                long id = o.getLong(Constants.id);
                long id2 = -1l;
                if (!o.isNull(ID_2)) {
                    id2 = o.getLong(ID_2);
                }
                if (!ids.contains(id)) { // && (Folder.Grad == 0 || o.getLong("benutzerid") == BenutzerID)) {
                    addAnswerToQuestionMain(ids, question, o, id);
                }
                if ((id2 > -1 && !ids2.contains(id2))) { // && (Folder.Grad == 0 || o.getLong("benutzerid") == BenutzerID)) {
                    addReAntwortToQuestion(id2, ids2, question, o);
                }
            }
        }
    }

    private void addReAntwortToQuestion(long id2, ArrayList<Long> ids2, Question question, JSONObject o) throws Throwable {
        Answer a = new Answer(_main, question, o);
        a.update2(question, o);
        if (a.reAntwortID > 0) {
            reAntwortEinsortieren(question, a);
        } else {
            question.items.add(a);
        }
        ids2.add(id2);
    }

    private void reAntwortEinsortieren(Question question, Answer a) throws Exception {
        for (int i = 0; i < question.items.size(); i++) {
            if (question.items.get(i).ID == a.reAntwortID) {
                a.level = question.items.get(i).level + 1;
                i++;
                while (i < question.items.size() && question.items.get(i).reAntwortID == a.reAntwortID) {
                    i++;
                }
                question.items.add(i, a);
                break;
            } else if (i == question.items.size() - 1) {
                throw new Exception("reAntwortID not found!");
            }
        }
    }

    private void addAnswerToQuestionMain(ArrayList<Long> ids, Question question, JSONObject o, Long id) throws JSONException, ParseException {
        Answer a = new Answer(_main, question, o);
        question.items.add(a);
        ids.add(id);
    }

    private static class cbItemHolder extends Object {
        public Answer item;
        public int ServiceID;
        public int groupPosition;
        public int childPosition;

        public cbItemHolder(Answer item, int ServiceID, int groupPosition, int childPosition) {
            this.item = item;
            this.ServiceID = ServiceID;
            this.groupPosition = groupPosition;
            this.childPosition = childPosition;
        }
    }

    private int mFirstVisibleItem = -1;
    private int mVisibleItemCount = -1;
    private boolean mIsScrolling = false;

    public OnScrollListener onScrollListener = new OnScrollListener() {

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            // TODO Auto-generated method stub
            mFirstVisibleItem = firstVisibleItem;
            mVisibleItemCount = visibleItemCount;
            //if (totalItemCount > 0) mIsScrolling = true;
        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            // TODO Auto-generated method stub
            /*
            int 	SCROLL_STATE_FLING 	The user had previously been scrolling using touch and had performed a fling.
			int 	SCROLL_STATE_IDLE 	The view is not scrolling.
			int 	SCROLL_STATE_TOUCH_SCROLL 	The user is scrolling using touch, and their finger is still on the screen
			*/
            if (scrollState == SCROLL_STATE_IDLE) {
                mIsScrolling = false;
                ExpandableListView lv = (ExpandableListView) view;
                //HBCIAdapter ppa = (HBCIAdapter)(lv.getAdapter());
                int firstVis = lv.getFirstVisiblePosition();
                int lastVis = lv.getLastVisiblePosition();
                int count = firstVis;

                while (count <= lastVis) {
                    long longposition = lv.getExpandableListPosition(count);
                    int type = ExpandableListView.getPackedPositionType(longposition);
                    int groupPosition = ExpandableListView.getPackedPositionGroup(longposition);
                    int childPosition = ExpandableListView.getPackedPositionChild(longposition);
                    if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                        boolean isLastChild = count == lastVis;
                        View ChildView = lv.getChildAt(count - firstVis);
                        if ((ChildView != null) && (ChildView.getTag() != null && ChildView.getTag() instanceof Answer)) {
                            Answer holder = (Answer) (ChildView.getTag());
                            if (holder != null) {
                                Answer item = holder;
                            }
                        }
                    } else {

                    }
                    count++;
                }
            } else {
                mIsScrolling = true;
            }
        }

    };


    private void saveBitmapAs(Bitmap mBitmap, File file) throws IOException {
        FileOutputStream filecon = new FileOutputStream(file);
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 90, filecon);
        if (filecon != null) filecon.close();

    }

    private void ShareUri(Cursor c, int id, Uri uri) {
		/*
		if (service.contains("Facebook")){
			lib.SharePictureOnFacebook(context , uri);
		}
		else if (service.contains("Twitter")){
			lib.SharePictureOnTwitter(context , uri);
		}
		else if (service.contains("Instagram")){
			lib.SharePictureOnInstagram(context , uri);
		}
		else if (service.contains("Pinterest")){
			lib.SharePictureOnPinterest(context , uri);
		}
		*/
        //lib.SharePicture(context, uri, c, id);
    }

    public boolean onTouchEvent(MotionEvent event) {
        //MainActivity a = (MainActivity)((context instanceof MainActivity) ? context : null);
        //a.lv.OnTouchEvent(e.Event);
        return false;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {

        if (groupPosition < rows.size()) return rows.get(groupPosition).items.get(childPosition);
        else return null;

    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void setThreadPolicy() {
        if (Build.VERSION.SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

}