package de.com.limto.limto1;

import android.Manifest;
import android.app.Activity;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.OpenableColumns;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import de.com.limto.limto1.Chat.dlgChat;
import de.com.limto.limto1.Chat.dlgContactsChat;
import de.com.limto.limto1.Controls.Answer;
import de.com.limto.limto1.Controls.Names;
import de.com.limto.limto1.Controls.NamesAdapter;
import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.Controls.QuestionsAdapter;
import de.com.limto.limto1.Errors.SQLiteDataActivity;
import de.com.limto.limto1.Errors.dbSqlite;
import de.com.limto.limto1.dummy.DummyContent;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;
import de.com.limto.limto1.logger.LogWrapper;
import de.com.limto.limto1.logger.MessageOnlyLogFilter;

import static android.os.Build.VERSION_CODES.M;
import static de.com.limto.limto1.Constants.MESSAGE_EXCEPTION;
import static de.com.limto.limto1.Controls.Question.sdftime;
import static de.com.limto.limto1.LimindoService.ONGOING_NOTIFICATION_ID_ANSWER;
import static de.com.limto.limto1.LimindoService.ONGOING_NOTIFICATION_ID_DEBUG;
import static de.com.limto.limto1.LimindoService.ONGOING_NOTIFICATION_ID_MESSAGES;
import static de.com.limto.limto1.LimindoService.ONGOING_NOTIFICATION_ID_QUESTION;
import static de.com.limto.limto1.LimindoService.STATE_CONNECTED;
import static de.com.limto.limto1.LimindoService.STATE_CONNECTING;
import static de.com.limto.limto1.LimindoService.SendType.Upload;
import static de.com.limto.limto1.LimindoService.UNCAUGHT_EXCEPTION_HANDLER;
import static de.com.limto.limto1.LimindoService.saveLogcatToFile;
import static de.com.limto.limto1.LimindoService.setLogCatFileRoot;
import static de.com.limto.limto1.LimtoWorker.RESTARTINTERVALL;
import static de.com.limto.limto1.broadcastReceiver.BOOT;
import static de.com.limto.limto1.clsHTTPS.ONLINE_STATE;
import static de.com.limto.limto1.clsHTTPS.TRUE;
import static de.com.limto.limto1.clsHTTPS.fmtDateTime;
import static de.com.limto.limto1.dlgLogin.RC_SIGN_IN;
import static de.com.limto.limto1.fragQuestion.VOTE;
import static de.com.limto.limto1.fragQuestions.DATUMLOGOFF;
import static de.com.limto.limto1.fragQuestions.ENTF;
import static de.com.limto.limto1.fragQuestions.ID;
import static de.com.limto.limto1.fragQuestions.SEARCH;
import static de.com.limto.limto1.fragSettings.ALL;
import static de.com.limto.limto1.fragSettings.LANG;
import static de.com.limto.limto1.fragSettings.MENGE;
import static de.com.limto.limto1.fragSettings.SYSTEM;
import static de.com.limto.limto1.lib.lib.SELECT_FILE;
import static de.com.limto.limto1.lib.lib.ShowException;
import static de.com.limto.limto1.lib.lib.ShowMessage;
import static de.com.limto.limto1.lib.lib.appendLog;
import static de.com.limto.limto1.lib.lib.debugMode;
import static de.com.limto.limto1.lib.lib.getLogFile;

//BenutzerFragenID 2397 FrageID 2223
public class MainActivity extends AppCompatActivity implements fragQuestions.OnListFragmentInteractionListener, fragAnswer.OnFragmentInteractionListener, fragQuestion.OnFragmentInteractionListener, DialogInterface.OnDismissListener, NavigationView.OnNavigationItemSelectedListener {

    public static final String MAIN_ACTIVITY = "MainActivity";
    public static final String FRAGEID = "frageid";
    public static final String FRAGEANTWORTENID = "frageantwortenid";
    public static final String BENUTZERID = "benutzerid";
    public static final String BENUTZERIDANTWORT = "benutzeridantwort";
    public static final String BENUTZERIDFRAGE = "benutzeridfrage";
    public static final String BENUTZERFRAGENID = "benutzerfragenid";
    public static final String BEWERTUNG = "bewertung";
    public static final String ANTWORT = "antwort";
    public static final String OEFFENTLICH = "oeffentlich";
    public static final String REOEFFENTLICH = "reoeffentlich";
    public static final String PARENTFRAGEANTWORTENID = "parentfrageantwortenid";
    public static final String BENUTZERIDSENDER = "benutzeridsender";
    public static final String BENUTZERIDEMPFAENGER = "benutzeridempfaenger";
    public static final String BREITENGRAD = "breitengrad";
    public static final String LAENGENGRAD = "laengengrad";
    public static final String ANTWORTID = "antwortid";
    public static final String BENUTZERNAME = "benutzername";
    public static final String EMAIL = "email";
    public static final String EMERGENCY = "emergency";
    public static final String DEAKTIVIERT = "deaktiviert";
    public static final String FRAG_LOGIN = "fragLogin";
    public static final String MAINACTIVITY_UN_CAUGHT = "Mainactivity UnCaught";
    public static final String DB_SQLITE = "dbSqlite";
    public static final String DEVICE_HELPER = "DeviceHelper";
    public static final String IS_TABLET_DEVICE_TRUE = "IsTabletDevice-True";
    public static final String IS_TABLET_DEVICE_FALSE = "IsTabletDevice-False";
    public static final String LASTQUERY = "lastquery";
    public static final String LAST_FRAGMENT = "lastFragment";
    public static final String CURRENT_QUESTION = "currentQuestion";
    public static final String CURRENT_ANSWER_ANSWER = "currentAnswerAnswer";
    public static final String CURRENT_ANSWER = "currentAnswer";
    public static final String LAST_START_GRAD = "lastStartGrad";
    public static final String LAST_END_GRAD = "lastEndGrad";
    public static final String LAST_GELOESCHT = "lastGeloescht";
    public static final String LAST_ARCHIVIERT = "lastArchiviert";
    public static final String LAST_PAUS = "lastPaus";
    public static final String LAST_LIMIT = "lastLimit";
    public static final String QUESTIONS_LOADED = "questionsLoaded";
    public static final String LAST_POSITION = "LastPosition";
    public static final String ANSWER = "Answer";
    public static final String QUESTION = "Question";
    //public static final String DT_END = "dtEnd";
    public static final String SPN_ENTF = "spnEntf";
    public static final String SPN_FEHLERGRAD = "spnFehlergrad";
    public static final String BTN_OEFFENTLICH = "btnOeffentlich";
    public static final String SETTINGS = "Settings";
    public static final String DATE_LAST_LOGOFF = "DateLastLogoff";
    public static final String READ_MESSAGE_NOT_FOUND = "readMessage not found!";
    public static final String FRAGE = "frage";
    public static final String QUESTION1 = "Question ";
    public static final String IS_ALREADY_FETCHED = " is already fetched!";
    public static final String ID_2 = "id2";
    public static final String RUFNUMMER = "rufnummer";
    public static final String SPN_GRADES = "spnGrades";
    public static final String E_MAIL = "EMail";
    public static final String BENUTZERNAME1 = "Benutzername";
    public static final String READ_MESSAGE = "readMessage";
    public static final String SERVICE_INTENT = "ServiceIntent";
    public static final String LICENSE_ACCEPTED = "LicenseAccepted";
    public static final String LICENSE = "LICENSE";
    public static final String LIMINDO_LIMINDO = "limindo://limindo";
    public static final String NOT_OFF = "NotOff";
    public static final String __A = "\\A";
    public static final String CREATE_SERVICE = "Create Service";
    public static final String BENUTZER_FRAGEN_ID = "BenutzerFragenID";
    public static final String SENDTYPE = "sendtype";
    public static final String M_HANDLER = "mHandler";
    public static final String EXPAND_GROUP = "expandGroup:";
    public static final String ROWS = "rows:";
    public static final String KONTAKTGRAD = "kontaktgrad";
    public static final String GRADE = "Grade";
    public static final String ORIGINALID = "originalid";
    public static final String BENUTZER_ID = "BenutzerID";
    public static final String GRAD = "Grad";
    public static final String ON_STOP_CHAT_SERVICE_UNBIND = "onStop:ChatService:unbind";
    public static final String ERROR = "Error";
    public static final String MAINACTIVITY = "Mainactivity";
    public static final String ON_STOP = "On Stop";
    public static final String CREDITS = "CREDITS";
    public static final String CREDITS1 = "Credits";
    public static final String PP_ACCEPTED = "PPAccepted";
    public static final String MAILTO = "mailto";
    public static final String CONTACT_EMAIL = "jhmgbl2@t-online.de";
    public static final String MESSAGE_RFC822 = "message/rfc822";
    public static final String FRAG_CHANGE_PW = "fragChangePW";
    public static final String ERROR_ = "Error:";
    public static final String ACCESSKEY = "Accesskey";
    public static final String XXXX = "xxxx";
    public static final String OPTIONS_ITEM_SELECTED = "OptionsItemSelected";
    public static final String SAVE_RESULTS_AND_FINISH = ".saveResultsAndFinish";
    public static final String ON_RESUME_RECONNECT_SERVICE = "On Resume reconnect service";
    public static final String TIME = "time";
    public static final String SPN_LAUFZ = "SpinnerLaufzeit";
    public static final int RGLOBAL = 5555;
    public static final int RVIEWFILE = 5556;
    public static final String FRAG_CHAT_VISIBLE = "fragChatVisible";
    public static final String EMPFAENGER_ID = "EmpfaengerID";
    public static final String EMPFAENGER_NAME = "EmpfaengerName";
    public static final String PENDING_READ = "pendingRead";
    static final String FRAG_CHAT = "fragChat";
    private static final String TAG = MAIN_ACTIVITY;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private static final int PICK_PHOTO = 4599;
    private static final String NAMEEMPFAENGER = "nameempfaenger";
    public static ArrayList<String> MessageCache = new ArrayList<>();
    public static String versionName;
    public static boolean nodialogs = false;
    public static boolean autoanswer;
    public static boolean inStanceStateSaved;
    public static boolean blnAccessibility;
    public static int dontStop;
    private static boolean dontStopView;
    public final Handler mHandler = new HandlerBound(this);
    private final Thread.UncaughtExceptionHandler defaultUEH;
    public boolean UnconfirmedORInvited;
    public int currentchildid = -1;
    public boolean isTV;
    public boolean isWatch;
    public boolean isTablet;
    public HackyViewPager mPager;
    public MyFragmentPagerAdapter fPA;
    public String lastQuery = "";
    public boolean blnSearchWholeWord;
    public ArrayList<Integer> selected = new ArrayList<>();
    public dlgLogin fragLogin = new dlgLogin();
    public dlgChat fragChat;
    public Location locFine = null;
    public Map<Long, QuestionsAdapter.Group> groups = Collections.synchronizedMap(new HashMap<Long, QuestionsAdapter.Group>());
    public boolean blnSearchTerms;
    public JSONObject user;
    public de.com.limto.limto1.clsHTTPS clsHTTPS;
    public Question currentQuestion;
    public Answer currentAnswer;
    public boolean loadcurrentquestion;
    public int currentgroupid = -1;
    public LimindoService mService;
    public int lastStartGrad;
    public int lastEndGrad;
    public Boolean lastGeloescht = false;
    public Boolean lastArchiviert = false;
    public Boolean lastPaus = false;
    public String readMessage = null;
    public FragmentManager fm;
    public Date lastLogoffDate;
    public boolean showAnswers;
    public Answer currentAnswerAnswer;
    public long lastLimit = 0l;
    public boolean blnNotOff;
    public boolean getUnconfirmed;
    public String loginEMail = null;
    public String loginBenutzername = null;
    public SimpleDateFormat dtFormatDay;
    public SimpleDateFormat dtFormatDayTime;
    public MenuItem mnuDeaktivieren;
    public boolean deaktiviert;
    public boolean questionsLoaded;
    public boolean hasRe; //wenn eine Reantwort vorhanden ist und die Antwort editiert werden soll
    public boolean getInvited;
    public JSONArray ArrInvited;
    public boolean GooglePlay;
    public MenuItem mnuUpload;
    public GoogleSignInAccount account;
    public Bitmap VNLOGO = null;
    public boolean blnServicWasDestroyed;
    dbSqlite db = null;
    Long timeOffsetGPS = null;
    int ServiceStarted = 0;
    boolean mServiceRequested;
    boolean showLogin;
    HackyViewPager layout;
    String Lang;
    MenuItem mnuOnlineVisible;
    MenuItem mnuOnlineInVisible;
    MenuItem mnuOnlineFreeForChat;
    MenuItem mnuOnlineBusy;
    OnlineState onlineState = OnlineState.visible;
    boolean mServiceInitialized;
    Boolean admin = null;
    boolean fragLoginUpdated;
    int LastPosition = -1;
    int LastLastPosition = fragQuestions.fragID;
    private ArrayList<String> _MessageCacheChat = new ArrayList<>();
    private TextView mTextMessage;
    private ViewGroup Layout;
    private FusedLocationProviderClient mFusedLocationClient;
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    try {
                        Log.e(TAG, MAINACTIVITY_UN_CAUGHT, ex);
                        try {
                            saveLogcatToFile();
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                    } catch (Throwable throwable) {
                        android.util.Log.e(TAG, UNCAUGHT_EXCEPTION_HANDLER, throwable);
                        throwable.printStackTrace();
                    }
                   finally {
                        defaultUEH.uncaughtException(thread, ex);
                    }
                }
            };
    private Bundle savedInstanceState;
    private boolean mRequestingLocationUpdates = true;
    private boolean mBound;
    private boolean initialized;
    private BottomNavigationView bottomNavigationView;
    private String mConnectedDeviceName;
    private String mConnectedDeviceAddress;
    private ActivityServiceConnection mConnection = new ActivityServiceConnection();
    private int lastFragment = -1;
    private boolean isRunning;
    private boolean isStartedFromNotification;
    private MenuItem mnuTestLoginTalk;
    private MenuItem mnuErrors;
    private MenuItem mnuTaet;
    private GoogleSignInClient mGoogleSignInClient;
    private long dtLastQuestion;
    private boolean fragChatCalled;
    private ArrayList<String> resolvedReadMessages = new ArrayList<>();
    private boolean isvisible;
    private Long benutzerID;
    private int newAnswersOther;
    private int newQuestions;
    private int newAnswers;
    private int ownQuestions;
    private int ownQuestionsAnswered;
    private int ownQuestionsUnanswered;
    private int ownQuestionsNotFetched;
    private MenuItem mnuChat;
    private Menu navigationmenu;
    private boolean hasMessages;
    private NavigationView navigationView;
    private Float globalfontsize = 100f;
    private DrawerLayout drawer;
    private boolean savedInstanceStateNullAndLoadedFromPrefs = false;
    private MenuItem mnuOnlineOffline;
    private ColorStateList oldIconTintList;
    private boolean savedInstanceStateNull;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mToggle;
    private ColorStateList iconTintlistMenuBak;


    public MainActivity() {
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        // setup handler for uncaught exception
        Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);

        fragLogin._main = this;
        fragLogin.context = this;
        //QuestionsAdapter.staticContext = this.g;


    }

    private static boolean isTabletDevice(Context activityContext) {
        boolean device_large = ((activityContext.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) >=
                Configuration.SCREENLAYOUT_SIZE_LARGE);

        if (device_large) {
            DisplayMetrics metrics = new DisplayMetrics();
            Activity activity = (Activity) activityContext;
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            if (metrics.densityDpi == DisplayMetrics.DENSITY_DEFAULT
                    || metrics.densityDpi == DisplayMetrics.DENSITY_HIGH
                    || metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM
                    || metrics.densityDpi == DisplayMetrics.DENSITY_TV
                    || metrics.densityDpi == DisplayMetrics.DENSITY_XHIGH) {
                Log.d(DEVICE_HELPER, IS_TABLET_DEVICE_TRUE);
                return true;
            }
        }
        Log.d(DEVICE_HELPER, IS_TABLET_DEVICE_FALSE);
        return false;
    }

    public static void saveOnlineState(Context context, int onlineState) {
        context.getSharedPreferences("Main", MODE_PRIVATE).edit().putInt(ONLINE_STATE, onlineState).apply();
    }

    public static OnlineState getOnlineState(Context context) {
        return OnlineState.getOnlineState(context.getSharedPreferences("Main", MODE_PRIVATE).getInt(ONLINE_STATE, 1));
    }

    public Date getLastSystemDate() throws ParseException {
        return LimindoService.DateTimeParser.parse((getPreferences(MODE_PRIVATE).getString
                ("dtLastSystemMessage", LimindoService.DateTimeParser.format(new Date(0)))));
    }

    public void initializeLogging(long userID, clsHTTPS clsHTTPS, dbSqlite db) {
        // Wraps Android's native log framework.
        if (userID == -1) {
            Log.setLogNode(null);
            return;
        }
        LogWrapper logWrapper = new LogWrapper(userID, clsHTTPS, db);
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);

        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.
        Log.i(TAG, "Ready");
        //Log.e(TAG,"Start",new RuntimeException("Start"));
    }

    @Override
    protected void onDestroy() {
        //super.onDestroy();
        this.isRunning = false;
        isvisible = false;
        try {
            saveMessageCacheChat();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        if (dontStop == 0 && mService != null && (!(fragChat != null && fragChat.isVisible() && fragChat.isvisible && !fragChat.isHidden()))) { //set to null by chatfragment.destroy
            //mChatService.stop();
            LimindoService.dontStop = false;
            try {
                getSupportActionBar().setSubtitle(R.string.disconnectingservice);
                mService.unBind(true);
            } catch (Throwable throwable) {
                Log.e(MAIN_ACTIVITY, ON_STOP_CHAT_SERVICE_UNBIND, throwable);
                throwable.printStackTrace();
                appendLog(throwable.getMessage(), this);
            }
            //getActionBar().setSubtitle(R.string.ServiceDisconnected);
            mService = null;
            mServiceInitialized = false;
            mServiceRequested = false;
            super.onDestroy();
        } else if (mService == null) {
            super.onDestroy();
        } else {
            super.onDestroy();
        }
        ServiceStarted = 0;

        /*if ((Debug.isDebuggerConnected() || BuildConfig.DEBUG) && mService != null) {
            try {
                mService.createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES, "on Destroy", "onDestroy", null, false, false);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

         */
    }

    void SaveInstanceState(Bundle savedInstanceState) {
        try {
            if (savedInstanceState == null) return;
            savedInstanceState.putString(LASTQUERY, lastQuery);
            if (mPager != null && fPA != null) {
                savedInstanceState.putInt(LAST_FRAGMENT, mPager.getCurrentItem());
                if (currentQuestion != null)
                    savedInstanceState.putLong(CURRENT_QUESTION, currentQuestion.ID);
                if (currentAnswerAnswer != null)
                    savedInstanceState.putLong(CURRENT_ANSWER_ANSWER, currentAnswerAnswer.ID);
                if (currentAnswer != null)
                    savedInstanceState.putLong(CURRENT_ANSWER, currentAnswer.ID);
                savedInstanceState.putInt(LAST_START_GRAD, lastStartGrad);
                savedInstanceState.putInt(LAST_END_GRAD, lastEndGrad);
                savedInstanceState.putBoolean(LAST_GELOESCHT, lastGeloescht);
                savedInstanceState.putBoolean(LAST_ARCHIVIERT, lastArchiviert);
                savedInstanceState.putBoolean(LAST_PAUS, lastPaus);
                savedInstanceState.putLong(LAST_LIMIT, lastLimit);
                savedInstanceState.putBoolean(QUESTIONS_LOADED, questionsLoaded);
                savedInstanceState.putInt(LAST_POSITION, LastPosition);
                if (mPager.getCurrentItem() == fragAnswer.fragID) {
                    fragAnswer f = (fragAnswer) fPA.findFragment(fragAnswer.fragID);
                    savedInstanceState.putString(ANSWER, f.txtAnswer.getText().toString());
                } else if (mPager.getCurrentItem() == fragQuestion.fragID) {
                    fragQuestion f = (fragQuestion) fPA.findFragment(fragQuestion.fragID);
                    savedInstanceState.putString(QUESTION, f.txtQuestion.getText().toString());
                    Date d = f.getSelectedLaufzeitDate();
                    //cal.set(f.dtEnd.getYear(), f.dtEnd.getMonth(), f.dtEnd.getDayOfMonth());
                    //savedInstanceState.putString(DT_END, clsHTTPS.fmtDateTime.format(cal.getTime()));
                    savedInstanceState.putInt(SPN_LAUFZ, f.spnLaufzeit.getSelectedItemPosition());
                    savedInstanceState.putInt(SPN_ENTF, f.spnEntf.getSelectedItemPosition());
                    savedInstanceState.putInt(SPN_FEHLERGRAD, f.spnFehlergrad.getSelectedItemPosition());
                    savedInstanceState.putBoolean(BTN_OEFFENTLICH, f.btnOeffentlich.isChecked());
                    savedInstanceState.putInt(SPN_GRADES, f.spnGrades.getSelectedItemPosition());
                }
                if (fragChat != null && fragChat.isVisible() && fragChat.getBenutzerIDEmpfaenger() != null) {
                    savedInstanceState.putBoolean(FRAG_CHAT, true);
                    savedInstanceState.putLong(BENUTZERIDEMPFAENGER, fragChat.getBenutzerIDEmpfaenger());
                    savedInstanceState.putString(NAMEEMPFAENGER, fragChat.getNameEmpfaenger());
                }

                if (fragChat != null) {
                    String strO = new Gson().toJson(dlgChat.pendingRead);
                    savedInstanceState.putString(PENDING_READ, strO);
                    if (fragChat.isvisible) {
                        savedInstanceState.putBoolean(FRAG_CHAT_VISIBLE, true);
                        savedInstanceState.putLong(EMPFAENGER_ID, fragChat.getBenutzerIDEmpfaenger());
                        savedInstanceState.putString(EMPFAENGER_NAME, fragChat.getNameEmpfaenger());
                    }
                }


            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw ex;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        try {
            SaveInstanceState(savedInstanceState);
            this.savedInstanceState = savedInstanceState;
            MainActivity.inStanceStateSaved = true;

        } catch (Throwable ex)
        {
            lib.ShowException(TAG, this, ex, true);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        lib.blnDebugOn = getSharedPreferences("lib", Context.MODE_PRIVATE).getBoolean("DebugOn", false);
        try {
            setLogCatFileRoot(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle psavedInstanceState = savedInstanceState;
        this.savedInstanceStateNull = savedInstanceState == null;
        Bundle s = loadSavedInstanceState(savedInstanceState);
        if (s != null) {
            psavedInstanceState = s;
        }
        initOnlineState();
        try {
            restoreMessageCacheChat();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        setAccessibility0(psavedInstanceState);
        super.onCreate(psavedInstanceState);
        getLanguage();
        create2(psavedInstanceState);
    }

    private void setAccessibility0(Bundle psavedInstanceState) {
        int acc = -1;
        if ((acc = getPreferences(MODE_PRIVATE).getInt("Accessibility", -1)) == -1) {
            setAccessibility();
        } else {
            if (acc == 0) {
                setTheme(R.style.AppThemeNormal);
                blnAccessibility = false;
            } else {
                setTheme(R.style.AppTheme);
                blnAccessibility = true;
            }
        }
    }

    private void getLanguage() {
        Lang = getApplicationContext().getSharedPreferences(LANG, Context.MODE_PRIVATE).getString(LANG, ALL);
        if (Lang.equalsIgnoreCase(ALL)) Lang = "";
        if (Lang.equalsIgnoreCase(SYSTEM))
            Lang = Locale.getDefault().toString().substring(0, 2);
    }

    private void restoreMessageCacheChat() throws Throwable {
        if (_MessageCacheChat.size() > 0) return;
        String str0 = getPreferences(Context.MODE_PRIVATE).getString("MessageCacheChat", "");
        if (str0.length() > 0) {
            Object o = new Gson().fromJson(str0, new TypeToken<ArrayList<String>>() {
            }.getType());
            _MessageCacheChat = (ArrayList<String>) o;
            //Debug
            lib.setStatusAndLog(this, TAG,"*** MessageCacheChat MainActivity loaded size " + _MessageCacheChat.size() );
        }

    }

    private void initOnlineState() {
        OnlineState.stateStrings.put(OnlineState.freeforchat, getString(R.string.freeforchat));
        OnlineState.stateStrings.put(OnlineState.visible, getString(R.string.visible));
        OnlineState.stateStrings.put(OnlineState.invisible, getString(R.string.invisible));
        OnlineState.stateStrings.put(OnlineState.busy, getString(R.string.busy));
        OnlineState.stateStrings.put(OnlineState.offline, getString(R.string.offline));
        onlineState = OnlineState.getOnlineState(getSharedPreferences("Main", MODE_PRIVATE).getInt(ONLINE_STATE, 1));

    }

    private Bundle loadSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            lib.DeleteLog(this);
            boolean r = getSharedPreferences("Service", MODE_PRIVATE).getBoolean("MainActivitySaved", false);
            if (r) {
                Bundle s = lib.loadPreferencesBundle(getSharedPreferences("Service", MODE_PRIVATE), "MainActivity");
                savedInstanceStateNullAndLoadedFromPrefs = true;
                getSharedPreferences("Service", MODE_PRIVATE).edit().putBoolean("MainActivitySaved", false).apply();
                return s;
            }

        }
        getSharedPreferences("Service", MODE_PRIVATE).edit().putBoolean("MainActivitySaved", false).apply();
        return null;
    }

    public void setDefaultfont(String defaultFontNameToOverride, Object customFontTypeface) throws NoSuchFieldException, IllegalAccessException {
        final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);

        defaultFontTypefaceField.setAccessible(true);

        defaultFontTypefaceField.set(null, customFontTypeface);


    }

    private void setAccessibility() {
        final CheckBox cbx = null; //new CheckBox(this);
        //cbx.setText(R.string.save);
        try {
            AlertDialog dlg = lib.ShowMessageYesNo(this, getString(R.string.badVision), getString(R.string.Accessibility), true, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setTheme(R.style.AppTheme);
                    blnAccessibility = true;
                    getPreferences(MODE_PRIVATE).edit().putInt("Accessibility", 1).apply();
                    if (mPager!= null) {
                        mPager.setCurrentItem(fragHome.fragID);
                        mPager.getHandler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recreate();
                            }
                        }, 1000);
                    }
                }

            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        setTheme(R.style.AppThemeNormal);
                        blnAccessibility = false;
                        getPreferences(MODE_PRIVATE).edit().putInt("Accessibility", 0).apply();
                        if (mPager!= null) {
                            mPager.setCurrentItem(fragHome.fragID);
                            mPager.getHandler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    recreate();
                                }
                            }, 1000);
                        }
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                        ShowException(TAG, MainActivity.this, ex, false);
                    }
                }
            }, cbx);
            dlg.show();

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }

    private void create2(Bundle savedInstanceState) {

        lib.setStatusAndLog( this, TAG, "create2");

        if (mService != null) mService.ccontext = this;
        if (false) startBoot();

        {
            initVariables();
            //ColorDrawable d = new ColorDrawable(ContextCompat.getColor(this, R.color.colorActionBar));
            //getSupportActionBar().setBackgroundDrawable(d);
            Intent intent = getIntent();
            readMessage = getReadMessage(intent);
            ClearMessageCacheOrRemoveReadMessage(readMessage);

            setContentView(lib.getLayoutResFromTheme(this, R.attr.activity_main_viewpager));

            toolbar = initActionBar();
            //toolbar.setNavigationIcon(R.drawable.vn_logo_c02);

            initDrawer(toolbar);

            initBottomNavigationView();

            initNavigationView();

            isvisible = true;

            getSupportActionBar().setSubtitle(versionName);

            if (savedInstanceState != null) {
                lastQuery = savedInstanceState.getString(LASTQUERY);
            }

            fm = getSupportFragmentManager();

            lib.ShowMessageDebug(this, "1. create2 FM created", "Debug");

            processSavedInstanceState(savedInstanceState);

            this.isRunning = true;

            try {
                this.GooglePlay = lib.checkGooglePlayServicesAvailable(getApplicationContext(), this);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            if (GooglePlay) {
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            }
            layout = (HackyViewPager) findViewById(R.id.pager);
            isvisible = true;


        }
    }

    private void initNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        increasemenufontsize(navigationView.getMenu(), globalfontsize);
        if (blnAccessibility)
            increasemenufontsize(navigationView.getMenu(), globalfontsize * 1.5f);
    }

    private void processSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        if (savedInstanceState != null) {
            lib.ShowMessageDebug(this, "savedInstanceState != null", "Debug");
            lastStartGrad = savedInstanceState.getInt(LAST_START_GRAD, lastStartGrad);
            lastEndGrad = savedInstanceState.getInt(LAST_END_GRAD, lastEndGrad);
            lastGeloescht = savedInstanceState.getBoolean(LAST_GELOESCHT, lastGeloescht);
            lastArchiviert = savedInstanceState.getBoolean(LAST_ARCHIVIERT, lastArchiviert);
            lastPaus = savedInstanceState.getBoolean(LAST_PAUS, lastPaus);
            lastLimit = savedInstanceState.getLong(LAST_LIMIT, lastLimit);
            questionsLoaded = savedInstanceState.getBoolean(QUESTIONS_LOADED, false);
            LastPosition = savedInstanceState.getInt(LAST_POSITION);
        }

    }

    private void initBottomNavigationView() {
        if (!blnAccessibility) {
            bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
            bottomNavigationView.setVisibility(View.VISIBLE);
            bottomNavigationView.setItemIconTintList(null);
            bottomNavigationView.getMenu().getItem(0).setCheckable(false);
            bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.mnuChat:
                            if (hasMessages) {
                                showFragChat(false, null, null, true, true);
                            } else {
                                if (fragChat == null) {
                                    try {
                                        initFragChat();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                new dlgContactsChat().show(fm, "dlgContectsChat");
                            }
                            break;
                        case R.id.mnuHome:
                            mPager.setCurrentItem(fragHome.fragID);
                            break;
                        case R.id.mnuQuestion:
                            mPager.setCurrentItem(fragQuestion.fragID);
                            break;
                        case R.id.mnuQuestions:
                            mPager.setCurrentItem(fragQuestions.fragID);
                            break;

                    }
                    return true;
                }
            });

        } else {
            bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
            bottomNavigationView.setVisibility(View.GONE);
        }

    }

    private void initDrawer(Toolbar toolbar) {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        mToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // Do whatever you want here
                //navigationView.getMenu().findItem(R.id.action_society_satzung).setVisible(false);
                //navigationView.getMenu().findItem(R.id.action_society_vorstand).setVisible(false);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // Do whatever you want here
                //navigationView.getMenu().findItem(R.id.action_society_satzung).setVisible(false);
                //navigationView.getMenu().findItem(R.id.action_society_vorstand).setVisible(false);
            }

        };
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(mToggle);
        mToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer!= null) drawer.openDrawer(GravityCompat.START);
            }
        });
        mToggle.syncState();
        setDrawerState(false, drawer, mToggle);
        toolbar.setNavigationIcon(mToggle.getDrawerArrowDrawable());

    }

    private Toolbar initActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar == null) {
            throw new IllegalArgumentException(" must provide a toolbar with id='toolbar'.");
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return toolbar;
    }

    private void ClearMessageCacheOrRemoveReadMessage(String readMessage) {
        if (readMessage == null || readMessage.length() == 0) {
            //Debug
            lib.setStatusAndLog(this, TAG, "*** clear MessageCache Mainactivity empty");
            MessageCache.clear();
        } else {
            //Debug
            lib.setStatusAndLog(this, TAG, "*** clear MessageCache Mainactivity remove " + readMessage);
            boolean res = MessageCache.remove(readMessage);
            if (!res) res = _MessageCacheChat.remove(readMessage);
            if (!res) {
                Log.i(TAG, READ_MESSAGE_NOT_FOUND);
                //Debug
                lib.setStatusAndLog(this, TAG, "*** clear MessageCache Mainactivity could not remove readMessage");
            }

        }
    }

    private void initVariables() {
        VNLOGO = BitmapFactory.decodeResource(this.getResources(), R.drawable.vn_logo_c02);
        try {
            String fmt = getString(R.string.dtFormatDay);
            dtFormatDay = new SimpleDateFormat(fmt);
            dtFormatDayTime = new SimpleDateFormat(getString(R.string.dtFormatDayTime));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        try {
            lastLogoffDate = fmtDateTime.parse(getApplicationContext().getSharedPreferences(SETTINGS, MODE_PRIVATE).getString(DATE_LAST_LOGOFF, fmtDateTime.format(Calendar.getInstance().getTime())));
            this.getSharedPreferences(SETTINGS, MODE_PRIVATE).edit().putString(DATE_LAST_LOGOFF, fmtDateTime.format(Calendar.getInstance().getTime())).apply();
            setblnNotOff(getPreferences(Context.MODE_PRIVATE).getBoolean(NOT_OFF, false));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            versionName = this.getPackageManager()
                    .getPackageInfo(this.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
        }
    }

    public void setDrawerState(boolean isEnabled, DrawerLayout mDrawerLayout, ActionBarDrawerToggle mDrawerToggle) {
        if (isEnabled) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.onDrawerStateChanged(DrawerLayout.STATE_IDLE);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mDrawerToggle.syncState();
            getSupportActionBar().setHomeButtonEnabled(true);

        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.onDrawerStateChanged(DrawerLayout.STATE_DRAGGING);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mDrawerToggle.syncState();
            getSupportActionBar().setHomeButtonEnabled(false);
        }
    }

    private void startBoot() {
        lib.setStatusAndLog( this, TAG, "startBoot");
        Intent serviceIntent;
        serviceIntent = new Intent(this, LimindoService.class);
        serviceIntent.putExtra(BOOT, true);
        serviceIntent.setAction(BOOT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(serviceIntent);
        } else {
            this.startService(serviceIntent);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        readMessage = getReadMessage(intent);
    }

    private String getReadMessage(Intent intent) {
        String readMessage = null;
        autoanswer = false;
        nodialogs = false;
        if (intent != null) {
            if (intent.getData() != null && intent.getData().toString().startsWith(LIMINDO_LIMINDO)) {
                Uri uri = intent.getData();
                try {
                    this.loginEMail = uri.getQueryParameter(E_MAIL);
                    this.loginBenutzername = uri.getQueryParameter(BENUTZERNAME1);
                } catch (Throwable ex) {
                    Log.e(TAG, null, ex);
                }
                this.showLogin = true;
            } else if (intent.hasExtra(READ_MESSAGE) && intent.getStringExtra(READ_MESSAGE) != null) {
                readMessage = intent.getStringExtra(READ_MESSAGE);
                intent.removeExtra(READ_MESSAGE);
                lib.setStatusAndLog(this,TAG, "*** found Readmessage " + readMessage);
            } else if (intent.hasExtra(SERVICE_INTENT) && isRunning) {
                this.isStartedFromNotification = true;
            } else if (intent.hasExtra("nodialogs")) {
                MainActivity.nodialogs = true;
                if (intent.hasExtra("autoanswer"))
                {
                    MainActivity.autoanswer = true;
                }
            } else if (isRunning) {
                this.isStartedFromNotification = true;
            }
        }
        else
        {
            lib.ShowMessageDebug(this, "Intent null" ,"Debug");
        }
        return readMessage;
    }

    public void init1(Bundle savedInstanceState) throws Throwable {
        if (user == null) {
            init2(savedInstanceState);
        } else {
            init2(savedInstanceState);
        }
    }

    public boolean init2(Bundle savedInstanceState) throws Throwable {
        boolean blnStop = false;
        boolean relogin = false;
        lib.ShowMessageDebug(this, "3. init2","Debug");
        //getting the kind of userinterface: television or watch or else
        if (!blnStop) {

            setUserInterface();

            initPagerAndFPA();

            if (user == null || showLogin) {
                if ((savedInstanceState == null || !MainActivity.inStanceStateSaved || savedInstanceStateNullAndLoadedFromPrefs) || showLogin) {
                    lib.ShowMessageDebug(this, "4. init2 user == null savedInstanceState = " + (savedInstanceState != null) + " ShowLogin = " + showLogin ,"Debug");
                    ShowFragLogin();
                } else {
                    lib.ShowMessageDebug(this, "4. init2 user == null savedInstanceState = " + (savedInstanceState != null) + " savedInstanceStateNullAndL... " + savedInstanceStateNullAndLoadedFromPrefs + " savedInstanceStatenull " + savedInstanceStateNull + " dont' ShowLogin","Debug");
                    loadInvited_Unconfirmed_Questions();
                    //if (fragLogin != null) fragLogin.dismiss();
                }
            } else {
                relogin = true;
                reloginService();
                //loadInvited_Unconfirmed_Questions();
                try {
                    processMessageCache();
                    if (fragLogin != null) fragLogin.dismiss();
                } catch (Throwable e) {

                }

            }

        }

        if (!relogin) restoreLast();

        this.savedInstanceState = null;

        return !relogin;

    }

    private void restoreLast() {
        if (this.lastFragment > -1 && savedInstanceState == null) {
            mPager.setCurrentItem(lastFragment);
            lastFragment = -1;
            if (fPA != null) {
                fragAnswer fA = (fragAnswer) fPA.findFragment(fragAnswer.fragID);
                if (fA != null) {
                    fA.init();
                }
            }
        } else if (savedInstanceState != null) {
            if (mPager != null && fPA != null) {
                getLast(savedInstanceState, 0);
            }

        }
    }

    private void reloginService() throws Throwable {
        lib.ShowMessageDebug(this, "4. relogin Service mService " + (mService != null),"Debug");
        if (mService != null) {
            mService.updateUserInterfaceTitle(false, true);
            if (((!mService.isWebSocketConnected) || mService.webSocketClient == null || !mService.webSocketClient.getisRunning())) {
                lib.ShowMessageDebug(this, "relogin Service started ","Debug");
                mService.activity = this;
                mService.relogin(true);
            }
        }

    }

    private void loadInvited_Unconfirmed_Questions() throws Throwable {
        lib.ShowMessageDebug(this, "loadInvited/Unconf/Questions savedInstanceState = " + (savedInstanceState != null),"Debug");
        boolean res;
        res = getInvited;
        if (!res) res = getInvited();
        if (!res) res = _getUnconfirmed();
        if (!res && !isStartedFromNotification) {
            if (readMessage == null) {
                mPager.setCurrentItem(fragHome.fragID);
                lib.ShowMessageDebug(this, "loadInvited/U/Q  loadQuestions ","Debug");
                loadQuestions();
            } else {
                mPager.setCurrentItem(fragQuestions.fragID);
                lib.ShowMessageDebug(this, "loadInvited/Unconf/Questions setFragQ readMessage = " + readMessage,"Debug");
            }
        }
    }

    /*
    public void setupLocClient()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult)
            {
                for (Location location : locationResult.getLocations())
                {
                    if (locCoarse == null || location.getTime() < location.getTime() || (location.getAccuracy() > 0.0f && location.getAccuracy() < locCoarse.getAccuracy()))
                    {
                        locCoarse = location;
                    }
                }
            }

            ;
        };
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>()
                            {
                                @Override
                                public void onSuccess(Location location)
                                {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null)
                                    {
                                        locCoarse = location;
                                    }
                                }
                            });
    }
    */

    private void ShowFragLogin() {
        if (fragLogin != null) {
            lib.ShowMessageDebug(this, "5. showFragLogin","Debug");
            fragLogin.initdoLogin(fm, false, savedInstanceState);
            fragLogin.showlogin = this.showLogin;
            showLogin = false;
            savedInstanceStateNullAndLoadedFromPrefs = false;
            fragLogin.mode = dlgLogin.LoginMode.login;
            if (fragLogin.isAdded()) {
                fragLogin.dismiss();
            }
            try {
                fragLogin.show(fm, FRAG_LOGIN);
            } catch (Throwable ex) {
                ex.printStackTrace();
                lib.ShowException(TAG, this, ex, "Show fraglogin", false);
            }
        }
        else
        {
            lib.ShowMessageDebug(this, "ShowLogin fragLogin null","Debug");
        }
    }

    private void initPagerAndFPA() {
        /* Getting a reference to ViewPager from the layout */
        if (mPager == null || fPA == null) {
            View pager = this.findViewById(R.id.pager);
            Layout = (ViewGroup) pager;
            mPager = (HackyViewPager) pager;
            /* Getting a reference to FragmentManager */
            //fm = getSupportFragmentManager();

            setPageChangedListener();

            /* Creating an instance of FragmentPagerAdapter */
            if (fPA == null) {
                fPA = new MyFragmentPagerAdapter(fm, this, savedInstanceState != null && !savedInstanceStateNullAndLoadedFromPrefs);
            } else {
                fPA.main = this;
            }

            /* Setting the FragmentPagerAdapter object to the viewPager object */
            mPager.setAdapter(fPA);

            //setupLocClient();
        }
    }

    private void setUserInterface() {
        int UIMode = lib.getUIMode(this);
        switch (UIMode) {
            case Configuration.UI_MODE_TYPE_TELEVISION:
                isTV = true;
                break;
            case Configuration.UI_MODE_TYPE_WATCH:
                isWatch = true;
                break;
            default:
                isTablet = isTabletDevice(this);
                break;

        }
    }

    private void getLast(final Bundle savedInstanceState, final int repeats) {
        lib.ShowMessageDebug(this, "getLast " + repeats, "Debug");
        int l = savedInstanceState.getInt(LAST_FRAGMENT, -1);
        long cq = savedInstanceState.getLong(CURRENT_QUESTION, -1);
        long caa = savedInstanceState.getLong(CURRENT_ANSWER_ANSWER, -1);
        long ca = savedInstanceState.getLong(CURRENT_ANSWER, -1);
        String Answer = savedInstanceState.getString(ANSWER, "");
        String Question = savedInstanceState.getString(QUESTION, "");
        boolean fragChatVisible = savedInstanceState.getBoolean(FRAG_CHAT, false);
        Fragment f;
        f = fPA.findFragment(fragQuestions.fragID);
        fragQuestions ff = (fragQuestions) f;
        if (ff != null && ff.adapter != null && l == fragQuestions.fragID) {
            mPager.setCurrentItem(l);
        }
        lib.ShowMessageDebug(this, String.format("getLast ff = %s LastFragment = %s cq = %s ca = %s ", "" + (ff != null), "" + l, "" + cq, "" + ca), "Debug");
        if (ff == null || ff.adapter == null) {
            lib.ShowMessageDebug(this, "getLast repeat " + repeats, "Debug");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    int r = repeats;
                    r++;
                    if (r < 6) getLast(savedInstanceState, r);
                }
            }, 1000);
        } else if ((cq > -1 || caa > -1 || ca > -1)) {
            if (cq > -1) {
                lib.ShowMessageDebug(this, "getLast findQuestion", "Debug");
                currentQuestion = ff.findQuestion(cq);
                if (currentQuestion != null) {
                    if (caa > -1) {
                        currentAnswerAnswer = currentQuestion.findAnswer(caa);
                    }
                    if (ca > -1) {
                        currentAnswer = currentQuestion.findAnswer(ca);
                        if (currentAnswer != null) {
                            boolean hasRe = false;
                            Question q = currentQuestion;
                            int childPosition = q.items.indexOf(currentAnswer);
                            if (childPosition >= 0) {
                                de.com.limto.limto1.Controls.Answer a = currentAnswer;
                                if (childPosition < q.items.size() - 1) {
                                    if (q.items.get(childPosition + 1).ParentFrageAntwortenID == a.ID
                                            || q.items.get(childPosition + 1).reAntwortID == a.ID) {
                                        hasRe = true;
                                    }
                                }

                            }
                            this.hasRe = hasRe;
                        }

                    }

                    if (l == fragAnswer.fragID) {
                        mPager.setCurrentItem(l);
                        lib.ShowMessageDebug(this, "ShowFragAnswer", "Debug");
                        fragAnswer fA = (fragAnswer) fPA.findFragment(l);
                        if (fA != null) {
                            fA.init();
                            fA.txtAnswer.setText(Answer);
                        }
                    }
                } else {
                    lib.ShowMessageDebug(this, "getLast repeat " + repeats, "Debug");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int r = repeats;
                            r++;
                            if (r < 5) getLast(savedInstanceState, r);
                        }
                    }, 1000);

                }
            }
        }
        if (ff != null && ff.adapter != null && l == fragQuestion.fragID) {
            mPager.setCurrentItem(l);
            lib.ShowMessageDebug(this, "getLast loadQuestion", "Debug");
            fragQuestion fQ = (fragQuestion) fPA.findFragment(fragQuestion.fragID);
            if (fQ != null) {
                if (currentQuestion != null) fQ.init(currentQuestion);
                fQ.txtQuestion.setText(Question);
                try {
                    //Calendar cal = Calendar.getInstance();
                    //Date startDate = cal.getTime();
                    //cal.setTime(clsHTTPS.fmtDateTime.parse(savedInstanceState.getString(DT_END)));
                    //long startTime = startDate.getTime();
                    //long endTime = cal.getTime().getTime();
                    //long diffTime = endTime - startTime;
                    //long diffDays = diffTime / (1000 * 60 * 60 * 24);
                    //((ArrayAdapter)fQ.spnLaufzeit.getAdapter()).add(diffDays + getString(R.string.days));
                    int pos = savedInstanceState.getInt(SPN_LAUFZ);
                    fQ.spnLaufzeit.setSelection(pos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fQ.spnEntf.setSelection(savedInstanceState.getInt(SPN_ENTF));
                fQ.spnFehlergrad.setSelection(savedInstanceState.getInt(SPN_FEHLERGRAD));
                fQ.btnOeffentlich.setChecked(savedInstanceState.getBoolean(BTN_OEFFENTLICH));
                fQ.spnGrades.setSelection(savedInstanceState.getInt(SPN_GRADES));
            }
        }

        String r = savedInstanceState.getString("pendingRead", "");
        if (r.length() > 0) {
            Object o = new Gson().fromJson(r, new TypeToken<ArrayList<JSONObject>>() {
            }.getType());
            dlgChat.pendingRead = (ArrayList<JSONObject>) o;
        }

        if (fragChatVisible && !fragChatCalled) {
            lib.ShowMessageDebug(this, "getLast ShowFragchat", "Debug");
            fragChatCalled = true;
            showLastFragChat(savedInstanceState);
        }


    }

    boolean showLastFragChat(final Bundle savedInstanceState) {
        if (user != null) {
            Long BenutzerIDEmpfaenger = savedInstanceState.getLong(BENUTZERIDEMPFAENGER);
            String NameEmpfaenger = savedInstanceState.getString(NAMEEMPFAENGER, "X");
            if (fragChat == null) {
                fragChat = showFragChat(false, BenutzerIDEmpfaenger, NameEmpfaenger, true, false);
            }
            return true;
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showLastFragChat(savedInstanceState);
                }
            }, 1000);
        }
        return false;
    }

    private void loadQuestions() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment f;
                f = fPA.findFragment(fragQuestions.fragID);
                fragQuestions ff = (fragQuestions) f;
                if (ff != null && (ff.questions == null || ff.questions.size() == 0 || ff.adapter == null || ff.adapter.rows == null || ff.adapter.rows.size() == 0))
                    try {
                        ((fragQuestions) f).initFragQuestions(false, -1, -1, null, null, null, null, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                else if (f == null) {
                    loadQuestions();
                } else {
                    if (ff.adapter != null) ff.adapter.notifyDataSetChanged();
                }
            }
        }, 500);


    }

    private void doLogin() {
        if (fragLogin != null) {
            fragLogin.doLogin(fm, false, savedInstanceState);
        } else {
            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doLogin();
                }
            }, 100);*/
        }
    }

    public void pickImage() {
        if (user == null) return;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        dontStop++;
        startActivityForResult(intent, PICK_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RVIEWFILE) MainActivity.dontStopView = true;
        if (dontStop > 0 && requestCode != RVIEWFILE) dontStop--;
        if (dontStop == 0) LimindoService.dontStop = false;
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
                double fact = bmp.getHeight() > bmp.getWidth() ? 200d / bmp.getHeight() : 200d / bmp.getWidth();
                bmp = Bitmap.createScaledBitmap(bmp, (int) (bmp.getWidth() * fact), (int) (bmp.getHeight() * fact), false);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                inputStream = new ByteArrayInputStream(out.toByteArray());
                String fname = getFileName(data.getData());
                fname = ".JPG";
                Long BenutzerID = user.getLong(Constants.id);
                fname = "Image" + BenutzerID + fname;
                if (clsHTTPS.hashImages.containsKey(fname)) clsHTTPS.hashImages.replace(fname, bmp);
                user.put(Constants.Image, bmp);
                clsHTTPS.upload(this, inputStream, fname, BenutzerID, null);
            } catch (Throwable e) {
                lib.ShowException(TAG, this, e, false);
                e.printStackTrace();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        } else if (requestCode == SELECT_FILE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                Uri thumb = null;
                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                String fname = ".txt";
                long size = 0;
                String path = "unknown";
                Uri selectedfile = data.getData(); //The uri with the location of the file
                String mimeType = getContentResolver().getType(selectedfile);
                try {
                    size = Long.parseLong(lib.getSizeFromURI(getContext(), selectedfile));
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
                try {
                    String p = lib.getPath(getContext(), selectedfile);
                    if (p != null) path = p;
                    fname = path;
                    File f = new File(path);
                    fname = f.getName();
                    if (f.length() > 0) size = f.length();
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
                UUID uuid = UUID.randomUUID();
                Long BenutzerID = user.getLong(Constants.id);
                fname = BenutzerID + "__" + uuid + "__" + fname;
                if (fragChat != null)
                    fragChat.setAttachment(fragChat.editText.getText().toString(), mimeType, path, fname, size, uuid, selectedfile);
                clsHTTPS.upload(this, inputStream, fname, BenutzerID, fragChat);
            } catch (Throwable e) {
                lib.ShowException(TAG, this, e, false);
                e.printStackTrace();
            }
            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
        }

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        else if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Log.e("signingoogle", "ActivityResult");
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }


    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            this.account = account;
            // Signed in successfully, show authenticated UI.
            if (fragLogin != null) {
                Log.e("signingoogle", "call fragLogin");
                fragLogin.updateUI(account);
                fragLogin.Login();
            }
        } catch (ApiException e) {
            e.printStackTrace();
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, "signInResult:failed code=" + e.getStatusCode());
            ShowMessage(this, getString(R.string.googleloginnotsuccessfull) + "\n" + getString(R.string.ErrorCode) + GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()) + ":" + e.getStatusCode(), getString(R.string.Error));
            if (fragLogin != null) fragLogin.updateUI(null);
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION) {//&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                init1(savedInstanceState);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
                lib.ShowException(TAG, this, throwable, false);
            }
        }
    }

    public Long getBenutzerID() throws Exception {
        if (benutzerID == null) benutzerID = user.getLong(Constants.id);
        return benutzerID;
    }

    public void setBenutzerID(Long benutzerID) {
        this.benutzerID = benutzerID;
    }

    public boolean isAdmin() throws Exception {
        if (admin == null) admin = user.getBoolean(Constants.administrator);
        return admin;
    }

    public Date getLastLogoffDate() {
        try {
            Date lastLogoff = fmtDateTime.parse(user.getString(DATUMLOGOFF));
            return (lastLogoffDate == null) ? lastLogoff : lastLogoffDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastLogoffDate;
    }

    public Context getContext() {
        return this;
    }

    public int getNewAnswersOther() {
        return newAnswersOther;
    }

    public void setNewAnswersOther(int newAnswersOther) {

        this.newAnswersOther = newAnswersOther;
    }

    public int getNewQuestions() {
        return newQuestions;
    }

    public void setNewQuestions(int newQuestions) {
        this.newQuestions = newQuestions;

    }

    public int getNewAnswers() {
        return newAnswers;
    }

    public void setNewAnswers(int newAnswers) {
        this.newAnswers = newAnswers;
    }

    public int getOwnQuestions() {
        return ownQuestions;
    }

    public void setOwnQuestions(int ownQuestions) {
        this.ownQuestions = ownQuestions;
    }

    public int getOwnQuestionsAnswered() {
        return ownQuestionsAnswered;
    }

    public void setOwnQuestionsAnswered(int ownQuestionsAnswered) {
        this.ownQuestionsAnswered = ownQuestionsAnswered;
    }

    public int getOwnQuestionsUnanswered() {
        return ownQuestionsUnanswered;
    }

    public void setOwnQuestionsUnanswered(int ownQuestionsUnanswered) {
        this.ownQuestionsUnanswered = ownQuestionsUnanswered;
    }

    public int getOwnQuestionsNotFetched() {
        return ownQuestionsNotFetched;
    }

    public void setOwnQuestionsNotFetched(int ownQuestionsNotFetched) {
        this.ownQuestionsNotFetched = ownQuestionsNotFetched;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        boolean res = onOptionsItemSelected(item);
        if (res) drawer.closeDrawers();
        return false;
    }

    private void increasemenufontsize(Menu menu, Float amount) {
        if (amount == 100) return;
        float factor = amount / 100f;
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(item.getTitle().toString());
            int end = spanString.length();
            if (amount == 0f) {
                RelativeSizeSpan[] spans = spanString.getSpans(0, end, RelativeSizeSpan.class);
                {
                    for (RelativeSizeSpan span : spans) {
                        spanString.removeSpan(span);
                    }
                }
            } else {
                spanString.setSpan(new RelativeSizeSpan(factor), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            item.setTitle(spanString);

            if (item.getSubMenu() != null) {
                item.getSubMenu().clearHeader();//setHeaderTitle("");//spanString);
                increasemenufontsize(item.getSubMenu(), amount);
            }
        }
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }

    public void loop(int ms) {
        try {
            //  At this point all draw events should be in the event queue,
            //  now add another event to the end of queue to finish the loop once everything is processed.
            Handler handler = new Handler(Looper.myLooper());

            handler.postDelayed(new Runnable() {


                @Override
                public void run() {
                    throw new FinishLoopRunTimeException();
                }
            }, ms);

            Looper.loop();
        } catch (FinishLoopRunTimeException e) {
        }
    }

    private void requestPermission(Bundle savedInstanceState) {
        if (savedInstanceState == null && Build.VERSION.SDK_INT >= M) {
            this.savedInstanceState = savedInstanceState;
            //requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_LOGS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_NETWORK_STATE}, REQUEST_WRITE_PERMISSION);
            if (lib.isOnMainThread()) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECEIVE_BOOT_COMPLETED, Manifest.permission.INTERNET, Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_WIFI_STATE}, REQUEST_WRITE_PERMISSION);
                try {
                    //init1(savedInstanceState);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            } else {
                ShowMessage(this, "Can't request permissions!", "Error");
            }
        } else {
            try {
                init1(savedInstanceState);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
                lib.ShowException(TAG, this, throwable, false);
            }
        }
    }

    public Location locCoarse() {
        if (mService != null) return mService.locCoarse;
        return null;
    }

    private void AcceptLicense() throws Throwable {
        boolean blnLicenseAccepted = getPreferences(Context.MODE_PRIVATE).getBoolean(LICENSE_ACCEPTED, false);
        if (!blnLicenseAccepted) {
            InputStream is = this.getAssets().open(LICENSE);
            Scanner s = new Scanner(is).useDelimiter(__A);
            String strLicense = s.hasNext() ? s.next() : "";
            s.close();
            is.close();
            lib.ShowMessageYesNo(this,
                    strLicense,
                    getString(R.string.licenseaccept),
                    true, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                lib.AcceptPrivacyPolicy(MainActivity.this, Locale.getDefault(), false);
                            } catch (Throwable ex) {
                                lib.ShowException(TAG, MainActivity.this, ex, "On Resume", false);
                            }
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }, null);

            //lib.yesnoundefined res2 = lib.AcceptPrivacyPolicy(this, Locale.getDefault());

            //lib.yesnoundefined res3 = lib.AcceptDisclaimer(this, Locale.getDefault());

            /*
            if (res == lib.yesnoundefined.yes && res2 == lib.yesnoundefined.yes && res3 == lib.yesnoundefined.yes) {
                getPreferences(Context.MODE_PRIVATE).edit().putBoolean(LICENSE_ACCEPTED, true).apply();
            } else {
                finish();
            }

             */

        }
        else
        {
            try {
                processCacheIfVisible();
                bindService0();
            } catch (Throwable ex) {
                lib.ShowException(TAG, this, ex, "On Resume", false);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        lib.ShowMessageDebug(this, "On Resume", "Debug");
        {
            try {
                boolean blnLicenseAccepted = getPreferences(Context.MODE_PRIVATE).getBoolean(LICENSE_ACCEPTED, false);
                if (!blnLicenseAccepted) {
                    AcceptLicense();
                }
                else
                {
                    try {
                        processCacheIfVisible();
                        bindService0();
                    } catch (Throwable ex) {
                        lib.ShowException(TAG, this, ex, "On Resume", false);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }


    }

    public void processCacheIfVisible() {
        if (!isvisible) {
            isvisible = true;
            processMessageCacheChat(_MessageCacheChat);
            if (mService != null) {
                processMessageCacheChat(mService.MessageCacheChatService);
                getSharedPreferences("LimindoService", Context.MODE_PRIVATE).edit().putString("MessageCacheChatService", "").apply();
            }
            /**ViewTreeObserver vto = _main.layout.getViewTreeObserver();
             final View finalConvertView = convertView;
             vto.addOnGlobalLayoutListener (new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override public void onGlobalLayout() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            _main.layout.getViewTreeObserver()
            .removeOnGlobalLayoutListener(this);
            } else {
            _main.layout.getViewTreeObserver()
            .removeGlobalOnLayoutListener(this);
            }
            return getView(i, finalConvertView, viewGroup);
            }
            });
             **/
            processMessageCache();

        }
    }

    private void processMessageCache() {
        if (MessageCache != null && fPA != null) {
            for (String msg : MainActivity.MessageCache) {
                try {
                    if (msg != null && msg.length() > 1) processReadMessage(msg, true, true, false);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    lib.ShowException(TAG, this, throwable, false);
                }
            }
            MessageCache.clear();
        }
    }

    private void processMessageCacheChat(final ArrayList<String> pMessageCacheChat)  {
        if (user != null && mService != null && pMessageCacheChat != null) {
            //Debug
            lib.setStatusAndLog(this, TAG, ("*** Process MessageCacheChat MainActivity size " + pMessageCacheChat.size()));
            for (String r : pMessageCacheChat) {
                try {
                    processReadMessage(r, true, true, false);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    lib.ShowException(TAG, this, throwable, true);
                }
            }
            pMessageCacheChat.clear();
        } else if (pMessageCacheChat != null && pMessageCacheChat.size() > 0) {
            lib.setStatusAndLog(this, TAG, ("*** Process MessageCacheChat MainActivity restart size " + pMessageCacheChat.size()));
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    processMessageCacheChat(pMessageCacheChat);
                }
            }, 1000);
        }

    }

    public void bindService0() {
        if (mServiceRequested || mServiceInitialized) return;
        lib.setStatusAndLog( this, TAG, "bindService0");
        this.getSupportActionBar().setSubtitle(R.string.connectingservice);
        if (mService == null || mService.blnWasDestroyed) {
            // Bind to LocalService
            try {
                lib.ShowMessageDebug(this, "Service null binding new Service","Debug");
                startNewServiceAndBind();
            } catch (Throwable ex) {
                ex.printStackTrace();
                lib.ShowException(TAG, this, ex, CREATE_SERVICE,false);
            }
        } else {
            initServiceAndReconnectIfNotConnected();
        }

        checkService();

    }

    private void checkService() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActionBar t = getSupportActionBar();
                if (mService == null) {
                    Log.e(TAG, getString(R.string.ServiceNull));//lib.ShowMessage(MainActivity.this, "Service null", "Error");
                    if (t != null) t.setSubtitle(R.string.ServiceNull);
                } else if (mService.mHandler != mHandler) {
                    Log.e(TAG, getString(R.string.WrongHandler));//lib.ShowMessage(MainActivity.this, "Wrong Handler", "Error");
                    if (t != null) t.setSubtitle(R.string.WrongHandler);
                }

            }
        }, 10000);

    }

    private void initServiceAndReconnectIfNotConnected() {
        mServiceInitialized = true;
        mService.removeRunnableStop();
        broadcastReceiver.service = mService;
        mServiceRequested = false;
        try {
            lib.ShowMessageDebug(this, "initServiceAndReconnectIfNotConnected","Debug");
            mService.init(MainActivity.this, MainActivity.this, mHandler, fragLogin, db, true);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mService.getState() != STATE_CONNECTED && mService.getState() != STATE_CONNECTING) {
                            mService.reconnect(false, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.i(TAG, ON_RESUME_RECONNECT_SERVICE);
                }
            }, 3000);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
        lib.setStatusAndLog(MainActivity.this, TAG, "*** bindservice0 initService + Messages: "+ mService.MessageCacheChatService.size());

    }

    private void startNewServiceAndBind() throws Throwable{
        getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).edit().putInt("restartInterval", RESTARTINTERVALL).apply();
        Intent intent = new Intent(this, LimindoService.class);
        intent.setAction("bindService0");
        if ((ServiceStarted++ < 5)) {
            lib.ShowMessageDebug(this, "starting Service","Debug");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                this.startForegroundService(intent);
            } else {
                this.startService(intent);
            }
        }

        mConnection.requestCode = 0; // requestCode;
        mConnection.resultCode = 0; //resultCode;
        mConnection.data = null; //data;
        this.bindService(intent, mConnection, Context.BIND_ADJUST_WITH_ACTIVITY);
        mServiceRequested = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mServiceRequested = false;
                initialized = true;
                if (mService == null) lib.ShowMessageDebug(MainActivity.this, "Service null after bind","Debug");
            }
        }, 5000);
    }

    private void processGroupMessage(String readMessage) throws Throwable {
        ExpandableListView lv = null;
        fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
        if (qq != null) {
            lv = qq.lv;
        }
        fragQuestion q = (fragQuestion) fPA.findFragment(fragQuestion.fragID);

        if (readMessage.startsWith("123456789")) {
            //Fragen gefunden zum auffüllen von txtQuestion
            processAutoCompleteQuestion(q, readMessage);
        } else if (readMessage.startsWith("12345678")) {
            //Suchen gefunden zum Auffüllten von txtSearch
            processAutoCompleteTxtSearch(qq,readMessage);
        } else if (readMessage.startsWith("1234567")) {
            processQuestionFetched(qq, lv, readMessage);
        } else {
            processMultipleAnswersFromReadMessage(qq, readMessage);
            /*
            View groupview = g.groupview;
            Question question = g.question;
            TextView textView = (TextView) groupview.findViewById(R.id.txtGrad);
            ImageView iv = (ImageView) groupview.findViewById(R.id.imgQuestion);
            if (question.fetched) {
                int count = -1;
                long userid = -1;
                if (this.mService != null) userid = this.mService.getUserID();
                if (this.user != null) try {
                    userid = this.user.getLong(Constants.id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (Answer a : question.items) {
                    try {
                        if (count == -1) count = 0;
                        if (a.BenutzerID != userid && (a.BenutzerIDSender == null || a.BenutzerIDSender != userid)) {
                            count++;
                        }
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
                int countFremde = count;
                count = -1;
                if (count == -1) count = question.items.size();
                String grad = getString(R.string.grade) + question.Grad + "(" + question.Kontaktgrad + ")";
                int lGrad = grad.length();
                //grad += " " + question.Locale + " " + count + " " + getString(R.string.answers);
                grad += " " + count + " " + getString(R.string.answers);
                int colorGrad = question.Grad >= 0 ? QuestionColors[question.Grad] : ContextCompat.getColor(this, R.color.colorGradMinus);
                SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(grad);
                wordtoSpan.setSpan(new BackgroundColorSpan(colorGrad), 0, lGrad, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
                textView.setText(wordtoSpan);

                ImageView imgIndicator = g.groupview.findViewById(R.id.imgIndicator);
                if (count > 0) {

                    if (lv != null && lv.isGroupExpanded(g.groupposition)) {
                        imgIndicator.setImageDrawable(getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage_open)));
                    } else {
                        imgIndicator.setImageDrawable(this.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
                    }
                } else {
                    imgIndicator.setImageDrawable(getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_unbeantwortetefrage)));
                }

                //textView.setText(question.Grad + "(" + question.Kontaktgrad + ")" + " " + count + " Antwort(en)");
                //QuestionsAdapter.this.notifyDataSetChanged();
                Date dt = question.DatumStart;
                textView = (TextView) groupview.findViewById(R.id.txtDate);
                textView.setText((question.oeffentlich != null && question.oeffentlich ? getString(R.string.__public) : getString(R.string.__private)) + dtFormatDayTime.format(dt));
                //textView.setText(dtFormatDayTime.format(dt));
                boolean blnNeueAntwort = false;
                for (Answer a : question.items) {
                    if (a.Datum.after(getLastLogoffDate())) {
                        blnNeueAntwort = true;
                        break;
                    }
                }
                dt = question.DatumNeu;
                int colorText;
                new QuestionsAdapter.GetImageTask(groupview, question, null, this).execute();
                boolean bold = false;
                if (question.Fehlergrad != null) {
                    textView.setText("{" + question.Fehlergrad + "}" + textView.getText());
                    colorText = (this.getResources().getColor(R.color.colorError));
                    iv.setBackgroundResource(R.color.colorError);
                    imgIndicator.setBackgroundResource(R.color.colorError);
                } else if (blnNeueAntwort) {
                    iv.setBackgroundResource(R.color.colorNeueAntwort);
                    colorText = (this.getResources().getColor(R.color.colorNeueAntwort));
                    if (lv != null && lv.isGroupExpanded(g.groupposition)) {
                        imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neueantworten_open));
                    } else {
                        imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neueantworten));
                    }
                    bold = true;
                } else if (question.DatumStart.after(this.getLastLogoffDate())) {
                    iv.setBackgroundResource(R.color.colorNeueFrage);
                    if (question.items.size() == 0) {
                        imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neuefragen));
                    } else if (lv.isGroupExpanded(g.groupposition)) {
                        imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
                    } else {
                        imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
                    }
                    colorText = (this.getResources().getColor(R.color.colorNeueFrage));
                    bold = true;
                } else {
                    iv.setBackgroundResource(0);
                    colorText = (this.getResources().getColor(R.color.colorDate));
                }
                String stxtDate = textView.getText().toString();
                int color = (question.Grad >= 0 ? LevelColors[question.Grad] : ContextCompat.getColor(this, R.color.colorGradMinussolid));
                stxtDate = (question.Grad >= 6 ? digits[question.Grad] : "■") + stxtDate;
                wordtoSpan = new SpannableStringBuilder(stxtDate);
                wordtoSpan.setSpan(new ForegroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
                wordtoSpan.setSpan(new ForegroundColorSpan(colorText), 1, stxtDate.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
                if (bold || blnAccessibility)
                    wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD), 1, stxtDate.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
                //new ImageSpan(this,R.drawable.common_full_open_on_phone);
                //wordtoSpan.setSpan(new BackgroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
                textView.setText(wordtoSpan);

                textView.setError(null);
            } else {
                textView.setError(getString(R.string.Fehler));
                //if (this.ex != null) Log.e(TAG,null,ex);
            }

             */
        }
    }

    private void processMultipleAnswersFromReadMessage(fragQuestions qq, String readMessage) throws Throwable {
        JSONArray j = new JSONArray(readMessage.substring(6));
        ArrayList<Long> ids = new ArrayList<>();
        ArrayList<Long> ids2 = new ArrayList<>();
        QuestionsAdapter.Group g = null;
        lib.Ref<Boolean> isReading = new lib.Ref<>(false);
        for (int ii = 0; ii < j.length(); ii++) {
            JSONObject o = j.getJSONObject(ii);
            long BenutzerFragenID = o.getLong(BENUTZERFRAGENID);
            g = getGroup(g, BenutzerFragenID, isReading);
            if (g == null) {
                return;
            }
            long id = o.getLong(Constants.id);
            long id2 = -1L;
            if (!o.isNull(ID_2)) {
                id2 = o.getLong(ID_2);
            }
            if (!ids.contains(id)) { // && (Folder.Grad == 0 || o.getLong("benutzerid") == BenutzerID)) {
                addNewAnswer(g,o,ids,id);
            }
            if ((id2 > -1 && !ids2.contains(id2))) { // && (Folder.Grad == 0 || o.getLong("benutzerid") == BenutzerID)) {
                addReAnswer(id2, g, o, ids2);
            }
        }
        {
            groups.remove(g);
        }
        qq.adapter.processGroup(g.question.fetched, getString(R.string.Fehler), g, null);

    }

    private QuestionsAdapter.Group getGroup(QuestionsAdapter.Group g, long BenutzerFragenID, lib.Ref<Boolean> isReading) {
        if (g == null) {
            g = groups.get(BenutzerFragenID);
            if (g != null) {
                if (g.question.fetched) {
                    Log.i(TAG, QUESTION1 + g.question.Frage + IS_ALREADY_FETCHED);
                    //return;
                    if (isReading.get()) return null;
                    g.question.items.clear();
                } else {
                    g.question.fetched = true;
                }
                isReading.set(true);
            } else {
                System.out.println(" BenutzerFragenID " + BenutzerFragenID); //"processgroupmessage 123456 ID not found!");
                return g;
            }
        }
        return g;

    }

    private void addReAnswer(long id2, QuestionsAdapter.Group g, JSONObject o, ArrayList<Long> ids2) throws Throwable {
        Answer a = new Answer(this, g.question, o);
        try {
            a.update2(g.question, o);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(this,TAG,"answer.update2", throwable);
        }
        if (a.reAntwortID > 0) {
            reAntwortEinsortieren(g,a);
        } else {
            g.question.items.add(a);
        }
        //g.question.items.add(a);
        ids2.add(id2);
    }

    private void reAntwortEinsortieren(QuestionsAdapter.Group g, Answer a) throws Exception {
        for (int i = 0; i < g.question.items.size(); i++) {
            if (g.question.items.get(i).ID == a.reAntwortID) {
                a.level = g.question.items.get(i).level + 1;
                i++;
                while (i < g.question.items.size() && g.question.items.get(i).reAntwortID == a.reAntwortID) {
                    i++;
                }
                g.question.items.add(i, a);
                break;
            }
            else if (i == g.question.items.size() -1)
            {
                //wenn der Eintrag nicht gefunden wurde
                lib.ShowException(TAG, this, new Exception("reAntwortID not found!"),false);
                break;
            }
        }
    }

    private void addNewAnswer(QuestionsAdapter.Group g, JSONObject o, ArrayList<Long> ids, long id) throws JSONException, ParseException {
        Answer a = new Answer(this, g.question, o);
        g.question.items.add(a);
        ids.add(id);
    }

    private void processQuestionFetched(fragQuestions qq, ExpandableListView lv, String readMessage) throws JSONException {
        JSONObject o = new JSONObject(readMessage.substring(7));
        long BenutzerFragenID = o.getLong(BENUTZER_FRAGEN_ID);
        QuestionsAdapter.Group g = groups.get(BenutzerFragenID);
        if (g == null) {
            System.out.println(BenutzerFragenID); // "processgroupmessage notfound");
            return;
        } else {

            groups.remove(g);
            System.out.println(g.question.Frage + " " + g.question.ID); // "processgroupmessage remove");

        }

        if (!g.question.fetched) {
            g.question.fetched = true;
        } else {
            Log.i(TAG, QUESTION1 + g.question.Frage + IS_ALREADY_FETCHED);
        }

        qq.adapter.processGroup(true,"",g,null);
        /*
        View groupview = g.groupview;
        Question question = g.question;

        TextView textView = (TextView) groupview.findViewById(R.id.txtGrad);
        ImageView iv = (ImageView) groupview.findViewById(R.id.imgQuestion);


        int count = -1;
        long userid = -1;
        if (this.mService != null) userid = this.mService.getUserID();
        if (count == -1) count = question.items.size();
        String grad = getString(R.string.grade) + question.Grad + "(" + question.Kontaktgrad + ")";
        int lGrad = grad.length();
        //grad += " " + question.Locale + " " + count + " " + getString(R.string.answers);
        grad += " " + count + " " + getString(R.string.answers);
        int colorGrad = question.Grad >= 0 ? QuestionColors[question.Grad] : ContextCompat.getColor(this, R.color.colorGradMinus);
        SpannableStringBuilder wordtoSpan = new SpannableStringBuilder(grad);
        wordtoSpan.setSpan(new BackgroundColorSpan(colorGrad), 0, lGrad, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        //wordtoSpan.setSpan(new ForegroundColorSpan(colorText), 1, wordtoSpan.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(wordtoSpan);
        ImageView imgIndicator = g.groupview.findViewById(R.id.imgIndicator);
        if (count > 0) {
            if (lv != null && lv.isGroupExpanded(g.groupposition)) {
                imgIndicator.setImageDrawable(getResources().getDrawable(lib.getDrawableResFromTheme(this, R.attr.ic_beantwortetefrage_open)));
            } else {
                imgIndicator.setImageDrawable(this.getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_beantwortetefrage)));
            }
        } else {
            imgIndicator.setImageDrawable(getResources().getDrawable(lib.getDrawableResFromTheme(getContext(), R.attr.ic_unbeantwortetefrage)));
        }
        //QuestionsAdapter.this.notifyDataSetChanged();
        Date dt = question.DatumStart;
        textView = (TextView) groupview.findViewById(R.id.txtDate);
        textView.setText((question.oeffentlich != null && question.oeffentlich ? getString(R.string.__public) : getString(R.string.__private)) + dtFormatDayTime.format(dt));
        //textView.setText(dtFormatDayTime.format(dt));
        int textColor;
        new QuestionsAdapter.GetImageTask(groupview, question, null, this).execute();
        boolean bold = false;
        if (question.Fehlergrad != null) {
            textView.setText("{" + question.Fehlergrad + "}" + textView.getText());
            textColor = (this.getResources().getColor(R.color.colorError));
            iv.setBackgroundResource(R.color.colorError);
        } else if (question.DatumStart.after(this.getLastLogoffDate())) {
            iv.setBackgroundResource(R.color.colorNeueFrage);
            if (question.items.size() == 0) {
                imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neuefragen));
            } else if (lv.isGroupExpanded(g.groupposition)) {
                imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neuefragealteantworten_open));
            } else {
                imgIndicator.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_neuefragealteantworten));
            }
            textColor = (this.getResources().getColor(R.color.colorNeueFrage));
            bold = true;
        } else {
            iv.setBackgroundResource(0);
            textColor = (this.getResources().getColor(R.color.colorDate));
        }
        String stxtDate = textView.getText().toString();
        int color = (question.Grad >= 0 ? LevelColors[question.Grad] : ContextCompat.getColor(this, R.color.colorGradMinussolid));
        stxtDate = (question.Grad >= 6 ? digits[question.Grad] : "■") + stxtDate;
        wordtoSpan = new SpannableStringBuilder(stxtDate);
        wordtoSpan.setSpan(new ForegroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(textColor), 1, stxtDate.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (bold || blnAccessibility)
            wordtoSpan.setSpan(new StyleSpan(Typeface.BOLD), 1, stxtDate.length(), SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        //wordtoSpan.setSpan(new BackgroundColorSpan(color), 0, 1, SpannableStringBuilder.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(wordtoSpan);

        textView.setError(null);
*/
    }

    private void processAutoCompleteTxtSearch(fragQuestions qq, String readMessage) throws JSONException {
        if (qq != null) {
            JSONArray j = new JSONArray(readMessage.substring(8));
            List<Names> a = new ArrayList<>();
            for (int i = 0; i < j.length(); i++) {
                JSONObject o = j.getJSONObject(i);
                a.add(new Names(StringEscapeUtils.unescapeJava(o.getString(FRAGE))));
            }
            NamesAdapter namesAdapter = new NamesAdapter(
                    MainActivity.this,
                    R.layout.fragquestions,
                    R.id.lbl_name,
                    a
            );
            //set adapter into listStudent
            qq.txtSearch.setAdapter(namesAdapter);
            qq.txtSearch.showDropDown();
            //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,a);
            //q.txtSearch.setAdapter(adapter);
        }}

    private void processAutoCompleteQuestion(fragQuestion fragQuestion, String readMessage) throws JSONException {
        if (fragQuestion != null) {
            JSONArray jsonArray = new JSONArray(readMessage.substring(9));
            List<Names> listNames = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject o = jsonArray.getJSONObject(i);
                String Frage = StringEscapeUtils.unescapeJava(o.getString(FRAGE));
                //if (Frage.startsWith(VOTE)) Frage = Frage.substring(VOTE.length());
                listNames.add(new Names(Frage));
            }
            NamesAdapter namesAdapter = new NamesAdapter(
                    MainActivity.this,
                    R.layout.fragquestions,
                    R.id.lbl_name,
                    listNames
            );
            //set adapter into listStudent
            fragQuestion.txtQuestion.setAdapter(namesAdapter);
            fragQuestion.txtQuestion.showDropDown();
            //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,a);
            //q.txtSearch.setAdapter(adapter);
        }
    }

    public void processReadMessage(String readMessage, boolean dontsend, boolean noTime, boolean show) throws Throwable {
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        try {
            jsonArray = getJSAONArrayFromReadMessage(readMessage);
            if (jsonArray == null) return;
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                if (resolvedReadMessages.contains(readMessage)) return;
                //resolvedReadMessages.add(readMessage);
                LimindoService.SendType t = LimindoService.SendType.valueOf(jsonObject.optString(SENDTYPE, "none"));
                lib.setStatusAndLog(this, TAG, ("*** Process readmessage " + jsonObject.optString(SENDTYPE, "none")));
                if (t == LimindoService.SendType.ValueAnswer) {
                    AntwortBewerten(jsonObject);
                } else if (t == LimindoService.SendType.Answer) {
                    AntwortAendernOderEinfuegen(jsonObject, dontsend);
                } else if (t == LimindoService.SendType.DelAnswer) {
                    AntwortLoeschen(jsonObject);

                } else if (t == LimindoService.SendType.Question) {
                    FrageEinfuegenOderLoeschen(jsonObject, noTime, dontsend);
                } else if (t == LimindoService.SendType.DelQuestion) {
                    FrageLoeschen(jsonObject);
                } else if (t == LimindoService.SendType.Logon && jsonObject != null && !jsonObject.isNull(BENUTZERID)) {
                    Long BenutzerID = jsonObject.getLong(BENUTZERID);
                    int oState = jsonObject.optInt(ONLINE_STATE, jsonObject.optInt("onlinestate", 1));
                    OnlineState onlineState = OnlineState.getOnlineState(oState);

                    setOnlineStateQuestions(BenutzerID, onlineState);
                    setOnlineStateContacts2(BenutzerID, onlineState);

                } else if (t == LimindoService.SendType.Logoff) {
                    setOnlineFalse(jsonObject);
                } else if (t == Upload) {
                    String FileName = jsonObject.getString("filename");
                    if (clsHTTPS.hashImages.containsKey(FileName))
                        clsHTTPS.hashImages.remove(FileName);
                } else if (t == LimindoService.SendType.Message) {
                    lib.setStatusAndLog(this, TAG, ("*** Process readmessage Message" + jsonObject));
                    boolean typing = jsonObject.optString("Message").contains("⸙");
                    dlgChat f = showFragChat(typing, null, null, show, false);
                    try {
                        f.messageAdapter.insertObject(jsonObject);
                    } catch (Throwable ex)
                    {
                        lib.ShowException(TAG,this,ex, true);
                    }
                } else {
                    System.out.println("readmessage has no type");
                }
            }
        } catch (Throwable ex) {
            Log.e(TAG, readMessage, ex);
            lib.ShowException(TAG, this, ex, readMessage, false);
        }

    }

    private void setOnlineFalse(JSONObject o) throws JSONException {
        Long BenutzerID = o.getLong(BENUTZERID);
        Integer ostate = o.optInt(ONLINE_STATE, -10);
        OnlineState onlineState = ostate > -10 ? OnlineState.getOnlineState(ostate) : null;
        fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
        if (qq != null && qq.adapter != null && qq.adapter.rows != null) {
            for (Question q : qq.adapter.rows) {
                if (q.BenutzerID == BenutzerID) {
                    //q.update(o);
                    q.online = false;
                    if (onlineState != null) q.onlineState = onlineState;
                } else {
                }
                for (Answer a : q.items) {
                    if (a.BenutzerID == BenutzerID)
                        a.online = false;
                    if (onlineState != null) a.onlineState = onlineState;
                }
            }
            qq.adapter.notifyDataSetChanged();
            fragContacts2 kk = (fragContacts2) fPA.findFragment(fragContacts2.fragID);
            if (kk != null) {
                AllContactsAdapter a = kk.contactAdapter;
                for (ContactVO item : a.contactVOList) {
                    if (item.getUserID() == BenutzerID)
                        item.setOnline(false);
                        if (onlineState != null) item.setOnlineState(onlineState);
                }
                a.notifyDataSetChanged();
            }
        }
    }

    private void FrageLoeschen(JSONObject o) throws JSONException {
        //mPager.setCurrentItem(fragQuestions.fragID);
        Long FrageID = o.getLong(FRAGEID);
        Long BenutzerID = o.getLong(BENUTZERID);
        Long BenutzerFragenID = o.getLong(BENUTZERFRAGENID);
        Long BenutzerIDFrage = o.getLong(BENUTZERIDFRAGE);
        fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
        if (qq != null) {
            Question q = null;
            q = qq.findQuestion(BenutzerFragenID);
            if (q != null) {
                //q.update(o);
                qq.adapter.rows.remove(q);
                qq.adapter.notifyDataSetChanged();
            } else {
            }
        }
    }

    private void FrageEinfuegenOderLoeschen(JSONObject o, boolean noTime, boolean dontsend) throws Throwable {
        //mPager.setCurrentItem(fragQuestions.fragID);

        String Frage = StringEscapeUtils.unescapeJava(o.getString(FRAGE)).replace(VOTE, "");
        String search = getApplicationContext().getSharedPreferences(SEARCH, MODE_PRIVATE).getString(SEARCH, "");
        boolean matches = true;
        if (search.length() > 0) {
            matches = false;
            String[] s = search.split("\\s");
            for (String ss : s) {
                ss = ss.trim();
                if (ss.length() > 0 && Frage.toUpperCase().contains(ss.toUpperCase())) {
                    matches = true;
                    break;
                }
            }
        }
        {
            Long FrageID = o.getLong(FRAGEID);
            Long BenutzerFragenID = o.getLong(Constants.id);
            Long BenutzerID = o.getLong(BENUTZERID);
            Long OriginalID = o.getLong(ORIGINALID);
            //lib.ShowMessage(MainActivity.this, Frage, "Frage");
            fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
            int _grad = 0;
            _grad = (!o.isNull(LimindoService.HandlerUnbound.GRAD) ? o.getInt(LimindoService.HandlerUnbound.GRAD) : -2);
            if (_grad == -2)
                _grad = clsHTTPS.getKontaktgrad(o.getInt(KONTAKTGRAD), user.getLong(Constants.id), BenutzerID);
            final int grad = _grad;
            final int Grad = getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);
            if (qq != null) {
                Question q = null;
                q = qq.findQuestion(BenutzerFragenID);
                if (q == null) q = qq.findQuestion(FrageID, BenutzerID);
                if (OriginalID > -1 && q == null) {
                    q = qq.findQuestion(OriginalID);
                }
                if (q != null) {
                    q.update(o);
                    q.updateUser(clsHTTPS.getUser(user.getLong(Constants.id), q.BenutzerID));
                    q.online = true;
                } else {
                    q = new Question(this, o);
                    int Menge = getApplicationContext().getSharedPreferences(MENGE, Context.MODE_PRIVATE).getInt(MENGE, 0);
                    if (grad > -1 && grad <= Grad && matches) {
                        Double e = null;
                        if (locCoarse() != null && q.Laengengrad != null && q.Breitengrad != null) {
                            e = lib.distanceInKm(q.Breitengrad, q.Laengengrad, locCoarse().getLatitude(), locCoarse().getLongitude());
                        }
                        int Entf = getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);
                        if ((Entf == 1000 || (e != null && e <= Entf && (q.RadiusKM == 1000 || e < q.RadiusKM))) && (noTime || dtLastQuestion < Calendar.getInstance().getTimeInMillis() - (Menge * 1000))) {
                            dtLastQuestion = Calendar.getInstance().getTimeInMillis();
                            if (qq.btnAlle.isChecked() || (qq.btnKontakte.isChecked() && grad > 0) || (qq.btnEigene.isChecked() && grad == 0)) {

                                q.Grad = grad;
                                q.online = true;
                                q.updateUser(clsHTTPS.getUser(user.getLong(Constants.id), q.BenutzerID));
                                qq.adapter.rows.add(0, q);
                                if (q.emergency != null && q.emergency)
                                    mService.createNewNotification(ONGOING_NOTIFICATION_ID_QUESTION
                                            , getString(R.string.Emergency) + " " + o.getString(BENUTZERNAME)
                                            , Frage
                                            , readMessage
                                            , true
                                            , false);
                                //qq.expandGroups();

                            } else if (!dontsend && !blnNotOff && grad <= 5 && grad > 0) {
                                mService.countQuestions++;
                                mService.createNewNotification(ONGOING_NOTIFICATION_ID_QUESTION
                                        , getString(R.string.question2) + o.getString(BENUTZERNAME)
                                                + " (" + mService.countQuestions + " "
                                                + mService.getString(R.string.questions2) + ")"
                                        , Frage
                                        , readMessage
                                        , !o.isNull(EMERGENCY) && o.getInt(EMERGENCY) != 0
                                        , false);
                            }
                        }
                    }
                }
                if (q != null && qq.adapter != null) {
                    qq.adapter.blnDontSetCollapsed = true;
                    qq.adapter.notifyDataSetChanged();
                    //qq.lv.setSelection(qq.adapter.rows.indexOf(q));
                    qq.expandGroups();
                    qq.adapter.blnDontSetCollapsed = false;
                }
            } else if (!dontsend && !blnNotOff) {
                Question q = new Question(this, o);
                int Menge = getApplicationContext().getSharedPreferences(MENGE, Context.MODE_PRIVATE).getInt(MENGE, 0);
                if (grad > 0 && grad <= Grad && matches) {
                    Double e = null;
                    if (locCoarse() != null && q.Laengengrad != null && q.Breitengrad != null) {
                        e = lib.distanceInKm(q.Breitengrad, q.Laengengrad, locCoarse().getLatitude(), locCoarse().getLongitude());
                    }
                    int Entf = getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);
                    if ((Entf == 1000 || e != null && e <= Entf) && (noTime || dtLastQuestion < Calendar.getInstance().getTimeInMillis() - (Menge * 1000))) {
                        dtLastQuestion = Calendar.getInstance().getTimeInMillis();
                        mService.countQuestions++;
                        mService.createNewNotification(ONGOING_NOTIFICATION_ID_QUESTION, getString(R.string.question2)
                                        + " (" + mService.countQuestions + " "
                                        + mService.getString(R.string.questions2) + ")"
                                        + o.getString(BENUTZERNAME),
                                Frage,
                                readMessage,
                                !o.isNull(EMERGENCY) && o.getInt(EMERGENCY) != 0,
                                false);
                    }
                }
            }
        }
    }

    private void AntwortLoeschen(JSONObject o) throws JSONException {
        //mPager.setCurrentItem(fragQuestions.fragID);
        Long FrageAntwortenID = o.getLong(FRAGEANTWORTENID);
        Long BenutzerID = o.getLong(BENUTZERID);
        Long BenutzerIDFrage = o.getLong(BENUTZERIDFRAGE);
        Long BenutzerFragenID = o.getLong(BENUTZERFRAGENID);
        Long FrageID = o.getLong(FRAGEID);
        fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
        if (qq != null) {
            Question q = null;
            q = qq.findQuestion(BenutzerFragenID);
            if (q == null) q = qq.findQuestion(FrageID, BenutzerIDFrage);
            if (q != null && q.fetched) {
                Answer a = q.findAnswer(FrageAntwortenID);
                if (a != null) {
                    q.items.remove(a);
                } else {
                }
                qq.adapter.notifyDataSetChanged();
            } else if (q != null) {
                int index = qq.adapter.rows.indexOf(q);
                if (index > -1) qq.lv.expandGroup(index);
            }
        }
    }

    private void AntwortAendernOderEinfuegen(JSONObject o, boolean dontsend) throws Throwable {
        //mPager.setCurrentItem(fragQuestions.fragID);
        String Antwort = StringEscapeUtils.unescapeJava(o.getString(ANTWORT));
        Long FrageID = o.getLong(FRAGEID);
        Long BenutzerID = o.getLong(BENUTZERID);
        Long BenutzerIDFrage = o.getLong(BENUTZERIDFRAGE);
        Long BenutzerFragenID = o.getLong(BENUTZERFRAGENID);
        Long ParentFrageAntwortenID = null;
        Long reAntwortID = null;
        Long BenutzerIDSender = null;
        Long BenutzerIDEmpfaenger = null;
        boolean oeffentlich = false;
        boolean reoeffentlich = false;
        boolean isEmpfaenger = false;
        if (!o.isNull(OEFFENTLICH)) oeffentlich = o.getBoolean(OEFFENTLICH);
        if (!o.isNull(REOEFFENTLICH)) reoeffentlich = o.getBoolean(REOEFFENTLICH);
        oeffentlich |= reoeffentlich;
        if (!o.isNull(PARENTFRAGEANTWORTENID)) {
            ParentFrageAntwortenID = o.getLong(PARENTFRAGEANTWORTENID);
            if (!o.isNull("reantwortid")) {
                reAntwortID = o.getLong("reantwortid");
            }
            if (!o.isNull(BENUTZERIDSENDER))
                BenutzerIDSender = o.getLong(BENUTZERIDSENDER);

        }
        if (!o.isNull(BENUTZERIDEMPFAENGER)) {
            BenutzerIDEmpfaenger = o.getLong(BENUTZERIDEMPFAENGER);
            if (BenutzerIDEmpfaenger == user.getLong(Constants.id))
                isEmpfaenger = true;
        }
        boolean isQuestioner = BenutzerIDFrage.equals(BenutzerID);
        if (oeffentlich || showAnswers || BenutzerIDFrage == user.getLong(Constants.id) || isEmpfaenger || isQuestioner) {
            Double Breitengrad = null;
            Double Laengengrad = null;
            if (o.isNull(BREITENGRAD)) Breitengrad = null;
            else Breitengrad = o.getDouble(BREITENGRAD);
            if (o.isNull(LAENGENGRAD)) Laengengrad = null;
            else Laengengrad = o.getDouble(LAENGENGRAD);
            //lib.ShowMessage(MainActivity.this, o.getString("antwort"), "Antwort");
            fragQuestions qq = null;
            if (fPA != null)
                qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
            if (qq != null) {
                Question q = null;
                q = qq.findQuestion(BenutzerFragenID);
                if (q == null) q = qq.findQuestion(FrageID, BenutzerIDFrage);
                //if (q == null) q = qq.findQuestion(FrageID, BenutzerID);
                if (q != null && q.fetched) {
                    Long FrageAntwortenID = o.getLong(FRAGEANTWORTENID);
                    Answer a = q.findAnswer(FrageAntwortenID);
                    if (a != null) {
                        a.Antwort = Antwort;
                        a.Datum = Calendar.getInstance().getTime();
                        a.Laengengrad = Laengengrad;
                        a.Breitengrad = Breitengrad;
                        a.updateUser(clsHTTPS.getUser(user.getLong(Constants.id), a.BenutzerID));
                    } else {
                        int pos = 0;
                        a = new Answer(this, FrageAntwortenID, o.getLong(ANTWORTID), 0, Antwort, q, clsHTTPS.getKontaktgrad(q.Kontaktgrad, user.getLong(Constants.id), BenutzerID), BenutzerID, o.getString(BENUTZERNAME), o.getString(EMAIL), Calendar.getInstance().getTime(), Breitengrad, Laengengrad, true, reoeffentlich, onlineState);
                        a.updateUser(clsHTTPS.getUser(user.getLong(Constants.id), a.BenutzerID));
                        if (ParentFrageAntwortenID != null) {
                            Answer aa = null;
                            a.ParentFrageAntwortenID = ParentFrageAntwortenID;
                            a.BenutzerIDEmpfaenger = BenutzerIDEmpfaenger;
                            a.BenutzerIDSender = BenutzerIDSender;
                            if (reAntwortID != null) {
                                a.reAntwortID = reAntwortID;
                                aa = q.findAnswer(reAntwortID);
                            } else {
                                aa = q.findAnswer(ParentFrageAntwortenID);
                            }
                            if (aa != null) {
                                pos = q.items.indexOf(aa);
                                pos++;
                                if (reAntwortID != null) {
                                    a.level = aa.level + 1;
                                }
                            }
                        }
                        q.items.add(pos, a);
                    }
                    qq.adapter.notifyDataSetChanged();
                } else if (q != null && qq.adapter != null && qq.adapter.rows != null && qq.lv != null) {
                    int groupposition = qq.adapter.rows.indexOf(q);
                    try {
                        if (groupposition > -1) {
                            qq.lv.setSelectedGroup(groupposition);
                            qq.lv.expandGroup(groupposition);
                        }
                        System.out.println(groupposition);
                    } catch (Throwable ex) {
                        Log.e(M_HANDLER, EXPAND_GROUP + " " + groupposition + " " + ROWS + " " + qq.adapter.rows.size(), ex);
                    }
                } else if (q == null) {
                    JSONArray oo = clsHTTPS.getQuestion(user.getLong(Constants.id), BenutzerFragenID);
                    if (oo.length() > 0) {
                        JSONObject ooo = oo.getJSONObject(0);
                        final int grad = clsHTTPS.getKontaktgrad(ooo.getInt(KONTAKTGRAD), user.getLong(Constants.id), BenutzerIDFrage);
                        final int Grad = getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);

                        if (grad > -1 && grad <= Grad) {
                            if (qq.adapter != null && qq.adapter.rows != null && qq.lv != null && (qq.btnAlle.isChecked() || (qq.btnKontakte.isChecked() && grad > 0) || (qq.btnEigene.isChecked() && grad == 0))) {
                                Question qqq = new Question(this, ooo);
                                q = qqq;
                                q.Grad = grad;
                                q.online = true;
                                qq.adapter.rows.add(0, q);
                                qq.lv.setSelection(0);
                                qq.lv.expandGroup(0);
                                //qq.expandGroups();
                            } else if (!dontsend && !blnNotOff) {
                                Question qqq = new Question(this, ooo);
                                q = qqq;
                                q.Grad = grad;
                                q.online = true;
                                mService.countAnswers++;
                                mService.createNewNotification(ONGOING_NOTIFICATION_ID_ANSWER
                                        , getString(R.string.answer) + " " + o.getString(BENUTZERNAME)
                                                + " (" + mService.countAnswers + " "
                                                + getString(R.string.answers2) + ")"
                                        , Antwort
                                        , readMessage
                                        , q.emergency != null && q.emergency
                                        , false);
                            }
                        }

                    }
                } else if (q == null && !dontsend && !blnNotOff) {
                    mService.countAnswers++;
                    mService.createNewNotification(ONGOING_NOTIFICATION_ID_ANSWER
                            , getString(R.string.answer) + " " + o.getString(BENUTZERNAME)
                                    + " (" + mService.countAnswers + " "
                                    + getString(R.string.answers2) + ")"
                            , Antwort
                            , readMessage,
                            false,
                            false);
                }
            } else if (!dontsend && !blnNotOff) {
                mService.countAnswers++;
                mService.createNewNotification(ONGOING_NOTIFICATION_ID_ANSWER
                        , getString(R.string.answer) + " " + o.getString(BENUTZERNAME)
                                + " (" + mService.countAnswers + " "
                                + getString(R.string.answers2) + ")"
                        , Antwort
                        , readMessage
                        , false
                        , false);
            }
        }
    }

    private void AntwortBewerten(JSONObject o) throws JSONException {
        Long FrageID = o.getLong(FRAGEID);
        Long FrageAntwortenID = o.getLong(FRAGEANTWORTENID);
        Long BenutzerID = o.getLong(BENUTZERID);
        Long BenutzerIDAntwort = o.getLong(BENUTZERIDANTWORT);
        Long BenutzerIDFrage = o.getLong(BENUTZERIDFRAGE);
        Long BenutzerFragenID = o.getLong(BENUTZERFRAGENID);
        int Bewertung = o.getInt(BEWERTUNG);
        fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
        if (qq != null) {
            Question q = null;
            q = qq.findQuestion(BenutzerFragenID);
            if (q == null) q = qq.findQuestion(FrageID, BenutzerIDFrage);
            //if (q == null) q = qq.findQuestion(FrageID, BenutzerID);
            if (q != null && q.fetched) {
                Answer a = q.findAnswer(FrageAntwortenID);
                if (a != null) {
                    a.Bewertung = Bewertung;
                    qq.adapter.notifyDataSetChanged();
                }
            }
        }
    }

    private JSONArray getJSAONArrayFromReadMessage(String readMessage) {
        JSONArray o0 = null;
        try {
            try {
                JSONObject o = new JSONObject(readMessage);
                o0 = new JSONArray();
                o0.put(o);
            } catch (JSONException e) {
                o0 = new JSONArray(readMessage);
            }
        } catch (JSONException ex) {
            if (debugMode()) {
                ShowMessage(this, readMessage, "JSONException ProcessReadMessage");
            }
            Log.i(TAG, readMessage, ex);
            return o0;
        }
        return  o0;
    }

    public void setOnlineStateContacts2(Long BenutzerID, OnlineState onlineState) {
        if (fPA == null) return;
        fragContacts2 kk = (fragContacts2) fPA.findFragment(fragContacts2.fragID);
        if (kk != null && kk.contactAdapter != null) {
            AllContactsAdapter a = kk.contactAdapter;
            for (ContactVO item : a.contactVOList) {
                if (item.getUserID() == BenutzerID) {
                    item.setOnline(true);
                    item.setOnlineState(onlineState);
                }
            }
            a.notifyDataSetChanged();
        }

    }

    public void setOnlineStateQuestions(long BenutzerID, OnlineState onlineState) {
        if (fPA == null) return;
        fragQuestions qq = (fragQuestions) fPA.findFragment(fragQuestions.fragID);
        if (qq != null && qq.adapter != null && qq.adapter.rows != null) {
            for (Question q : qq.adapter.rows) {
                if (q.BenutzerID == BenutzerID) {
                    //q.update(o);
                    q.online = true;
                    q.onlineState = onlineState;
                }
                for (Answer a : q.items) {
                    if (a.BenutzerID == BenutzerID) {
                        a.online = true;
                        a.onlineState = onlineState;
                    }
                }
            }
            qq.adapter.notifyDataSetChanged();
        }
    }

    private void showInfo(String string) {
        ShowMessage(this, string, getString(R.string.info));
    }

    void setStatus(String s, boolean nosound) {
        getSupportActionBar().setSubtitle(s);
        if (mService != null) try {
            mService.sendMessageToNotification(s, nosound, false);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
    }

    public void connectService(boolean restart, boolean isalive, boolean quiet) throws Throwable {

        if (mService != null && user != null && (mService.getUserID() != user.getLong(Constants.id) || restart)) {
            //mService.stop();
            //mService.start();
            //mService.stopConnection();
            Map<String, String> p = new HashMap<String, String>();
            String res = null;
            long BenutzerID = user.getLong(Constants.id);
            p.put(BENUTZER_ID, String.valueOf(BenutzerID));
            int Grad = getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);
            mService.Grad = Grad;
            mService.user = user;

            p.put(GRAD, "" + Grad);

            res = mService.initReceiverAndConnect(0, p, null, isalive, quiet);
            mService.setUserID(BenutzerID);
            mService.setGrad(Grad);
            {
                int neueFragen = user.getInt("neuefragen");
                int neueAntworten = user.getInt("neueantworten");
                if (neueAntworten > 0 || neueFragen > 0) {
                    String msg = "";
                    if (neueFragen > 0) {
                        msg = neueFragen + " " + getString(R.string.newQuestions);
                        if (neueAntworten > 0) msg += "\n";
                    }
                    if (neueAntworten > 0) {
                        msg += neueAntworten + " " + getString(R.string.newAnswers);
                    }
                    if (msg.length() > 0)
                        mService.sendMessageToNotification(msg, false, false, true);
                }
                mService.neueAntworten = 0;
                mService.neueFragen = 0;
            }
        }

    }

    public void setblnNotOff(boolean blnNotOff) {
        this.blnNotOff = blnNotOff;
        if (mService != null) mService.blnNotOff = blnNotOff;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (dontStopView) {
            dontStop--;
            dontStopView = false;
        }
        processCacheIfVisible();
        if (GooglePlay && !fragLoginUpdated) {
            fragLoginUpdated = true;
            try {
                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
                if (fragLogin != null)
                    fragLogin.updateUI(account);
                this.account = account;
            } catch (Throwable ex) {
                Log.e(TAG, null, ex);
            }
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        isvisible = false;
        if (mService != null) {
            mService.setActivityStopped(this, true);
        }
        //stopLocationUpdates();
    }
/*
    private void startLocationUpdates() throws Throwable
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (mLocationRequest == null)
        {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(600000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper *///);
    //}

    @Override
    public void onStop() {
        try {
            super.onStop();
            isvisible = false;
            if (mPager != null) this.lastFragment = mPager.getCurrentItem();
            if (dontStop == 0 && mService != null && (!(fragChat != null && fragChat.isVisible() && fragChat.isvisible && !fragChat.isHidden()))) { //set to null by chatfragment.destroy
                //mChatService.stop();
                LimindoService.dontStop = dontStop != 0;
                try {
                    getSupportActionBar().setSubtitle(R.string.disconnectingservice);
                    mService.unBind(true);
                } catch (Throwable throwable) {
                    lib.setStatusAndLog(this, TAG, throwable.getMessage());
                    Log.e(MAIN_ACTIVITY, ON_STOP_CHAT_SERVICE_UNBIND, throwable);
                    throwable.printStackTrace();
                    lib.ShowException(TAG, this, throwable, false);
                }
                //getActionBar().setSubtitle(R.string.ServiceDisconnected);
                mService = null;
                ServiceStarted = 0;
                mServiceInitialized = false;
                mServiceRequested = false;
                saveLastDate();
            } else if (mService != null){
                mService.createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++mService.DebugMessages, "Debug", "Did not unbind Service at " + Calendar.getInstance().getTime().toString(), null, false, false);
            }
        } catch (Throwable ex) {
            Log.e(MAINACTIVITY, ON_STOP, ex);
        }
        if (fragChat != null && fragChat.messageAdapter != null && user != null) {
            try {
                fragChat.saveMessages(false, user.getLong(Constants.id));
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        try {
            saveMessageCacheChat();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        /*
        if (dontStop == 0) {
            try {
                lib.getMessageYesNo(this,getString(R.string.finish), getString(R.string.message), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, null, null);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

        }
        */
        //dontStop = false;
    }

    private void saveMessageCacheChat() throws Throwable {
        String strO = new Gson().toJson(_MessageCacheChat);
        getPreferences(Context.MODE_PRIVATE).edit().putString("MessageCacheChat", strO).apply();
        //Debug
        lib.setStatusAndLog(this, TAG,_MessageCacheChat.size() + " *** Messages saved MessageCacheChat Mainactivity");
    }

    private void saveLastDate() {
        this.getSharedPreferences(SETTINGS, MODE_PRIVATE).edit().putString(DATE_LAST_LOGOFF, fmtDateTime.format(Calendar.getInstance().getTime())).apply();
        getPreferences(MODE_PRIVATE).edit().putString
                ("dtLastSystemMessage", LimindoService.DateTimeParser.format(Calendar.getInstance().getTime())).apply();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (mPager.getCurrentItem() > 0) {
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        } else {
            stopserviceandfinish(false);
        }
    }

    void stopserviceandfinish(boolean dontAsk) {
        /*
        try {
            if (mService!=null) {
                mService.unBind();
                mService = null;
            }
            finish();
        } catch (Throwable ex) {
            lib.ShowException(MainActivity.this, ex);
        }
        */
        try {
            if (dontAsk) {
                stopAll();
            } else {
                AlertDialog.Builder A = new AlertDialog.Builder(this);
                A.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            if (mService != null) {
                                //mChatService.stop();
                                mService.unBind(true);
                                mService = null;

                            }
                            getSharedPreferences("Service", MODE_PRIVATE).edit().putBoolean("MainActivitySaved", false).apply();
                            finish();
                            readMessage = null;
                            MessageCache.clear();
                        } catch (Throwable ex) {
                            lib.ShowException(TAG, MainActivity.this, ex, true);
                        }
                    }
                });
                A.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        stopAll();
                    }
                });
                A.setMessage(getString(R.string.stopbenservice));
                A.setTitle(getString(R.string.stopsben));
                A.show();
            }
        } catch (Throwable ex) {
            lib.ShowException(TAG, this, ex, false);
        }
    }

    private void stopAll() {
        try {
            if (mService != null) {
                //mChatService.stop();
                mService.removeWorkRequestsAndRunnableStop();
                mService.blnKill = true;
                mService.stopService(false, false);
                mService = null;

            }
            readMessage = null;
            MessageCache.clear();
            getSharedPreferences("Service", MODE_PRIVATE).edit().putBoolean("MainActivitySaved", false).apply();
            finish();
            {
                Process.killProcess(Process.myPid());
                System.exit(1);
            }
        } catch (Throwable ex) {
            lib.ShowException(TAG, MainActivity.this, ex, true);
        }
    }

    /*
    private void stopLocationUpdates()
    {
        if (mFusedLocationClient != null && mLocationCallback != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        Menu m = navigationView.getMenu();
        mnuDeaktivieren = m.findItem(R.id.mnuDeactivateAccount);
        mnuTestLoginTalk = m.findItem(R.id.mnuTestLoginTalk);
        mnuErrors = menu.findItem(R.id.mnuErrors);
        mnuUpload = m.findItem(R.id.mnuUpload);
        mnuTaet = m.findItem(R.id.mnuTaetigk);
        mnuOnlineVisible = menu.findItem(R.id.mnuOnlineVisible);
        mnuOnlineInVisible = menu.findItem(R.id.mnuOnlineInVisible);
        mnuOnlineFreeForChat = menu.findItem(R.id.mnuOnlineFreeForChat);
        mnuOnlineBusy = menu.findItem(R.id.mnuOnlinebusy);
        mnuOnlineOffline = menu.findItem(R.id.mnuOnlineOffline);


        /*
        if (!this.blnAccessibility)
        {
            menu.findItem(R.id.mnuChat).setVisible(false);
            menu.findItem(R.id.mnuHome).setVisible(false);
            menu.findItem(R.id.mnuQuestion).setVisible(false);
        }
        else
        {
            menu.findItem(R.id.mnuChat).setVisible(true);
            menu.findItem(R.id.mnuHome).setVisible(true);
            menu.findItem(R.id.mnuQuestion).setVisible(true);
        }
        */
        this.navigationmenu = m;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (navigationmenu != null) {
            MenuItem item = navigationmenu.findItem(R.id.mnuEditAccount);
            if (item != null) {
                if (user != null) item.setVisible(true);
                else item.setVisible(false);
            }
        }
        try {
            if (user != null && user.getBoolean(DEAKTIVIERT)) {
                mnuDeaktivieren.setTitle(getString(R.string.KontoAktivieren));
                this.deaktiviert = true;
            } else {
                this.deaktiviert = false;
            }
            /*
            if (!this.blnAccessibility)
            {
                menu.findItem(R.id.mnuChat).setVisible(false);
                menu.findItem(R.id.mnuHome).setVisible(false);
                menu.findItem(R.id.mnuQuestion).setVisible(false);
            }
            else
            {
                menu.findItem(R.id.mnuChat).setVisible(true);
                menu.findItem(R.id.mnuHome).setVisible(true);
                menu.findItem(R.id.mnuQuestion).setVisible(true);
            }
             */

            if (fPA != null && fPA.PAGE_COUNT == 7) {
                mnuErrors.setVisible(false);
            }

            if (user != null && user.getBoolean(Constants.administrator)) {
                mnuTestLoginTalk.setVisible(true);
                mnuErrors.setVisible(true);
            } else {
                mnuTestLoginTalk.setVisible(false);
                mnuErrors.setVisible(true);
            }
            if (user != null && user.getBoolean("gewerblich")) {
                mnuTaet.setVisible(true);
            } else {
                mnuTaet.setVisible(false);
            }
            if (user == null) mnuUpload.setEnabled(false);
            else mnuUpload.setEnabled(true);
            switch (onlineState) {
                case visible:
                    if (user != null) clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    mnuOnlineVisible.setChecked(true);
                    break;
                case invisible:
                    //mnuOnlineVisible.setChecked(false);
                    mnuOnlineInVisible.setChecked(true);
                    if (user != null) clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    break;
                case busy:
                    mnuOnlineBusy.setChecked(true);
                    if (user != null) clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    break;
                case freeforchat:
                    mnuOnlineFreeForChat.setChecked(true);
                    if (user != null) clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    break;
                case offline:
                    mnuOnlineOffline.setChecked(true);
                    if (user != null) clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    break;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            lib.ShowException(TAG, this, e, true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {
            switch (item.getItemId()) {
                case R.id.mnuChatDrawer:
                    if (hasMessages) {
                        showFragChat(false, null, null, true, false);
                    } else {
                        if (fragChat == null) initFragChat();
                        new dlgContactsChat().show(fm, "dlgContectsChat");
                    }
                    return true;
                case R.id.mnuHome:
                    mPager.setCurrentItem(fragHome.fragID);
                    return true;
                case R.id.mnuQuestionDrawer:
                    mPager.setCurrentItem(fragQuestion.fragID);
                    return true;
                case android.R.id.home:
                    onBackPressed();
                    return true;
                case R.id.mnuCredits:
                    InputStream is = this.getAssets().open(CREDITS);
                    Scanner s = new Scanner(is).useDelimiter(__A);
                    String strCredits = s.hasNext() ? s.next() : "";
                    s.close();
                    is.close();
                    String versionName = this.getPackageManager()
                            .getPackageInfo(this.getPackageName(), 0).versionName;
                    Spannable spn = lib.getSpanableString(strCredits + "\n" + getString(R.string.version_abbr) + versionName);
                    ShowMessage(this, spn, CREDITS1);
                    return true;
                case R.id.mnuPrivacyPolicy:
                    lib.AcceptPrivacyPolicy(this, Locale.getDefault(), true);


                    return true;
                case R.id.mnuContact:
                    Intent intent = new Intent(Intent.ACTION_SEND, Uri.fromParts(MAILTO, CONTACT_EMAIL, null));
                    intent.setType(MESSAGE_RFC822);
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CONTACT_EMAIL});
                    String versionName2 = this.getPackageManager()
                            .getPackageInfo(this.getPackageName(), 0).versionName;
                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " " + versionName2);
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.Contact));
                    dontStop++;
                    this.startActivityForResult(Intent.createChooser(intent, getString(R.string.SendMail)), RGLOBAL);
                    return true;
                case R.id.mnuSendLog:
                    intent = new Intent(Intent.ACTION_SEND, Uri.fromParts(MAILTO, CONTACT_EMAIL, null));
                    Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", getLogFile(this));
                    intent.setType(MESSAGE_RFC822);
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{CONTACT_EMAIL});
                    versionName2 = this.getPackageManager()
                            .getPackageInfo(this.getPackageName(), 0).versionName;
                    intent.putExtra(Intent.EXTRA_STREAM, uri);
                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " " + versionName2);
                    String txt = fragQuestion.getFehlerInfo(this, getBenutzerID());
                    txt += "\n" + ((LogWrapper) Log.getLogNode()).getErrors(10);
                    intent.putExtra(Intent.EXTRA_TEXT, txt);
                    dontStop++;
                    this.startActivityForResult(Intent.createChooser(intent, getString(R.string.SendMail)), RGLOBAL);
                    return true;
                case R.id.mnuDeleteLog:
                    lib.deleteLog(this);
                    return true;
                case R.id.mnuShowLog:
                    final EditText input = new EditText(getContext());
                    AlertDialog d = lib.getInputBox(getContext(), getString(R.string.finderrors), getString(R.string.entersearch), "", false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                showTxtLog(input.getText().toString());
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                showTxtLog("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, input);


                    return true;
                case R.id.mnuHelp:
                    lib.ShowHelp(this, Locale.getDefault());
                    return true;
                case R.id.mnuLogin:
                    //mPager.setCurrentItem(fragQuestions.fragID);
                    fragLogin.mode = (dlgLogin.LoginMode.login);
                    if (fragLogin.isAdded()) fragLogin.dismiss();
                    fragLogin.show(fm, FRAG_LOGIN);
                    fragLogin.updateUI(account);
                    return true;
                case R.id.mnuRegister:
                    fragLogin.mode = (dlgLogin.LoginMode.register);
                    if (fragLogin.isAdded()) fragLogin.dismiss();
                    fragLogin.show(fm, FRAG_LOGIN);
                    fragLogin.updateUI(account);
                    return true;
                case R.id.mnuEditAccount:
                    if (user == null) {
                        ShowMessage(this, getString(R.string.nichtAngemeldet), getString(R.string.message));
                        return true;
                    }
                    fragLogin.mode = (dlgLogin.LoginMode.edtdata);
                    if (fragLogin.isAdded()) fragLogin.dismiss();
                    fragLogin.show(fm, FRAG_LOGIN);
                    fragLogin.updateUI(account);
                    return true;
                case R.id.mnuChangePW:
                    if (user == null) {
                        ShowMessage(this, getString(R.string.nichtAngemeldet), getString(R.string.message));
                        return true;
                    }
                    try {
                        if (user.getBoolean("loginwithgoogle") && mGoogleSignInClient != null) {
                            ShowMessage(this, getString(R.string.CantChangeGooglePW), getString(R.string.message));
                            return true;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    fragChangePW fragChangePW = new fragChangePW();
                    fragChangePW.show(fm, FRAG_CHANGE_PW);
                    return true;
                case R.id.mnuDeactivateAccount:
                    boolean activate;
                    if (user == null) {
                        ShowMessage(this, getString(R.string.nichtAngemeldet), getString(R.string.message));
                        return true;
                    }
                    if (item.getTitle().equals(getString(R.string.KontoDeaktivieren))) {
                        activate = false;
                    } else {
                        activate = true;
                    }
                    if (clsHTTPS != null && user != null) {
                        String res = clsHTTPS.deleteAccount(false, activate, user.getLong(Constants.id), -1l);
                        if (res.startsWith(ERROR_)) {
                            ShowMessage(this, res.replace(ERROR_, getString(R.string.Error_)), getString(R.string.Error));
                        } else {
                            item.setTitle(activate ? getString(R.string.KontoDeaktivieren) : getString(R.string.KontoAktivieren));
                            this.deaktiviert = !activate;
                            user.put(DEAKTIVIERT, deaktiviert);
                        }
                    }

                    return true;
                case R.id.mnuDeleteAccount:
                    if (user == null) {
                        ShowMessage(this, getString(R.string.nichtAngemeldet), getString(R.string.message));
                        return true;
                    }
                    AlertDialog dlg = lib.ShowMessageYesNo(this, getString(R.string.reallyDeleteAccount), getString(R.string.delete), false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (clsHTTPS != null && user != null) {
                                String res = null;
                                try {
                                    res = clsHTTPS.deleteAccount(true, false, user.getLong(Constants.id), -1l);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (res.startsWith(ERROR_)) {
                                    ShowMessage(MainActivity.this, res.replace(ERROR_, getString(R.string.Error_)), getString(R.string.Error));
                                } else if (res != null && res.equalsIgnoreCase(TRUE)) {
                                    try {
                                        if (user.getBoolean("loginwithgoogle") && mGoogleSignInClient != null) {
                                            mGoogleSignInClient.revokeAccess().addOnCompleteListener(MainActivity.this, new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    account = null;
                                                    if (fragLogin != null) {
                                                        fragLogin.account = account;
                                                        if (fragLogin.isAdded() && !fragLogin.isDetached()) {
                                                            fragLogin.updateUI(account);
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    user = null;
                                    lib.getMessageOKCancel(MainActivity.this, getString(R.string.AccountDeletedLoginOrRegister), getString(R.string.AccountDeleted), false, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            fragLogin.mode = (dlgLogin.LoginMode.login);
                                            if (fragLogin.isAdded()) fragLogin.dismiss();
                                            fragLogin.show(fm, FRAG_LOGIN);
                                            fragLogin.updateUI(account);
                                        }
                                    }, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            try {
                                                //MainActivity.this.init1(null);
                                                finish();
                                            } catch (Throwable throwable) {
                                                throwable.printStackTrace();
                                            }
                                        }
                                    });
                                } else {
                                    ShowMessage(MainActivity.this, res, getString(R.string.AccountDeleted));
                                    try {
                                        if (user.getBoolean("loginwithgoogle") && mGoogleSignInClient != null) {
                                            mGoogleSignInClient.revokeAccess().addOnCompleteListener(MainActivity.this, new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    account = null;
                                                    if (fragLogin != null) {
                                                        fragLogin.account = account;
                                                        if (fragLogin.isAdded() && !fragLogin.isDetached()) {
                                                            fragLogin.updateUI(account);
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }
                    }, null, null);
                    dlg.show();
                    return true;
                case R.id.mnuContacts1:
                    mPager.setCurrentItem(fragContacts.fragID);
                    return true;
                case R.id.mnuContacts2:
                    mPager.setCurrentItem(fragContacts2.fragID);
                    return true;
                case R.id.mnuSettings:
                    mPager.setCurrentItem(fragSettings.fragID);
                    return true;
                case R.id.mnuAccessibility:
                    setAccessibility();
                    return true;
                case R.id.mnuErrors:
                    mPager.setCurrentItem(SQLiteDataActivity.fragID);
                    return true;
                case R.id.mnuReconnect:
                    if (mService != null && mService.mHandler == mHandler) {
                        mService.reconnect(true, false);
                    } else {
                        bindService0();
                    }
                    return true;
                case R.id.mnuAbout:
                    showInfo(getString(R.string.version_) + MainActivity.versionName);
                    return true;
                case R.id.mnuFinish:
                    stopserviceandfinish(false);
                    return true;
                case R.id.mnuUpload:
                    pickImage();
                    return true;
                case R.id.mnuTaetigk:
                    setTaetigk();
                    return true;
                case R.id.mnuTestLoginTalk:
                    Map<String, String> p = new HashMap<>();
                    p.put(BENUTZER_ID, "57");
                    p.put(GRAD, "5");
                    p.put(ACCESSKEY, XXXX);
                    mService.initReceiverAndConnect(0, p, null, false, false);
                    return true;
                case R.id.mnuOnlineVisible:
                    onlineState = OnlineState.getOnlineState(1);
                    clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    mnuOnlineVisible.setChecked(true);
                    return true;
                case R.id.mnuOnlineInVisible:
                    //mnuOnlineVisible.setChecked(false);
                    mnuOnlineInVisible.setChecked(true);
                    onlineState = OnlineState.invisible;
                    clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    return true;
                case R.id.mnuOnlinebusy:
                    mnuOnlineBusy.setChecked(true);
                    onlineState = OnlineState.busy;
                    clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    return true;
                case R.id.mnuOnlineFreeForChat:
                    mnuOnlineFreeForChat.setChecked(true);
                    onlineState = OnlineState.freeforchat;
                    clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    return true;
                case R.id.mnuOnlineOffline:
                    mnuOnlineOffline.setChecked(true);
                    onlineState = OnlineState.offline;
                    clsHTTPS.setOnline(getBenutzerID(), onlineState);
                    return true;

            }
        } catch (Throwable ex) {
            Log.e(TAG, OPTIONS_ITEM_SELECTED, ex);
        }
        return super.onOptionsItemSelected(item);
    }

    private void showTxtLog(String searchString) throws Exception {
        String txt = fragQuestion.getFehlerInfo(this, getBenutzerID());
        // txt += "\n" + ((LogWrapper) Log.getLogNode()).getErrors(10);
        dlgLog d = dlgLog.show(fm);
        d.appendlog(txt);
        File log = getLogFile(this);
        txt = lib.getStringFromFile(log, searchString);
        d.appendlog(txt);

    }

    private void setTaetigk() throws JSONException {
        String Taetigk = user.getString("taetigkeitsschwerpunkte");
        final EditText input = new EditText(this);
        AlertDialog dlg = lib.getInputBox(this, getString(R.string.Taetigkeitsschwerpunkte), getString(R.string.Taetigkeitsschwerpunkte), Taetigk, false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    clsHTTPS.setTaetigkeitsschwerpunkt(user.getLong(Constants.id), input.getText().toString());
                    user.put("taetigkeitsschwerpunkte", input.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, null, input);
    }

    private void setPageChangedListener() {
        ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                super.onPageSelected(position);
                if (LastPosition == fragQuestion.fragID) {
                    resetFragQuestion();
                    //mnuUploadToQuizlet.setEnabled(true);
                } else if (LastPosition == fragSettings.fragID) {
                    {
                        processFragSettingsTxtServer();
                    }
                } else if (LastPosition == fragQuestions.fragID) {
                    resetFragQuestions();
                } else if (LastPosition == fragContacts2.fragID) {
                    if (UnconfirmedORInvited) {
                        resetContactsContacts2();
                    }
                }


                if (position == fragQuestions.fragID) {
                    {
                        selectFragQuestions();
                    }
                } else if (position == fragSettings.fragID) {
                    {
                        selectFragSettings();
                    }
                } else if (position == fragQuestion.fragID) {
                    {
                        MainActivity.this.getSupportActionBar().setTitle(getString(R.string.Frage0));
                    }

                } else if (position == fragContacts.fragID) {
                    {
                        MainActivity.this.getSupportActionBar().setTitle(getString(R.string.addInviteContacts));
                    }

                } else if (position == fragHome.fragID) {
                    {
                        MainActivity.this.getSupportActionBar().setTitle(getString(R.string.mnuHome));
                    }

                } else if (position == fragContacts2.fragID) {
                    {
                        selectFragContacts2();
                    }

                } else if (position == fragAnswer.fragID) {
                    {
                        selectFragAnswer();
                    }

                } else if (position == SQLiteDataActivity.fragID) {
                    {
                        selectSQLiteDataActivity();
                    }

                } else {
                    MainActivity.this.getSupportActionBar().setTitle(getString(R.string.app_name));
                }

                LastPosition = position;
                if (LastPosition > -1 && LastPosition != fragAnswer.fragID)
                    LastLastPosition = position;

            }

            private void selectSQLiteDataActivity() {
                try {
                    if (false && MainActivity.this.user != null && !MainActivity.this.user.getBoolean(Constants.administrator)) {
                        if (LastPosition > -1)
                            mPager.setCurrentItem(LastPosition);
                        else
                            mPager.setCurrentItem(fragQuestions.fragID);
                        fPA.PAGE_COUNT = 6;
                        fPA.notifyDataSetChanged();
                    } else {
                        MainActivity.this.getSupportActionBar().setTitle(getString(R.string.Fehlerdatenbank));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            private void selectFragAnswer() {
                MainActivity.this.getSupportActionBar().setTitle(getString(R.string.Antwort));
                fragAnswer frag = (fragAnswer) fPA.findFragment((fragAnswer.fragID));
                if (currentAnswerAnswer != null && currentAnswerAnswer.level > 1) {
                    if (frag != null) {
                        frag.txtAnswer.setError(null); //getString(R.string.AntwortNichtMöglich));
                        ShowMessage(MainActivity.this, getString(R.string.AntwortNichtMoeglich), getString(R.string.message));
                        frag.txtAnswer.setEnabled(false);
                        if (LastLastPosition == fragQuestions.fragID)
                            mPager.setCurrentItem(LastLastPosition);
                    }

                } else {
                    if (frag != null) {
                        frag.txtAnswer.setError(null);
                        frag.txtAnswer.setEnabled(true);
                    }
                }
            }

            private void selectFragContacts2() {
                if (!getUnconfirmed) {
                    MainActivity.this.getSupportActionBar().setTitle(getString(R.string.KontakteLimindo));
                } else if (getUnconfirmed) {
                    MainActivity.this.getSupportActionBar().setTitle(R.string.unconfirmed_contacts);
                } else if (getInvited) {
                    MainActivity.this.getSupportActionBar().setTitle(R.string.invited_contacts);
                }
            }

            private void selectFragSettings() {
                try {
                    MainActivity.this.getSupportActionBar().setTitle(getString(R.string.Einstellungen));
                            /*
                            int Language = fPA.fragSettings.getIntent().getIntExtra(
                                    "Language", org.de.jmg.learn.vok.Vokabel.EnumSprachen.undefiniert.ordinal());
                            fPA.fragSettings.spnLanguages.setSelection(Language);
                            fPA.fragSettings.setSpnMeaningPosition();
                            fPA.fragSettings.setSpnWordPosition();
                            fPA.fragSettings.setChkTSS();
                            */
                } catch (Throwable ex) {
                    Log.e(SAVE_RESULTS_AND_FINISH, ex.getMessage(), ex);
                }
            }

            private void selectFragQuestions() {
                try {
                    fragQuestions frag = (fragQuestions) fPA.findFragment((fragQuestions.fragID));
                    //
                    MainActivity.this.getSupportActionBar().setTitle(getString(R.string.Fragen));
                    //fPA.fragQuestions._main = MainActivity.this;
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }

            private void resetContactsContacts2() {
                UnconfirmedORInvited = false;
                fragContacts.listLoaded = false;
                fragContacts.contactVOListAndroid = null;
                fragContacts2.listLoaded = false;
                fragContacts2.contactVOListLimindo = null;
                fragContacts2 frag = (fragContacts2) fPA.findFragment((fragContacts2.fragID));
                if (frag != null) frag.refresh();
                fragContacts frag2 = (fragContacts) fPA.findFragment((fragContacts.fragID));
                if (frag2 != null) frag2.reLoad();
            }

            private void resetFragQuestions() {
                fragQuestions frag = (fragQuestions) fPA.findFragment((fragQuestions.fragID));
                if (frag != null) frag.initialized = false;
            }

            private void processFragSettingsTxtServer() {
                fragSettings fragSettings = (de.com.limto.limto1.fragSettings) fPA.findFragment(de.com.limto.limto1.fragSettings.fragID);
                if (fragSettings != null && fragSettings.txtServer != null) {
                    fragSettings.txtServer.toString();
                    if (!de.com.limto.limto1.clsHTTPS.MYURL.equalsIgnoreCase(fragSettings.txtServer.toString())) {
                        de.com.limto.limto1.clsHTTPS.MYURLROOT = fragSettings.txtServer.getText().toString();
                        clsHTTPS.reset();
                    }
                }
            }

            private void resetFragQuestion() {
                try {
                    if (fPA != null) {
                        try {
                            fragQuestion fragQuestion = (de.com.limto.limto1.fragQuestion) fPA.findFragment(de.com.limto.limto1.fragQuestion.fragID);
                            if (fragQuestion != null) {
                                fragQuestion.FrageID = -1l;
                                fragQuestion.ID = -1l;
                                fragQuestion.chkEmergency.setChecked(false);
                                fragQuestion.chkEmergency.setChecked(false);
                            }
                        } catch (Throwable ex) {
                            Log.e(SAVE_RESULTS_AND_FINISH, ex.getMessage(), ex);
                        }
                                        /*
                                        if (lib.NookSimpleTouch())
                    					{
                    						RemoveFragSettings();
                    					}
                    					*/
                    }

                } catch (Throwable e) {

                    lib.ShowException(TAG, MainActivity.this, e, false);
                }

            }

        };

        mPager.addOnPageChangeListener(pageChangeListener);

    }

    public boolean _getUnconfirmed() throws Throwable {
        if (isStartedFromNotification) return false;
        if (this.clsHTTPS != null && this.user != null) {
            String res = null;
            try {
                res = this.clsHTTPS.getUnconfirmed(this.user.getLong(Constants.id));
                JSONArray rres = null;
                if (res != null && res.length() > 0) rres = new JSONArray(res);
                if (rres != null && rres.length() > 0) {
                    this.getUnconfirmed = true;
                    Fragment frag = this.fPA.findFragment(fragContacts2.fragID);
                    if (frag != null) {
                        if (this.mPager.getCurrentItem() == fragContacts2.fragID
                                || this.mPager.getCurrentItem() == fragContacts.fragID
                                || this.mPager.getCurrentItem() == SQLiteDataActivity.fragID) {
                            fragContacts2 f = (fragContacts2) frag;
                            f.refresh();
                        }
                    }
                    this.mPager.setCurrentItem(fragContacts2.fragID);
                    return true;
                } else {
                    //getInvited();
                    return false;
                }
            } catch (Throwable ex) {
                Log.e(TAG, res);
            }

        }

        return false;
    }

    public boolean getInvited() throws Throwable {
        if (user == null) return false;
        String res = this.clsHTTPS.getInvited(this.user.getLong(Constants.id), null, null);
        JSONArray rres = null;

        rres = getJSONArrayFromRes(res);

        if (rres != null && rres.length() > 0) {
            this.getInvited = true;
            this.ArrInvited = rres;
            Fragment frag = this.fPA.findFragment(fragContacts2.fragID);
            if (frag != null) {
                if (this.mPager.getCurrentItem() == fragContacts2.fragID
                        || this.mPager.getCurrentItem() == fragContacts.fragID
                        || this.mPager.getCurrentItem() == SQLiteDataActivity.fragID) {
                    fragContacts2 f = (fragContacts2) frag;
                    f.refresh();
                }
            }
            this.mPager.setCurrentItem(fragContacts2.fragID);
            return true;
        } else {
            res = this.clsHTTPS.getInvited(this.user.getLong(Constants.id), user.getString(EMAIL), user.getString(RUFNUMMER));
            rres = null;
            rres = getJSONArrayFromRes(res);
            if (rres != null && rres.length() > 0) {
                this.getInvited = true;
                this.ArrInvited = rres;
                Fragment frag = this.fPA.findFragment(fragContacts2.fragID);
                if (frag != null) {
                    if (this.mPager.getCurrentItem() == fragContacts2.fragID
                            || this.mPager.getCurrentItem() == fragContacts.fragID
                            || this.mPager.getCurrentItem() == SQLiteDataActivity.fragID) {
                        fragContacts2 f = (fragContacts2) frag;
                        f.refresh();
                    }
                }
                this.mPager.setCurrentItem(fragContacts2.fragID);
                return true;
            }
        }
        return false;
    }

    private JSONArray getJSONArrayFromRes(String res) {
        JSONArray rres = null;
        try
        {
            if (res != null && res.length() > 0) rres = new JSONArray(res);
        }
        catch (Throwable ex)
        {
            lib.ShowMessageDebug(this,"getJSON From res " + res + " Error " + ex.getMessage(), "getJSONArrayFromRes");
        }
        return rres;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void signingoogle() {
        if (mGoogleSignInClient == null) return;
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        Log.e("signingoogle", "start intent");
        dontStop++;
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signoutgoogle() {
        if (mGoogleSignInClient == null) return;
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                account = null;
                if (fragLogin != null && !fragLogin.isDetached() && fragLogin.isAdded()) {
                    fragLogin.updateUI(account);
                    fragLogin.mBenutzername.setText("");
                    fragLogin.mPasswordView.setText("");
                    fragLogin.mPasswordView2.setText("");
                }
            }
        });

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (fragChat != null && fragChat.messageAdapter != null && user != null) {
            try {
                fragChat.saveMessages(false, this.user.getLong(Constants.id));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                lib.ShowException(TAG, this, throwable, false);
            }
        }
    }

    public dlgChat showFragChat(boolean typing, Long EmpfaengerBenutzerID, String Empfaengername, boolean show, boolean direct) {
        try {
            fm.executePendingTransactions();

            fragChat = initFragChat();

            showFragChatOrSetEmpfaenger(fragChat, typing, EmpfaengerBenutzerID, Empfaengername, show);

        } catch (Throwable ex) {
            ex.printStackTrace();
            lib.ShowException(TAG, this, ex, false);
        }
        return fragChat;
    }

    private void showFragChatOrSetEmpfaenger(dlgChat fragChat, boolean typing, Long EmpfaengerBenutzerID, String Empfaengername, boolean show) {

        boolean isVisible = fragChat.isAdded() && fragChat.isvisible && fragChat.isVisible() && !fragChat.isHidden();
        boolean gleicherEmpfaenger = EmpfaengerBenutzerID != null && EmpfaengerBenutzerID.equals(fragChat.getBenutzerIDEmpfaenger());
        if (!show) {
            if (EmpfaengerBenutzerID != null && !gleicherEmpfaenger)
            {
                fragChat.setEmpfaenger(EmpfaengerBenutzerID, Empfaengername);
            }
            if (!typing) hasnewMessages(isVisible);

        } else {
            if (fragChat.isVisible() || fragChat.isAdded()) fragChat.dismiss();
            fragChat.isvisible = true;
            fragChat.show(fm, FRAG_CHAT);
            if (EmpfaengerBenutzerID != null)
                fragChat.setEmpfaenger(EmpfaengerBenutzerID, Empfaengername);

        }
    }

    public dlgChat initFragChat() throws Exception {
        boolean isnew = false;
        if (fragChat == null) {
            fragChat = new dlgChat();
            isnew = true;
        }
        fragChat._main = this;
        fragChat.context = this;
        fragChat.messageAdapter._main = this;
        fragChat.loadMessages(false, getBenutzerID());
        if (isnew) fragChat.isvisible = false;
        return fragChat;
    }

    public void messagesShown() {
        if (bottomNavigationView != null) {
            mnuChat = bottomNavigationView.getMenu().findItem(R.id.mnuChat);
            bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.chat_n);
            mnuChat.setChecked(false);
            hasMessages = false;
        }
        if (navigationmenu != null) {
            mnuChat = navigationmenu.findItem(R.id.mnuChatDrawer);
            mnuChat.setChecked(false);
            mnuChat.setIcon(ContextCompat.getDrawable(this, R.drawable.chat_a));
            toolbar.setNavigationIcon(mToggle.getDrawerArrowDrawable());
            // if (iconTintlistMenuBak != null) navigationView.setItemIconTintList(iconTintlistMenuBak);
            hasMessages = false;
        }
        if (dlgChat.pendingRead.size() > 0 && fragChat != null) {
            lib.setStatusAndLog(this, TAG, "*** processing pendingread size " + dlgChat.pendingRead.size());
            for (JSONObject o : dlgChat.pendingRead.toArray(new JSONObject[1])) {
                try {
                    de.com.limto.limto1.Chat.Message mm = fragChat.messageAdapter.getMessage(o);
                    boolean res = fragChat.messageAdapter.trysendMessageRead(o, 0, true, mm);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    lib.ShowException(TAG, this, throwable, true);
                }
            }
            try {
                fragChat.savePendingRead(getBenutzerID());
            } catch (Throwable e) {
                lib.ShowException(TAG, getContext(), e, true);
            }
        }
    }

    private void hasnewMessages(boolean isVisible) {

        if (bottomNavigationView != null && !isVisible) {
            mnuChat = bottomNavigationView.getMenu().findItem(R.id.mnuChat);
            bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.message_available);
            mnuChat.setChecked(true);
            hasMessages = true;
        }
        if (navigationmenu != null && !isVisible) {
            iconTintlistMenuBak = navigationView.getItemIconTintList();
            // navigationView.setItemIconTintList(null);
            mnuChat = navigationmenu.findItem(R.id.mnuChatDrawer);
            mnuChat.setChecked(true);
            mnuChat.setIcon(R.drawable.green_chat);
            toolbar.setNavigationIcon(R.drawable.message_available);
            hasMessages = true;
        }
        if (!isVisible) lib.ShowToast(this, getString(R.string.newMessage));
    }

    public enum OnlineState {
        visible(1), freeforchat(2), invisible(0), busy(-1), offline(-2);

        public static HashMap<OnlineState, String> stateStrings = new HashMap<OnlineState, String>();

        public final int state;

        OnlineState(int i) {
            this.state = i;
        }


        public static OnlineState getOnlineState(int anInt) {
            for (OnlineState o : OnlineState.values()) {
                if (o.state == anInt) {
                    return o;
                }
            }
            ;
            return null;
        }

        public String getStateString() {
            return stateStrings.get(this);
        }
    }

    private static class FinishLoopRunTimeException extends RuntimeException {
    }

    public static class HandlerBound extends Handler {
        public static final String BENUTZER_NICHT_ANGEMELDET = "Benutzer nicht angemeldet!";
        public static final String DIE_IDENTITÄT_KONNTE_NICHT_ÜBERPRÜFT_WERDEN = "Die Identität konnte nicht überprüft werden!";
        public static final String FRAGE = MainActivity.FRAGE;
        public static final String FAILED_TO_CONNECT = "failed to connect";
        public static final String ERROR = ERROR_;
        public static final String READMESSAGE = "readmessage: ";
        public static final String FRAG_LOGIN_LOGON = "fragLogin:logon";
        public static final String FRAG_LOGIN_NULL = "fragLogin null";
        private final MainActivity _MainActivity;
        private long lastBenutzerID;
        private int lastCountMessages;
        private CharSequence lastSS;

        HandlerBound(MainActivity a) {
            this._MainActivity = a;
        }

        @Override
        public void handleMessage(Message msg) {
            AppCompatActivity activity = _MainActivity;
            try {
                String readMessage = null;
                boolean isalive = false;
                boolean quiet = false;
                Bundle b = msg.peekData();
                if (b != null && b.containsKey(Constants.ISALIVE)) {
                    isalive = b.getBoolean(Constants.ISALIVE);
                }
                if (b != null && b.containsKey(Constants.QUIET)) {
                    quiet = b.getBoolean(Constants.QUIET);
                }
                boolean networkError = false;
                boolean showUserInterface = false;
                boolean isChatMessage = false;

                if (msg.what == Constants.MESSAGE_READ) {
                    readMessage = getReadMessage(msg);
                    if (readMessage != null && readMessage.startsWith("54321")) {
                        isChatMessage = true;
                    }
                }


                if (!isChatMessage && (!_MainActivity.isvisible && _MainActivity.mService != null)) {
                    _MainActivity.mService.mHandlerUnbound.handleMessage(msg);
                    return;
                }
                if (b != null && b.containsKey(Constants.SHOWUSERINTERFACE)) {
                    showUserInterface = b.getBoolean(Constants.SHOWUSERINTERFACE);
                }
                if (b != null && b.containsKey(Constants.ISNETWORKERROR)) {
                    networkError = b.getBoolean(Constants.ISNETWORKERROR);
                }
                switch (msg.what) {
                    case MESSAGE_EXCEPTION:
                        showExceptionOnDebug(msg);
                        break;
                    case Constants.MESSAGE_STATE_CHANGE:
                        showStatusMessages(msg, isalive, activity, b);
                        break;
                    case Constants.MESSAGE_WRITE:
                        String writeMessage = getWriteMessage(msg);
                        break;
                    case Constants.MESSAGE_READ:
                        try {

                            if (readMessage != null) {
                                processReadMessageHandler(isChatMessage, readMessage, activity);
                            }

                        } catch (Throwable e) {
                            e.printStackTrace();
                            Log.e(TAG, null, e);
                            lib.ShowException(TAG, _MainActivity, e, false);
                        }
                        //line += readMessage;
                        //if (line.endsWith("\n"))

                        break;
                    case Constants.MESSAGE_DEVICE_NAME:
                        // save the connected device's name
                        _MainActivity.mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                        //Toast.makeText(activity, getString(R.string.connectedto)
                        //        + " " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                        break;
                    case Constants.MESSAGE_DEVICE_ADDRESS:
                        // save the connected device's name
                        _MainActivity.mConnectedDeviceAddress = msg.getData().getString(Constants.DEVICE_ADDRESS);
                        //Toast.makeText(activity, getString(R.string.connectedto)
                        //        + " " + mConnectedDeviceAddress, Toast.LENGTH_SHORT).show();
                        break;
                    case Constants.MESSAGE_TOAST:
                        if (_MainActivity.mService != null)
                            try {
                            //mService.sendMessageToNotification(msg.getData().getString(Constants.TOAST), false, false);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            Log.e(TAG, null, throwable);
                        }
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_LONG).show();
                        break;
                    case Constants.MESSAGE_INFO:
                        if (null != activity) {
                            _MainActivity.showInfo(msg.getData().getString(Constants.INFO));
                        }
                        break;
                    case Constants.MESSAGE_RECONNECT:
                        reconnect(showUserInterface, isalive, quiet, networkError);
                        break;
                    case Constants.MESSAGE_RELOGIN:
                        relogin();
                        break;
                    case Constants.MESSAGE_UPLOADCOMPLETE:
                        Fragment f;
                        f = _MainActivity.fPA.findFragment(fragQuestions.fragID);
                        fragQuestions ff = (fragQuestions) f;
                        if (ff != null && ff.adapter != null) {
                            ff.adapter.notifyDataSetChanged();
                        }
                    default:
                        break;


                }
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                lib.ShowException(TAG, _MainActivity, e, false);
            }
        }

        private void processReadMessageHandler(boolean isChatMessage, String readMessage, AppCompatActivity activity) throws Throwable {
            if (readMessage.length() > 100000) {
                Exception ex = new Exception("Long Message size: " + readMessage.length());
                Log.e(TAG, null, ex);
                //lib.ShowException(TAG,context,ex,false);
            }

            if (readMessage.startsWith("54321")) {
                showChatMessage(readMessage);
            } else if (readMessage.startsWith("123456")) {
                _MainActivity.processGroupMessage(readMessage);
            } else if (!readMessage.startsWith(ERROR)) {
                if ((debugMode()) && !_MainActivity.blnNotOff) {
                    showReadMessageInNotification(readMessage, activity);
                }
                if (_MainActivity.fPA == null) {
                    MainActivity.MessageCache.add(readMessage);
                } else {
                    _MainActivity.processReadMessage(readMessage, false, false, false);
                }
            } else {
                processError(readMessage);
            }
        }

        private void relogin() throws Throwable {
            if (_MainActivity.fragLogin.mAuthTask != null || (_MainActivity.fragLogin.isVisible() || _MainActivity.fragLogin.isvisible()))
                return;
            _MainActivity.user = null;
            _MainActivity.fragLogin.mode = dlgLogin.LoginMode.none;
            if (_MainActivity.mService != null) {
                _MainActivity.mService.user = null;
                _MainActivity.mService.stop(false);
            }
            _MainActivity.user = null;
            _MainActivity.fragLogin.logon(_MainActivity.fm, false, _MainActivity.savedInstanceState);
            //fragLogin.mode = de.org.Limto.limindo2.fragLogin.LoginMode.login;
            //MainActivity.this.fragLogin.show(fm,"fragLogin");
        }

        private void reconnect(boolean showUserInterface, boolean isalive, boolean quiet, boolean networkError) throws Throwable {
            Context context = _MainActivity;
            _MainActivity.mService.updateUserInterfaceTitle(isalive, quiet);
            if (showUserInterface) {
                AlertDialog dlg = lib.ShowMessageYesNo(context,
                        context.getString(R.string.connectionlostreconnect),
                        context.getString(R.string.connectionlost),
                        false,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //mService.start();
                                try {
                                    _MainActivity.mService.lastWasNetworkError = false;
                                    _MainActivity.mService.connectionretries = 0;
                                    _MainActivity.mService.reconnectMainThread(false, false, true);
                                } catch (Throwable e) {
                                    e.printStackTrace();
                                    Log.e(TAG, null, e);
                                }
                            }
                        },
                        null, null);
                //dlg.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

            } else {
                _MainActivity.mService.reconnectMainThread(networkError, isalive, quiet);
                final boolean finalNetworkError = networkError;
                final boolean finalIsalive = isalive;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (_MainActivity.mService.getState() != STATE_CONNECTED) {
                            try {
                                _MainActivity.mService.reconnectwhenonline(null, true, finalNetworkError, finalIsalive);
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                                lib.ShowException(TAG, _MainActivity, throwable, false);
                            }
                        }
                    }
                }, 5000);
            }
        }

        private void processError(String readMessage) throws Throwable {
            Log.e(TAG, READMESSAGE + readMessage);
            if (readMessage.contains(BENUTZER_NICHT_ANGEMELDET) || readMessage.contains(DIE_IDENTITÄT_KONNTE_NICHT_ÜBERPRÜFT_WERDEN)) {
                _MainActivity.user = null;
                if (_MainActivity.mService != null) {
                    _MainActivity.mService.user = null;
                    _MainActivity.mService.setUserID(-1);
                }
                dlgLogin f = _MainActivity.fragLogin;
                if (f != null) {
                    if (f.isAdded()) f.dismiss();
                    Log.e(TAG, FRAG_LOGIN_LOGON);
                    f.mode = dlgLogin.LoginMode.none;
                    f.logon(_MainActivity.fm, false, _MainActivity.savedInstanceState);
                } else {
                    Log.e(TAG, FRAG_LOGIN_NULL);
                }
                                    /*
                                    if (fragLogin!=null)
                                    {
                                        fragLogin.mode = (de.org.Limto.limindo2.fragLogin.LoginMode.login);
                                        if (fragLogin.isAdded()) fragLogin.dismiss();
                                        fragLogin.show(fm, "fragLogin");
                                    }
                                    else
                                    {

                                    }
                                    */
            } else {
                lib.ShowToast(_MainActivity, _MainActivity.getString(R.string.Fehler) + " " + readMessage.replace(ERROR, ""));
            }
        }

        private void showReadMessageInNotification(String readMessage, AppCompatActivity activity) {
            JSONObject o;
            try {
                JSONArray oo = new JSONArray();
                try {
                    o = new JSONObject(readMessage);
                    oo.put(o);
                } catch (Throwable ex) {
                    oo = new JSONArray(readMessage);
                }
                for (int i = 0; i < oo.length(); i++) {
                    o = oo.getJSONObject(i);
                    LimindoService.SendType t = LimindoService.SendType.valueOf(o.getString(SENDTYPE));

                    if (t == LimindoService.SendType.Answer) {
                        _MainActivity.mService.createNewNotification(ONGOING_NOTIFICATION_ID_ANSWER, activity.getString(R.string.answer) + " " + o.getString(BENUTZERNAME), o.getString(ANTWORT), readMessage, false, false);
                    } else if (t == LimindoService.SendType.Question) {
                        _MainActivity.mService.createNewNotification(ONGOING_NOTIFICATION_ID_QUESTION, activity.getString(R.string.question) + " " + o.getString(BENUTZERNAME), o.getString(FRAGE), readMessage, !o.isNull(EMERGENCY) && o.getInt(EMERGENCY) != 0, false);
                    } else {
                        //mService.createNewNotification("mHandler1 Event", t.name(), readMessage);
                    }
                }
            } catch (Throwable ex) {
                Log.e(TAG, readMessage, ex);
            }

        }

        private String getWriteMessage(Message msg) {
            byte[] writeBuf = (byte[]) msg.obj;
            // construct a string from the buffer
            String writeMessage = new String(writeBuf);
            //mConversationArrayAdapter.add("Me:  " + writeMessage);
            return  writeMessage;
        }

        private void showChatMessage(String readMessage) throws Throwable {
            JSONObject o = new JSONObject(readMessage.substring(5));
            long BenutzerIDEmpfaenger = o.getLong(BENUTZERIDEMPFAENGER);
            long BenutzerID = o.getLong(BENUTZER_ID);
            if (!o.has(SENDTYPE))
                o.put(SENDTYPE, LimindoService.SendType.Message);
            //a.mService.createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES, context.getString(R.string.message), o.getString("Message"), o.toString(), false, false);
            if (_MainActivity.isvisible || (_MainActivity.fragChat != null && _MainActivity.fragChat.isAdded() && !_MainActivity.fragChat.isHidden() && _MainActivity.fragChat.isvisible && _MainActivity.fragChat.isVisible())) {
                sendChatMessageToDlgChat(o);
            } else {
                if (!isStatusMessage(o)) {
                    sendChatMessageToNotification(o, BenutzerID);
                }
                // Achtung hier wird eventuell doppelt eingefügt
                InsertChatMessageIntoMessageAdaptorAndAddToMessageCacheChat(o);
            }

        }

        private void InsertChatMessageIntoMessageAdaptorAndAddToMessageCacheChat(JSONObject o) {
            if (_MainActivity.fragChat != null && _MainActivity.fragChat.messageAdapter != null) {
                try {
                    _MainActivity.fragChat.messageAdapter.insertObject(o);
                } catch (Throwable ex) {
                    lib.ShowException(TAG, _MainActivity, ex, false);
                }
            } else {
                //a._MessageCacheChat.add(o.toString());
            }
            _MainActivity._MessageCacheChat.add(o.toString());
            lib.ShowException(TAG, _MainActivity, new Exception("InsertChatMessageIntoMessageAdaptor failed!"), false);

        }

        private boolean isStatusMessage(JSONObject o) throws JSONException {
            return (o.optString("Message").equalsIgnoreCase("*received*")) || (!o.has("sent") || !o.getBoolean("sent")) && (!o.has("read") || !o.getBoolean("read"));
        }

        private void sendChatMessageToNotification(JSONObject o, long BenutzerID) throws Throwable {
            _MainActivity.mService.countMessages++;
            Long t;
            t = (o.optLong(TIME, 0));
            if (t == 0) {
                t = Calendar.getInstance().getTimeInMillis();
            }
            Date time = new Date(t);
            Long id = _MainActivity.user.getLong(ID);
            JSONObject user = _MainActivity.clsHTTPS.getUser(id, BenutzerID);
            CharSequence ss = null;
            if (o.optString("Message").startsWith("(JSONObject)")) {
                showClickableSpan(o, time, user);
            } else {
                int countMessages = _MainActivity.mService.countMessages;
                ss = o.getString("Message");
                if (ss.toString().equalsIgnoreCase("⸙")) {
                    ss = showIsWriting(BenutzerID, countMessages);
                } else {
                    lib.Ref<Integer> refCount = new lib.Ref<Integer>(countMessages);
                    resetStateIsWriting(BenutzerID, refCount);
                    countMessages = refCount.get();
                }
                boolean noSound = lastBenutzerID == BenutzerID;
                _MainActivity.mService.createNewNotification(countMessages + ONGOING_NOTIFICATION_ID_MESSAGES, _MainActivity.getString(R.string.message), sdftime.format(time) + " " + user.getString(BENUTZERNAME) + ": " + ss.toString(), o.toString(), false, noSound);
            }
        }

        private void resetStateIsWriting(long BenutzerID, lib.Ref<Integer> countMessages) {
            if (BenutzerID == lastBenutzerID) {
                countMessages.set(lastCountMessages);
            }
            lastBenutzerID = -1;
            lastCountMessages = countMessages.get();
            lastSS = null;

        }

        private CharSequence showIsWriting(long BenutzerID, int countMessages) {
            CharSequence ss = ".";
            if (BenutzerID == lastBenutzerID) {
                countMessages = lastCountMessages;
                ss = TextUtils.concat(lastSS, ss);
            }
            lastBenutzerID = BenutzerID;
            lastCountMessages = countMessages;
            lastSS = ss;
            return ss;
        }

        private void showClickableSpan(JSONObject o, Date time, JSONObject user) throws Throwable {
            SpannableString m = new SpannableString(sdftime.format(time) + " " + user.getString(BENUTZERNAME) + ": ");
            CharSequence ss = lib.getClickableSpanFromMessage(_MainActivity.clsHTTPS, _MainActivity.getBenutzerID(), _MainActivity, o.optString("Message"), true);

            ss = TextUtils.concat(m, ss);
            _MainActivity.mService.createNewNotification(_MainActivity.mService.countMessages + ONGOING_NOTIFICATION_ID_MESSAGES, _MainActivity.getString(R.string.message), ss, o.toString(), false, false);

        }

        private void sendChatMessageToDlgChat(JSONObject o) {
            boolean typing = o.optString("Message").contains("⸙");
            dlgChat res = _MainActivity.showFragChat(typing, null, null, false, false);
            try {
                de.com.limto.limto1.Chat.Message m = res.insertObject(o);
            } catch (Throwable ex) {
                lib.ShowException(TAG, _MainActivity, ex, res != null ? res.toString() : o.toString(), false);
            }
        }

        private void showStatusMessages(Message msg, boolean isalive, AppCompatActivity activity, Bundle b) {
            switch (msg.arg1) {
                case STATE_CONNECTED:
                    showConnected(isalive, activity);
                    break;
                case STATE_CONNECTING:
                    _MainActivity.setStatus(_MainActivity.getString(R.string.title_connecting), true);
                    break;
                case LimindoService.STATE_LISTEN:
                case LimindoService.STATE_NONE:
                    setStatusNone(isalive);
                    break;
                case LimindoService.STATE_ERROR:
                    showErrorMessage(b);
                    break;
            }
        }

        private void showErrorMessage(Bundle b) {
            String mErrMsg = (b != null ? b.getString(Constants.ERR_MSG) : "");
            if (mErrMsg != null && mErrMsg.startsWith(FAILED_TO_CONNECT)) {
                mErrMsg = _MainActivity.getString(R.string.failedtoconnect);
            }
            if (_MainActivity.blnServicWasDestroyed) {
                _MainActivity.setStatus(_MainActivity.getString(R.string.ServiceDestroyed), false);
            } else {
                _MainActivity.setStatus(_MainActivity.getString(R.string.error) + " " + mErrMsg, true);
            }
        }

        private void setStatusNone(boolean isalive) {
            if (_MainActivity.blnServicWasDestroyed) {
                _MainActivity.setStatus(_MainActivity.getString(R.string.ServiceDestroyed), false);
            } else if (isalive) {
                _MainActivity.setStatus(_MainActivity.getString(R.string.title_connection_renewing), true);
            } else {
                _MainActivity.setStatus(_MainActivity.getString(R.string.title_not_connected), true);
            }

        }

        private void showConnected(boolean isalive, AppCompatActivity activity) {
            if (isalive) {
                _MainActivity.setStatus(_MainActivity.getString(R.string.title_connected_to) + " " + activity.getString(R.string.LimindoServiceRenewed) + LimindoService.alivecount, true); // + MYURL);
            } else {
                _MainActivity.setStatus(_MainActivity.getString(R.string.title_connected_to) + " " + _MainActivity.getString(R.string.LimindoService), true); // + MYURL);
            }
        }

        private void showExceptionOnDebug(Message msg) {
            Exception eex = (Exception) msg.obj;
            String message = msg.getData().getString(Constants.MESSAGE);
            lib.ShowException(TAG, _MainActivity, eex, message, false);
        }

        private String getReadMessage(Message msg) {
            byte[] readBuf = (byte[]) msg.obj;
            // construct a string from the valid bytes in the buffer
            try {
                String readMessage = new String(readBuf, 0, msg.arg1);
                return readMessage;
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
            return  null;
        }
    }

    private class ActivityServiceConnection implements ServiceConnection {
        public static final String SERVICE_DISCONNECTED = "Service disconnected";
        int requestCode;
        int resultCode;
        Intent data;

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            try {
                LimindoService.LocalBinder binder = (LimindoService.LocalBinder) service;
                mService = binder.getService();
                mService.runnableStopServiceEnabled = true;
                mService.removeRunnableStop();
                //((HandlerBound) MainActivity.this.mHandler).service = mService;
                //if (MainActivity.this.db != null) mService.db = MainActivity.this.db; else MainActivity.this.db = mService.db;
                broadcastReceiver.service = mService;
                initialized = true;
                mService.setRestartCount(1);
                lib.ShowMessageDebug(MainActivity.this, "Service bound","Debug");
                //doLogin();
                ServiceBound();
            } catch (Throwable throwable) {
                Log.e(TAG, null, throwable);
                lib.ShowException(TAG, MainActivity.this, throwable, false);
            } finally {
                mServiceRequested = false;
                mServiceInitialized = true;
            }

        }


        private void ServiceBound() throws Throwable {

            mService.init(MainActivity.this, MainActivity.this, mHandler, fragLogin, db, true);
            if (mService.user != null) MainActivity.this.user = mService.user;
            mService.serviceConnection = this;
            mBound = true;
            mService.isBound = true;
            if (data != null) onActivityResult(requestCode, resultCode, data);

            mService.setActivityStopped(MainActivity.this, false);
            lib.ShowMessageDebug(MainActivity.this, "2. Service initialized","Debug");
            requestPermission(savedInstanceState);
            lib.setStatusAndLog(MainActivity.this, TAG, "*** Service bound + Messages: "+ mService.MessageCacheChatService.size());
            if (readMessage != null) {
                mService.MessageCacheChatService.remove(readMessage);
            }
            processMessageCacheChat(mService.MessageCacheChatService);



        }


        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mServiceRequested = false;
            mBound = false;
            mService = null;
            Log.e(TAG, SERVICE_DISCONNECTED);

        }
    }
}
