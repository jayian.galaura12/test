package de.com.limto.limto1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.TooltipCompat;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.com.limto.limto1.Controls.Answer;
import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.fragQuestion.VOTE;
import static de.com.limto.limto1.lib.lib.debugMode;
import static de.com.limto.limto1.lib.lib.fromHtml;
import static de.com.limto.limto1.lib.lib.gStatus;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fragAnswer.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fragAnswer#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragAnswer extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "fragAnswer";
    public static final String ANSWER = "Answer";
    public static final String DEAKTIVIERT = "deaktiviert";
    public static final String BENUTZERNAME = "benutzername";
    public static final String EMAIL = "email";
    public static final String FRAGEANTWORTENID = "frageantwortenid";
    public static final String ANTWORTID = "antwortid";
    public static final String BEWERTUNGEN = "bewertungen";
    public static final String ANTWORTEN = "antworten";
    public static final String FRAGEN = "fragen";
    public static final String ANTWORTZEIT = "antwortzeit";
    public static final String UPDATED = "updated";
    public static final String AUSBL = "ausbl";
    public static final String DUPLICATE = "duplicate";
    public static final String YES = "yes";
    public static final String NO = "no";
    public static final String UNDEFINED = "undefined";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public MainActivity _main;
    public final static int fragID = 3;
    public EditText txtAnswer;
    public TextView tvQuestion;
    private boolean initialized;
    public RadioGroup grpOeffentlich;
    private RadioButton optOeffentlich;
    private RadioButton optPrivat;
    private boolean blnVote;

    public fragAnswer() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragAnswer.
     */
    // TODO: Rename and change types and number of parameters
    public static fragAnswer newInstance(String param1, String param2) {
        fragAnswer fragment = new fragAnswer();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _main = (MainActivity) getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(lib.getLayoutResFromTheme(getContext(), R.attr.fraganswer), container, false);
        return v;
    }

    @Override
    public void onViewCreated(android.view.View view, android.os.Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View v = view;
        tvQuestion = (TextView) v.findViewById(R.id.txtQuestion);
        txtAnswer = (EditText) v.findViewById((R.id.txtAnswer));
        grpOeffentlich = (RadioGroup) v.findViewById(R.id.grpOeffentlich);
        optOeffentlich = (RadioButton) v.findViewById(R.id.btnOeffentlich);
        optPrivat = (RadioButton) v.findViewById(R.id.btnPrivat);
        TooltipCompat.setTooltipText(optOeffentlich, optOeffentlich.getContentDescription());
        TooltipCompat.setTooltipText(optPrivat, optPrivat.getContentDescription());
        init();
        ImageButton btnOK = v.findViewById(R.id.btnOK);
        txtAnswer.setImeOptions(EditorInfo.IME_ACTION_DONE);
        txtAnswer.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != EditorInfo.IME_ACTION_DONE) return false;
                try {
                    return addAnswer();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    lib.ShowException(TAG, getContext(), throwable, true);
                }
                return false;
            }
        });
        txtAnswer.addTextChangedListener(new TextWatcher() {
            private int sel;
            String lastText;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                lastText = txtAnswer.getText().toString();
                sel = txtAnswer.getSelectionStart() - 1;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtAnswer.getText().length() > 240) {
                    lib.ShowToast(getContext(), getString(R.string.left) + (255 - txtAnswer.getText().length()) + getString(R.string.leftover));
                }
                String txt = txtAnswer.getText().toString();
                String[] l = txt.split("\\n", -1);
                boolean multipleInstancesOfSameChar = false;
                multipleInstancesOfSameChar = findMultipleInstancesOfSameChar(getContext(), txt, false);

                if (multipleInstancesOfSameChar || (l.length > 3 && l[l.length - 3].length() < 10 && l[l.length - 2].length() < 10)) {
                    lib.ShowToast(getContext(), getString(R.string.spam));
                    if (lastText.endsWith("\n"))
                        lastText = lastText.substring(0, lastText.length() - 1);
                    txtAnswer.setText(lastText);
                    txtAnswer.setSelection(txtAnswer.getText().length());
                }
                int selength = StringEscapeUtils.escapeJava(txtAnswer.getText().toString()).length();
                if (selength > 1024 || txtAnswer.getText().toString().split("\\n").length > 10 || txtAnswer.getLineCount() > 10) {
                    lib.ShowToast(getContext(), getString(R.string.maxlength));
                    txtAnswer.setText(lastText);
                    txtAnswer.setSelection(txtAnswer.getText().length());
                }
            }


        });
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addAnswer();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    lib.ShowException(TAG, getContext(), throwable, true);
                }
            }
        });
        if (savedInstanceState != null) {
            txtAnswer.setText(savedInstanceState.getString(ANSWER, ""));
        }
        this.initialized = true;

    }


    public static boolean findMultipleInstancesOfSameChar(Context context, String txt, boolean last) {
        int countEmojis = 0;
        int countEntities = 0;
        int len = txt.length();
        try {

            String htmlifiedText = StringEscapeUtils.escapeJava(txt);
            htmlifiedText = StringEscapeUtils.escapeHtml3(htmlifiedText);
// regex to identify html entitities in htmlified text
            Pattern htmlEntityPattern = Pattern.compile("&\\w+;");
            String emo_regex = "([\\u20a0-\\u32ff\\ud83c\\udc00-\\ud83d\\udeff\\udbb9\\udce5-\\udbb9\\udcee])";
            Matcher matcher = htmlEntityPattern.matcher(htmlifiedText);
            while (matcher.find()) {
                String emojiCode = matcher.group();
                countEntities++;
            }
            Matcher matcher2 = Pattern.compile(emo_regex).matcher(txt);
            while (matcher2.find()) {
                countEmojis++;
            }
        } catch (Throwable ex) {
            lib.ShowException(TAG, context, ex, false);
        }
        len -= countEmojis;
        if (len > 6 && (countEmojis > len / 3 || countEntities > len / 2)) return last;
        if (len > 3 && !txt.matches(".*[^\\.]\\.\\.\\.$") && !txt.matches(".*\\d*$")) {
            String c = txt.substring(len - 1);
            if (c.equals(txt.substring(len - 2, len - 1))
                    && (c.equals(txt.substring(len - 3, len - 2)))
                    && (c.equals(txt.substring(len - 4, len - 3)))) {
                return true;
            }
        }
        if (last)
        {
            HashMap<Integer, Integer> charcount = new HashMap<>();
            for (char c: txt.toCharArray())
            {
                Integer CharCode = (int)c;
                if (charcount.containsKey(CharCode)) {
                    Integer value = charcount.get(CharCode);
                    value++;
                    charcount.put(CharCode, value);
                }
                else
                {
                    charcount.put(CharCode, 1);
                }
            }
            for (int i: charcount.keySet())
            {
                if (charcount.get(i) > len/3) return true;
            }
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null || !initialized) return;
        savedInstanceState.putString(ANSWER, this.txtAnswer.getText().toString());
    }

    public void init() {
        txtAnswer.setError(null);
        if (_main.currentQuestion != null) {
            if (_main.currentQuestion.isVOTE()) {
                this.blnVote = true;
                optOeffentlich.setText(getString(R.string.yes));
                optPrivat.setText(getString(R.string.no));
                txtAnswer.setVisibility(View.GONE);
                grpOeffentlich.setVisibility(View.VISIBLE);
                optOeffentlich.setEnabled(true);
                optPrivat.setEnabled(true);
                optOeffentlich.setChecked(false);
                optPrivat.setChecked(false);
                tvQuestion.setText(_main.currentQuestion.Frage.replace(VOTE, ""));
                View.OnTouchListener TouchListenerVote = new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        RadioButton b = (RadioButton) v;
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            if (b.isChecked()) {
                                b.setChecked(false);
                                return true;
                            } else {
                                if (b.equals(optOeffentlich)) {
                                    optPrivat.setChecked(true);
                                } else {
                                    optOeffentlich.setChecked(true);
                                }
                                b.setChecked(true);
                                return true;
                            }
                        }
                        return false;
                    }


                };
                optOeffentlich.setOnTouchListener(TouchListenerVote);
                optPrivat.setOnTouchListener(TouchListenerVote);
            } else {
                this.blnVote = false;
                tvQuestion.setText(_main.currentQuestion.getFrage(true));
            }
        }
        if (_main.currentAnswerAnswer != null) {
            tvQuestion.setText(_main.currentAnswerAnswer.Antwort);
            optPrivat.setChecked(true);
            if (_main.currentAnswerAnswer.Frage.oeffentlich != null && _main.currentAnswerAnswer.Frage.oeffentlich && _main.currentAnswerAnswer.oeffentlich != null && _main.currentAnswerAnswer.oeffentlich) {
                grpOeffentlich.setVisibility(View.VISIBLE);
                optOeffentlich.setEnabled(true);
                optPrivat.setEnabled(true);
            } else {
                grpOeffentlich.setVisibility(View.VISIBLE);
                optOeffentlich.setEnabled(false);
                optPrivat.setEnabled(false);
            }
            optPrivat.setChecked(true);
        } else if (!blnVote) {
            grpOeffentlich.setVisibility(View.VISIBLE);
            optPrivat.setChecked(true);
            if (_main.currentQuestion != null) {
                optOeffentlich.setChecked(_main.currentQuestion.oeffentlich != null ? _main.currentQuestion.oeffentlich : false);
                optPrivat.setChecked((_main.currentQuestion.oeffentlich != null ? !_main.currentQuestion.oeffentlich : true));
            }
            optOeffentlich.setEnabled(false);
            optPrivat.setEnabled(false);

        }
        if (_main.currentAnswer != null) {
            String txt = _main.currentAnswer.Antwort;
            txtAnswer.setText(txt);
            if (!blnVote) {
                optOeffentlich.setChecked(_main.currentAnswer.oeffentlich != null ? _main.currentAnswer.oeffentlich : false);
                optPrivat.setChecked((_main.currentAnswer.oeffentlich != null ? !_main.currentAnswer.oeffentlich : true));
            } else {
                optOeffentlich.setChecked(true);
                optPrivat.setChecked(true);
                optOeffentlich.setChecked(true);
                optOeffentlich.setChecked(txt.startsWith(YES));
                optPrivat.setChecked(txt.startsWith(NO));
                if (txt.startsWith(UNDEFINED)) {
                    optOeffentlich.setChecked(false);
                    optPrivat.setChecked(false);
                }
            }
            if (blnVote || (_main.currentAnswer.Frage.oeffentlich != null && _main.currentAnswer.Frage.oeffentlich && (_main.currentAnswer.ParentFrageAntwortenID != null || !_main.currentAnswer.oeffentlich))) {
                grpOeffentlich.setVisibility(View.VISIBLE);
                optOeffentlich.setEnabled(true);
                optPrivat.setEnabled(true);
            } else {
                grpOeffentlich.setVisibility(View.VISIBLE);
                optOeffentlich.setEnabled(false);
                optPrivat.setEnabled(false);
            }

            int found = _main.currentAnswer.Frage.items.indexOf(_main.currentAnswerAnswer);
            if (found > -1 && _main.currentAnswer.Frage.items.size() > found + 1) {
                Answer nextAnswer = _main.currentAnswer.Frage.items.get(found + 1);
                if (nextAnswer.reAntwortID == _main.currentAnswer.reAntwortID || nextAnswer.ParentFrageAntwortenID.longValue() == _main.currentAnswer.reAntwortID) {
                    _main.hasRe = true;
                }
            }

            if (_main.hasRe) {
                lib.ShowMessage(getContext(), getString(R.string.newAnswer), getString(R.string.answer));
                _main.hasRe = false;
            }

        } else {
            txtAnswer.setText("");
            if (_main.currentAnswerAnswer != null && _main.currentAnswerAnswer.reAntwortID > 0) {
                //txtAnswer.setEnabled(false);
                //txtAnswer.setError(getString(R.string.AntwortNichtMöglich));
            } else {
                txtAnswer.setEnabled(true);
                txtAnswer.setError(null);
            }
        }

    }

    private boolean addAnswer() throws Throwable {
        if (!isInitialzed()) return false;
        String txt = "";
        if (!blnVote) {
            txt = txtAnswer.getText().toString();
        } else {
            txt = initTxtVote();
        }

        if (txt.length() < 2) return false;

        if (checkLineSpam(txt)) return false;

        if (findMultipleInstancesOfSameChar(getContext(),txt,true)) {
            lib.ShowToast(getContext(), getString(R.string.spam));
            return false;
        }

        final Answer a;
        ProgressDialog p = null;
        try {
            p = ProgressDialog.show(getContext(), getString(R.string.answer), getString(R.string.saving));

            a = createNewAnswer();

            Long ParentFrageAntwortenID = a.ParentFrageAntwortenID;

            int pos = 0;
            if (_main.currentAnswerAnswer != null) {
                pos = _main.currentQuestion.items.indexOf(_main.currentAnswerAnswer);
                pos += 1;
            } else if (ParentFrageAntwortenID != null) {
                pos = _main.currentQuestion.items.indexOf(_main.currentAnswer);
                pos += 1;
            }

            _main.currentQuestion.items.add(pos, a);

            final Answer ca = _main.currentAnswer;
            final Question cq = _main.currentQuestion;

            deleteCurrentAnswerFromDatabase(a, ca, cq);

            if (_main.currentAnswerAnswer == null && ca != null && ca.BenutzerID.longValue() == a.BenutzerID.longValue()) {
                removeCurrentAnswer(a, cq, ca);
            }
            _main.currentAnswerAnswer = null;
            _main.loadcurrentquestion = true;

            showAnswer();

            promptHideQuestion(a, cq);

            //_main.currentQuestion.fetched = false;
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
            String msg = e.getMessage();
            if (msg != null && (msg.toLowerCase().contains(DUPLICATE) || msg.toLowerCase().contains("doppelt"))) {
                msg = getString(R.string.duplicate);
                txtAnswer.setError(msg);
            } else {
                txtAnswer.setError(msg);
            }

            lib.ShowException(TAG, getContext(), e, true);
            return false;
        } finally {
            if (p != null) {
                p.dismiss();
                p.cancel();
            }
        }
    }

    private boolean checkLineSpam(String txt) {
        int lines = txtAnswer.getLineCount();
        int length = txt.length();
        int linebreaks = txt.split("\\n").length;
        return lines > 1 && length > 5 && length / linebreaks < 5;
    }

    private String initTxtVote() {
        String txt = null;
        if (optOeffentlich.isChecked()) {
            txt = YES;
        } else if (optPrivat.isChecked()) {
            txt = NO;
        } else {
            txt = UNDEFINED;
        }
        return txt;
    }

    private boolean isInitialzed() throws Exception {
        if (_main == null) return false;
        if (_main.currentQuestion == null || _main.user == null || _main.clsHTTPS == null)
            return false;
        if (_main.currentQuestion.FrageID == 1 && !_main.isAdmin() && !debugMode())
            return false;
        try {
            if (_main.user.getBoolean(DEAKTIVIERT)) {
                lib.ShowMessage(getContext(), getString(R.string.Accountdeactivated), "");
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void promptHideQuestion(final Answer a, final Question cq) {
        int ausbl = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(AUSBL, 0);
        if (ausbl == 0) {
            View checkBoxView = View.inflate(getContext(), R.layout.checkbox_layout, null);
            final CheckBox cbx = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
            cbx.setText(getString(R.string.AntwortMerken));
            //ViewGroup.LayoutParams lp = cbx.getLayoutParams();
            //cbx.setL
            //android:layout_gravity="center_horizontal"
            //android:layout_margin="5dp"
            final AlertDialog dlg;
            try {
                dlg = lib.ShowMessageYesNo(getContext(), getString(R.string.ausblenden), getString(R.string.Frage2), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hideQuestion(cq, a);
                        if (cbx.isChecked()) {
                            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putInt(AUSBL, 1).apply();
                        }
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (cbx.isChecked()) {
                            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putInt(AUSBL, 2).apply();
                        }
                    }
                }, checkBoxView);
                try {
                    dlg.show();
                } catch (Throwable e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            //dlg.setView(checkBoxView);

        } else if (ausbl == 1) {
            hideQuestion(cq, a);
        }
    }

    private void showAnswer() {
        _main.mPager.setCurrentItem(fragQuestions.fragID, true);
        final Fragment f = _main.fPA.findFragment(fragQuestions.fragID);
        if (f != null) {
            fragQuestions fragQuestions = (fragQuestions) f;
            if (fragQuestions.initialized || (fragQuestions.adapter != null && fragQuestions.adapter.rows.size() > 0)) {
                fragQuestions.adapter.notifyDataSetChanged();
                int groupPos = fragQuestions.findPos(_main.currentQuestion.ID);
                if (groupPos >= 0) {
                    _main.currentgroupid = groupPos;
                    if (!fragQuestions.lv.isGroupExpanded(groupPos)) {
                        fragQuestions.lv.setSelection(groupPos);
                        fragQuestions.lv.expandGroup(groupPos);
                    }
                    _main.loadcurrentquestion = false;
                }
            } else {
                //_main.mPager.setCurrentItem(fragQuestions.fragID);
            }

        } else {
            //_main.loadcurrentquestion = true;
            //_main.mPager.setCurrentItem(fragQuestions.fragID);
        }
    }

    private void removeCurrentAnswer(Answer a, final Question cq, Answer ca) {
        _main.currentQuestion.items.remove(_main.currentAnswer);
        if (a.ID.longValue() != ca.ID.longValue()) {
            HashSet<Long> reIDs = new HashSet<>();
            reIDs.add(ca.ID);
            for (int i = 0; i < cq.items.size(); i++) {
                final Answer aa = cq.items.get(i);

                if (aa.ParentFrageAntwortenID != null && aa.ParentFrageAntwortenID.longValue() == ca.ID.longValue()) {
                    cq.items.remove(aa);
                    i--;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                _main.clsHTTPS.deleteAnswer(aa.ID, aa.BenutzerID, cq.BenutzerID, cq.ID, cq.FrageID, cq.Kontaktgrad);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e(TAG, null, e);
                            }
                        }
                    }, 1000);

                } else if (aa.reAntwortID == ca.ID.longValue() || reIDs.contains(aa.reAntwortID)) {
                    cq.items.remove(aa);
                    reIDs.add(aa.ID);
                    i--;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                _main.clsHTTPS.deleteAnswer(aa.ID, aa.BenutzerID, cq.BenutzerID, cq.ID, cq.FrageID, cq.Kontaktgrad);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e(TAG, null, e);
                            }
                        }
                    }, 1000);

                }
            }
        }
        _main.currentAnswer = null;
    }

    private void deleteCurrentAnswerFromDatabase(Answer a, final Answer ca, final Question cq) {
        if (_main.currentAnswerAnswer == null && ca != null && ca.BenutzerID.longValue() == a.BenutzerID.longValue() && ca.ID.longValue() != a.ID.longValue()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        _main.clsHTTPS.deleteAnswer(ca.ID, ca.BenutzerID, cq.BenutzerID, cq.ID, cq.FrageID, cq.Kontaktgrad);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, null, e);
                    }
                }
            }, 1000);
        }
    }

    private Answer createNewAnswer() throws Throwable {

        Answer a = null;
        Double Breite = null;
        Double Laenge = null;
        if (_main.locCoarse() != null) {
            Breite = _main.locCoarse().getLatitude();
            Laenge = _main.locCoarse().getLongitude();
        }
        Long ParentFrageAntwortenID = null;
        long reAntwortID = 0;
        int level = 0;
        if (_main == null || _main.user == null) {
            throw new Exception(getString(R.string.nichtAngemeldet));
        }
        gStatus = "getBenutzerID()";
        Long BenutzerIDSender = _main.user.optLong(Constants.id, _main.getBenutzerID());
        Long BenutzerIDEmpfaenger = null;
        boolean update = false;
        if (_main.currentAnswerAnswer != null) {
            if (_main.currentAnswerAnswer.ParentFrageAntwortenID == null) {
                ParentFrageAntwortenID = _main.currentAnswerAnswer.ID;
                BenutzerIDEmpfaenger = _main.currentAnswerAnswer.BenutzerID;
                reAntwortID = 0;
            } else {
                ParentFrageAntwortenID = _main.currentAnswerAnswer.ParentFrageAntwortenID;
                BenutzerIDEmpfaenger = _main.currentAnswerAnswer.BenutzerIDSender;
                reAntwortID = _main.currentAnswerAnswer.ID;
                level = _main.currentAnswerAnswer.level + 1;
            }
        } else if (_main.currentAnswer != null) {
            ParentFrageAntwortenID = _main.currentAnswer.ParentFrageAntwortenID;
            BenutzerIDEmpfaenger = _main.currentAnswer.BenutzerIDEmpfaenger;
            reAntwortID = _main.currentAnswer.reAntwortID;
            level = _main.currentAnswer.level;
            update = true;
        } else {
            BenutzerIDEmpfaenger = _main.currentQuestion.BenutzerID;
        }
        String txt = "";
        if (!blnVote) {
            txt = txtAnswer.getText().toString();
        } else {
            txt = initTxtVote();
            int count = 0;
            count = _main.currentQuestion.hasAnswers(txt);
            txt += BenutzerIDSender;
        }
        boolean oeffentlich = optOeffentlich.isChecked();
        if (_main.currentQuestion.isVOTE()) {
            oeffentlich = _main.currentQuestion.oeffentlich;
        }

        a = new Answer(_main, 0l, ParentFrageAntwortenID, reAntwortID, 0l, 0, txt, txt, _main.currentQuestion, _main.currentQuestion.Kontaktgrad, BenutzerIDSender, BenutzerIDSender, BenutzerIDEmpfaenger, _main.user.optString(BENUTZERNAME), _main.user.optString(EMAIL), _main.clsHTTPS.getServerDate(BenutzerIDSender), Breite, Laenge, true, oeffentlich, _main.onlineState);

        JSONObject o = null;
        try {
            o = _main.clsHTTPS.answer(ParentFrageAntwortenID, reAntwortID, a.Frage.ID, a.Frage.BenutzerID, a.Frage.oeffentlich, a.BenutzerID, a.BenutzerIDSender, a.BenutzerIDEmpfaenger, a.EMail, a.BenutzerName, (a.Antwort), a.Frage.FrageID, a.Frage.Kontaktgrad, a.Breitengrad, a.Laengengrad, a.oeffentlich, update);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.ShowException(TAG, _main, throwable, true);
            throw throwable;
        }
        //a.Bewertungen = _main.clsHTTPS.getBewertungen(a.BenutzerID,a.BenutzerID,null);
        if (o == null) return null;

        a.Grad = 0;
        a.ID = o.optLong(FRAGEANTWORTENID);
        a.AntwortID = o.optLong(ANTWORTID);
        a.Bewertungen = o.optInt(BEWERTUNGEN);
        a.Antworten = o.optInt(ANTWORTEN);
        a.Fragen = o.optInt(FRAGEN);
        a.Antwortzeit = o.optInt(ANTWORTZEIT);
        a.level = level;

        try {
            a.updateUser(_main.user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        boolean updated = !o.isNull(UPDATED) && o.optBoolean(UPDATED);

        return a;
    }

    private void hideQuestion(Question cq, Answer a) {
        try {
            _main.clsHTTPS.hideQuestion(cq.ID, a.BenutzerID);
            final Fragment f = _main.fPA.findFragment(fragQuestions.fragID);
            if (f != null) {
                fragQuestions fragQuestions = (fragQuestions) f;
                Question x = fragQuestions.findQuestion(cq.ID);
                if (x != null) {
                    fragQuestions.adapter.rows.remove(fragQuestions.findQuestion(cq.ID));
                    fragQuestions.adapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, null, e);
            e.printStackTrace();
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
