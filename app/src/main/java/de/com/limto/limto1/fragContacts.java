package de.com.limto.limto1;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.MainActivity.dontStop;

public class fragContacts extends Fragment {

    public final static int fragID = 4;
    private static final String TAG = "fragContacts";
    public static final String ASC = " ASC";
    public static final String EQUAL = "equal";
    public static boolean listLoaded = false;
    public static List<ContactVO> contactVOListAndroid;
    public static ArrayList<ContactVO> ArrCopied = new ArrayList<ContactVO>();
    public MainActivity _main;
    RecyclerView rvContacts;
    ProgressBar pb;
    AllContactsAdapter contactAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        _main = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_all_contacts, container, false);

        rvContacts = (RecyclerView) v.findViewById(R.id.rvContacts);
        final ProgressBar pb = (ProgressBar) v.findViewById(R.id.pb);
        this.pb = pb;
        new ListAsyncTask(this, fragContacts2.listLoaded, fragContacts2.contactVOListLimindo, false).execute();
        return v;
    }

    private void setAdapter(List<ContactVO> contactVOs) throws Throwable {
        contactAdapter = new AllContactsAdapter(contactVOs, getActivity().getApplicationContext(), this);
        rvContacts.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rvContacts.setAdapter(contactAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater f) {
        f.inflate(R.menu.contacts_invite_menu, menu);
        //getMenuInflater().inflate(R.menu.home_menu, menu);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (dontStop > 0) dontStop--;
        if (dontStop == 0) LimindoService.dontStop = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_refresh:
                try {
                    new ListAsyncTask(this, fragContacts2.listLoaded, fragContacts2.contactVOListLimindo, true).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return true;
            case R.id.mnu_invite:
                inviteAllContacts();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void inviteAllContacts() {
        final List<ContactVO> l = fragContacts.contactVOListAndroid;
        if (l != null && l.size() > 0) {
            ArrayList<String> emails = new ArrayList<>();
            for (final ContactVO c : l) {
                if (!c.isLimindoContact() && !c.isLimindoUser() && c.getContactEmail() != null && c.getContactEmail().length() > 5 && lib.isValidEmailAddress(c.getContactEmail())) {
                    emails.add(c.getContactEmail());
                }
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (final ContactVO c : l) {
                        if (!c.isLimindoContact() && !c.isLimindoUser() && c.getContactEmail() != null && c.getContactEmail().length() > 5 && lib.isValidEmailAddress(c.getContactEmail())) {
                            try {
                                _main.clsHTTPS.Invite(_main.getBenutzerID(), c.getContactEmail(), lib.formatPhoneNumber(c.getContactNumber()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }).start();

            if (emails.size() > 0) {
                try {
                    lib.sendMails(_main, emails);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }


            }
        }

    }

    public List<ContactVO> getcontactVOList(AppCompatActivity a, boolean listLoaded, List<ContactVO> listCompare) throws CloneNotSupportedException {
        List<ContactVO> contactVOList = new ArrayList();
        ContactVO contactVO;

        ContentResolver contentResolver = a.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME +  " COLLATE NOCASE" + ASC);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                contactVO = new ContactVO();
                contactVO.setContactName(name);

                if (hasPhoneNumber > 0) {
                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    while (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        if (phoneNumber.length() == 0) continue;
                        phoneNumber = lib.formatPhoneNumber(phoneNumber);
                        if (phoneNumber.length() == 0 || contactVO.getContactNumber().contains(phoneNumber))
                            continue;
                        contactVO.setContactNumber(phoneNumber);
                    }

                    phoneCursor.close();
                }
                Cursor emailCursor = contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);

                while (emailCursor.moveToNext()) {
                    String emailId = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    emailId = emailId.trim().toLowerCase();
                    if (emailId.length() == 0 || contactVO.getContactEmail().contains(emailId))
                        continue;
                    contactVO.setContactEmail(emailId);
                }

                emailCursor.close();
                boolean hasChildren = false;
                if (listLoaded && listCompare != null) {
                    for (ContactVO c : listCompare) {
                        String EMailLimContact = c.getContactEmail().toLowerCase() + ",";
                        String EMailContact = contactVO.getContactEmail().toLowerCase() + ",";
                        String phoneLimContact = c.getContactNumber() + ",";
                        String phoneContact = contactVO.getContactNumber() + ",";
                        if (phoneContact.contains(phoneLimContact)) {
                            System.out.println(EQUAL);
                        }

                        if ((EMailContact.length() > 3 && EMailLimContact.length() > 3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length() > 3 && phoneLimContact.length() > 3 && phoneContact.contains(phoneLimContact))) {
                            //contactVO.setIsLimindoContact(true);
                            //contactVO.setUserID((c.getUserID()));
                            //contactVO.setContactID(c.getContactID());
                            //contactVO.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            if (!c.isCopied && !contactVO.isCopied) {
                                c.setIsLimindoContact(true);
                                ContactVO cc = contactVO.copy();
                                contactVO.copycount++;
                                cc.isCopied = true;
                                //c.isCopied = true;
                                contactVO.setIsLimindoContact(true);
                                cc.setIsLimindoContact(true);
                                cc.setUserID((c.getUserID()));
                                cc.setContactID(c.getContactID());
                                cc.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                                hasChildren = true;
                                contactVOList.add(cc);
                            }
                        }
                    }
                }


                if (!hasChildren) contactVOList.add(contactVO);

            }
        }
        return contactVOList;
    }

    private List<ContactVO> refreshList(boolean listLoaded, List<ContactVO> listCompare, List<ContactVO> contactVOList) throws Exception {
        ContactVO contactVO;
        if (_main.user == null) return listCompare;
        if (contactVOList.size() > 0) {
            for (int i = 0; i < contactVOList.size(); i++) {

                contactVO = contactVOList.get(i);
                String EMailContact = contactVO.getContactEmail().toLowerCase() + ",";
                String phoneContact = contactVO.getContactNumber() + ",";
                if (EMailContact.length() < 5 && phoneContact.length() < 5) continue;

                if (listLoaded && listCompare != null) {
                    if (contactVO.isCopied) continue;
                    for (ContactVO c : listCompare) {
                        String EMailLimContact = c.getContactEmail().toLowerCase() + ",";
                        String phoneLimContact = c.getContactNumber() + ",";
                        if (EMailLimContact.length() < 5 && phoneLimContact.length() < 5) continue;
                        if (phoneContact.contains(phoneLimContact)) {
                            System.out.println(EQUAL);
                        }
                        if ((EMailContact.length() > 3 && EMailLimContact.length() > 3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length() > 3 && phoneLimContact.length() > 3 && phoneContact.contains(phoneLimContact))) {

                            c.setIsLimindoContact(true);
                            //contactVO.setIsLimindoContact(true);
                            //contactVO.setUserID((c.getUserID()));
                            //contactVO.setContactID(c.getContactID());
                            //contactVO.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            if (c.isCopied) continue;
                            ContactVO cc = contactVO.copy();
                            contactVO.copycount++;
                            cc.isCopied = true;
                            //c.isCopied = true;
                            contactVO.setIsLimindoContact(true);
                            cc.setIsLimindoContact(true);
                            cc.setUserID((c.getUserID()));
                            cc.setContactID(c.getContactID());
                            cc.setContactName(contactVO.getOriginalName() + ", " + c.getContactName());
                            contactVOList.add(i + 1, cc);
                            i++;
                            if (contactVO.copycount < 2) ArrCopied.add(contactVO);
                        }
                    }
                }


            }
        }
        if (listLoaded && listCompare != null) {
            for (ContactVO c : ArrCopied) {
                contactVOList.remove(c);
            }
            ArrCopied.clear();
        }
        return contactVOList;

    }

    public void reLoad() {
        new ListAsyncTask(this, fragContacts2.listLoaded, fragContacts2.contactVOListLimindo, false).execute();
    }

    private static class ListAsyncTask extends AsyncTask<Void, Void, List<ContactVO>> {

        private final fragContacts _fragContacts;
        private final boolean listLoaded;
        private final List<ContactVO> listCompare;
        private final boolean refresh;

        ListAsyncTask(fragContacts fragContacts, boolean listLoaded, List<ContactVO> listCompare, boolean refresh) {
            this._fragContacts = fragContacts;
            this.listLoaded = listLoaded;
            this.listCompare = listCompare;
            this.refresh = refresh;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            _fragContacts.pb.setVisibility(View.VISIBLE);
            _fragContacts.rvContacts.setVisibility(View.GONE);
            if (!refresh) {
                fragContacts.listLoaded = false;
                fragContacts.contactVOListAndroid = null;
            }
        }

        @Override
        protected List<ContactVO> doInBackground(Void... integers) {
            try {
                if (!refresh) {
                    return _fragContacts.getcontactVOList(_fragContacts._main, this.listLoaded, listCompare);
                } else {
                    return _fragContacts.refreshList(fragContacts2.listLoaded, fragContacts2.contactVOListLimindo, contactVOListAndroid);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ContactVO> contactVOs) {
            super.onPostExecute(contactVOs);
            try {
                if (_fragContacts == null || contactVOs == null) return;
                _fragContacts.setAdapter(contactVOs);
                _fragContacts.pb.setVisibility(View.GONE);
                _fragContacts.rvContacts.setVisibility(View.VISIBLE);
                fragContacts.contactVOListAndroid = contactVOs;
                fragContacts.listLoaded = true;
                if (!refresh && !this.listLoaded && fragContacts2.listLoaded && fragContacts2.contactVOListLimindo != null)
                    new ListAsyncTask(_fragContacts, fragContacts2.listLoaded, fragContacts2.contactVOListLimindo, true).execute();
                if (refresh) {
                    fragContacts2 fr = (fragContacts2) _fragContacts._main.fPA.findFragment(fragContacts2.fragID);
                    if (fr != null && fr.contactAdapter != null) {
                        fr.contactAdapter.notifyDataSetChanged();
                    }
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }
    }
}