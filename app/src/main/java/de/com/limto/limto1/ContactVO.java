package de.com.limto.limto1;

import android.text.TextUtils;

import org.json.JSONObject;

import java.util.ArrayList;

import de.com.limto.limto1.Chat.dlgChat.MemberData;
import de.com.limto.limto1.lib.lib;

import static de.com.limto.limto1.MainActivity.KONTAKTGRAD;
import static de.com.limto.limto1.clsHTTPS.ONLINE_STATE;
import static de.com.limto.limto1.fragContacts2.BENUTZERNAME;
import static de.com.limto.limto1.fragContacts2.EINLADUNGEN_ID;
import static de.com.limto.limto1.fragContacts2.EMAIL;
import static de.com.limto.limto1.fragContacts2.KONTAKTID;
import static de.com.limto.limto1.fragContacts2.NAME;
import static de.com.limto.limto1.fragContacts2.ONLINE;
import static de.com.limto.limto1.fragContacts2.RUFNUMMER;
import static de.com.limto.limto1.fragContacts2.SPERREID;
import static de.com.limto.limto1.fragContacts2.VORNAME;

/**
 * Created by jhmgbl on 09.02.18.
 */

public class ContactVO {
    static final String BESTAETIGT2 = "bestaetigt2";
    public static final String BESTAETIGT0 = "bestaetigt";
    private boolean bestaetigt0;
    private int ContactImage;
    private String ContactName;
    private ArrayList<String> ContactNumber = new ArrayList<>();
    private ArrayList<String> ContactEmail = new ArrayList<>();
    private long contactID = -1;
    private long userID = -1;
    private int sperreID = -1;
    private boolean online;
    private boolean isLimindoContact;
    private boolean isLimindoUser;
    private boolean bestaetigt;
    private String OriginalName;
    private long contactID2 = -1;
    public boolean isAbgelehnt;
    public boolean isCopied;
    public int copycount = 0;
    private long einladungenID = -1;
    private boolean checked;
    private int kontaktgrad = -1;
    private MemberData memberData;
    private MainActivity.OnlineState onlineState = MainActivity.OnlineState.offline;
    private boolean blocked;

    public ContactVO(JSONObject user) {
        ContactVO contactVO = this;
        String BenutzerName = user.optString(BENUTZERNAME);
        String name = "";
        if (!user.isNull(NAME)) name = user.optString(NAME);
        if (!user.isNull(VORNAME)) name = user.optString(VORNAME) + " " + name;
        BenutzerName += "(" + name + ")";
        if (!user.isNull(KONTAKTID)) {
            contactVO.setContactImage(R.drawable.ic_action_insert_emoticon);
            contactVO.setContactID(user.optInt(KONTAKTID));
            //BenutzerName = user.getInt("kontaktid") + ": " + BenutzerName;
        }

        if (!user.isNull(SPERREID))
            contactVO.setSperreID(user.optInt(SPERREID));
        contactVO.setContactName(BenutzerName);
        contactVO.setUserID(user.optLong(Constants.id));
        contactVO.setIsLimindoUser(true);
        String phoneNumber = user.optString(RUFNUMMER);
        phoneNumber = lib.formatPhoneNumber(phoneNumber);
        contactVO.setContactNumber(phoneNumber);

        String emailId = user.optString(EMAIL);
        contactVO.setContactEmail(emailId);
        boolean hasChildren = false;
        if (!user.isNull(ONLINE)) contactVO.setOnline(user.optBoolean(ONLINE));
        onlineState = MainActivity.OnlineState.getOnlineState(user.optInt(ONLINE_STATE, MainActivity.OnlineState.offline.state));
        if (!user.isNull(EINLADUNGEN_ID))
            contactVO.setEinladungenID(user.optLong(EINLADUNGEN_ID));
        if (user.has(KONTAKTGRAD)) contactVO.setKontaktgrad (user.optInt(KONTAKTGRAD));
        bestaetigt = user.optBoolean(BESTAETIGT2);
        bestaetigt0 = user.optBoolean(BESTAETIGT0);

    }

    public ContactVO() {

    }

    public ContactVO copy() {
        ContactVO clone = new ContactVO();
        clone.isCopied = this.isCopied;
        clone.OriginalName = this.OriginalName;
        clone.setContactID(this.getContactID());
        clone.setUserID(this.getUserID());
        clone.setContactName(this.getContactName());
        clone.setContactID2(this.getContactID2());
        clone.bestaetigt = this.bestaetigt;
        clone.ContactEmail = this.ContactEmail;
        clone.ContactImage = this.ContactImage;
        clone.ContactNumber = this.ContactNumber;
        clone.setSperreID(this.getSperreID());
        clone.isAbgelehnt = this.isAbgelehnt;
        clone.isLimindoContact = this.isLimindoContact;
        clone.isLimindoUser = this.isLimindoUser;
        clone.online = this.online;
        clone.setSperreID(this.getSperreID());
        return  clone;
    }

    public int getContactImage() {
        return ContactImage;
    }

    public void setContactImage(int contactImage) {
        this.ContactImage = contactImage;
    }

    public String getContactName() {
        return ContactName;
    }

    public void setContactName(String contactName) {
        if (OriginalName == null) OriginalName = contactName;
        ContactName = contactName;
    }

    public String getOriginalName()
    {
        return  OriginalName;
    }

    public String getContactNumber() {
        if (ContactNumber.size()==0) return "";
        return TextUtils.join(",", ContactNumber);
    }

    public void setContactNumber(String contactNumber) {
        ContactNumber.add(contactNumber);
    }

    public String getContactEmail() {
        if (ContactEmail.size()==0) return "";
        return TextUtils.join(",", ContactEmail);
    }

    public void setContactEmail(String contactEmail) {
        ContactEmail.add(contactEmail);
    }

    public void setContactID(long contactID) {
        this.contactID = contactID;
    }

    public long getContactID() {
        return contactID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public long getUserID() {
        return userID;
    }


    public int getSperreID()
    {
        return sperreID;
    }

    public void setSperreID(int sperreID)
    {
        this.sperreID = sperreID;
    }

    public void setOnline(boolean online)
    {
        this.online = online;
    }

    public boolean isOnline()
    {
        return online;
    }

    public void setIsLimindoContact(boolean isLimindoContact) {
        this.isLimindoContact = isLimindoContact;
    }

    public boolean isLimindoContact() {
        return isLimindoContact;
    }

    public void setLimindoContact(boolean isLimindoContact) {
        this.isLimindoContact = isLimindoContact;
    }

    public void setIsLimindoUser(boolean isLimindoUser)
    {
        this.isLimindoUser = isLimindoUser;
    }

    public boolean isLimindoUser()
    {
        return isLimindoUser;
    }

    public void setLimindoUser(boolean isLimindoUser)
    {
        this.isLimindoUser = isLimindoUser;
    }

    public void setBestaetigt(boolean bestaetigt)
    {
        this.bestaetigt = bestaetigt;
    }

    public boolean isBestaetigt()
    {
        return bestaetigt;
    }

    public boolean isBestaetigt0()
    {
        return bestaetigt0;
    }
    public void setContactID2(long contactID2)
    {
        this.contactID2 = contactID2;
    }

    public long getContactID2()
    {
        return contactID2;
    }

    public void setEinladungenID(long einladungenID) {
        this.einladungenID = einladungenID;
    }

    public long getEinladungenID() {
        return einladungenID;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setKontaktgrad(int kontaktgrad) {
        this.kontaktgrad = kontaktgrad;
    }

    public int getKontaktgrad() {
        return kontaktgrad;
    }

    public void setMemberData(MemberData memberData) {
        this.memberData = memberData;
    }

    public MemberData getMemberData() {
        return memberData;
    }

    public void setOnlineState(MainActivity.OnlineState onlineState) {
        this.onlineState = onlineState;
    }

    public MainActivity.OnlineState getOnlineState() {
        return onlineState;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean getBlocked() {
        return blocked;
    }
}
