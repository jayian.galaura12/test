/*
 * Copyright (c) 2015 GPL by J.M.Goebel. Distributed under the GNU GPL v3.
 *
 * 08.06.2015
 *
 * This file is part of learnforandroid.
 *
 * learnforandroid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  learnforandroid is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.com.limto.limto1.lib;

//import android.support.v7.app.ActionBarActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;

import androidx.appcompat.app.AlertDialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.UiModeManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentManager;
import androidx.loader.content.CursorLoader;

import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnWindowFocusChangeListener;
import android.webkit.MimeTypeMap;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import de.com.limto.limto1.BuildConfig;
import de.com.limto.limto1.LimindoService;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.RichTextStripper;
import de.com.limto.limto1.YourService;
import de.com.limto.limto1.clsHTTPS;
import de.com.limto.limto1.dlgLog;
import de.com.limto.limto1.logger.Log;

import static android.content.Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION;
import static de.com.limto.limto1.Controls.Question.sdfdate;
import static de.com.limto.limto1.Controls.Question.sdftime;
import static de.com.limto.limto1.MainActivity.LICENSE_ACCEPTED;
import static de.com.limto.limto1.MainActivity.PP_ACCEPTED;
import static de.com.limto.limto1.MainActivity.blnAccessibility;
import static de.com.limto.limto1.broadcastReceiver.BOOT;
import static de.com.limto.limto1.dlgLogin.Laendervorwahl;
import static java.util.Locale.getDefault;

//import com.microsoft.live.*;
//import android.runtime.*;

public class lib {

    public static final String TAG = "org.de.jmg.lib.lib";
    public static final int SELECT_FILE = 0xa3b4;
    // private static final String ONEDRIVE_APP_ID = "48122D4E";
    private static final String ClassName = "lib.lib";
    public static final String ERROR = "Error";
    public static final String LIMTO = "Limto";
    public static final boolean SHOWEXCEPTIONSDEBUG = true;
    public static boolean sndEnabled = true;
    public static boolean AntwWasRichtig;
    public static ArrayList<DialogInterface> OpenDialogs = new ArrayList<>();
    //public static MainActivity main;
    public static String[] AssetSounds = new String[13];
    public static PrefsOnMultiChoiceClickListener cbListener = new PrefsOnMultiChoiceClickListener();
    public static boolean blnDebugOn = false;
    static AlertDialog dlgOK;
    static Bitmap bmpimg;
    private static String _status = "";
    private static yesnoundefined DialogResultYes = yesnoundefined.undefined;
    private static OnClickListener listenerYesNo = new OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    // Yes button clicked
                    DialogResultYes = yesnoundefined.yes;
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    // No button clicked
                    DialogResultYes = yesnoundefined.no;
                    break;
            }
        }
    };
    /*
     * public static String rtfToHtml(Reader rtf) throws IOException {
     * JEditorPane p = new JEditorPane(); p.setContentType("text/rtf");
     * EditorKit kitRtf = p.getEditorKitForContentType("text/rtf"); try {
     * kitRtf.read(rtf, p.getDocument(), 0); kitRtf = null; EditorKit kitHtml =
     * p.getEditorKitForContentType("text/html"); Writer writer = new
     * StringWriter(); kitHtml.write(writer, p.getDocument(), 0,
     * p.getDocument().getLength()); return writer.toString(); } catch
     * (BadLocationException e) { e.printStackTrace(); Log.e(TAG,null,e); } return null; }
     */
    private static long SLEEP_TIME = 2; // for 2 second
    private static CountDownLatch latch;
    public static String gStatus;
    public static int getFolderItemLock;
    private static Point ScreenSize;
    public static int LastgroupPosition;
    public static Drawable origBackColor = null;

    public lib() {

    }

    private static final String SAVED_PREFS_BUNDLE_KEY_SEPARATOR = "§§";

    public static boolean debugMode()
    {
        return blnDebugOn;
        //return Debug.isDebuggerConnected() || BuildConfig.DEBUG;
    }

    /**
     * Save a Bundle object to SharedPreferences.
     * <p>
     * NOTE: The editor must be writable, and this function does not commit.
     *
     * @param prefs       SharedPreferences Editor
     * @param key         SharedPreferences key under which to store the bundle data. Note this key must
     *                    not contain '§§' as it's used as a delimiter
     * @param preferences Bundled preferences
     */
    public static void savePreferencesBundle(SharedPreferences prefs, String key, Bundle preferences) {
        Set<String> keySet = preferences.keySet();
        Iterator<String> it = keySet.iterator();
        String prefKeyPrefix = key + SAVED_PREFS_BUNDLE_KEY_SEPARATOR;
        Set<String> keySetPrefs = prefs.getAll().keySet();
        Iterator<String> itPrefs = keySetPrefs.iterator();
        Editor editor = prefs.edit();
        while (itPrefs.hasNext()) {
            key = itPrefs.next();
            if (key.startsWith(prefKeyPrefix)) {
                editor.remove(key);
            }
        }
        while (it.hasNext()) {
            String bundleKey = it.next();
            Object o = preferences.get(bundleKey);
            if (o == null) {
                editor.remove(prefKeyPrefix + bundleKey);
            } else if (o instanceof Integer) {
                editor.putInt(prefKeyPrefix + bundleKey, (Integer) o);
            } else if (o instanceof Long) {
                editor.putLong(prefKeyPrefix + bundleKey, (Long) o);
            } else if (o instanceof Boolean) {
                editor.putBoolean(prefKeyPrefix + bundleKey, (Boolean) o);
            } else if (o instanceof CharSequence) {
                editor.putString(prefKeyPrefix + bundleKey, ((CharSequence) o).toString());
            } else if (o instanceof Bundle) {
                savePreferencesBundle(prefs, prefKeyPrefix + bundleKey, ((Bundle) o));
            }
        }
        editor.putBoolean("MainActivitySaved", true);
        editor.apply();
    }

    /**
     * Load a Bundle object from SharedPreferences.
     * (that was previously stored using savePreferencesBundle())
     * <p>
     * NOTE: The editor must be writable, and this function does not commit.
     *
     * @param sharedPreferences SharedPreferences
     * @param key               SharedPreferences key under which to store the bundle data. Note this key must
     *                          not contain '§§' as it's used as a delimiter
     * @return bundle loaded from SharedPreferences
     */
    public static Bundle loadPreferencesBundle(SharedPreferences sharedPreferences, String key) {
        Bundle bundle = new Bundle();
        Map<String, ?> all = sharedPreferences.getAll();
        Iterator<String> it = all.keySet().iterator();
        String prefKeyPrefix = key + SAVED_PREFS_BUNDLE_KEY_SEPARATOR;
        Set<String> subBundleKeys = new HashSet<String>();

        while (it.hasNext()) {

            String prefKey = it.next();

            if (prefKey.startsWith(prefKeyPrefix)) {
                String bundleKey = StringUtils.removeStart(prefKey, prefKeyPrefix);

                if (!bundleKey.contains(SAVED_PREFS_BUNDLE_KEY_SEPARATOR)) {

                    Object o = all.get(prefKey);
                    if (o == null) {
                        // Ignore null keys
                    } else if (o instanceof Integer) {
                        bundle.putInt(bundleKey, (Integer) o);
                    } else if (o instanceof Long) {
                        bundle.putLong(bundleKey, (Long) o);
                    } else if (o instanceof Boolean) {
                        bundle.putBoolean(bundleKey, (Boolean) o);
                    } else if (o instanceof CharSequence) {
                        bundle.putString(bundleKey, ((CharSequence) o).toString());
                    }
                } else {
                    // Key is for a sub bundle
                    String subBundleKey = StringUtils.substringBefore(bundleKey, SAVED_PREFS_BUNDLE_KEY_SEPARATOR);
                    subBundleKeys.add(subBundleKey);
                }
            } else {
                // Key is not related to this bundle.
            }
        }

        // Recursively process the sub-bundles
        for (String subBundleKey : subBundleKeys) {
            Bundle subBundle = loadPreferencesBundle(sharedPreferences, prefKeyPrefix + subBundleKey);
            bundle.putBundle(subBundleKey, subBundle);
        }


        return bundle;
    }

    public static String convertStreamToString(InputStream is, String searchString) throws IOException {
        // http://www.java2s.com/Code/Java/File-Input-Output/ConvertInputStreamtoString.htm
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        boolean firstLine = true;
        while ((line = reader.readLine()) != null) {
            if (searchString == null || searchString.length() == 0 || line.toLowerCase().contains(searchString.toLowerCase())) {
                if (firstLine) {
                    sb.append(line);
                    firstLine = false;
                } else {
                    sb.append("\n").append(line);
                }
            }
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (File file, String searchString) throws IOException {
        File fl = file;
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin, searchString);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void scheduleJob(Context context) {
        /*ComponentName serviceComponent = new ComponentName(context, JobIntentService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(1 * 1000); // wait at least
        builder.setOverrideDeadline(3 * 1000); // maximum delay
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
        */
        lib.setStatusAndLog(context, TAG, "scheduleJob LimindoService.class");

        Intent service = new Intent(context, LimindoService.class);
        service.setAction("SCHEDULE");
        service.putExtra(BOOT, true);
        YourService.enqueueWork(context, service);
    }

    public static File getLogFile(Context c) {
        File storage = Environment.getExternalStorageDirectory();
        File F2 = c.getExternalFilesDir(null);
        if (F2 != null) storage = F2;
        File logFile = new File(storage, "Log.txt");
        return logFile;
    }

    public static void appendLog(String text, Context c) {
        if (c == null) c = LimindoService.ApplicationContext;
        try {
            File logFile = getLogFile(c);
            boolean res = true;
            if (!logFile.exists()) {
                try {
                    res = logFile.createNewFile();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (res) {
                Date time = Calendar.getInstance().getTime();
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                buf.append(sdftime.format(time) + ": ");
                if (gStatus != null && !gStatus.equalsIgnoreCase(text))
                    buf.append(gStatus + ": ");
                buf.append(text);
                buf.newLine();
                buf.close();
            } else {
                lib.ShowToast(c, c.getString(R.string.couldnotcreatelogfile));
            }
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            android.util.Log.e(LIMTO, e.getMessage(), e);
            //lib.ShowToast(c, e.getMessage());
        }
    }

    public static void deleteLog(Context c)
    {
        try {
            File logFile = getLogFile(c);
            if (logFile.exists()) {
                try {
                       boolean res = logFile.delete();
                        System.out.println(res);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            android.util.Log.e(LIMTO, e.getMessage(), e);
            //lib.ShowToast(c, e.getMessage());
        }
    }

    public static void DeleteLog(Context c) {
        try {
            File logFile = getLogFile(c);
            if (logFile.exists()) {
                try {
                    long time = logFile.lastModified();
                    long currenttime = Calendar.getInstance().getTimeInMillis();
                    long diff = currenttime - time;
                    if (diff > 1000 * 60 * 60) {
                        boolean res = logFile.delete();
                        System.out.println(res);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            android.util.Log.e(LIMTO, e.getMessage(), e);
            //lib.ShowToast(c, e.getMessage());
        }
    }


    public static String formatPhoneNumber(String number) {
        if (number != null && number.length() > 3) {
            number = number.trim();
            number = number.replace(Laendervorwahl, "");
            boolean plus = number.startsWith("+");
            number = number.replaceAll("[^\\d]", "");
            if (number.startsWith("00")) number = number.replaceFirst("00", "+");
            if (number.startsWith("0")) number = number.replaceFirst("0", Laendervorwahl);
            if (!number.startsWith("+")) number = (plus ? "+" : Laendervorwahl) + number;
            return number;
        }
        return number;
    }

    public static String getgstatus() {
        return _status;
    }

    public static Bitmap resizeBM(Bitmap b, float factX, float factY) {
        if (b != null) {
            Matrix matrix = new Matrix();
            matrix.postScale(factX, factY);
            return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        }
        return null;
    }


    public static void removeDlg(DialogInterface dlg) {
        if (OpenDialogs.contains(dlg)) {
            OpenDialogs.remove(dlg);
        }
    }

    /*
     * private static class ExStateInfo { public Context context; public
     * RuntimeException ex; public ExStateInfo(Context context, RuntimeException
     * ex) { this.context = context; this.ex = ex; } }
     */

    public static void setgstatus(String value) {
        _status = value;
        gStatus = value;
        System.out.println(value);
    }

    public static boolean ExtensionMatch(String value, String extension) {
        String ext;
        if (value.contains(".")) {
            ext = value.substring(value.lastIndexOf("."));
        } else {
            return false;
        }

        if (extension.toLowerCase(getDefault()).contains(ext.toLowerCase(getDefault())))
            return true;

        String itext = extension;
        if (!itext.startsWith(".")) itext = "." + itext;
        itext = itext.replace(".", "\\.");
        itext = itext.toLowerCase(getDefault());
        ext = ext.toLowerCase(getDefault());
        return ext.matches(itext.replace("?", ".{1}").replace("*", ".*"));


    }

    public static void setStatusAndLog(Context context, String tag, String status, Throwable ex) {
        Log.e(tag, status, ex);
        setgstatus(status);
        appendLog(ex.getMessage(), context);
    }

    public static void setStatusAndLog(Context context, String tag, String message) {
        Log.e(tag, message);
        setgstatus(message);
        appendLog(gStatus, context);
    }

    public static void ShowMessageDebug(Context context, String msg, String title) {
      ShowMessageDebug(context, msg, title, false);
    }
        public static void ShowMessageDebug(Context context, String msg, String title, boolean showalways) {
        if (context != null && !MainActivity.nodialogs && SHOWEXCEPTIONSDEBUG && (showalways || debugMode()))
        {
            if (lib.isOnMainThread()) {
                FragmentManager fm;
                if (context instanceof MainActivity && (fm =((MainActivity) context).fm) != null)
                {
                    dlgLog dlg = dlgLog.show(fm);
                    dlg.appendlog(msg);
                }
                else
                {
                    // ShowMessage(context, msg, title);
                }
            } else {
                Handler handler = new Handler(context.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        ShowMessageDebug(context,msg,title);
                    }
                });
            }

        }
    }

    public static boolean isOnMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static class Ref<T> {

        private T value;

        public Ref(T value) {
            this.value = value;
        }

        public T get() {
            return value;
        }

        public void set(T anotherValue) {
            value = anotherValue;
        }

        @Override
        public String toString() {
            return value.toString();
        }

        @Override
        public boolean equals(Object obj) {
            return value.equals(obj);
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }
    }

    public static boolean RegexMatchVok(String FileName) {
        String ext = lib.getExtension(FileName);
        if (!libString.IsNullOrEmpty(ext)) {
            ext = ext.toLowerCase(getDefault());
            if (ext.startsWith(".k") || ext.startsWith(".v")) return true;
        }
        return false;
		/*
		if (FileName.toLowerCase().matches(".+\\.(v.{2})|(k.{2})$")) return true;
		if (FileName.toLowerCase().matches("\\/.+\\.(v.{2})|(k.{2})$")) return true;
		return false;
		*/
    }

    public static boolean NookSimpleTouch() {
        //return true;

        String MANUFACTURER = getBuildField("MANUFACTURER");
        @SuppressWarnings("unused")
        String MODEL = getBuildField("MODEL");
        String DEVICE = getBuildField("DEVICE");
        return (MANUFACTURER.equalsIgnoreCase("BarnesAndNoble") && DEVICE.equalsIgnoreCase("zoom2"));

    }

    private static String getBuildField(String fieldName) {

        try {
            return (String) Build.class.getField(fieldName).get(null);
        } catch (Exception e) {
            Log.d("cr3", "Exception while trying to check Build." + fieldName);
            return "";
        }
    }

    public static String getThumbnailPath(Uri uri, Context activity) throws Throwable {
        String[] proj = {MediaStore.Images.Media.DATA};

        // This method was deprecated in API level 11
        // Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        CursorLoader cursorLoader = new CursorLoader(activity, uri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);

        cursor.moveToFirst();
        long imageId = cursor.getLong(column_index);
        //cursor.close();
        String result = "";
        cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(activity.getContentResolver(), imageId,
                MediaStore.Images.Thumbnails.MINI_KIND, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
            cursor.close();
        }
        return null;
    }

    public Bitmap getThumbnailBitmap(Uri uri, Context activity) {
        String[] proj = {MediaStore.Images.Media.DATA};

        // This method was deprecated in API level 11
        // Cursor cursor = managedQuery(contentUri, proj, null, null, null);

        CursorLoader cursorLoader = new CursorLoader(activity, uri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);

        cursor.moveToFirst();
        long imageId = cursor.getLong(column_index);
        //cursor.close();

        Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(
                activity.getContentResolver(), imageId,
                MediaStore.Images.Thumbnails.MINI_KIND,
                (BitmapFactory.Options) null);

        return bitmap;
    }

    public static String getRealFilePath(final Context context, final Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if
        (ContentResolver.SCHEME_CONTENT.equals(scheme)) {

            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
                    String res = cursor.getString(0);
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    } else {
                        index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME);
                        if (index > -1) {
                            data = cursor.getString(index);
                        }
                    }
                }
                cursor.close();
            }
        }
        return data;
    }

    public static String getRealPathFromURI(Activity context,
                                            Uri contentURI) throws Exception {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentURI, proj, null,
                    null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return null;
            }

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) throws Throwable {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type) || type != null) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        } else {
            return getRealFilePath(context, uri);
        }

        return getRealFilePath(context, uri);
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String getSizeFromURI(Context context,
                                        Uri contentURI) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.SIZE};
            cursor = context.getContentResolver().query(contentURI, proj, null,
                    null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.SIZE);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return null;
            }

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /*
     * public static <E> getEnumByOrdinal(<E> object, int ordinal) throws
     * RuntimeException { E value; return value; }
     */

    public static void StartViewer(Activity context, Uri uri) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(uri, "image/*");
        MainActivity.dontStop++;
        context.startActivityForResult(i, MainActivity.RGLOBAL);
    }

    public static void copyFile(String Source, String Dest) throws IOException {
        File source = new File(Source);
        File dest = new File(Dest);
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } finally {
            sourceChannel.close();
            destChannel.close();
        }
    }

    public static float convertFromDp(Context context, float input) {
        int minWidth = context.getResources().getDisplayMetrics().widthPixels;
        int minHeight = context.getResources().getDisplayMetrics().heightPixels;
        if (minHeight < minWidth)
            minWidth = minHeight;
        final float scale = 768.0f / (float) minWidth;
        return ((input - 0.5f) / scale);
    }

    public static boolean checkGooglePlayServicesAvailable(Context AppContext, Activity act) throws Throwable {
        final int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(AppContext);
        if (status == ConnectionResult.SUCCESS) {
            return true;
        }

        Log.e(TAG, "Google Play Services not available: " + GooglePlayServicesUtil.getErrorString(status));

        if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
            final Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(status, act, 1);
            if (errorDialog != null) {
                errorDialog.show();
            }
        }

        return false;
    }

    public static boolean like(final String str, final String expr) {
        String regex = quotemeta(expr);
        regex = regex.replace("_", ".").replace("*", ".*?");
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE
                | Pattern.DOTALL);
        return p.matcher(str).matches();
    }

    public static String quotemeta(String s) {
        if (s == null) {
            throw new IllegalArgumentException("String cannot be null");
        }

        int len = s.length();
        if (len == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder(len * 2);
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if ("[](){}.+?$^|#\\".indexOf(c) != -1) {
                sb.append("\\");
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static int countMatches(String str, String sub) {
        if (libString.IsNullOrEmpty(str) || libString.IsNullOrEmpty(sub)) {
            return 0;
        }
        int count = 0;
        int idx = 0;
        while ((idx = str.indexOf(sub, idx)) != -1) {
            count++;
            idx += sub.length();
        }
        return count;

    }

    public static String MakeMask(String strBed) {
        try {
            int i = 0;
            int Len = strBed.length();
            if (Len == 0)
                return "";
            lib.gStatus = ClassName + ".MakeMask";
            for (i = 0; i <= Len - 1; i++) {
                if ((".,;/[]()".indexOf(strBed.charAt(i)) > -1)) {
                    strBed = strBed.substring(0, i) + "*"
                            + (i < Len - 1 ? strBed.substring(i + 1) : "");
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, null, ex);
            ex.printStackTrace();
        }
        return strBed;

    }

    /**
     * Returns a pseudo-random number between min and max, inclusive. The
     * difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value. Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see Random#nextInt(int)
     */
    public static int rndInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static <T> T[] ResizeArray(T Array[], int newSize) {
        @SuppressWarnings("unchecked")
        T[] NewArr = (T[]) java.lang.reflect.Array.newInstance(
                Array.getClass(), newSize);
        int length = Array.length;
        if (length > newSize)
            length = newSize;
        System.arraycopy(Array, 0, NewArr, 0, length);
        return NewArr;
    }

    public static int[] ResizeArray(int[] Array, int newSize) {
        int[] NewArr = new int[newSize];
        int length = Array.length;
        if (length > newSize)
            length = newSize;
        System.arraycopy(Array, 0, NewArr, 0, length);
        return NewArr;
    }

    public static void ShowMessage(Context context, String msg, String title) {
        ShowMessage(context, new SpannableString(msg), title, null);
    }

    public static void ShowMessage(Context context, String msg, String title, View v) {
        ShowMessage(context, new SpannableString(msg), title, v);
    }

    public static void ShowMessage(Context context, Spannable msg, String title) {
        ShowMessage(context, msg, title, null);
    }

    public static void ShowMessage(Context context, Spannable msg, String title, View v) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (MainActivity.nodialogs) return;
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.message);

        AlertDialog.Builder A = new AlertDialog.Builder(context);
        A.setPositiveButton(R.string.OK, new listener());
        A.setMessage(msg);
        A.setTitle(title);
        if (v != null) A.setView(v);
        AlertDialog dlg = A.create();
        dlg.show();
        ((TextView) dlg.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
        OpenDialogs.add(dlg);
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                removeDlg(dialog);
            }
        });

    }

    public static AlertDialog getDialogOKAndShow(Context context, String msg, String title, OnClickListener listener) {
        return getDialogOKAndShow(context, new SpannableString(msg), title, listener);
    }


    public static AlertDialog getDialogOKAndShow(Context context, Spannable msg, String title, OnClickListener listener) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.message);

        AlertDialog.Builder A = new AlertDialog.Builder(context);
        A.setPositiveButton(R.string.OK, listener);
        A.setMessage(msg);
        A.setTitle(title);
        AlertDialog dlg = A.create();
        dlg.show();
        ((TextView) dlg.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
        return dlg;

    }

    public static boolean ShowMessageWithCheckbox(Context context, String title, String msg, String CheckboxTitle) throws Exception {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.message);

        AlertDialog.Builder A = new AlertDialog.Builder(context);

        A.setTitle(title);
        A.setMessage(msg);

        View checkBoxView = View.inflate(context, R.layout.checkbox_layout, null);
        final CheckBox cbx = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
        cbx.setText(CheckboxTitle);

        A.setView(checkBoxView);
        DialogResultYes = yesnoundefined.undefined;
        A.setPositiveButton(context.getString(R.string.ok), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //throw new MessageException();
                DialogResultYes = yesnoundefined.yes;
            }
        });
        //A.setMessage(msg);
        AlertDialog dlg = A.create();
        dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                throw new MessageException();
            }
        });
        dlg.show();
        OpenDialogs.add(dlg);
        try {
            Looper.loop();
        } catch (Exception e2) {
            // Looper.myLooper().quit();
            if (dlg.isShowing()) {
                dlg.setOnDismissListener(null);
                dlg.dismiss();
            }
            removeDlg(dlg);
            dlg = null;
            if (!(e2 instanceof MessageException)) throw e2;
        }
        return DialogResultYes == yesnoundefined.yes && cbx.isChecked();
    }

    public static void ShowException(String TAG, Context context, Throwable ex, boolean showalways) {
        ShowException(TAG, context, ex, null, showalways);
    }

    public static void ShowException(final String TAG, final Context context, final Throwable ex, final String message, boolean showalways) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        try {
            if (context != null && MainActivity.nodialogs == false && SHOWEXCEPTIONSDEBUG && (showalways || (lib.debugMode()))) {
                if (lib.isOnMainThread()) {
                    AlertDialog.Builder A = new AlertDialog.Builder(context);
                    A.setPositiveButton("OK", new listener());
                    A.setMessage("Status: " + gStatus + "\n" + ex.getMessage() + "\n"
                            + (ex.getCause() == null ? "" : ex.getCause().getMessage())
                            + "\n" + android.util.Log.getStackTraceString(ex)
                            + (message != null ? "\n" + message : ""));
                    A.setTitle("Error:" + sdfdate.format(Calendar.getInstance().getTime()));
                    dlgOK = A.create();
                    dlgOK.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            removeDlg(dialog);
                        }
                    });
                    dlgOK.show();
                    OpenDialogs.add(dlgOK);
                } else {
                    Handler handler = new Handler(context.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ShowException(TAG, context, ex, message, false);
                        }
                    });
                }
            }
        } catch (Throwable e) {
            android.util.Log.e("ShowException", "Error", e);
        }
        Log.e(TAG, message, ex);
    }

    public static yesnoundefined ShowMessageYesNo(Context context,
                                                  String msg, String title) {
        return ShowMessageYesNo(context, msg, title, false);
    }

    public static yesnoundefined ShowMessageYesNo(Context context,
                                                  String msg, String title, boolean center) {
        return ShowMessageYesNo(context, new SpannableString(msg), title, center);
    }

    public static yesnoundefined ShowMessageYesNo(Context context,
                                                  Spanned msg, String title, boolean center) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);
        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(context.getString(R.string.yes), listenerYesNo);
            A.setNegativeButton(context.getString(R.string.no), listenerYesNo);
            A.setMessage(msg);
            A.setTitle(title);
            AlertDialog dlg = A.create();
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    throw new MessageException();
                }
            });

            dlg.show();
            OpenDialogs.add(dlg);
            if (center) {
                TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
            try {
                Looper.loop();
            } catch (Exception e2) {
                // Looper.myLooper().quit();
                if (dlg.isShowing()) {
                    dlg.setOnDismissListener(null);
                    dlg.dismiss();
                }
                removeDlg(dlg);
                dlg = null;
                if (!(e2 instanceof MessageException)) throw e2;
            }
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);
        }
        return DialogResultYes;
    }

    public static AlertDialog ShowMessageYesNo(Context context,
                                               String msg, String title, boolean center, OnClickListener OnClickListenerYes, OnClickListener OnClickListenerNo, View cbx) throws Throwable {
        return ShowMessagePosNeg(context, new SpannableString(msg), title, context.getString(R.string.yes), context.getString(R.string.no), center, OnClickListenerYes, OnClickListenerNo, cbx);
    }

    public static AlertDialog ShowMessageYesNo(Context context,
                                               Spanned msg, String title, boolean center, OnClickListener OnClickListenerYes, OnClickListener OnClickListenerNo, View cbx) throws Throwable {
        return ShowMessagePosNeg(context, msg, title, context.getString(R.string.yes), context.getString(R.string.no), center, OnClickListenerYes, OnClickListenerNo, cbx);
    }

    public static AlertDialog ShowMessagePosNeg(Context context,
                                                Spanned msg, String title, String positive, String negative, boolean center, OnClickListener OnClickListenerPos, OnClickListener OnClickListenerNeg, View cbx) throws Throwable {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (MainActivity.nodialogs) return null;
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);
        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(positive, OnClickListenerPos);
            A.setNegativeButton(negative, OnClickListenerNeg);
            A.setMessage(msg);
            A.setTitle(title);
            try {
                if (cbx != null) A.setView(cbx);
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
            AlertDialog dlg = A.create();
            dlg.show();
            if (center) {
                TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
            return dlg;
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);
        }
        return null;
    }


    public static yesnoundefined ShowMessageOKCancel(Context context,
                                                     String msg, String title, boolean center) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);
        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(context.getString(R.string.ok), listenerYesNo);
            A.setNegativeButton(context.getString(R.string.cancel), listenerYesNo);
            A.setMessage(msg);
            A.setTitle(title);
            AlertDialog dlg = A.create();
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    throw new MessageException();
                }
            });

            dlg.show();
            OpenDialogs.add(dlg);
            if (center) {
                TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
            try {
                Looper.loop();
            } catch (Exception e2) {
                // Looper.myLooper().quit();
                if (dlg.isShowing()) {
                    dlg.setOnDismissListener(null);
                    dlg.dismiss();
                }
                removeDlg(dlg);
                dlg = null;
                if (!(e2 instanceof MessageException)) throw e2;
            }
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);
        }
        return DialogResultYes;
    }

    public static AlertDialog getMessageOKCancel(Context context,
                                                 String msg, String title, boolean center, OnClickListener listenerOK, OnClickListener listenerCancel) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);

        DialogResultYes = yesnoundefined.undefined;
        AlertDialog.Builder A = new AlertDialog.Builder(context);
        A.setPositiveButton(context.getString(R.string.ok), listenerOK);
        A.setNegativeButton(context.getString(R.string.cancel), listenerCancel);
        A.setMessage(msg);
        A.setTitle(title);
        AlertDialog dlg = A.create();
        dlg.show();
        if (center) {
            TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);
        }
        return dlg;
    }


    public static YesNoCheckResult ShowMessageYesNoWithCheckbox(Context context,
                                                                String title,
                                                                String msg,
                                                                String CheckBoxTitle,
                                                                boolean center) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);
        gStatus = "ShowMessageYesNoWithCheckbox";
        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(context.getString(R.string.yes), listenerYesNo);
            A.setNegativeButton(context.getString(R.string.no), listenerYesNo);
            A.setMessage(msg);
            A.setTitle(title);
            View checkBoxView = View.inflate(context, R.layout.checkbox_layout, null);
            final CheckBox cbx = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
            cbx.setText(CheckBoxTitle);

            A.setView(checkBoxView);
            AlertDialog dlg = A.create();
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    throw new MessageException();
                }
            });

            dlg.show();
            OpenDialogs.add(dlg);
            if (center) {
                TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }

            try {
                Looper.loop();
            } catch (Exception e2) {
                // Looper.myLooper().quit();
                if (dlg.isShowing()) {
                    dlg.setOnDismissListener(null);
                    dlg.dismiss();
                }
                removeDlg(dlg);
                dlg = null;
                if (!(e2 instanceof MessageException)) throw e2;
            }
            return new YesNoCheckResult(DialogResultYes, cbx.isChecked());
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);
        }
        return null;
    }

    public static AlertDialog getInputBox(Context context,
                                          String title,
                                          String msg,
                                          String prompt,
                                          boolean center, OnClickListener listenerOK, OnClickListener listenerCancel, final EditText input) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);
        gStatus = "ShowMessageYesNoWithCheckbox";
        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(context.getString(R.string.ok), listenerOK);
            A.setNegativeButton(context.getString(R.string.cancel), listenerCancel);
            A.setMessage(msg);
            A.setTitle(title);
            // = new EditText(context);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(prompt);
            A.setView(input);
            AlertDialog dlg = A.create();
            dlg.show();
            if (center) {

                TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
                if (messageView != null) messageView.setGravity(Gravity.CENTER);
            }
            return dlg;
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);
        }
        return null;
    }

    public static OkCancelStringResult InputBox(Context context,
                                                String title,
                                                String msg,
                                                String prompt,
                                                boolean center) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));
        if (libString.IsNullOrEmpty(title)) title = context.getString(R.string.question);
        gStatus = "ShowMessageYesNoWithCheckbox";
        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(context.getString(R.string.ok), listenerYesNo);
            A.setNegativeButton(context.getString(R.string.cancel), listenerYesNo);
            A.setMessage(msg);
            A.setTitle(title);
            final EditText input = new EditText(context);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(prompt);
            A.setView(input);
            AlertDialog dlg = A.create();
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    throw new MessageException();
                }
            });

            dlg.show();
            OpenDialogs.add(dlg);
            if (center) {
                TextView messageView = (TextView) dlg.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }

            try {
                Looper.loop();
            } catch (Exception e2) {
                // Looper.myLooper().quit();
                if (dlg.isShowing()) {
                    dlg.setOnDismissListener(null);
                    dlg.dismiss();
                }
                removeDlg(dlg);
                dlg = null;
                if (!(e2 instanceof MessageException)) throw e2;
            }
            okcancelundefined res;
            if (DialogResultYes == yesnoundefined.yes) {
                res = okcancelundefined.ok;
            } else if (DialogResultYes == yesnoundefined.no) {
                res = okcancelundefined.cancel;
            } else {
                res = okcancelundefined.undefined;
            }
            return new OkCancelStringResult(res, input.getText().toString());
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);
        }
        return null;
    }

    public static yesnoundefined ShowMessageYesNoWithCheckboxes(Context context,
                                                                String msg,
                                                                CharSequence[] items,
                                                                boolean[] checkedItems,
                                                                OnMultiChoiceClickListener cbListener) {
        // System.Threading.SynchronizationContext.Current.Post(new
        // System.Threading.SendOrPostCallback(DelShowException),new
        // ExStateInfo(context, ex));

        try {

            DialogResultYes = yesnoundefined.undefined;
            AlertDialog.Builder A = new AlertDialog.Builder(context);
            A.setPositiveButton(context.getString(R.string.yes), listenerYesNo);
            A.setNegativeButton(context.getString(R.string.no), listenerYesNo);
            //A.setMessage(msg);
            A.setTitle(msg);
            A.setMultiChoiceItems(items, checkedItems, cbListener);
            AlertDialog dlg = A.create();
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    throw new MessageException();
                }
            });

            dlg.show();
            OpenDialogs.add(dlg);

            try {
                Looper.loop();
            } catch (Exception e2) {
                // Looper.myLooper().quit();
                if (dlg.isShowing()) {
                    dlg.setOnDismissListener(null);
                    dlg.dismiss();
                }
                removeDlg(dlg);
                dlg = null;
                if (!(e2 instanceof MessageException)) throw e2;
            }
        } catch (Exception ex) {
            ShowException(TAG, context, ex, false);

        }
        return DialogResultYes;
    }

    public static void ShowToast(Context context, String msg) {
        /* Looper.prepare(); */
        Toast T = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        T.show();
    }

    public static String getExtension(String Filename) {
        return getExtension(new File(Filename));
    }

    public static String getExtension(File F) {
        String extension = "";

        int i = F.getName().lastIndexOf('.');
        if (i > 0) {
            extension = F.getName().substring(i);
            return extension;
        } else {
            return null;
        }
    }

    public static String getFilenameWithoutExtension(String Filename) {
        return getFilenameWithoutExtension(new File(Filename));
    }

    public static String getFilenameWithoutExtension(File F) {
        String Filename = F.getPath();

        int i = Filename.lastIndexOf('.');
        int ii = Filename.lastIndexOf(File.pathSeparatorChar);
        if (i > 0 && i > ii) {
            Filename = Filename.substring(0, i);
            return Filename;
        } else {
            return Filename;
        }
    }

    public static void Sleep(int Seconds) throws InterruptedException {
        latch = new CountDownLatch(1);
        SLEEP_TIME = Seconds;
        MyLauncher launcher = new MyLauncher();
        launcher.start();
        // latch.await();
    }

    public static int[] getColors() {
        int[] Colors = new int[4096];
        for (int i = 0; i < 16; i++) {
            for (int ii = 0; ii < 16; ii++) {
                for (int iii = 0; iii < 16; iii++) {
                    Colors[i + ii * 16 + iii * 16 * 16] = Color.rgb(i * 16,
                            ii * 16, iii * 16);
                }
            }
        }
        ;
        ;
        return Colors;
    }

    public static int[] getIntArrayFromPrefs(SharedPreferences prefs,
                                             String name) {
        int count = prefs.getInt(name, -1);
        if (count > -1) {
            int[] res = new int[count + 1];
            for (int i = 0; i <= count; i++) {
                res[i] = prefs.getInt(name + i, 0);
            }

            return res;
        } else {
            return null;
        }

    }

    public static void putIntArrayToPrefs(SharedPreferences prefs, int array[],
                                          String name) {
        Editor edit = prefs.edit();
        if (array == null) {
            edit.putInt(name, -1);
            //edit.putInt(name + 0, 1);
        } else {
            int count = array.length - 1;
            edit.putInt(name, count);
            for (int i = 0; i <= count; i++) {
                edit.putInt(name + i, array[i]);
            }
        }


        edit.commit();

    }

    public static void initSounds() {
        AssetSounds[0] = "snd/clapping_hurray.ogg";
        AssetSounds[1] = "snd/Fireworks Finale-SoundBible.com-370363529.ogg";
        AssetSounds[2] = "snd/Red_stag_roar-Juan_Carlos_-2004708707.ogg";
        AssetSounds[3] = "snd/Fireworks Finale-SoundBible.com-370363529.ogg";
        AssetSounds[4] = "snd/clapping_hurray.ogg";
        AssetSounds[5] = "snd/clapping_hurray.ogg";
        AssetSounds[6] = "snd/chickens_demanding_food.ogg";
        AssetSounds[7] = "snd/Cow And Bell-SoundBible.com-1243222141.ogg";
        AssetSounds[8] = "snd/gobbler_bod.ogg";
        AssetSounds[9] = "snd/Toilet_Flush.ogg";
        AssetSounds[10] = "snd/ziegengatter.ogg";
        AssetSounds[11] = "snd/ziegengatter.ogg";
        AssetSounds[12] = "snd/Pew_Pew-DKnight556-1379997159.ogg";

    }


    public static void playSound(AssetManager assets, String name)
            throws Exception {
        if (!sndEnabled)
            return;
        AssetFileDescriptor afd = assets.openFd(name);
        try {
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(),
                    afd.getLength());
            player.prepare();
            player.start();
        } catch (Exception ex) {
            Log.e(ERROR, "log.playSound", ex);
        }
    }


    public static void playSound(File F) throws Exception {
        if (!sndEnabled)
            return;
        try {
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(F.getPath());
            player.prepare();
            player.start();
        } catch (Exception ex) {
            Log.e(ERROR, "playSound File " + F.getPath(), ex);
        }
    }

    public static void play_sound(final Context context, int ID) throws Throwable {
        new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... params) {
                try {
                    MediaPlayer player = MediaPlayer.create(context, params[0]);
                    player.setLooping(false); // Set looping
                    player.setVolume(100, 100);
                    player.start();
                } catch (Throwable ex) {
                    Log.i("play_sound();", ex.getMessage());
                }
                return null;
            }

        }.execute(new Integer[]{ID});
    }

    public static Drawable scaleImage(Context context, Drawable image,
                                      float scaleFactor) {

        if ((image == null) || !(image instanceof BitmapDrawable)) {
            throw new RuntimeException("Not BitmapDrawable!");
        }

        Bitmap b = ((BitmapDrawable) image).getBitmap();

        int sizeX = Math.round(image.getIntrinsicWidth() * scaleFactor);
        int sizeY = Math.round(image.getIntrinsicHeight() * scaleFactor);

        Bitmap bitmapResized = Bitmap
                .createScaledBitmap(b, sizeX, sizeY, false);

        image = new BitmapDrawable(context.getResources(), bitmapResized);

        return image;

    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static Drawable getDefaultCheckBoxDrawable(Context context) {
        int resID = 0;

        if (Build.VERSION.SDK_INT <= 10) {
            // pre-Honeycomb has a different way of setting the CheckBox button
            // drawable
            resID = Resources.getSystem().getIdentifier("btn_check",
                    "drawable", "android");
        } else {
            // starting with Honeycomb, retrieve the theme-based indicator as
            // CheckBox button drawable
            TypedValue value = new TypedValue();
            context.getApplicationContext()
                    .getTheme()
                    .resolveAttribute(
                            android.R.attr.listChoiceIndicatorMultiple, value,
                            true);
            resID = value.resourceId;
        }
        if (Build.VERSION.SDK_INT < 22) {
            return context.getResources().getDrawable(resID);
        } else {
            return context.getResources().getDrawable(resID, null);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBgCheckBox(CheckBox c, Drawable d) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            c.setBackgroundDrawable(d);
        } else {
            c.setBackground(d);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBg(RelativeLayout l, ShapeDrawable rectShapeDrawable) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            l.setBackgroundDrawable(rectShapeDrawable);
        } else {
            l.setBackground(rectShapeDrawable);
        }

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBgEditText(TextView e, Drawable drawable) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            e.setBackgroundDrawable(drawable);
        } else {
            e.setBackground(drawable);
        }

    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int spToPx(float sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    @SuppressLint("InlinedApi")
    public static void SelectFile(Activity context, Uri defaultURI) throws Exception {

        gStatus = "Select File";
        Intent intent = new Intent();
        if (defaultURI != null) {
            intent.setData(defaultURI);
        } else {
            //intent.setType("file/*");
        }
        SharedPreferences prefs = context.getPreferences(Context.MODE_PRIVATE);
        String msg = context.getString(R.string.msgShowDocumentProvider);
        String key = "ShowAlwaysDocumentProvider";
        int ShowAlwaysDocumentProvider = prefs.getInt(key, 999);
        String checkBoxTitle = context.getString(R.string.msgRememberChoice);
        YesNoCheckResult res = null;
        if (Build.VERSION.SDK_INT >= 19 && ShowAlwaysDocumentProvider == 999) {
            res = ShowMessageYesNoWithCheckbox(context, "", msg, checkBoxTitle, false);
            if (res.res == yesnoundefined.undefined) return;
        }

        if (Build.VERSION.SDK_INT < 19 || ShowAlwaysDocumentProvider == 0 ||
                (res != null && res.res == yesnoundefined.no)) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
            if (res != null && res.checked) {
                prefs.edit().putInt(key, 0).commit();
            }

        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            if (res != null && res.checked) {
                prefs.edit().putInt(key, -1).commit();
            }
        }
        intent.addFlags(FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        Intent chooser = Intent.createChooser(intent, "Open");
        MainActivity.dontStop++;
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivityForResult(chooser, SELECT_FILE);
        } else {
            intent.setData(null);
            context.startActivityForResult(chooser, SELECT_FILE);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void removewfcListener(ViewTreeObserver observer,
                                         OnWindowFocusChangeListener wfcListener) {

        if (Build.VERSION.SDK_INT >= 18) {
            observer.removeOnWindowFocusChangeListener(wfcListener);
        }
    }

    public static void removeLayoutListener(ViewTreeObserver observer,
                                            OnGlobalLayoutListener listener) {
        if (Build.VERSION.SDK_INT < 16) {
            removeLayoutListenerPre16(
                    observer, listener);
        } else {
            removeLayoutListenerPost16(
                    observer, listener);
        }
    }

    @SuppressWarnings("deprecation")
    private static void removeLayoutListenerPre16(ViewTreeObserver observer,
                                                  OnGlobalLayoutListener listener) {
        observer.removeGlobalOnLayoutListener(listener);
    }

    @TargetApi(16)
    private static void removeLayoutListenerPost16(ViewTreeObserver observer,
                                                   OnGlobalLayoutListener listener) {
        observer.removeOnGlobalLayoutListener(listener);
    }

    public static String dumpUriMetaData(Activity context, Uri uri) {

        // The query, since it only applies to a single document, will only return
        // one row. There's no need to filter, sort, or select fields, since we want
        // all fields for one document.
        Cursor cursor;
        String mimeType = null;
        try {
            cursor = context.getContentResolver().query(uri, null, null, null, null);
            mimeType = context.getContentResolver().getType(uri);
        } catch (Exception ex) {
            Log.e("DumpUri", "getContentResolver().query " + uri.toString(), ex);
            cursor = null;
        }
        try {
            // moveToFirst() returns false if the cursor has 0 rows.  Very handy for
            // "if there's anything to look at, look at it" conditionals.
            if (cursor != null && cursor.moveToFirst()) {

                // Note it's called "Display Name".  This is
                // provider-specific, and might not necessarily be the file name.
                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                Log.i(TAG, "Display Name: " + displayName);
                String path = uri.getPath();
                path = URLDecoder.decode(path, "UTF-8");
                path = path.substring(path.lastIndexOf("/") + 1);
                int found = path.indexOf(displayName);
                if (found > -1 && (found + displayName.length() < path.length())) {
                    displayName += path.substring(found + displayName.length());
                }

                int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                // If the size is unknown, the value stored is null.  But since an
                // int can't be null in Java, the behavior is implementation-specific,
                // which is just a fancy term for "unpredictable".  So as
                // a rule, check if it's null before assigning to an int.  This will
                // happen often:  The storage API allows for remote files, whose
                // size might not be locally known.
                String size = null;
                if (!cursor.isNull(sizeIndex)) {
                    // Technically the column stores an int, but cursor.getString()
                    // will do the conversion automatically.
                    size = cursor.getString(sizeIndex);
                } else {
                    size = "Unknown";
                }
                Log.i(TAG, "Size: " + size);


                return displayName + ":" + size + ":" + mimeType;
            }

        } catch (Exception ex) {
            lib.ShowException(TAG, context, ex, false);
        } finally {
            if (cursor != null) cursor.close();
        }
        return "";
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void getHitRect(View v, Rect rect) {
        rect.left = (int) (v.getLeft() + v.getTranslationX());
        rect.top = (int) (v.getTop() + v.getTranslationY());
        rect.right = rect.left + v.getWidth();
        rect.bottom = rect.top + v.getHeight();
        //rect.top -= v.dividerValue/2;
        //rect.bottom +=dividerValue/2;
    }

    @SuppressLint({"InlinedApi", "NewApi"})
    public static void CheckPermissions(Activity container, Uri uri, boolean blnShowMessage) throws Exception {
        try {
            int Flags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
            Flags = Flags | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
            if (Build.VERSION.SDK_INT >= 19) {
                //Flags = Flags | FLAG_GRANT_PERSISTABLE_URI_PERMISSION;
                container.getContentResolver().takePersistableUriPermission(uri, Flags);
            }


            //container.grantUriPermission("org.de.jmg.learn", uri , Flags);
        } catch (Exception ex) {
            try {
                Log.e("lib.GrantAllPermissions", ex.getMessage(), ex);
                int Flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
                //container.grantUriPermission("org.de.jmg.learn", uri , Flags);
                if (Build.VERSION.SDK_INT >= 19) {
                    container.getContentResolver().takePersistableUriPermission(uri, Flags);
                }
            } catch (Exception ex2) {
                Log.e("lib.GrantAllPermissions", ex2.getMessage(), ex2);
                if (blnShowMessage) {
                    SharedPreferences prefs = container.getPreferences(Context.MODE_PRIVATE);
                    String key = "DontShowPersistableURIMessage";
                    int DontShowPersistableURIMessage = prefs.getInt(key, 999);
                    String CheckBoxTitle = container.getString(R.string.msgDontShowThisMessageAgain);
                    String msg = container.getString(R.string.msgNoPersistableUriPermissionCouldBeTaken);
                    String title = "";
                    if (DontShowPersistableURIMessage != -1) {
                        DontShowPersistableURIMessage = ShowMessageWithCheckbox(container, title, msg, CheckBoxTitle) ? -1 : 0;
                        prefs.edit().putInt(key, DontShowPersistableURIMessage).commit();
                    }
                }
            }
            //if (force) lib.ShowException(container, ex);
            //throw new RuntimeException("CheckPermissions", ex);
        }
    }

    public static SpannableString getSpanableString(String txt) throws Exception {
        if (libString.IsNullOrEmpty(txt)) return new SpannableString("");
        final Pattern pattern = Pattern.compile("(?i)<a.*?</a>");
        final Pattern patternLI = Pattern.compile("(?i)<li>.*?<//li>", Pattern.DOTALL);
        Matcher matcherLI = patternLI.matcher(txt);
        if (txt.startsWith("{\\rtf1\\")) {
            // txt = Java2Html.convertToHtml(txt,
            // JavaSourceConversionOptions.getDefault());
            // return Html.fromHtml(txt);
            // return new SpannedString(stripRtf(txt));
            return new SpannableString(RichTextStripper.StripRichTextFormat(txt));
        }
        SpannableString span = null;
        if (txt.contains("<link://")) {
            ArrayList<String> urls = new ArrayList<String>();
            ArrayList<String> links = new ArrayList<String>();
            ArrayList<Integer> positions = new ArrayList<Integer>();
            int found = -1;
            while (txt.indexOf("<link://", found + 1) > -1) {
                found = txt.indexOf("<link://", found + 1);
                int Start = found + 8;
                int End = txt.indexOf("/>", Start);
                String repl = txt.substring(found, End + 2);
                if (End > 0) {
                    String Link = txt.substring(Start, End);
                    int LinkEnd = Link.indexOf(" ");
                    if (LinkEnd > -1) {
                        String url = Link.substring(0, LinkEnd);
                        urls.add(url);
                        String linkText = Link.substring(LinkEnd + 1, Link.length());
                        links.add(linkText);
                        positions.add(found);
                        txt = txt.replace(repl, linkText);
                    }
                }
            }
            span = new SpannableString(txt);
            for (int i = 0; i < links.size(); i++) {
                int Start = positions.get(i);
                int End = Start + links.get(i).length();
                URLSpan spn = new URLSpan(urls.get(i));

                span.setSpan(spn, Start,
                        End, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        } else {
            if (matcherLI.find()) {
                int start = 0;
                do {
                    String LI = matcherLI.group();
                    LI = LI.replace("<//", "</");
                    SpannableString spnLI = new SpannableString(Html.fromHtml(LI));
                    spnLI.setSpan(new android.text.style.BulletSpan(), 0, spnLI.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (span == null || start == 0) {
                        span = new SpannableString(txt.substring(start, matcherLI.start()));
                    } else {
                        span = new SpannableString(TextUtils.concat(span, txt.substring(start, matcherLI.start())));
                    }
                    start = matcherLI.end() + 1;
                    span = new SpannableString(TextUtils.concat(span, spnLI));
                }
                while (matcherLI.find());
                if (span != null) txt = span.toString();
            }
            Matcher matcher = pattern.matcher(txt);
            if (false && matcher.find()) {
                int start = 0;
                boolean blnNewSpan = false;
                do {
                    String Anchor = matcher.group();
                    SpannableString spnAnchor = new SpannableString(Html.fromHtml(Anchor));
                    if (span == null) {
                        span = new SpannableString(txt.substring(start, matcher.start()));
                        blnNewSpan = true;
                    } else if (blnNewSpan) {
                        span = new SpannableString(TextUtils.concat(span, txt.substring(start, matcher.start())));
                    }
                    start = matcher.end() + 1;
                    if (blnNewSpan) {
                        span = new SpannableString(TextUtils.concat(span, spnAnchor));
                    } else {
                        span = new SpannableString(TextUtils.replace
                                (span, new String[]{Anchor}, new CharSequence[]{spnAnchor}));
                    }
                }
                while (matcher.find());

                if (span != null) txt = span.toString();
            }
        }
        if ((txt.contains("http://") || txt.contains("https://"))) {

            int found = 0;
            int found1 = 0;
            int found2 = 0;
            //final String reg_exUrl = "\\b(?:(?:https?|ftp|file)://|www\\.|ftp\\.)(?:\\([-A-Z0-9+&@#/%=~_|$?!:,.]*\\)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:\\([-A-Z0-9+&@#/%=~_|$?!:,.]*\\)|[A-Z0-9+&@#/%=~_|$])";
            //Pattern p = Pattern.compile(reg_exUrl);  // insert your pattern here
            //Matcher m = p.matcher(txt);
            found1 = txt.indexOf("http://");
            found2 = txt.indexOf("https://");
            if ((found1 > -1 && found1 < found2) || found2 == -1) {
                found = found1;
            } else {
                found = found2;
            }
            if (span == null) span = new SpannableString(txt);
            txt = txt.replace("\r", " ");
            txt = txt.replace("\n", " ");
            txt += " ";
            while (found != -1) {
                int start = found;
                int end = txt.indexOf(" ", found + 1);
                int endhyphen = txt.indexOf("\"", found + 1);
                int endbracked = txt.indexOf(")", found + 1);
                if (end == -1 || (endhyphen > -1 && endhyphen < end)) end = endhyphen;
                if (end == -1 || (endbracked > -1 && endbracked < end)) end = endbracked;
                if (end != -1) {
                    URLSpan urls[] = span.getSpans(start, end, URLSpan.class);
                    if (urls == null || urls.length == 0) {
                        String url = txt.substring(start, end);
                        span.setSpan(new URLSpan(url), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    found1 = txt.indexOf("http://", end + 1);
                    found2 = txt.indexOf("https://", end + 1);
                    if ((found1 > -1 && found1 < found2) || found2 == -1) {
                        found = found1;
                    } else {
                        found = found2;
                    }
                } else {
                    found = end;
                }
            }
			/*
			URLSpan[] urls = span.getSpans(0, txt.length(), URLSpan.class);
	        for(URLSpan urlspan : urls) {
	            makeLinkClickable(span, urlspan);
	        }
	        */
            return span;

			/*
			Pattern pattern = Pattern.compile(
		            "\\b(((ht|f)tp(s?)\\:\\/\\/|~\\/|\\/)|www.)" +
		            "(\\w+:\\w+@)?(([-\\w]+\\.)+(com|org|net|gov" +
		            "|mil|biz|info|mobi|name|aero|jobs|museum" +
		            "|travel|[a-z]{2}))_txtMeaning1.setMovementMethod(LinkMovementMethod.getInstance());
		(:[\\d]{1,5})?" +
		            "(((\\/([-\\w~!$+|.,=]|%[a-f\\d]{2})+)+|\\/)+|\\?|#)?" +
		            "((\\?([-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" +
		            "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)" +
		            "(&(?:[-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" +
		            "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)*)*" +
		            "(#([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)?\\b");

		        Matcher matcher = pattern.matcher(txt);
		        SpannableString span = new SpannableString(txt);

		        while (matcher.find()) {
		        	String url = txt.substring(matcher.start(),matcher.end());
					span.setSpan(new URLSpan(url), matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		        }
		        */
        }
        if (span == null) return new SpannableString(txt);
        else return span;
    }

    public static Bitmap downloadpicture(final String Url) {
        final CountDownLatch l = new CountDownLatch(1);
        bmpimg = null;
        new Thread(new Runnable() {
            @Override
            public void run() {
                InputStream in = null;
                try {
                    URL url = new URL(Url);
                    URLConnection urlConn = url.openConnection();
                    HttpURLConnection httpConn = (HttpURLConnection) urlConn;
                    httpConn.connect();
                    in = httpConn.getInputStream();
                    bmpimg = BitmapFactory.decodeStream(in);
                    in.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.i(TAG, null, e);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.i(TAG, null, e);
                } finally {
                    l.countDown();
                }
            }
        }).start();
        try {
            l.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
        }


        return bmpimg;
    }

    protected static void makeLinkClickable(SpannableString strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                Log.d("Link", span.getURL());
                // Do something with span.getURL() to handle the link click...
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    public static int getUIMode(Context context) {
        if (Build.VERSION.SDK_INT >= 8) {
            UiModeManager uiModeManager = (UiModeManager) context.getSystemService(Context.UI_MODE_SERVICE);
            return uiModeManager.getCurrentModeType();
        } else {
            return 0;
        }
    }

    public static String toLanguageTag(Locale l) {
        String localeId = l.toString();//= MessageFormat.format("{0}-{1}",
        //        l.getLanguage(),
        //        l.getCountry());
        return localeId;
    }

    public static Locale forLanguageTag(String s) {
        /*
        StringTokenizer tempStringTokenizer = new StringTokenizer(s,"-");
        String l = null;
        String c = null;
        if(tempStringTokenizer.hasMoreTokens())
            l = tempStringTokenizer.nextToken();
        if(tempStringTokenizer.hasMoreTokens())
            c = tempStringTokenizer.nextToken();
        if (l!=null && c!=null)return new Locale(l,c);
        if (l!=null) return new Locale(l);
        */
        s = s.replace("-", "_");
        if (s.length() > 1 && s.endsWith("_")) s = s.substring(0, s.length() - 1);
        return new Locale(s);
        //return null;
    }

    public static String ReplaceLinks(String txt) {
        if (txt.contains("<link://")) {
            int found = -1;
            while (txt.indexOf("<link://", found + 1) > -1) {
                found = txt.indexOf("<link://", found + 1);
                int Start = found + 8;
                int End = txt.indexOf("/>", Start);
                String repl = txt.substring(found, End + 2);
                if (End > 0) {
                    String Link = txt.substring(Start, End);
                    int LinkEnd = Link.indexOf(" ");
                    if (LinkEnd > -1) {
                        String url = Link.substring(0, LinkEnd);
                        String linkText = Link.substring(LinkEnd + 1, Link.length());
                        txt = txt.replace(repl, "<a href=\"" + url + "\">" + linkText + "</a>");
                    }
                }
            }
        }
        return txt;
    }

    public static void setLocale(Context c, String l) throws Exception {

        Resources res = c.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(l);
        res.updateConfiguration(conf, dm);

    }

    public static void AcceptPrivacyPolicy(MainActivity context, Locale L, boolean standalone) throws Throwable {
        InputStream is;
        if ((L.equals(Locale.GERMAN)) || (L.equals(Locale.GERMANY))) {
            is = context.getAssets().open("PrivacyPolicyDe");
        } else {
            is = context.getAssets().open("PrivacyPolicy");
        }
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        String strPrivacyPolicy = s.hasNext() ? s.next() : "";
        s.close();
        is.close();
        lib.ShowMessageYesNo(context,
                fromHtml(strPrivacyPolicy),
                context.getString(R.string.PrivacyPolicyAccept),
                true, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!standalone) {
                            try {
                                lib.AcceptDisclaimer(context, Locale.getDefault());
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                        {
                            context.getPreferences(Context.MODE_PRIVATE).edit().putBoolean(PP_ACCEPTED, true).apply();
                        }
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.getPreferences(Context.MODE_PRIVATE).edit().putBoolean(PP_ACCEPTED, false).apply();
                        context.finish();
                    }
                }, null);

    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static void AcceptDisclaimer(MainActivity context, Locale L) throws Throwable {
        InputStream is;
        if ((L.equals(Locale.GERMAN)) || (L.equals(Locale.GERMANY))) {
            is = context.getAssets().open("DISCLAIMERDe");
        } else {
            is = context.getAssets().open("DISCLAIMER");
        }
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        String strPrivacyPolicy = s.hasNext() ? s.next() : "";
        s.close();
        is.close();
        lib.ShowMessageYesNo(context,
                strPrivacyPolicy,
                context.getString(R.string.DisclaimerAccept),
                true, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.getPreferences(Context.MODE_PRIVATE).edit().putBoolean(LICENSE_ACCEPTED, true).apply();
                        context.processCacheIfVisible();
                        context.bindService0();
                    }
                }, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.finish();
                    }
                }, null);

    }


    public static void ShowHelp(Context context, Locale L) throws IOException {
        InputStream is;
        if ((L.equals(Locale.GERMAN)) || (L.equals(Locale.GERMANY))) {
            is = context.getAssets().open("READMEDe");
        } else {
            is = context.getAssets().open("README");
        }
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        String strPrivacyPolicy = s.hasNext() ? s.next() : "";
        s.close();
        is.close();
        lib.ShowMessage(context,
                strPrivacyPolicy,
                context.getString(R.string.Help)
        );
    }


    public static String arrStrToCSV(String[] arrSaves) {
        StringBuilder strSaves = new StringBuilder();
        for (String s : arrSaves) {
            if (libString.IsNullOrEmpty(s)) continue;
            if (strSaves.length() > 0) strSaves.append(";");
            else strSaves.append("");
            strSaves.append("\"" + s + "\"");
        }
        return strSaves.toString();
    }

    public static Point getScreenSize(Activity context) {
        if (ScreenSize != null) return ScreenSize;
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        final int width;
        final int height;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);

        } else {
            //noinspection deprecation
            width = display.getWidth();  // deprecated
            //noinspection deprecation
            height = display.getHeight();  // deprecated
            size = new Point(width, height);
        }
        ScreenSize = size;
        return ScreenSize;
    }

    public static String getSetting(Context c, String name, String def) throws Throwable {
        return getSetting(c, name, def, false);
    }

    public static String getSetting(Context c, String name, String def, boolean renew) throws Throwable {
        SharedPreferences prefs = c.getSharedPreferences(LIMTO, Context.MODE_PRIVATE);
        if (!renew && prefs.contains(name)) {
            return prefs.getString(name, def);
        } else {
            if (renew && prefs.contains(name)) def = prefs.getString(name, def);
            OkCancelStringResult res = lib.InputBox(c, c.getString(R.string.inputValueFor) + " " + name, name, def, false);
            if (res.res == okcancelundefined.ok && !libString.IsNullOrEmpty(res.input)) {
                prefs.edit().putString(name, res.input).commit();
                return res.input;
            }

        }
        throw new RuntimeException("No value!");
    }

    public static Bitmap drawableToBitmap(Drawable drawable, int width, int height) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }
        if (width <= 0 || height <= 0) {
            if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }
        } else {
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static @DrawableRes
    int getDrawableResFromTheme(Context context, int res) {

        TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{res});
        int attributeResourceId = a.getResourceId(0, 0);
        @DrawableRes int drawable = attributeResourceId;
        // Drawable drawable = getResources().getDrawable(attributeResourceId);
        return drawable;

    }

    public static @LayoutRes
    int getLayoutResFromTheme(Context context, int res) {

        TypedArray a = context.getTheme().obtainStyledAttributes(new int[]{res});
        @LayoutRes int drawable = a.getResourceId(0, 0);
        // Drawable drawable = getResources().getDrawable(attributeResourceId);
        return drawable;

    }

    public static int dpToPx(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static float getDimensionFromTheme(Context context, int attr) {
        TypedArray styledAttributes = null;
        try {
            styledAttributes = context.getTheme().obtainStyledAttributes(
                    new int[]{attr});
            float res = styledAttributes.getDimension(0, 40f);
            return res;
        } finally {
            if (styledAttributes != null) styledAttributes.recycle();
        }
    }

    public static void setFontsBold(View view) {
        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

            View v = ((ViewGroup) view).getChildAt(i);
            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setTypeface(tv.getTypeface(), Typeface.BOLD);
            } else {
                setFontsBold(v);
            }
        }
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static void sendMails(Activity context, ArrayList<String> emails) throws Throwable {
        if (emails.size() > 99) {
            ArrayList<String> emails100 = new ArrayList<>();
            String aEmailBCCList[] = emails.toArray(new String[emails.size()]);
            for (int i = 0; i < 99; i++) {
                emails100.add(aEmailBCCList[i]);
                emails.remove(0);
            }
            sendMails(context, emails100);
            sendMails(context, emails);
        } else {
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            String aEmailList[] = {"vnetz2511@gmail.com"};
            //String aEmailCCList[] = { "user3@fakehost.com","user4@fakehost.com"};
            String aEmailBCCList[] = emails.toArray(new String[emails.size()]);
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
            //emailIntent.putExtra(android.content.Intent.EXTRA_CC, aEmailCCList);
            emailIntent.putExtra(android.content.Intent.EXTRA_BCC, aEmailBCCList);
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "LIMTO Corona Edition");
            emailIntent.setType("text/plain");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, context.getString(R.string.plsInstall));

            context.startActivityForResult(emailIntent, MainActivity.RGLOBAL);
            MainActivity.dontStop++;
            //context.startActivityForResult(Intent.createChooser(emailIntent, context.getString(R.string.sendMail))),RGLOBAL);
        }
    }

    public static @ColorInt
    int getColorResFromThme(Context context, int attr) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(attr, typedValue, true);
        @ColorInt int color = typedValue.data;
        return color;
    }

    public static SpannableString getClickableSpanFromMessage(final clsHTTPS clsHTTPS, final Long BenutzerID, final Context context, String message, boolean blnWhite) throws JSONException {
        JSONObject o = new JSONObject(message.substring(12));
        String _message = o.optString("message");
        String _path = o.optString("path");
        final String _file = o.optString("file");
        final String _mimetype = o.optString("mimetype");
        String thumb = o.optString("thumb");
        UUID _uuid = UUID.fromString(o.optString("uuid"));

        String m = new File(_path).getName();
        SpannableString ss = new SpannableString(m);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                MimeTypeMap myMime = MimeTypeMap.getSingleton();
                final Intent newIntent = new Intent(Intent.ACTION_VIEW);
                final String mimeType = _mimetype;
                File file = null;
                try {
                    new AsyncTask<Object, Object, File>() {
                        private ProgressDialog dialog;

                        protected void onPreExecute() {
                            dialog = new ProgressDialog(context);
                            this.dialog.setMessage(context.getString(R.string.downloading));
                            this.dialog.show();
                        }

                        private Exception ex;

                        @Override
                        protected File doInBackground(Object... objects) {
                            try {
                                File file = clsHTTPS.downloadFile(context, _file, BenutzerID, true);
                                return file;
                            } catch (Exception e) {
                                e.printStackTrace();
                                this.ex = e;
                                return null;
                            }
                        }

                        @Override
                        protected void onPostExecute(File file) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                                dialog = null;
                            }
                            if (file != null && file.exists()) {
                                MediaScannerConnection.scanFile(context,
                                        new String[]{file.getAbsolutePath()}, null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                                newIntent.setDataAndType(uri, mimeType);
                                                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                try {
                                                    if (context instanceof Activity) {
                                                        MainActivity.dontStop++;
                                                        ((Activity) context).startActivityForResult(newIntent, MainActivity.RVIEWFILE);
                                                    } else {
                                                        context.startActivity(newIntent);
                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(context, context.getString(R.string.NoHandlerFound), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });
                            } else {
                                try {
                                    if (ex != null) throw ex;
                                    else
                                        throw new Exception(file != null ? "Datei existiert nicht!" : "Konnte Datei nicht herunterladen!");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    lib.ShowException(TAG, context, e, true);
                                }

                            }

                        }
                    }.execute();

                } catch (Exception e) {
                    e.printStackTrace();
                    lib.ShowException(TAG, context, e, false);
                }


            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                if (blnAccessibility) {
                    ds.setUnderlineText(true);
                    ds.setColor(context.getResources().getColor(android.R.color.black));
                } else {
                    ds.setUnderlineText(false);
                }
            }
        };
        Drawable d = null;
        if (thumb != null && thumb.length() > 0 && !thumb.equalsIgnoreCase("null")) {
            try {
                Bitmap I = clsHTTPS.downloadImage(context, "thumb_" + _file, BenutzerID);
                d = new BitmapDrawable(context.getResources(), I);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (d == null) {
            if (!blnWhite) {
                d = context.getResources().getDrawable(R.drawable.ic_file_download_48px);
            } else {
                d = context.getResources().getDrawable(R.drawable.ic_file_download_white_48px);
            }
        }
        d.setBounds(0, 0, lib.dpToPx(80), lib.dpToPx(80));
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_CENTER);
        m = "THUMB\n" + m;
        ss = new SpannableString(m);
        ss.setSpan(span, 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (_message != null && _message.length() > 0) {
            _message = "\n" + _message;
            SpannableString ss2 = new SpannableString(_message);
            ss2.setSpan(new RelativeSizeSpan(0.8f), 0, _message.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss = new SpannableString(TextUtils.concat(ss, ss2));
        }
        ss.setSpan(clickableSpan, 0, m.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    public enum yesnoundefined {
        yes, no, undefined
    }

    public enum okcancelundefined {
        ok, cancel, undefined
    }


    public static class YesNoCheckResult {

        public yesnoundefined res;
        public boolean checked;

        public YesNoCheckResult(yesnoundefined res, boolean checked) {
            this.res = res;
            this.checked = checked;
        }
    }

    public static class OkCancelStringResult {

        public okcancelundefined res;
        public String input;

        public OkCancelStringResult(okcancelundefined res, String input) {
            this.res = res;
            this.input = input;
        }
    }

    public static class libString {
        public static boolean IsNullOrEmpty(String s) {
            if (s == null || s == "" || s.length() == 0) {
                return true;
            } else {
                return false;
            }
        }

        public static String MakeFitForQuery(String qry, boolean blnTrim) {
            qry = qry.replace("'", "''");
            qry = qry.replace("%", "[%]");
            if (blnTrim) qry = qry.trim();
            return qry;
        }

        public static int InStr(String s, String Search) {
            int Start = 1;
            return InStr(Start, s, Search);
        }

        public static int InStr(int Start, String s, String Search) {
            return s.indexOf(Search, Start - 1) + 1;
        }

        public static String Chr(int Code) {
            char c[] = {(char) Code};
            return new String(c);
        }

        public static String Left(String s, int length) {
            return s.substring(0, length);
        }

        public static int Len(String s) {
            return s.length();
        }

        public static String Right(String wort, int i) {

            return wort.substring(wort.length() - i);
        }

    }

    public static class MessageException extends RuntimeException {

    }

    private static class listener implements OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            removeDlg(dialog);
        }
    }

    private static class MyLauncher extends Thread {
        @Override
        /**
         * Sleep for 2 seconds as you can also change SLEEP_TIME 2 to any.
         */
        public void run() {
            try {
                // Sleeping
                Thread.sleep(SLEEP_TIME * 1000);
                latch.countDown();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            // do something you want to do
            // And your code will be executed after 2 second
        }
    }

    public static class PrefsOnMultiChoiceClickListener implements OnMultiChoiceClickListener {

        public SharedPreferences prefs;
        public String key;

        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {

            prefs.edit().putInt(key, isChecked ? -1 : 0).commit();
        }
    }

    public static double distanceInKm(double lat1, double lon1, double lat2, double lon2) {
        int radius = 6371;
        double lat = Math.toRadians(lat2 - lat1);
        double lon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(lat / 2) * Math.sin(lat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lon / 2) * Math.sin(lon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = radius * c;
        return Math.abs(d);
    }

    public static int getIndex(Spinner spinner, String myString, Context c) {
        int index = 0;
        if (myString.equalsIgnoreCase(c.getString(R.string.km1000)))
            myString = c.getString(R.string.beliebig);
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

}
