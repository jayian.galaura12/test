package de.com.limto.limto1.pedometer;

/**
 * Created by hmnatalie on 27.09.17.
 */

// Will listen to step alerts
public interface StepListener {

    public void step(long timeNs);

}