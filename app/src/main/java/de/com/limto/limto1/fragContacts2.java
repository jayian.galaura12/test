package de.com.limto.limto1;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.Constants.administrator;
import static de.com.limto.limto1.MainActivity.dontStop;

public class fragContacts2 extends Fragment
{

    public final static int fragID = 5;
    private static final String TAG = "fragContacts2";
    public static final String NAME = "name";
    public static final String VORNAME = "vorname";
    public static final String BENUTZERNAME = "benutzername";
    public static final String KONTAKTID = "kontaktid";
    public static final String KONTAKTID_2 = "kontaktid2";
    public static final String SPERREID = "sperreid";
    public static final String RUFNUMMER = "rufnummer";
    public static final String EMAIL = "email";
    public static final String ONLINE = "online";
    public static final String EINLADUNGEN_ID = "einladungenid";
    public static boolean listLoaded = false;
    public static List<ContactVO> contactVOListLimindo;
    public static ArrayList<ContactVO> ArrCopied;
    public MainActivity _main;
    RecyclerView rvContacts;
    private ProgressBar pb;
    AllContactsAdapter contactAdapter;
    public boolean getUnconfirmed;
    public boolean getInvited;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (dontStop > 0) dontStop--;
        if (dontStop == 0) LimindoService.dontStop = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        _main = (MainActivity) getActivity();
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater f)
    {
        f.inflate(R.menu.questions_menu, menu);
        //getMenuInflater().inflate(R.menu.home_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.mnu_refresh:
                try
                {
                    this.getUnconfirmed = false;
                    this.getInvited = false;
                    getContactsAsync();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.activity_all_contacts, container, false);

        rvContacts = (RecyclerView) v.findViewById(R.id.rvContacts);
        pb = (ProgressBar) v.findViewById(R.id.pb);
        getContactsAsync();
        return v;
    }

    public void getContactsAsync() {
        TaskGetContacts t = new TaskGetContacts(this, fragContacts.listLoaded, fragContacts.contactVOListAndroid, false);
        t.execute();
    }

    private static class TaskGetContacts extends AsyncTask<Void, Void, List<ContactVO>> {
        private final fragContacts2 f;
        private final boolean listLoaded;
        private final List<ContactVO> listCompare;
        private final boolean refresh;
        Throwable ex;

        TaskGetContacts(fragContacts2 f, boolean listLoaded, List<ContactVO> contactVOListAndroid, boolean refresh)
        {
            this.f = f;
            this.listLoaded = listLoaded;
            this.listCompare = contactVOListAndroid;
            this.refresh = refresh;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            f.pb.setVisibility(View.VISIBLE);
            f.rvContacts.setVisibility(View.GONE);
            if (! refresh)
            {
                fragContacts2.listLoaded = false;
                contactVOListLimindo = null;
            }

        }

        @Override
        protected List<ContactVO> doInBackground(Void... integers) {
            try {
                if (!refresh)
                {
                    return f.getcontactVOList(this.listLoaded, this.listCompare)[0];
                }
                else
                {
                    return f.refreshList(this.listLoaded,this.listCompare,contactVOListLimindo);
                }
            } catch (Throwable e) {
                ex = e;
                e.printStackTrace();
                Log.e(TAG, null, e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<ContactVO> contactVOs) {
            super.onPostExecute(contactVOs);
            try {
                f.pb.setVisibility(View.GONE);
                if (contactVOs !=null )
                {
                    f.setAdapter(contactVOs);
                    if (f.getInvited)
                    {
                        System.out.println(f.getInvited);
                        //f.contactAdapter.notifyDataSetChanged();
                    }
                    fragContacts2.contactVOListLimindo = contactVOs;
                    fragContacts2.listLoaded = true;
                    f.rvContacts.setVisibility(View.VISIBLE);
                    if (f.getUnconfirmed) {
                        f._main.getSupportActionBar().setTitle(f._main.getString(R.string.unconfirmed_contacts));
                    }
                    else if (f.getInvited) {
                        f._main.getSupportActionBar().setTitle((R.string.invited_contacts));
                    }
                    if (!refresh && !this.listLoaded && fragContacts.listLoaded && fragContacts.contactVOListAndroid != null) {
                        new TaskGetContacts(f, fragContacts.listLoaded, fragContacts.contactVOListAndroid, true).execute();
                    }
                    if (refresh)
                    {
                        fragContacts fr = (fragContacts) f._main.fPA.findFragment(fragContacts.fragID);
                        if (fr != null && fr.contactAdapter != null)
                        {
                            fr.contactAdapter.notifyDataSetChanged();
                        }
                    }
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }
    }


    private List[] getcontactVOList(boolean listLoaded, List<ContactVO> listCompare) throws Exception
    {
        List<ContactVO> contactVOList = new ArrayList<>();
        List<ContactVO> contactVOListVisible = new ArrayList<>();
        ContactVO contactVO;
        if (_main.user == null) return new List[]{contactVOList,contactVOListVisible};
        this.getUnconfirmed = _main.getUnconfirmed;
        this.getInvited = _main.getInvited;
        JSONArray contacts;
        if (getInvited) {
            contacts = _main.ArrInvited;
        } else {
            contacts = _main.clsHTTPS.getUsers(_main.user.getLong(Constants.id), 0, getUnconfirmed);
        }
        if (contacts.length() > 0)
        {
            for (int i = 0; i < contacts.length(); i++)
            {
                JSONObject user = contacts.getJSONObject(i);
                if (user.optLong(Constants.id, 1) == 1) continue;
                contactVO = new ContactVO(user);
                /*
                String BenutzerName = user.getString(BENUTZERNAME);
                String name = "";
                if (!user.isNull(NAME)) name = user.getString(NAME);
                if (!user.isNull(VORNAME)) name = user.getString(VORNAME) + " " + name;
                BenutzerName += "(" + name + ")";
                if (!user.isNull(KONTAKTID))
                {
                    if (!getUnconfirmed)contactVO.setContactImage(android.R.drawable.ic_menu_add);
                    else contactVO.setContactImage(android.R.drawable.ic_dialog_info);
                    contactVO.setContactID(user.getInt(KONTAKTID));
                    //BenutzerName = user.getInt("kontaktid") + ": " + BenutzerName;
                }
                */
                if (getUnconfirmed && !user.isNull(KONTAKTID_2))
                {
                    contactVO.setContactImage(android.R.drawable.ic_menu_add);
                    contactVO.setContactID2(user.getInt(KONTAKTID_2));
                    //BenutzerName = user.getInt("kontaktid") + ": " + BenutzerName;
                }
                /*
                if (!user.isNull(SPERREID))
                    contactVO.setSperreID(user.getInt(SPERREID));
                contactVO.setContactName(BenutzerName);
                contactVO.setUserID(user.getLong(Constants.id));
                */
                contactVO.setIsLimindoUser(true);
                /*
                String phoneNumber = user.getString(RUFNUMMER);
                phoneNumber = lib.formatPhoneNumber(phoneNumber);
                contactVO.setContactNumber(phoneNumber);

                String emailId = user.getString(EMAIL);
                contactVO.setContactEmail(emailId);
                */
                boolean hasChildren = false;
                if (!user.isNull(ONLINE)) contactVO.setOnline(user.getBoolean(ONLINE));
                if (!user.isNull(EINLADUNGEN_ID)) contactVO.setEinladungenID(user.getLong(EINLADUNGEN_ID));

                if(listLoaded && listCompare!=null)
                {
                    for (int ii = 0; ii < listCompare.size(); ii++)
                    {
                        ContactVO c = listCompare.get(ii);
                        String EMailContact = c.getContactEmail().toLowerCase() + ",";
                        String EMailLimContact = contactVO.getContactEmail().toLowerCase() + ",";
                        String phoneContact = c.getContactNumber() + ",";
                        String phoneLimContact = contactVO.getContactNumber() + ",";
                        if (phoneContact.contains(phoneLimContact))
                        {
                            System.out.println("equal");
                        }
                        if ((EMailContact.length()>3 && EMailLimContact.length()>3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length()>3 && phoneLimContact.length()>3 && phoneContact.contains(phoneLimContact)))
                        {
                            c.setIsLimindoContact(true);
                            contactVO.setIsLimindoContact(true);
                            //c.setUserID( contactVO.getUserID());
                            //c.setContactID(contactVO.getContactID());
                            //c.setContactName(c.getOriginalName() + ", " + contactVO.getContactName());
                            if (c.isCopied || contactVO.isCopied) continue;
                            ContactVO cc = c.copy();
                            c.copycount++;
                            if (c.copycount<2) fragContacts.ArrCopied.add(c);
                            cc.isCopied = true;
                            //contactVO.isCopied = true;
                            cc.setIsLimindoContact(true);
                            cc.setUserID( contactVO.getUserID());
                            cc.setContactID(contactVO.getContactID());
                            cc.setContactName(c.getOriginalName() + ", " + contactVO.getContactName());
                            listCompare.add(ii + c.copycount, cc); ii++;
                            hasChildren = true;
                        }
                    }
                }
                contactVOList.add(contactVO);
                if (!getUnconfirmed && contactVO.getContactID()>-1)
                {
                    contactVOListVisible.add(contactVO);
                }

            }
        }
        if (listLoaded && listCompare!=null)
        {
            for (ContactVO c : fragContacts.ArrCopied)
            {
                listCompare.remove(c);
            }
            fragContacts.ArrCopied.clear();
        }
        _main.UnconfirmedORInvited = getUnconfirmed || getInvited;
        _main.getUnconfirmed = false;
        _main.getInvited = false;
        return new List[]{contactVOList,contactVOListVisible};

    }
    private List<ContactVO>refreshList(boolean listLoaded, List<ContactVO> listCompare, List<ContactVO> contactVOList) throws Exception
    {
        ContactVO contactVO;
        if (_main.user == null) return listCompare;
        if (contactVOList.size() > 0)
        {
            for (int i = 0; i < contactVOList.size(); i++)
            {
                boolean hasChildren;
                contactVO = contactVOList.get(i);
                String EMailLimContact = contactVO.getContactEmail().toLowerCase() + ",";
                String phoneLimContact = contactVO.getContactNumber() + ",";
                if (EMailLimContact.length() < 5 && phoneLimContact.length() < 5) continue;
                if(listLoaded && listCompare!=null)
                {
                    for (int ii = 0; ii<listCompare.size();ii++)
                    {
                        ContactVO c = listCompare.get(ii);
                        String EMailContact = c.getContactEmail().toLowerCase() + ",";
                        String phoneContact = c.getContactNumber() + ",";
                        if (EMailContact.length() < 5 && phoneContact.length() < 5) continue;

                        if (phoneContact.contains(phoneLimContact))
                        {
                            System.out.println("equal");
                        }
                        if ((EMailContact.length()>3 && EMailLimContact.length()>3 && EMailContact.contains(EMailLimContact))
                                || (phoneContact.length()>3 && phoneLimContact.length()>3 && phoneContact.contains(phoneLimContact)))
                        {
                            c.setIsLimindoContact(true);
                            contactVO.setIsLimindoContact(true);
                            //c.setUserID( contactVO.getUserID());
                            //c.setContactID(contactVO.getContactID());
                            //c.setContactName(c.getOriginalName() + ", " + contactVO.getContactName());
                            if (c.isCopied || contactVO.isCopied) continue;
                            ContactVO cc = c.copy();
                            c.copycount++;
                            if (c.copycount<2) fragContacts.ArrCopied.add(c);
                            cc.isCopied = true;
                            //contactVO.isCopied = true;
                            cc.setIsLimindoContact(true);
                            c.setIsLimindoContact(true);
                            contactVO.setIsLimindoContact(true);
                            cc.setUserID( contactVO.getUserID());
                            cc.setContactID(contactVO.getContactID());
                            cc.setContactName(c.getOriginalName() + ", " + contactVO.getContactName());
                            listCompare.add(ii + c.copycount, cc); ii++;
                            hasChildren = true;
                        }
                    }
                }


            }
        }

        if (listLoaded && listCompare!=null)
        {
            for (ContactVO c : fragContacts.ArrCopied)
            {
                listCompare.remove(c);
            }
            fragContacts.ArrCopied.clear();
        }

        return contactVOList;

    }



    private static class TaskCheckLimdoContacts extends AsyncTask<Void,Void,Void>
    {
        private final ContactVO contactVO;

        public TaskCheckLimdoContacts(ContactVO contactVO)
        {
            this.contactVO = contactVO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if(fragContacts.contactVOListAndroid!=null)
            {
                boolean hasChildren = false;
                for (int i = 0; i <fragContacts.contactVOListAndroid.size(); i++)
                {
                    ContactVO c = fragContacts.contactVOListAndroid.get(i);
                    String EMailContact = c.getContactEmail().toLowerCase() + ",";
                    String EMailLimContact = contactVO.getContactEmail().toLowerCase() + ",";
                    String phoneContact = c.getContactNumber() + ",";
                    String phoneLimContact = contactVO.getContactNumber() + ",";
                    if (phoneContact.contains(phoneLimContact))
                    {
                        System.out.println("equal");
                    }

                    if ((EMailContact.length()>3 && EMailLimContact.length()>3 && EMailContact.contains(EMailLimContact))
                            || (phoneContact.length()>3 && phoneLimContact.length()>3 && phoneContact.contains(phoneLimContact)))
                    {
                        c.setIsLimindoContact(true);
                        contactVO.setIsLimindoContact(true);
                        //c.setUserID( contactVO.getUserID());
                        //c.setContactID(contactVO.getContactID());
                        //c.setContactName(c.getOriginalName() + ", " + contactVO.getContactName());
                        if (c.isCopied||contactVO.isCopied) continue;
                        ContactVO cc = c.copy();
                        c.copycount++;
                        if (c.copycount<2) fragContacts.ArrCopied.add(c);
                        cc.isCopied = true;
                        //contactVO.isCopied = true;
                        cc.setIsLimindoContact(true);
                        cc.setUserID( contactVO.getUserID());
                        cc.setContactID(contactVO.getContactID());
                        cc.setContactName(c.getOriginalName() + ", " + contactVO.getContactName());
                        fragContacts.contactVOListAndroid.add(i + c.copycount, cc); i++;
                        hasChildren = true;
                    }
                }
                if (listLoaded)
                {
                    for (ContactVO c : fragContacts.ArrCopied)
                    {
                        fragContacts.contactVOListAndroid.remove(c);
                    }
                    fragContacts.ArrCopied.clear();
                }
            }
            return null;
        }
    }


    private void setAdapter(List<ContactVO> contactVOList) throws Throwable
    {
        List<ContactVO> contactVOListAdded;
        if (getInvited || (_main.isAdmin())) {
            System.out.println (getInvited);
            contactVOListAdded = contactVOList;
        }
        else {
            contactVOListAdded = new ArrayList<>();
            for (ContactVO v : contactVOList) {
                if (v.getContactID() > -1 || v.getSperreID() >= 0) {
                    contactVOListAdded.add(v);
                }
            }
        }
        contactAdapter = new AllContactsAdapter(contactVOListAdded, getActivity().getApplicationContext(),this);
            rvContacts.setLayoutManager(new LinearLayoutManager(this.getActivity()));
            rvContacts.setAdapter(contactAdapter);
    }

    public void refresh() {
        getContactsAsync();
    }
}
