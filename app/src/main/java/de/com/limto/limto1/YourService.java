package de.com.limto.limto1;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.JobIntentService;

import de.com.limto.limto1.logger.Log;

/**
 * JobService to be scheduled by the JobScheduler.
 * start another service
 */
@RequiresApi(api = Build.VERSION_CODES.M)
/*public class LimindoJobService extends JobService {
    private static final String TAG = "SyncService";

    @Override
    public boolean onStartJob(JobParameters params) {
        Intent service = new Intent(getApplicationContext(), LimindoService.class);
        service.putExtra(BOOT, true);
        YourService.enqueueWork(getApplicationContext(),service);
        lib.scheduleJob(getApplicationContext()); // reschedule the job
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

}
*/
public class YourService extends JobIntentService {

    public static final int JOB_ID = 1;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, YourService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        try
        {
            ComponentName res = getApplicationContext().startService(intent);
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
            Log.e("JobIntentService", "Start Service",ex);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getApplicationContext().startForegroundService(intent);
            }
        }

    }

}
