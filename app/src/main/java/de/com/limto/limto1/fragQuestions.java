package de.com.limto.limto1;

import androidx.appcompat.app.AlertDialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;

import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.TooltipCompat;

import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import de.com.limto.limto1.Controls.Answer;
import de.com.limto.limto1.Controls.NoSelectionAutoComplete;
import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.Controls.QuestionsAdapter;
import de.com.limto.limto1.Controls.ZoomExpandableListview;
import de.com.limto.limto1.dummy.DummyContent.DummyItem;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static android.content.Context.MODE_PRIVATE;
import static de.com.limto.limto1.clsHTTPS.fmtDateTime;
import static de.com.limto.limto1.lib.lib.debugMode;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class fragQuestions extends Fragment {

    // Constants
    public final static int fragID = 1;
    private static final String TAG = "fragQuestions";
    public static final String SENDTYPE = "sendtype";
    public static final String FRAGEID = "frageid";
    public static final String BENUTZERID = "benutzerid";
    public static final String BENUTZERIDFRAGE = "benutzeridfrage";
    public static final String FRAGE = "frage";
    public static final String ORIGINALID = "originalid";
    public static final String MAX_QUESTIONS = "maxQuestions";
    public static final String VORNAME = "vorname";
    public static final String NAME = "name";
    public static final String GRADE = "Grade";
    public static final String ENTF = "Entf";
    public static final String SEARCH = "search";
    public static final String BENUTZER_ID = "BenutzerID";
    public static final String FRAG_QUESTIONS_GET_FRAGEN_TALK = "fragQuestions.getFragenTalk";
    public static final String SERVICE_NOT_BOUND_OR_NOT_INITIALIZED = "Service not bound or not initialized!";
    public static final String RADIUS_KM = "RadiusKM";
    public static final String KONTAKTGRAD = "Kontaktgrad";
    public static final String ARCHIVIERT = "archiviert";
    public static final String GELOESCHT = "geloescht";
    public static final String AUS = "aus";
    public static final String LIMIT = "limit";
    public static final String TIME_QUERY = "TimeQuery ";
    public static final String ERROR = "Error:";
    public static final String FRAG_QUESTIONS_GET_QUESTIONS = "fragQuestions.getQuestions";
    public static final String GRAD = "grad";
    public static final String ONLINE = "online";
    public static final String TIME_ADD_QUESTIONS = "TimeAddQuestions ";
    public static final String ID = "id";
    public static final String TEST = "test";
    public static final String READ_MESSAGE = "readMessage = ";
    public static final String SETTINGS = "Settings";
    public static final String ANZARCH = "anzarch";
    public static final String DATUMLOGOFF = "datumlogoff";
    public static final String TAETIGKEITSSCHWERPUNKTE = "taetigkeitsschwerpunkte";
    public static final String EIGENE = "Eigene";
    public static final String KONTAKTE = "Kontakte";
    public static final String ALLE = "Alle";
    public static final String CHARSEARCHWASCLICKEDONEMPTYTXT = "\u2063";

    //public properties
    public boolean initialized;
    public MainActivity _main;
    public ZoomExpandableListview lv;
    public int lastGroupPosition = -1;
    public ArrayList<Question> questions;

    //Private Properties
    private int Entf;
    private int lastStartGrad;
    private int lastEndGrad;
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 2;
    private OnListFragmentInteractionListener mListener;

    //Views
    private ProgressBar pb;
    private RadioGroup llButtons;
    private LinearLayout llButtonsLV;
    //Buttons
    private Button btnFirst;
    private Button btnPrevious;
    public QuestionsAdapter adapter;
    public RadioButton btnEigene;
    public RadioButton btnKontakte;
    public RadioButton btnAlle;
    public RadioButton btnGeloescht;
    public RadioButton btnArchiviert;
    public RadioButton btnAus;
    private Button btnNext;
    private ImageButton btnSearch;
    public NoSelectionAutoComplete txtSearch;
    public ImageButton btnContext;
    private CheckBox chkEmergencyBlind;
    private TextView tvQuestionsTitle;

    private boolean dontsearchAutoComplete = false;
    int lastChildPosition = -1;


    //private ArrayList<Integer> ids;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public fragQuestions() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static fragQuestions newInstance(int columnCount) {
        fragQuestions fragment = new fragQuestions();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _main = (MainActivity) getActivity();
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onViewCreated(android.view.View view, android.os.Bundle savedInstanceState) {
        //lv = new ZoomExpandableListview(this.getContext()); //FindViewById<ExpandableListView> (Resource.Id.lvItems);
        //view.addView(lv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        lv = (ZoomExpandableListview) view.findViewById(R.id.lvQuestions);
        //DisplayMetrics metrics = new DisplayMetrics();
        //_main.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        //int width = metrics.widthPixels;

        initLV();

        pb = (ProgressBar) view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);

        final int Grad = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);

        initButtons(view);

        initSearch(view, Grad);

        initButtons2(view, Grad);

        initSelf();


        this.initialized = true;

    }

    private void initSelf() {
        try {
            // Es ist ein Benutzer angemeldet und die Form ist noch nicht initialisiert und der adapter hat bereits Einträge
            if (_main.user != null && (!initialized && !(adapter != null && adapter.rows.size() > 0))) {
                initFragQuestions(true, -1, -1, null, null, null, null, false);
            } else if (adapter != null) { //es ist kein Benutzer angemeldet oder die Form ist bereits initialisiert
                // neuen Adapter erstellen und Form neu initialisieren
                lv.setAdapter((ExpandableListAdapter) null);
                adapter = new QuestionsAdapter(this, adapter.rows);
                lv.setAdapter(adapter);
                initFragQuestions(false, -1, -1, null, null, null, null, false);
                initialized = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initButtons2(View view, final int Grad) {
        llButtons = (RadioGroup) view.findViewById(R.id.llButtons);
        this.btnEigene = (RadioButton) view.findViewById(R.id.btnEigene);
        btnKontakte = (RadioButton) view.findViewById(R.id.btnKontakte);
        btnAlle = (RadioButton) view.findViewById(R.id.btnAlle);
        btnAlle.setVisibility(View.VISIBLE);
        btnAus = (RadioButton) view.findViewById(R.id.btnAus);
        this.btnGeloescht = (RadioButton) view.findViewById(R.id.btnGeloescht);
        this.btnArchiviert = (RadioButton) view.findViewById(R.id.btnArchiviert);
        this.tvQuestionsTitle = (TextView) view.findViewById(R.id.tvQuestionsTitle);
        llButtons.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton btn = (RadioButton) llButtons.findViewById(checkedId);
                if (btn.isChecked()) tvQuestionsTitle.setText(btn.getContentDescription());
            }
        });
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            btnEigene.setButtonDrawable(android.R.color.transparent);
            btnKontakte.setButtonDrawable(android.R.color.transparent);
            btnAus.setButtonDrawable(android.R.color.transparent);
            btnArchiviert.setButtonDrawable(android.R.color.transparent);
            btnAlle.setButtonDrawable(android.R.color.transparent);
            btnGeloescht.setButtonDrawable(android.R.color.transparent);
        }
        btnEigene.performClick();
        TooltipCompat.setTooltipText(btnEigene, btnEigene.getContentDescription());
        TooltipCompat.setTooltipText(btnKontakte, btnKontakte.getContentDescription());
        TooltipCompat.setTooltipText(btnAlle, btnAlle.getContentDescription());
        TooltipCompat.setTooltipText(btnAus, btnAus.getContentDescription());
        TooltipCompat.setTooltipText(btnGeloescht, btnGeloescht.getContentDescription());
        TooltipCompat.setTooltipText(btnArchiviert, btnArchiviert.getContentDescription());
        btnEigene.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtSearch.setText("");
                    if (llButtons != null) {
                        for (int i = 0; i < llButtons.getChildCount(); i++) {
                            final RadioButton r = (RadioButton) llButtons.getChildAt(i);
                            if (r != btnEigene) r.setChecked(false);
                        }
                    }
                    initFragQuestions(true, 0, 0, false, false, false, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }

        });
        btnGeloescht.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtSearch.setText("");
                    if (llButtons != null) {
                        for (int i = 0; i < llButtons.getChildCount(); i++) {
                            final RadioButton r = (RadioButton) llButtons.getChildAt(i);
                            if (r != btnGeloescht) r.setChecked(false);
                        }
                    }
                    initFragQuestions(true, 0, 0, true, false, false, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }

        });

        btnArchiviert.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtSearch.setText("");
                    if (llButtons != null) {
                        for (int i = 0; i < llButtons.getChildCount(); i++) {
                            final RadioButton r = (RadioButton) llButtons.getChildAt(i);
                            if (r != btnArchiviert) r.setChecked(false);
                        }
                    }
                    initFragQuestions(true, 0, 0, false, true, false, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }

        });

        btnKontakte.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtSearch.setText("");
                    if (llButtons != null) {
                        for (int i = 0; i < llButtons.getChildCount(); i++) {
                            final RadioButton r = (RadioButton) llButtons.getChildAt(i);
                            if (r != btnKontakte) r.setChecked(false);
                        }
                    }
                    initFragQuestions(true, 1, Grad, false, false, false, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }
        });
        btnAlle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtSearch.setText("");
                    if (llButtons != null) {
                        for (int i = 0; i < llButtons.getChildCount(); i++) {
                            final RadioButton r = (RadioButton) llButtons.getChildAt(i);
                            if (r != btnAlle) r.setChecked(false);
                        }
                    }
                    initFragQuestions(true, 0, Grad, false, false, false, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }
        });

        btnAus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    txtSearch.setText("");
                    if (llButtons != null) {
                        for (int i = 0; i < llButtons.getChildCount(); i++) {
                            final RadioButton r = (RadioButton) llButtons.getChildAt(i);
                            if (r != btnAus) r.setChecked(false);
                        }
                    }
                    initFragQuestions(true, 0, Grad, false, false, true, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }
        });


    }

    private void initSearch(View view, final int Grad) {
        btnSearch = (ImageButton) view.findViewById(R.id.btnSearch);
        txtSearch = (NoSelectionAutoComplete) view.findViewById(R.id.txtSearch);
        Entf = getActivity().getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);

        txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    boolean blnMore;
                    if (_main.lastLimit < 0) blnMore = false;
                    else blnMore = true;
                    _main.lastLimit = 0;
                    try {
                        if (txtSearch.getText().toString().length() == 0) {
                            txtSearch.setText(CHARSEARCHWASCLICKEDONEMPTYTXT);
                        }
                        txtSearch.dismissDropDown();
                        initFragQuestions(true, -1, -1, null, null, null, 0l, blnMore);
                    } catch (Exception e) {
                        Log.e(TAG, null, e);
                    }
                    return true;
                }
                return false;
            }
        });

        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (_main == null || _main.user == null || _main.clsHTTPS == null || dontsearchAutoComplete) {
                    dontsearchAutoComplete = false;
                    return;
                }
                String s = txtSearch.getText().toString();
                if (s != null && s.length() >= 3) {
                    s = StringEscapeUtils.escapeJava(s);
                    s = s.replace("\\", "\\\\");
                    s = s.replace("'", "\\'");
                    HashMap<String, String> p = new java.util.HashMap<String, String>();
                    Long BenutzerID = null;
                    try {
                        BenutzerID = _main.user.getLong(Constants.id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    p.put(BENUTZER_ID, "" + BenutzerID);
                    p.put(SEARCH, "" + s);
                    if (_main.mService == null || _main.mService.mHandler != _main.mHandler) {
                        Log.e(FRAG_QUESTIONS_GET_FRAGEN_TALK, SERVICE_NOT_BOUND_OR_NOT_INITIALIZED);
                        _main.bindService0();
                    }
                    _main.clsHTTPS.getFragenTalk(p, _main);
                }

            }
        });


        btnSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean blnMore;
                dontsearchAutoComplete = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dontsearchAutoComplete = false;
                    }
                }, 500);
                if (_main == null) return;
                if (_main.lastLimit < 0) blnMore = false;
                else blnMore = true;
                _main.lastLimit = 0;
                try {
                    if (txtSearch.getText().toString().length() == 0) {
                        txtSearch.setText(CHARSEARCHWASCLICKEDONEMPTYTXT);
                    }
                    txtSearch.dismissDropDown();
                    initFragQuestions(true, -1, -1, null, null, null, 0l, blnMore);
                } catch (Exception e) {
                    Log.e(TAG, null, e);
                }
            }
        });

    }

    private void initButtons(View view) {
        llButtonsLV = (LinearLayout) view.findViewById(R.id.llButtonsLV);
        llButtonsLV.setVisibility(View.GONE);
        btnFirst = (Button) view.findViewById(R.id.btnFirst);
        btnPrevious = (Button) view.findViewById(R.id.btnPrevious);
        btnNext = (Button) view.findViewById(R.id.btnNext);
        btnContext = (ImageButton) view.findViewById(R.id.btnContextMenu);
        registerForContextMenu(btnContext);
        btnContext.setLongClickable(false);
        btnContext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                _main.openContextMenu(v);
            }
        });

        btnFirst.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean blnMore;
                    if (_main.lastLimit < 0) blnMore = false;
                    else blnMore = true;
                    _main.lastLimit = 0;
                    initFragQuestions(true, -1, -1, null, null, null, 0l, blnMore);
                } catch (Exception e) {
                    Log.e(TAG, null, e);
                    e.printStackTrace();
                }
            }
        });
        btnPrevious.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean blnMore;
                    if (_main.lastLimit < 0) blnMore = false;
                    else blnMore = true;
                    initFragQuestions(true, -1, -1, null, null, null, (_main.lastLimit >= 50 ? _main.lastLimit - 50 : (_main.lastLimit == -1 ? -1 : 0l)), blnMore);
                } catch (Exception e) {
                    Log.e(TAG, null, e);
                    e.printStackTrace();
                }
            }
        });
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    boolean blnMore;
                    if (_main.lastLimit < 0) blnMore = false;
                    else blnMore = true;
                    initFragQuestions(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                } catch (Exception e) {
                    Log.e(TAG, null, e);
                    e.printStackTrace();
                }
            }
        });

    }

    private void initLV() {
        lv.setIndicatorBounds(lib.dpToPx(5), lib.dpToPx(30));

        lv.setChoiceMode(ExpandableListView.CHOICE_MODE_MULTIPLE);
        lv.setClickable(true);
        lv.setLongClickable(true);
        lv.setFocusable(true);
        lv.setFocusableInTouchMode(true);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int i) {
                if (i < adapter.rows.size()) {
                    Question q = adapter.rows.get(i);
                    q.expanded = true;
                    fragQuestions.this.lastGroupPosition = i;
                    _main.currentgroupid = i;
                    _main.currentQuestion = q;
                    if (_main.fPA != null) {
                        fragAnswer fragAnswer = (fragAnswer) _main.fPA.findFragment(de.com.limto.limto1.fragAnswer.fragID);
                        if (fragAnswer != null) fragAnswer.init();
                    }
                } else {
                    try {
                        boolean blnMore;
                        if (_main.lastLimit < 0) blnMore = false;
                        else blnMore = true;
                        initFragQuestions(true, -1, -1, null, null, null, (_main.lastLimit >= 0 ? _main.lastLimit + 50 : (_main.lastLimit == -1 ? -1 : 0)), blnMore);
                    } catch (Exception e) {
                        Log.e(TAG, null, e);
                        e.printStackTrace();
                    }

                }
            }


        });

        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Question q = adapter.rows.get(i);
                if (q.isVOTE()) return false;
                _main.currentQuestion = q;
                Answer a = q.items.get(i1);
                _main.currentAnswer = a;

                boolean hasRe = false;
                int childPosition = i1;
                if (childPosition >= 0) {
                    if (childPosition < q.items.size() - 1) {
                        if (q.items.get(childPosition + 1).ParentFrageAntwortenID == a.ID
                                || q.items.get(childPosition + 1).reAntwortID == a.ID) {
                            hasRe = true;
                        }
                    }
                    lastChildPosition = i1;
                    _main.currentchildid = i1;
                }
                _main.hasRe = hasRe;

                if (_main.fPA != null) {
                    fragAnswer fragAnswer = (fragAnswer) _main.fPA.findFragment(de.com.limto.limto1.fragAnswer.fragID);
                    if (fragAnswer != null) fragAnswer.init();
                }
                return false;
            }


        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    //return (processChildViewLongClick(view,id));

                }

                return false;
            }
        });
/*
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick( AdapterView<?> parent, View view, int position, long id) {

                long packedPosition = lv.getExpandableListPosition(position);

                int itemType = ExpandableListView.getPackedPositionType(packedPosition);
                int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
                int childPosition = ExpandableListView.getPackedPositionChild(packedPosition);


        //  if group item clicked
                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    //  ...
                    onGroupLongClick(groupPosition);
                }

        //  if child item clicked /
                else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    //  ...
                    onChildLongClick(groupPosition, childPosition);
                }


                return false;
            }
        });
        */
        lv.setBackgroundResource(R.color.colorLV);

    }

    private boolean processChildViewLongClick(View view, long id) {
        int groupPosition = ExpandableListView.getPackedPositionGroup(id);
        int childPosition = ExpandableListView.getPackedPositionChild(id);
        if (groupPosition < adapter.rows.size()) {
            Question q = adapter.rows.get(groupPosition);
            _main.currentQuestion = q;
            Answer a = q.items.get(childPosition);
            a.selected ^= true;
            if (a.getView().equals(view)) {
                if (a.selected) {
                    a.getView().setBackgroundResource(R.color.colorQuestionSelected);
                } else {
                    a.getView().setBackgroundResource(R.color.colorBGFrage);
                }
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragquestions, container, false);
        return view;
    }


    ArrayList<Question> getQuestions(boolean reload, boolean blnScroll, int startGrad, int endGrad, int Entf, boolean archiviert, boolean geloescht, boolean ausgeblendet, long limit, boolean more, String search, Date DatumNeu) throws Exception {
        int i = 0;
        while (i < 10 && _main.user == null) {
            i++;
            Thread.sleep(500);
        }
        if (i >= 10) throw new Exception("_main.user == null");
        return _getQuestions(reload, blnScroll, startGrad, endGrad, Entf, archiviert, geloescht, ausgeblendet, limit, more, search, DatumNeu);
    }

    private ArrayList<Question> _getQuestions(boolean reload, boolean blnScroll, int startGrad, int endGrad, int Entf, boolean archiviert, boolean geloescht, boolean ausgeblendet, long limit, boolean more, String search, Date DatumNeu) throws Exception {
        if (_main.user == null) return null;
        try {
            if (questions == null || reload) {
                String res = null;
                Calendar cal = Calendar.getInstance();
                Long TimeStart = cal.getTimeInMillis();
                questions = new ArrayList<Question>();
                //if (ids == null || !more) ids = new ArrayList<Integer>();
                clsHTTPS https = _main.clsHTTPS;
                HashMap<String, String> p = generateParametersGetQuestions(startGrad, endGrad, Entf, archiviert, geloescht, ausgeblendet, limit, more, search, DatumNeu);
                JSONArray json = null;
                int retries = 0;
                Long TimeTotal;
                while (json == null && retries <= 10) {
                    res = https.getQuestions(p, false);//https.connect(p, "getquestionsp");
                    TimeTotal = Calendar.getInstance().getTimeInMillis() - TimeStart;
                    System.out.println(TIME_QUERY + TimeTotal);
                    TimeStart = Calendar.getInstance().getTimeInMillis();
                    if (res.startsWith(ERROR)) throw new Exception(res);
                    try {
                        json = new JSONArray(res);
                    } catch (Throwable ex) {
                        Log.e(FRAG_QUESTIONS_GET_QUESTIONS, res, ex);
                        if (retries >= 10) throw ex;
                        Thread.sleep(100);
                    }
                    retries++;
                }
                int i = 0;
                if ((!blnScroll || limit <= 10)) {
                    if (_main.isAdmin())
                        questions.add(new Question(_main, -1l, -1l, getString(R.string.gemAW), -1l, "", "", -1, null, null, -1, -1d, -1d, false, true, null, false, _main.onlineState));
                    Date dtLastSystemMessage = LimindoService.DateTimeParser.parse
                            (_main.getPreferences(MODE_PRIVATE).getString
                                    ("dtLastSystemMessage", LimindoService.DateTimeParser.format(Calendar.getInstance().getTime())));
                    questions.add(new Question(_main, 1l, 1l, getString(R.string.SystemMessages), 1l, getString(R.string.System), "", 0, dtLastSystemMessage, Calendar.getInstance().getTime(), -1, 7.5889959, 50.3569429, false, true, null, false, MainActivity.OnlineState.freeforchat));
                }
                parseJSON(questions, json, startGrad, ausgeblendet);
                TimeTotal = Calendar.getInstance().getTimeInMillis() - TimeStart;
                System.out.println(TIME_ADD_QUESTIONS + (TimeTotal));
            }
        } catch (Throwable ex) {
            Log.e(TAG, null, ex);
            ex.printStackTrace();
            throw ex;
        }
        return questions;
    }

    private void parseJSON(ArrayList<Question> questions, JSONArray json, int startGrad, boolean ausgeblendet) throws JSONException, ParseException {
        int Grad;
        for (int ii = 0; ii < json.length(); ii++) {
            JSONObject o = json.getJSONObject(ii);
            Grad = o.getInt(GRAD);
            if (Grad == 0) {
                if (_main.onlineState.state > 0) o.put(ONLINE, true);
            }
            long ID = o.getLong(Constants.id);
            //if (!ids.contains(ID))
            {
                if (Grad >= startGrad || Grad == -1) {
                    Question q = new Question(_main, o);
                    q.Grad = Grad;
                    //if (q.Kontaktgrad < i) throw new Exception("falscher Kontakgrad!");
                    Double e = null;
                    if (_main.locCoarse() != null && q.Laengengrad != null && q.Breitengrad != null) {
                        e = lib.distanceInKm(q.Breitengrad, q.Laengengrad, _main.locCoarse().getLatitude(), _main.locCoarse().getLongitude());
                    }

                    if (q.Grad <= q.Kontaktgrad) {
                        if (Grad <= 0 || (Entf == 1000 && q.RadiusKM == 1000) || (e == null && Entf == 1000) || (e != null && e < Entf && e < q.RadiusKM))
                            q.aus = ausgeblendet;
                        questions.add(q);
                    }
                }
                //ids.add(ID);
            }
        }

    }

    private HashMap<String, String> generateParametersGetQuestions(int startGrad, int endGrad, int entf, boolean archiviert, boolean geloescht, boolean ausgeblendet, long limit, boolean more, String search, Date DatumNeu) throws JSONException {
        HashMap<String, String> p = new java.util.HashMap<String, String>();


        p.put(BENUTZER_ID, "" + _main.user.getLong(Constants.id));
        p.put(RADIUS_KM, "" + Entf);
        if (_main.locCoarse() != null) {
            p.put("Laengengrad", String.valueOf(_main.locCoarse().getLongitude()));
            p.put("Breitengrad", String.valueOf(_main.locCoarse().getLatitude()));
        }
        p.put(KONTAKTGRAD, "" + endGrad);
        p.put(ARCHIVIERT, "" + archiviert);
        p.put(GELOESCHT, "" + geloescht);
        p.put(AUS, "" + ausgeblendet);
        p.put(LIMIT, "" + limit);
        if (search != null && search.length() > 0) {
            search = StringEscapeUtils.escapeJava(search);
            search = search.replace("\\", "\\\\");
            search = search.replace("'", "\\'");
            p.put(SEARCH, search);
        }

        if (DatumNeu != null) p.put("DatumNeu", fmtDateTime.format(DatumNeu));
        p.put("lang", _main.Lang);
        //for (int i = 0; i <= endGrad; i++) {
        //p.remove("Kontaktgrad");
        //p.put("Kontaktgrad", "" + endGrad);

        return p;

    }

    private void onChildLongClick(int groupPosition, int childPosition) {

    }

    private void onGroupLongClick(int groupPosition) {
        //Question q = a.rows.get(groupPosition);
        // _main.currentQuestion = q;

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
        {
            int itemType;
            int groupPosition;
            int childPosition;

            if (info != null) {
                long packedPosition = info.packedPosition;
                itemType = ExpandableListView.getPackedPositionType(packedPosition);
                groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
                childPosition = ExpandableListView.getPackedPositionChild(packedPosition);
                this.lastGroupPosition = groupPosition;
                this.lastChildPosition = childPosition;
                _main.currentgroupid = groupPosition;
                _main.currentchildid = childPosition;
            } else {
                groupPosition = _main.currentgroupid;
                childPosition = _main.currentchildid;
            }
            if (groupPosition > -1) {
                MenuInflater inflater = _main.getMenuInflater();
                inflater.inflate(R.menu.context_menu, menu);

                menu.findItem(R.id.cmnuSperren).setVisible(false);
                menu.findItem(R.id.cmnuNotification).setVisible(false);
                if (groupPosition >= 0 && _main != null && _main.user != null && !_main.user.isNull(ID) && this.adapter != null && this.adapter.rows != null & groupPosition < this.adapter.rows.size()) {
                    Question q = this.adapter.rows.get(groupPosition);
                    Answer a = null;
                    if (childPosition >= 0) a = q.items.get(childPosition);
                    try {
                        if (_main.isAdmin()) {
                            menu.findItem(R.id.cmnuSperren).setVisible(true);
                            menu.findItem(R.id.cmnuNotification).setVisible(true);
                        } else {
                            menu.findItem(R.id.cmnuSperren).setVisible(false);
                            menu.findItem(R.id.cmnuNotification).setVisible(false);
                        }

                        if (_main.getBenutzerID().equals(a!=null?a.BenutzerID:q.BenutzerID))
                        {
                            menu.findItem(R.id.cmnuBlock).setVisible(false);
                        }
                        else
                        {
                            menu.findItem(R.id.cmnuBlock).setVisible(true);
                            menu.findItem(R.id.cmnuChat).setVisible(true);
                        }

                        if (a != null && a.gemeldet || a == null && q.gemeldet) {
                            menu.findItem(R.id.cmnuMelden).setTitle(getString(R.string.MeldungAufheben));
                            if (!_main.user.getBoolean(Constants.administrator))
                                menu.findItem(R.id.cmnuMelden).setVisible(false);


                        }
                        if (q.BenutzerID != _main.user.getLong(Constants.id)) {
                            if (!_main.user.getBoolean(Constants.administrator)) {
                                menu.findItem(R.id.cmnuDelete).setVisible(false);
                                menu.findItem(R.id.cmnuArchive).setVisible(false);
                            }
                            menu.findItem(R.id.cmnuEdit).setVisible(false);
                            menu.findItem(R.id.cmnuHide).setVisible(true);
                            if (q.aus)
                                menu.findItem(R.id.cmnuHide).setTitle(getString(R.string.Einblenden));
                        } else {
                            menu.findItem(R.id.cmnuHide).setVisible(false);
                            if (a == null) menu.findItem(R.id.cmnuChat).setVisible(false);
                        }
                        if (a != null) {
                            //menu.findItem(R.id.cmnuAnswer).setTitle(R.string.mnuAnswerPrivate);
                            if (a.BenutzerID == _main.user.getLong(Constants.id)) menu.findItem(R.id.cmnuChat).setVisible(false);
                            if (a.BenutzerID == _main.user.getLong(Constants.id) || _main.user.getBoolean(Constants.administrator)) {
                                menu.findItem(R.id.cmnuDelete).setVisible(true);
                                if (!(a.BenutzerID == _main.user.getLong(Constants.id)) && _main.user.getBoolean(Constants.administrator)) {
                                    menu.findItem(R.id.cmnuEdit).setVisible(false);
                                } else {
                                    menu.findItem(R.id.cmnuEdit).setVisible(true);
                                }
                                menu.findItem(R.id.cmnuAnswer).setVisible(true);
                                menu.findItem(R.id.cmnuArchive).setVisible(false);
                                menu.findItem(R.id.cmnuHide).setVisible(false);
                            } else {
                                menu.findItem(R.id.cmnuDelete).setVisible(false);
                                menu.findItem(R.id.cmnuEdit).setVisible(false);
                                menu.findItem(R.id.cmnuAnswer).setVisible(true);
                                menu.findItem(R.id.cmnuArchive).setVisible(false);
                                menu.findItem(R.id.cmnuHide).setVisible(false);
                            }
                            try {
                                if (a.ParentFrageAntwortenID == null && q.BenutzerID == _main.user.getLong(Constants.id) && a.BenutzerID != _main.user.getLong(Constants.id) && _main.clsHTTPS.getAnswerValue(_main.user.getLong(Constants.id), a.ID) == 0) {
                                    menu.findItem(R.id.cmnuValuate).setVisible(true);
                                } else {
                                    menu.findItem(R.id.cmnuValuate).setVisible(false);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            menu.findItem(R.id.cmnuValuate).setVisible(false);
                        }
                        if (btnArchiviert.isChecked() || btnGeloescht.isChecked()) {
                            if (btnGeloescht.isChecked())
                                menu.findItem(R.id.cmnuDelete).setVisible(false);
                            menu.findItem(R.id.cmnuEdit).setVisible(true);
                            menu.findItem(R.id.cmnuAnswer).setVisible(false);
                            menu.findItem(R.id.cmnuArchive).setVisible(false);
                        } else if (a == null) {
                            // erst archvieren, dann löschen;
                            menu.findItem(R.id.cmnuDelete).setVisible(false);
                        }
                        if (_main.user.getBoolean(Constants.administrator))
                            menu.findItem(R.id.cmnuDelete).setVisible(true);
                        if (_main.user.getBoolean(Constants.administrator) || debugMode()) {
                            MenuItem item = menu.findItem(R.id.cmnuNotification);
                            item.setVisible(true);
                            boolean res = getContext().getSharedPreferences(TEST, MODE_PRIVATE).getBoolean(TEST, false);
                            item.setCheckable(true);
                            item.setChecked(res);
                            //if (res) item.setTitle(R.string.mnuCreateRandomMessageON); else item.setTitle(R.string.mnuCreateRandomMessageOff);
                        } else {
                            menu.findItem(R.id.cmnuNotification).setVisible(false);
                        }
                        if (a != null) {
                            if (a.Grad > 1) {
                                menu.findItem(R.id.cmnuAdden).setVisible(true);
                            } else {
                                menu.findItem(R.id.cmnuAdden).setVisible(false);
                            }
                        } else if (q != null) {
                            if (q.Grad > 1) {
                                menu.findItem(R.id.cmnuAdden).setVisible(true);
                            } else {
                                menu.findItem(R.id.cmnuAdden).setVisible(false);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, null, e);
                        lib.ShowException(TAG, getContext(), e, false);
                        getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        try {
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();

            int itemType;
            int groupPosition;
            int childPosition;

            if (info != null) {
                long packedPosition = info.packedPosition;
                itemType = ExpandableListView.getPackedPositionType(packedPosition);
                groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
                childPosition = ExpandableListView.getPackedPositionChild(packedPosition);
                this.lastChildPosition = childPosition;
                this.lastGroupPosition = groupPosition;
                _main.currentchildid = childPosition;
                _main.currentgroupid = groupPosition;
            } else {
                groupPosition = _main.currentgroupid;
                childPosition = _main.currentchildid;
            }


            switch (item.getItemId()) {
                case R.id.cmnuAnswer:
                    if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        if (q.FrageID == 1 && !_main.isAdmin() && !debugMode()) return true;
                        _main.currentQuestion = q;
                        _main.loadcurrentquestion = false;
                        _main.currentgroupid = groupPosition;
                        _main.currentchildid = childPosition;
                        _main.currentAnswer = null;
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        _main.currentAnswerAnswer = a;

                        _main.mPager.setCurrentItem(fragAnswer.fragID);
                        if (_main.fPA != null) {
                            fragAnswer fA = (fragAnswer) _main.fPA.findFragment(fragAnswer.fragID);
                            if (fA != null) {
                                fA.init();
                            }
                        }
                    }
                    return true;
                case R.id.cmnuEdit:
                    if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        if (q.FrageID == 1 && _main.isAdmin()) return true;
                        Answer a = null;
                        boolean hasRe = false;
                        if (childPosition >= 0) {
                            a = q.items.get(childPosition);
                            if (childPosition < q.items.size() - 1) {
                                if (q.items.get(childPosition + 1).ParentFrageAntwortenID != null && q.items.get(childPosition + 1).ParentFrageAntwortenID.longValue() == a.ID.longValue()
                                        || q.items.get(childPosition + 1).reAntwortID == a.ID.longValue()) {
                                    hasRe = true;
                                }
                            }

                        }
                        _main.hasRe = hasRe;

                        _main.currentQuestion = q;
                        _main.currentAnswer = null;
                        _main.currentAnswerAnswer = null;
                        if (a != null && (a.BenutzerID == _main.user.getLong(Constants.id) || _main.user.getBoolean(Constants.administrator))) {
                            _main.currentAnswer = a;
                            _main.mPager.setCurrentItem(fragAnswer.fragID);
                            if (_main.fPA != null) {
                                fragAnswer fragAnswer = (fragAnswer) _main.fPA.findFragment(de.com.limto.limto1.fragAnswer.fragID);
                                if (fragAnswer != null) fragAnswer.init();
                            }
                        } else {
                            _main.mPager.setCurrentItem(fragQuestion.fragID);
                            if (_main.fPA != null) {
                                fragQuestion fragQuestion = (de.com.limto.limto1.fragQuestion) _main.fPA.findFragment(de.com.limto.limto1.fragQuestion.fragID);
                                if (fragQuestion != null) fragQuestion.init(q);
                            }
                        }
                    }
                    return true;
                case R.id.cmnuDelete:
                    final ArrayList<Question> sel = new ArrayList<>();
                    for (Question q : adapter.rows) {
                        if (q.selected) sel.add(q);
                    }
                    if (sel.size() > 0) {
                        AlertDialog dlg = lib.ShowMessageYesNo(getContext(), getString(R.string.doyou2) + " " + sel.size() + " " + getString(R.string.deletequestions),
                                getString(R.string.delete), false, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        for (Question q : sel) {
                                            q.selected = false;
                                            boolean res = false;
                                            try {
                                                res = _main.clsHTTPS.deleteQuestion(q.ID, q.BenutzerID);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            if (res) {
                                                lv.collapseall();
                                                adapter.rows.remove(q);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }, null, null);
                        dlg.show();
                    } else if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        _main.currentQuestion = null;
                        _main.currentAnswer = null;
                        if (a != null && (a.BenutzerID == _main.user.getLong(Constants.id) || _main.user.getBoolean(Constants.administrator))) {
                            boolean res = _main.clsHTTPS.deleteAnswer(a.ID, a.BenutzerID, q.BenutzerID, q.ID, q.FrageID, q.Kontaktgrad);
                            if (res) {
                                int i = q.items.indexOf(a);
                                q.items.remove(i);
                                if (a.ParentFrageAntwortenID == null) {
                                    Answer aa;
                                    while (i < q.items.size() && (aa = q.items.get(i)) != null) {
                                        if (aa.ParentFrageAntwortenID != null) {
                                            q.items.remove(i);
                                        } else {
                                            break;
                                        }
                                    }
                                }
                                this.adapter.notifyDataSetChanged();
                            }
                        } else {
                            boolean res = _main.clsHTTPS.deleteQuestion(q.ID, q.BenutzerID);
                            if (res) {
                                lv.collapseall();
                                this.adapter.rows.remove(q);
                                this.adapter.notifyDataSetChanged();
                            }
                        }
                    }
                    return true;
                case R.id.cmnuArchive:
                    final ArrayList<Question> sel2 = new ArrayList<>();
                    for (Question q : adapter.rows) {
                        if (q.selected) sel2.add(q);
                    }
                    if (sel2.size() > 0) {
                        AlertDialog dlg = lib.ShowMessageYesNo(getContext(), getString(R.string.doyou2) + " " + sel2.size() + " " + getString(R.string.archivequestions),
                                getString(R.string.archive), false, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        for (Question q : sel2) {
                                            q.selected = false;
                                            boolean res = false;
                                            try {
                                                res = _main.clsHTTPS.archiveQuestion(q.ID, q.BenutzerID);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            if (res) {
                                                lv.collapseall();
                                                adapter.rows.remove(q);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }, null, null);
                        dlg.show();
                    } else if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        if (childPosition >= 0) return true;
                        _main.currentQuestion = null;
                        _main.currentAnswer = null;

                        boolean res = _main.clsHTTPS.archiveQuestion(q.ID, q.BenutzerID);
                        if (res) {
                            lv.collapseall();
                            this.adapter.rows.remove(q);
                            this.adapter.notifyDataSetChanged();
                        }

                    }
                    return true;
                case R.id.cmnuHide:
                    final ArrayList<Question> sel3 = new ArrayList<>();
                    boolean aus = false;
                    for (Question q : adapter.rows) {
                        if (q.selected) sel3.add(q);
                        if (q.aus) aus = true;
                    }
                    if (sel3.size() > 0) {
                        AlertDialog dlg = lib.ShowMessageYesNo(getContext(), getString(R.string.doyou2) + " " + sel3.size() + " " + getString(R.string.questions) + " " + (aus ? getString(R.string.einblenden) : getString(R.string.ausblenden2)),
                                (aus ? getString(R.string.einblenden2) : getString(R.string.ausblenden3)), false, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        for (Question q : sel3) {
                                            q.selected = false;
                                            boolean res = false;
                                            try {
                                                if (q.aus = false) {
                                                    res = _main.clsHTTPS.hideQuestion(q.ID, _main.user.getLong(Constants.id));
                                                } else {
                                                    res = _main.clsHTTPS.showQuestion(q.ID, _main.user.getLong(Constants.id));
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            if (res) {
                                                lv.collapseall();
                                                adapter.rows.remove(q);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }, null, null);
                        dlg.show();
                    } else if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        if (childPosition >= 0) return true;
                        _main.currentQuestion = null;
                        _main.currentAnswer = null;

                        boolean res = false;

                        if (!q.aus) {
                            res = _main.clsHTTPS.hideQuestion(q.ID, _main.user.getLong(Constants.id));
                        } else {
                            res = _main.clsHTTPS.showQuestion(q.ID, _main.user.getLong(Constants.id));
                        }
                        if (res) {
                            lv.collapseall();
                            this.adapter.rows.remove(q);
                            this.adapter.notifyDataSetChanged();
                        }

                    }
                    return true;
                case R.id.cmnuNotification:
                    boolean res = getContext().getSharedPreferences(TEST, MODE_PRIVATE).getBoolean(TEST, false);
                    res = res ^ true;
                    getContext().getSharedPreferences(TEST, MODE_PRIVATE).edit().putBoolean(TEST, res).commit();
                    return true;
                case R.id.cmnuValuate:
                    if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        _main.currentQuestion = null;
                        _main.currentAnswer = null;
                        if (a != null) {
                            int rres = _main.clsHTTPS.valueAnswer(_main.user.getLong(Constants.id), a.ID, a.BenutzerID, 1, q.BenutzerID, q.ID, q.FrageID, q.Kontaktgrad);
                            if (rres > 0) {
                                a.Bewertung = rres;
                                this.adapter.notifyDataSetChanged();
                            }
                        }
                    }
                    return true;
                case R.id.cmnuMelden:
                    if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        String txt = (a != null ? a.ID + " " + a.Antwort + " " + a.Antwort2 : q.ID + " " + q.Frage);
                        Long FrageAntwortenID = null;
                        Long BenutzerFragenID = null;
                        Boolean value = true;
                        if (a != null) {
                            FrageAntwortenID = a.ID;
                            if (a.gemeldet) value = false;
                            a.gemeldet = value;
                        } else {
                            BenutzerFragenID = q.ID;
                            if (q.gemeldet) value = false;
                            q.gemeldet = value;
                        }
                        Long r = _main.clsHTTPS.setGemeldet(_main.user.getLong(Constants.id), FrageAntwortenID, BenutzerFragenID, value);
                        if (r > 0)
                            lib.ShowMessage(this.getContext(), txt, getString(R.string.Meldung_MeldungAufgehoben));
                    }

                    return true;
                case R.id.cmnuBlock:
                    MainActivity m = _main;
                    if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        Long BenID = (a != null ? a.BenutzerID : q.BenutzerID);

                        if (item.getTitle().equals(getString(R.string.entsperren))) {
                            try {
                                int SperreID = Integer.parseInt(item.getTitle().toString().substring(item.getTitle().toString().lastIndexOf(":") + 1));
                                boolean rres = m.clsHTTPS.removeSperre(m.user.getLong(Constants.id), SperreID);
                                if (rres) {
                                    item.setTitle(R.string.sperren);
                                }
                            } catch (Exception ex) {
                                Log.e(TAG, null, ex);
                                ex.printStackTrace();
                            }
                        } else {
                            try {
                                int rres = m.clsHTTPS.addSperre(m.user.getLong(Constants.id), BenID);
                                if (rres >= 0) {
                                    item.setTitle(R.string.entsperren + ":" + rres);
                                }
                            } catch (Exception ex) {
                                Log.e(TAG, null, ex);
                                ex.printStackTrace();
                            }
                        }
                    }

                    return true;
                case R.id.cmnuSperren:
                    final ArrayList<Question> selsp = new ArrayList<>();
                    for (Question q : adapter.rows) {
                        if (q.selected) selsp.add(q);
                    }
                    if (selsp.size() > 0) {
                        AlertDialog dlg = lib.ShowMessageYesNo(getContext(), getString(R.string.doyou2) + " " + selsp.size() + " " + getString(R.string.fragensperren),
                                getString(R.string.delete), false, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        for (Question q : selsp) {
                                            q.selected = false;
                                            Long res = null;
                                            Long FrageID = null;
                                            Boolean value = true;
                                            FrageID = q.FrageID;
                                            try {
                                                res = _main.clsHTTPS.setGesperrt(_main.user.getLong(Constants.id), null, FrageID, null, value);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            if (res != null && res > 0) {
                                                lv.collapseall();
                                                adapter.rows.remove(q);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }, null, null);
                        dlg.show();
                    } else if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        Long FrageID = null;
                        Long AntwortID = null;
                        Boolean value = true;
                        if (a != null) {
                            AntwortID = a.AntwortID;
                        } else {
                            FrageID = q.FrageID;
                        }
                        Long r = _main.clsHTTPS.setGesperrt(_main.user.getLong(Constants.id), AntwortID, FrageID, null, value);
                        if (r != null && r > 0) {
                            if (a != null) {
                                int i = q.items.indexOf(a);
                                q.items.remove(i);
                                if (a.ParentFrageAntwortenID == null) {
                                    Answer aa;
                                    while (i < q.items.size() && (aa = q.items.get(i)) != null) {
                                        if (aa.ParentFrageAntwortenID != null) {
                                            q.items.remove(i);
                                        } else {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                lv.collapseall();
                                this.adapter.rows.remove(q);
                            }
                            this.adapter.notifyDataSetChanged();

                        }
                    }

                    return true;
                case R.id.cmnuProfil:
                    if (groupPosition >= 0) {
                        Question q = adapter.rows.get(groupPosition);
                        Answer a = null;
                        if (childPosition >= 0) a = q.items.get(childPosition);
                        dlgProfile fragP = new dlgProfile();
                        Dialog f = fragP.getDialog();
                        fragP.show(_main.fm, "fragProfile");
                        fragP.setValues(q, a);
                        /*
                        JSONObject N = _main.clsHTTPS.getUser(_main.user.getLong(ID), q.BenutzerID);

                        String msg = getString(R.string.ID) + q.BenutzerID
                                + "\n" + getString(R.string.Benutzername) + q.Benutzername
                                + "\n" + getString(R.string.name2) + (N.isNull(VORNAME)?"":N.getString(VORNAME)) + " " + (N.isNull(NAME)?"":N.getString(NAME))
                                + (N.getBoolean("gewerblich") ? "\n" + getString(R.string.Adresse) + ":" + (N.isNull("adresse")? "" : N.getString("adresse")) : "")
                                + (N.isNull(TAETIGKEITSSCHWERPUNKTE) || N.getString(TAETIGKEITSSCHWERPUNKTE).length() == 0 ? "" : "\n" + getString(R.string.Taetigkeitsschwerpunkte) + ":" + N.getString(TAETIGKEITSSCHWERPUNKTE))
                                + "\n" + getString(R.string.Grad) + ":" + q.Grad
                                + "\n" + getString(R.string.cmnuFragen)  + q.Fragen
                                + "\n" + getString(R.string.Antworten) + q.Antworten
                                + "\n" + getString(R.string.Bewertungen) + q.Bewertungen
                                + "\n" + getString(R.string.Antwortzeit) + (q.Antwortzeit!=null ? DateUtils.formatElapsedTime(q.Antwortzeit) : "null")
                                + "\n" + getString(R.string.Aktivitaetsindex) + ":" + q.Aktivitaetsindex
                                + "\n" + getString(R.string.Bestaendigkeit) + ":" + q.Bestaendigkeit
                                + "\n" + getString(R.string.Wertung) + ":" + q.Wertung;
                                ;
                        if (a!=null)
                        {
                            N = _main.clsHTTPS.getUser(_main.user.getLong(ID), a.BenutzerID);
                            msg = getString(R.string.ID) + a.BenutzerID
                                    + "\n" + getString(R.string.Benutzername) + a.BenutzerName
                                    + "\n" + getString(R.string.name2) + (N.isNull(VORNAME)?"":N.getString(VORNAME)) + " " + (N.isNull(NAME)?"":N.getString(NAME))
                                    + (N.getBoolean("gewerblich") ? "\n" + getString(R.string.Adresse) + ":" + (N.isNull("adresse")? "" : N.getString("adresse")) : "")
                                    + (N.isNull(TAETIGKEITSSCHWERPUNKTE) || N.getString(TAETIGKEITSSCHWERPUNKTE).length() == 0 ? "" : "\n" + getString(R.string.Taetigkeitsschwerpunkte) + ":" + N.getString(TAETIGKEITSSCHWERPUNKTE))
                                    + "\n" + getString(R.string.Grad) + ":" + a.Grad
                                    + "\n" + getString(R.string.cmnuFragen)  + a.Fragen
                                    + "\n" + getString(R.string.Antworten) + a.Antworten
                                    + "\n" + getString(R.string.Bewertungen) + a.Bewertungen
                                    + "\n" + getString(R.string.Antwortzeit) + (a.Antwortzeit!=null ? DateUtils.formatElapsedTime(a.Antwortzeit) : "null")
                                    + "\n" + getString(R.string.Aktivitaetsindex) + ":" + a.Aktivitaetsindex
                                    + "\n" + getString(R.string.Bestaendigkeit) + ":" + a.Bestaendigkeit
                                    + "\n" + getString(R.string.Wertung) + ":" + a.Wertung;

                        }
                        Bitmap profile = null;
                        try
                        {
                            String fname;
                            fname = ".JPG";
                            Long BenutzerID = a!= null ? a.BenutzerID : q.BenutzerID;
                            Bitmap in = _main.clsHTTPS.downloadImage(this.getContext(), "Image" + BenutzerID + fname, _main.user.getLong(Constants.id));
                            if (in != null) {
                                profile = in;
                            }
                        }
                        catch (Throwable e)
                        {
                            e.printStackTrace();
                            Log.e(TAG,null,e);
                        }

                        ImageView v = null;
                        if (profile != null)
                        {
                            v = new ImageView(getContext());
                            v.setImageBitmap(profile);
                        }

                        lib.ShowMessage(_main, msg, getString(R.string.Profil), v);
                        */
                    }
                    return true;
                case R.id.cmnuAdden: {
                    Question q = adapter.rows.get(groupPosition);
                    Answer a = null;
                    if (childPosition >= 0) a = q.items.get(childPosition);
                    if (a != null) {
                        int KontaktID = _main.clsHTTPS.addContact(_main.user.getLong(Constants.id), a.BenutzerID, false);
                        a.Grad = 1;
                    } else if (q != null) {
                        int KontaktID = _main.clsHTTPS.addContact(_main.user.getLong(Constants.id), q.BenutzerID, false);
                        q.Grad = 1;
                    }
                    return true;
                }
                case R.id.cmnuChat:
                    Question q = adapter.rows.get(groupPosition);
                    Answer a = null;
                    Long BenutzerIDEmpfaenger = null;
                    String nameEmpfaenger = null;
                    if (childPosition >= 0) a = q.items.get(childPosition);
                    if (a != null) {
                        BenutzerIDEmpfaenger = a.BenutzerID;
                        nameEmpfaenger = a.BenutzerName;
                        if (a.BenutzerIDSender != null) BenutzerIDEmpfaenger = a.BenutzerIDSender;
                    } else if (q != null) {
                        BenutzerIDEmpfaenger = q.BenutzerID;
                        nameEmpfaenger = q.Benutzername;
                    }
                    _main.showFragChat(false, BenutzerIDEmpfaenger, nameEmpfaenger, true, false);
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }
        } catch (Throwable ex) {
            lib.ShowException(TAG, getContext(), ex, false);
            return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater f) {
        f.inflate(R.menu.questions_menu, menu);
        //getMenuInflater().inflate(R.menu.home_menu, menu);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.mnu_archive);
        item.setEnabled(btnEigene.isChecked());

        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        if (b == null || !initialized) return;
        //savestate
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_refresh:
                try {
                    initFragQuestions(true, -1, -1, null, null, null, 0l, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
                return true;
            case R.id.mnu_archive:
                archive(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //public void init(final boolean reload, int pstartGrad, int pendGrad, Boolean pgeloescht, Boolean parchviert, Boolean paus, Long limit, boolean more) throws Exception {
    //    init(false, false, reload, pstartGrad, pendGrad, pgeloescht, parchviert, paus, limit, more, 0);
    //}

    public void initFragQuestions(final boolean reload, int pstartGrad, int pendGrad, Boolean pgeloescht, Boolean parchviert, Boolean paus, Long limit, boolean more) throws Exception {
        initFragQuestions(false, false, reload, pstartGrad, pendGrad, pgeloescht, parchviert, paus, limit, more, 0);
    }

    public void initFragQuestions(final boolean blnScroll, final boolean reload, int pstartGrad, int pendGrad, Boolean pgeloescht, Boolean parchviert, Boolean paus, Long limit, boolean more) throws Exception {
        initFragQuestions(false, blnScroll, reload, pstartGrad, pendGrad, pgeloescht, parchviert, paus, limit, more, 0);
    }

    public void initFragQuestions(final boolean blnScroll, final boolean reload, int pstartGrad, int pendGrad, Boolean pgeloescht, Boolean parchviert, Long limit, boolean more) throws Exception {
        initFragQuestions(false, blnScroll, reload, pstartGrad, pendGrad, pgeloescht, parchviert, false, limit, more, 0);
    }

    public void initFragQuestions(final boolean secondrun, final boolean blnScroll, final boolean reload, int pstartGrad, int pendGrad, Boolean pgeloescht, Boolean parchviert, Boolean paus, Long plimit, final boolean more, final int reloadcount) throws Exception {


        checkMain();

        if (_main == null) return;

        resetLastPositionInListview();

        // restoreMissingOrEmptyParametersFromMainActivityLast
        if (pstartGrad == -1) pstartGrad = _main.lastStartGrad;
        if (pendGrad == -1) pendGrad = _main.lastEndGrad;
        if (pgeloescht == null) pgeloescht = _main.lastGeloescht;
        if (parchviert == null) parchviert = _main.lastArchiviert;
        if (paus == null) paus = _main.lastPaus;
        if (plimit == null) plimit = _main.lastLimit;

        resetService();


        final int startGrad = pstartGrad;
        final int endGrad = pendGrad;
        final boolean geloescht = pgeloescht;
        final boolean archiviert = parchviert;
        final boolean ausgeblendet = paus;
        final long limit = plimit;

        checkAdminGeloescht();

        try {

            if (pb == null || lv == null || _main.user == null) {
                reloadInitQuestions(secondrun, blnScroll, reload, startGrad, endGrad, geloescht, archiviert, ausgeblendet, limit, more, reloadcount);
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
            lib.ShowException(TAG, _main, e, false);
        }
        if (questions == null || reload || startGrad != lastStartGrad || endGrad != lastEndGrad) {
            getQuestionsFromDatabase(secondrun, startGrad, endGrad, plimit, geloescht, archiviert, ausgeblendet, limit, blnScroll, reload, more);
        } else if (adapter != null) {
            adapter.notifyDataSetChanged();
            processReadMessageQuestions(secondrun, blnScroll, limit, more);
        } else {
            processReadMessageQuestions(secondrun, blnScroll, limit, more);
        }


    }

    private void reloadInitQuestions(boolean secondrun, boolean blnScroll, boolean reload, int startGrad, int endGrad, boolean geloescht, boolean archiviert, boolean ausgeblendet, long limit, boolean more, int reloadcount) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    int r = reloadcount;
                    r++;
                    if (r > 10) {
                        if (_main.user == null) {
                            lib.ShowMessage(getContext(), getString(R.string.notloggedin), getString(R.string.Fehler));
                            return;
                        } else {
                            lib.ShowMessage(getContext(), getString(R.string.fragQuestionsNotInitialized), getString(R.string.Fehler));
                        }

                    } else {
                        initFragQuestions(secondrun, blnScroll, reload, startGrad, endGrad, geloescht, archiviert, ausgeblendet, limit, more, r);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, null, e);
                }
            }
        }, 1000);

    }

    private void checkAdminGeloescht() {
        try {
            if (_main != null && _main.user != null && _main.user.getBoolean(Constants.administrator)) {
                btnGeloescht.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void resetService() {
        if (_main.mService != null) {
            _main.mService.countQuestions = 0;
            _main.mService.countAnswers = 0;
            _main.mService.NotificatonPopupIntervall = 60 * 1000;
        }
    }

    private void checkMain() {
        if (_main == null) _main = (MainActivity) getActivity();
        if (_main != null && _main.user == null && _main.mService != null)
            _main.user = _main.mService.user;
    }

    private void resetLastPositionInListview() {
        this.lastChildPosition = -1;
        this.lastGroupPosition = -11;
        _main.currentchildid = -1;
        _main.currentgroupid = -1;
    }

    private void getQuestionsFromDatabase(boolean secondrun, int startGrad, int endGrad, long plimit, boolean geloescht, boolean archiviert, boolean ausgeblendet, long limit, boolean blnScroll, boolean reload, boolean more) {
        String SearchType = "";
        lib.Ref<Long> _pLimit = new lib.Ref<>(plimit);

        SearchType = setSearchType(startGrad, endGrad, _pLimit, geloescht, archiviert, ausgeblendet);
        plimit = _pLimit.get();

        setLastSearch(startGrad, endGrad, archiviert, geloescht, ausgeblendet, limit);


//to be changed
        restoreLastSearchIfEmpty(SearchType);

        if (txtSearch.getText().toString().equals(CHARSEARCHWASCLICKEDONEMPTYTXT))
            txtSearch.setText("");

        if (txtSearch.length() > 0 && txtSearch.length() < 3) {
            lib.ShowException(TAG, getContext(), new Exception("Searchtext < 3 char!"), false);
        }

        saveCurrentSearchInPreferences(SearchType);

        AsyncTaskGetQ AsyncTaskGetQuestions = new AsyncTaskGetQ(this, secondrun, blnScroll, reload, startGrad, endGrad, Entf, archiviert, geloescht, ausgeblendet, plimit, more, txtSearch.getText().toString());
        AsyncTaskGetQuestions.execute();

    }

    private void setLastSearch(int startGrad, int endGrad, boolean archiviert, boolean geloescht, boolean ausgeblendet, long limit) {
        _main.lastStartGrad = startGrad;
        _main.lastEndGrad = endGrad;
        _main.lastArchiviert = archiviert;
        _main.lastGeloescht = geloescht;
        _main.lastPaus = ausgeblendet;
        _main.lastLimit = limit;
        _main.questionsLoaded = true;
        lastEndGrad = endGrad;
        lastStartGrad = startGrad;
    }

    private void saveCurrentSearchInPreferences(String SearchType) {
        getContext().getSharedPreferences(SEARCH, MODE_PRIVATE).edit().putString(SEARCH + SearchType, txtSearch.getText().toString()).apply();
        if (SearchType.equals(KONTAKTE) && _main != null) {
            _main.getApplicationContext().getSharedPreferences(SEARCH, MODE_PRIVATE).edit().putString(SEARCH, txtSearch.getText().toString()).apply();
        }
    }

    private void restoreLastSearchIfEmpty(String SearchType) {
        if (txtSearch.getText().toString().length() == 0) {
            dontsearchAutoComplete = true;
            txtSearch.setText(getContext().getSharedPreferences(SEARCH, MODE_PRIVATE).getString(SEARCH + SearchType, ""));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dontsearchAutoComplete = false;
                }
            }, 500);
        }

    }

    private String setSearchType(int startGrad, int endGrad, lib.Ref<Long> pLimit, boolean geloescht, boolean archiviert, boolean ausgeblendet) {
        String SearchType = null;
        if (endGrad == 0) {
            _main.setNewAnswers(0);
            btnEigene.setChecked(true);
            pLimit.set(-1l);
            SearchType = EIGENE;
        } else if (startGrad == 0 && endGrad > 0) {
            btnAlle.setChecked(true);
            SearchType = ALLE;
            _main.setNewAnswersOther(0);
            _main.setNewQuestions(0);
            //btnKontakte.setChecked(true);
            SearchType = KONTAKTE;
        } else if (startGrad == 1) {
            _main.setNewAnswersOther(0);
            _main.setNewQuestions(0);
            btnKontakte.setChecked(true);
            SearchType = KONTAKTE;
        }
        if (geloescht) {
            btnGeloescht.setChecked(true);
            SearchType = "geloescht";
        }
        if (archiviert) {
            btnArchiviert.setChecked(true);
            SearchType = "archiviert";
        }
        if (ausgeblendet) {
            btnAus.setChecked(true);
            SearchType = "ausgeblendet";
        }

        return SearchType;

    }

    private void processReadMessageQuestions(boolean secondrun, boolean blnScroll, Long limit, boolean more) {

        if (!secondrun && _main.readMessage != null) {
            final int Grad = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(fragSettings.GRADE, 5);
            try {
                JSONObject o = new JSONObject(_main.readMessage);
                LimindoService.SendType t = LimindoService.SendType.valueOf(o.getString(SENDTYPE));
                if (t == LimindoService.SendType.Answer) {
                    int FrageID = o.getInt(FRAGEID);
                    int BenutzerID = o.getInt(BENUTZERID);
                    int BenutzerIDFrage = o.getInt(BENUTZERIDFRAGE);
                    if (BenutzerIDFrage != _main.user.getLong(Constants.id)) {
                        initFragQuestions(true, blnScroll, true, 1, Grad, false, false, false, limit, more, 0);
                        return;
                    } else {
                        //init(true,0,0,false,false);
                    }
                } else if (t == LimindoService.SendType.Question) {
                    String Frage = o.getString(FRAGE);
                    int FrageID = o.getInt(FRAGEID);
                    int BenutzerID = o.getInt(BENUTZERID);
                    int OriginalID = o.getInt(ORIGINALID);
                    if (BenutzerID != _main.user.getLong(Constants.id)) {
                        initFragQuestions(true, blnScroll, true, 1, Grad, false, false, false, limit, more, 0);
                        //btnKontakte.performClick();
                        return;
                    } else {
                        //init(true,0,0,false,false);
                    }
                }
            } catch (Throwable ex) {
                Log.e(TAG, READ_MESSAGE + _main.readMessage, ex);
            }

        } else {
        }
    }

    public void expandGroups() {
        if (adapter == null) return;
        for (int i = 0; i < adapter.rows.size(); i++) {
            if (adapter.rows.get(i).expanded) {
                lv.expandGroup(i);
            }
        }
    }

    public static class AsyncTaskGetQ extends AsyncTask<Void, Void, ArrayList<Question>> {
        public static final String DEAKTIVIERT = "deaktiviert";
        public static final String TIME_GET_QUESTIONS_TOTAL = "TimeGetQuestionsTotal ";
        public static final String SETTINGS = fragQuestions.SETTINGS;
        public static final String TIME_SHOW_QUESTIONS = "TimeShowQuestions";
        public static final String FAILED_TO_CONNECT = "failed to connect";
        private final fragQuestions fragQ;
        private final boolean reload;
        private final int startGrad;
        private final int endGrad;
        private final int Entf;
        private final boolean archiviert;
        private final boolean geloescht;
        private final boolean ausgeblendet;
        private final long limit;
        private final boolean more;
        private final String search;
        private final boolean blnScroll;
        private final MainActivity _main;
        private final boolean secondrun;
        Throwable ex = null;

        AsyncTaskGetQ(fragQuestions fragQuestions,
                      boolean secondrun, boolean blnScroll, boolean reload,
                      int startGrad,
                      int endGrad,
                      int entf,
                      boolean archiviert,
                      boolean geloescht,
                      boolean ausgeblendet,
                      long limit,
                      boolean more, String Search) {
            this.fragQ = fragQuestions;
            this.reload = reload;
            this.startGrad = startGrad;
            this.endGrad = endGrad;
            this.Entf = entf;
            this.archiviert = archiviert;
            this.geloescht = geloescht;
            this.ausgeblendet = ausgeblendet;
            this.limit = limit;
            this.more = more;
            this.search = Search;
            this.blnScroll = blnScroll;
            this._main = (MainActivity) fragQuestions.getActivity();
            this.secondrun = secondrun;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!blnScroll) {
                fragQ.pb.setVisibility(View.VISIBLE);
                fragQ.lv.setVisibility(View.GONE);
                fragQ.llButtons.setVisibility(View.GONE);
                fragQ.llButtonsLV.setVisibility(View.GONE);
                fragQ.txtSearch.setVisibility(View.GONE);
                fragQ.btnSearch.setVisibility(View.GONE);
            } else {
                fragQ.llButtonsLV.setVisibility(View.GONE);
            }
        }

        @Override
        protected ArrayList<Question> doInBackground(Void... integers) {
            try {
                Calendar cal = Calendar.getInstance();
                Long TimeStart = cal.getTimeInMillis();
                ArrayList<Question> res = fragQ.getQuestions(reload, blnScroll, startGrad, endGrad, Entf, archiviert, geloescht, ausgeblendet, limit, more, search, _main.getLastLogoffDate());
                Long TimeTotal = Calendar.getInstance().getTimeInMillis() - TimeStart;
                System.out.println(TIME_GET_QUESTIONS_TOTAL + (TimeTotal));
                return res;
            } catch (Throwable e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
                this.ex = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Question> questions) {
            super.onPostExecute(questions);
            if (questions != null) {
                try {
                    Calendar cal = Calendar.getInstance();
                    Long TimeStart = cal.getTimeInMillis();
                    if ((blnScroll) && fragQ.adapter != null) {
                        fragQ.adapter.rows.addAll(questions);
                        fragQ.adapter.notifyDataSetChanged();
                    } else {
                        fragQ.adapter = new QuestionsAdapter(fragQ, questions);
                        fragQ.adapter.context = fragQ.getContext();
                        fragQ.adapter.ServiceCursor = null;
                        fragQ.adapter.CountVisible = 0;
                        fragQ.lv.setAdapter(fragQ.adapter);
                        fragQ.lv.setOnScrollListener(fragQ.adapter.onScrollListener);
                        fragQ.initialized = true;
                    }
                    if (questions.size() == 0) {
                        //_main.lastLimit = -10;
                        if (blnScroll) _main.lastLimit = -10;
                        fragQ.btnNext.setEnabled(false);
                        if (limit == 0 && !fragQ._main.showLogin && !(fragQ._main.fragLogin != null && fragQ._main.fragLogin.isAdded())) {
                            if (!fragQ._main.user.getBoolean(DEAKTIVIERT)) {
                                lib.ShowMessage(fragQ.getContext(), fragQ.getString(R.string.AddQuestions), fragQ.getString(R.string.KeineFragenGefunden));
                            }
                        }
                    } else {
                        fragQ.btnNext.setEnabled(true);
                    }
                    if (fragQ._main.readMessage != null) {
                        try {
                            lib.setStatusAndLog(fragQ.getContext(),TAG, "*** processReadMessage " + fragQ._main.readMessage);
                            fragQ.processReadMessageQuestions(secondrun, blnScroll, limit, more);
                            final String msg = fragQ._main.readMessage;
                            fragQ._main.readMessage = null;
                            fragQ._main.processReadMessage(msg, true, true, true);
                                    /*
                                    fragQ._main.mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                fragQ._main.processReadMessage(msg);
                                            } catch (Throwable throwable) {
                                                throwable.printStackTrace();
                                                Log.e(TAG, "readMessage", throwable);
                                            }
                                        }
                                    }, 3000);
                                    */
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                        //fragQ._main.readMessage = null;
                    } else if (fragQ._main.loadcurrentquestion) {
                        fragQ._main.loadcurrentquestion = false;
                        int groupid = fragQ._main.currentgroupid;
                        fragQ._main.currentgroupid = -1;
                        if (fragQ._main.currentQuestion != null && (groupid < 0 || groupid >= fragQ.adapter.rows.size())) {
                            groupid = fragQ.findPos(fragQ._main.currentQuestion.ID);
                        }
                        if (groupid >= 0 && fragQ.adapter.rows.size() > groupid) {
                            fragQ._main.currentgroupid = -1;
                            fragQ.lv.setSelection(groupid);
                            fragQ.lv.expandGroup(groupid);
                        }
                    }
                    if (this.endGrad == 0 && fragQ.btnEigene.isChecked()) {
                        int maxQuestions = fragQ.getActivity().getSharedPreferences(SETTINGS, MODE_PRIVATE).getInt(MAX_QUESTIONS, 50);
                        if (fragQ.adapter.rows.size() > maxQuestions) {
                            fragQ.archive(true);
                        }
                    }
                    Long TimeTotal = (Calendar.getInstance().getTimeInMillis() - TimeStart);
                    System.out.println(TIME_SHOW_QUESTIONS + TimeTotal);

                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            } else {
                if (this.ex != null) {
                    //lib.ShowException(getContext(),ex);
                    try {
                        lib.ShowException(TAG, fragQ.getContext(), this.ex, true);
                        if (this.ex.getMessage().toLowerCase().contains(FAILED_TO_CONNECT)
                                || this.ex.getMessage().toLowerCase().contains("unable to resolve host")) {
                            lib.ShowMessage(fragQ.getContext(), fragQ.getString(R.string.not_connectiontoserver), fragQ.getString(R.string.Fehler));
                        } else {
                            lib.ShowMessage(fragQ.getContext(), fragQ.getString(R.string.ErrorGetData), fragQ.getString(R.string.Fehler));
                        }
                    } catch (Throwable ex) {
                        Log.e(TAG, this.ex.getMessage(), ex);
                    }
                } else {
                    if (limit == 0 && !fragQ._main.showLogin && !(fragQ._main.fragLogin != null && fragQ._main.fragLogin.isAdded())) {
                        try {
                            if (!fragQ._main.user.getBoolean(DEAKTIVIERT)) {
                                lib.ShowMessage(fragQ.getContext(), fragQ.getString(R.string.AddQuestions), fragQ.getString(R.string.KeineFragenGefunden));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            fragQ.pb.setVisibility(View.GONE);
            fragQ.lv.setVisibility(View.VISIBLE);
            fragQ.llButtons.setVisibility(View.VISIBLE);
            fragQ.txtSearch.setVisibility(View.VISIBLE);
            fragQ.btnSearch.setVisibility(View.VISIBLE);
            //if (!blnScroll) fragQ.llButtonsLV.setVisibility(View.VISIBLE);
        }
    }

    private void archive(final boolean auto) {
        AlertDialog d;
        final EditText inputBox = new EditText(getContext());
        int anz = _main.getSharedPreferences(SETTINGS, MODE_PRIVATE).getInt(ANZARCH, 30);
        d = lib.getInputBox(getContext(), getString(R.string.archive), getString(R.string.archivequestionsx), "" + anz, true, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {

                    Date now = _main.clsHTTPS.getServerDate(_main.user.getLong(Constants.id));
                    Date lastLogoff = _main.getLastLogoffDate();
                    Integer days = Integer.parseInt(inputBox.getText().toString());
                    _main.getSharedPreferences(SETTINGS, MODE_PRIVATE).edit().putInt(ANZARCH, days).commit();
                    Date nowOffset = org.apache.commons.lang3.time.DateUtils.addDays(now, -days);
                    if (lastLogoff != null) {// && nowOffset.before(org.apache.commons.lang3.time.DateUtils.addDays(lastLogoff, -3))) {
                        final ArrayList<Question> sel2 = new ArrayList<>();
                        for (Question q : adapter.rows) {
                            if (q.DatumEnde != null && q.DatumEnde.before(nowOffset) && q.DatumEnde.before(org.apache.commons.lang3.time.DateUtils.addDays(lastLogoff, -3))) {
                                sel2.add(q);
                                q.selected = true;
                            }

                        }
                        if (sel2.size() > 0) {
                            AlertDialog dlg = lib.ShowMessageYesNo(getContext(), getString(R.string.doyou) + sel2.size() + " " + getString(R.string.archivequestions_),
                                    getString(R.string.archive), false, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            boolean _break = false;
                                            for (final Question q : sel2) {
                                                for (final Answer a : q.items) {
                                                    if (a.Bewertung == 0) {
                                                        try {
                                                            Spanned sp = Html.fromHtml(getString(R.string.doyouwanttoarchive) + a.BenutzerName + ": " + a.Antwort + getString(R.string.onquestion) + q.getFrage(true) + getString(R.string.ratepositive));
                                                            lib.yesnoundefined res = lib.ShowMessageYesNo(getContext(), sp, getString(R.string.evaluation), true);
                                                            if (res == lib.yesnoundefined.yes) {
                                                                if (a != null) {
                                                                    int rres = 0;
                                                                    try {
                                                                        rres = _main.clsHTTPS.valueAnswer(_main.user.getLong(Constants.id), a.ID, a.BenutzerID, 1, q.BenutzerID, q.ID, q.FrageID, q.Kontaktgrad);
                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                    if (rres > 0) {
                                                                        a.Bewertung = rres;
                                                                        fragQuestions.this.adapter.notifyDataSetChanged();
                                                                    }
                                                                }
                                                            }
                                                            if (res == lib.yesnoundefined.undefined) {
                                                                _break = true;
                                                                break;
                                                            }
                                                            //dlg2.show();
                                                        } catch (Throwable throwable) {
                                                            throwable.printStackTrace();
                                                        }
                                                    }

                                                }
                                                if (_break) break;
                                                q.selected = false;
                                                boolean res = false;
                                                try {
                                                    res = _main.clsHTTPS.archiveQuestion(q.ID, q.BenutzerID);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                if (res) {
                                                    lv.collapseall();
                                                    adapter.rows.remove(q);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                            if (!_break) {
                                                fragQuestions.this.getActivity().getSharedPreferences(SETTINGS, MODE_PRIVATE).edit().putInt(MAX_QUESTIONS, 50).commit();
                                            } else {
                                                for (final Question q : sel2) {
                                                    q.selected = false;
                                                }
                                                adapter.notifyDataSetChanged();
                                            }

                                        }
                                    }, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (auto) {
                                                int maxQuestions = fragQuestions.this.getActivity().getSharedPreferences(SETTINGS, MODE_PRIVATE).getInt(MAX_QUESTIONS, 50);
                                                fragQuestions.this.getActivity().getSharedPreferences(SETTINGS, MODE_PRIVATE).edit().putInt(MAX_QUESTIONS, adapter.rows.size() + 25).commit();
                                            }
                                            for (final Question q : sel2) {
                                                q.selected = false;
                                            }
                                            adapter.notifyDataSetChanged();
                                        }
                                    }, null);
                            dlg.show();
                        } else {
                            lib.ShowMessage(getContext(), getString(R.string.noquestionsfound), getString(R.string.archive));
                        }
                    } else {
                        lib.ShowMessage(getContext(), getString(R.string.nologoff), getString(R.string.archive));
                    }
                } catch (Throwable ex) {
                    Log.e(TAG, null, ex);
                }
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (auto) {
                    int maxQuestions = fragQuestions.this.getActivity().getSharedPreferences(SETTINGS, MODE_PRIVATE).getInt(MAX_QUESTIONS, 50);
                    fragQuestions.this.getActivity().getSharedPreferences(SETTINGS, MODE_PRIVATE).edit().putInt(MAX_QUESTIONS, adapter.rows.size() + 25).commit();
                }
            }
        }, inputBox);
        // wird in get aufgerufen!! d.show();
    }

    public Question findQuestion(long frageID, long BenutzerID) {
        if (adapter == null || adapter.rows == null) return null;
        for (Question x : adapter.rows) {
            if (x.FrageID == frageID && x.BenutzerID == BenutzerID) {
                return x;
            }
        }
        return null;
    }

    public Question findQuestion(long BenutzerFragenID) {
        if (adapter == null) return null;
        for (Question x : adapter.rows) {
            if (x.ID == BenutzerFragenID) {
                return x;
            }
        }
        return null;
    }

    public int findPos(long BenutzerFragenID) {
        int i = 0;
        for (Question x : adapter.rows) {
            if (x.ID == BenutzerFragenID) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }
}
