package de.com.limto.limto1;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Locale;

import de.com.limto.limto1.lib.lib;

/**
 * Created by hmnatalie on 29.08.17.
 */

public class fragSettings extends Fragment {
public final  static int fragID = 6;
    public static final String GRADE = "Grade";
    public static final String FRM_LOGIN = "frmLogin";
    public static final String FRAG_LOGIN = "fragLogin";
    public static final String ENTF = "Entf";
    public static final String AUSBL = "ausbl";
    public static final String SETTINGS = "Settings";
    public static final String GPS_OFF = "GPSOff";
    public static final String NOT_OFF = "NotOff";
    public static final int[] Mengen = new int[]{0, 20, 60, 120, 720};
    public static final String SYSTEM = "system";
    public static final String ALL = "all";
    public static final String[] langvalues = new String[]{ALL, SYSTEM, "de", "en", "fr", "es", "hu"};
    public static final String RESTARTINTERVALL = "restartintervall";
    static final String MENGE = "Menge";
    static final String LANG = "lang";
    private static final String TAG = "fragSettings";
    public static boolean gpsOff;
    public MainActivity _main;
    public EditText txtServer;
    protected Toolbar toolbar;
    private ViewGroup viewGroup;
    private Spinner spnGrade;
    private Button btnSet;
    private Spinner spnEntf;
    private Spinner spnAusblenden;
    private RadioButton btnNotOn;
    private RadioButton btnNotOff;
    private RadioButton btnGPSOff;
    private RadioButton btnGPSOn;
    private Spinner spnMenge;
    private Spinner spnLang;
    private Spinner spnRestart;
    private RadioButton btnDebugOn;
    private RadioButton btnDebugOff;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _main = (MainActivity) getActivity();

    }
    @Override
    public void onViewCreated(View view, Bundle savedinstancestate) {
        //initTreeView(view);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedinstancestate) {
        try
        {
            View v = inflater.inflate(R.layout.activity_settings, container,false);
            spnGrade = (Spinner) v.findViewById(R.id.spinnerGrad);
            spnGrade.setSelection(getActivity().getPreferences(Context.MODE_PRIVATE).getInt(GRADE,5)-1);
            spnGrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                {
                    getActivity().getPreferences(Context.MODE_PRIVATE).edit().putInt(GRADE,i+1).apply();
                    getActivity().getApplicationContext().getSharedPreferences(GRADE, Context.MODE_PRIVATE).edit().putInt(GRADE,i+1).apply();
                    ((MainActivity)getActivity()).lastEndGrad = i+1;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView)
                {

                }
            });

            spnRestart = (Spinner) v.findViewById(R.id.spinnerrestartintervall);
            Integer intervall = (getContext().getSharedPreferences("LimindoService", Context.MODE_PRIVATE).getInt(RESTARTINTERVALL, LimindoService.MaximumRestartInvervallMins));
            int pos = Arrays.asList(getActivity().getResources().getStringArray(R.array.arrrestart)).indexOf(intervall.toString());
            if (pos < 0) pos = 0;
            spnRestart.setSelection(pos);
            spnRestart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
            {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                {
                    int restartIntervall = Integer.parseInt(spnRestart.getSelectedItem().toString());
                    getContext().getSharedPreferences("LimindoService", Context.MODE_PRIVATE).edit().putInt(RESTARTINTERVALL,restartIntervall).apply();
                    LimindoService.MaximumRestartInvervallMins = restartIntervall;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView)
                {

                }
            });

            initLayoutServer(v);

            spnEntf = (Spinner) v.findViewById(R.id.spinnerEntfernung);
            enableSpnEntf();
            spnEntf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        int radius = 1000;
                        if (spnEntf.getSelectedItemPosition() >= 0) {
                            String entf = spnEntf.getSelectedItem().toString();
                            if (!entf.equalsIgnoreCase(getString(R.string.beliebig))) {
                                radius = Integer.parseInt(entf.replace(getString(R.string._km), ""));
                            }
                            getActivity().getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).edit().putInt(ENTF, radius).apply();
                        }
                    }
                    catch (Throwable ex)
                    {
                        lib.ShowException(TAG, getContext(),ex, "Entf", false);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spnMenge = (Spinner) v.findViewById(R.id.spinnerMenge);
            int Menge = getActivity().getApplicationContext().getSharedPreferences(MENGE,Context.MODE_PRIVATE).getInt(MENGE,0);
            pos = Arrays.binarySearch(Mengen,Menge);
            spnMenge.setSelection(pos);
            spnMenge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spnMenge.getSelectedItemPosition()>= 0)
                    {
                        int Menge = Mengen[spnMenge.getSelectedItemPosition()];
                        getActivity().getApplicationContext().getSharedPreferences(MENGE,Context.MODE_PRIVATE).edit().putInt(MENGE, Menge).apply();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spnLang = (Spinner) v.findViewById(R.id.spinnerLanguage);
            String Lang = getActivity().getApplicationContext().getSharedPreferences(LANG,Context.MODE_PRIVATE).getString(LANG, ALL);
            pos = Arrays.binarySearch(langvalues, Lang);
            spnLang.setSelection(pos);
            spnLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spnLang.getSelectedItemPosition()>= 0)
                    {
                        String Lang = langvalues[spnLang.getSelectedItemPosition()];
                        getActivity().getApplicationContext().getSharedPreferences(LANG,Context.MODE_PRIVATE).edit().putString(LANG, Lang).apply();
                        if (Lang.equalsIgnoreCase(ALL)) Lang = "";
                        if (Lang.equalsIgnoreCase(SYSTEM)) Lang = Locale.getDefault().toString().substring(0,2);
                        _main.Lang = Lang;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            spnAusblenden = (Spinner) v.findViewById(R.id.spinnerAusblenden);
            int ausbl = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(AUSBL,0);
            spnAusblenden.setSelection(ausbl);
            spnAusblenden.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    int ausbl = spnAusblenden.getSelectedItemPosition();
                    getActivity().getPreferences(Context.MODE_PRIVATE).edit().putInt(AUSBL,ausbl).apply();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            btnDebugOn = (RadioButton) v.findViewById(R.id.btnDebugOn);
            btnDebugOff = (RadioButton) v.findViewById(R.id.btnDebugOff);
            btnDebugOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    getContext().getSharedPreferences("lib", Context.MODE_PRIVATE).edit().putBoolean("DebugOn", btnDebugOn.isChecked()).apply();
                    lib.blnDebugOn = btnDebugOn.isChecked();
                }
            });
            btnDebugOn.setChecked(lib.blnDebugOn);
            btnDebugOff.setChecked(!lib.blnDebugOn);
            btnNotOn = (RadioButton) v.findViewById(R.id.btnNotOn);
            btnNotOff = (RadioButton) v.findViewById(R.id.btnNotOff);
            btnNotOn.setChecked(!_main.blnNotOff);
            btnNotOff.setChecked(_main.blnNotOff);
            btnNotOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    getActivity().getPreferences(Context.MODE_PRIVATE).edit().putBoolean(NOT_OFF,btnNotOff.isChecked()).apply();
                    _main.setblnNotOff (btnNotOff.isChecked());

                }
            });
            btnGPSOff = (RadioButton) v.findViewById(R.id.btnGPSOff);
            btnGPSOn = (RadioButton) v.findViewById(R.id.btnGPSOn);
            boolean gpsOff = getActivity().getSharedPreferences(SETTINGS,Context.MODE_PRIVATE).getBoolean(GPS_OFF, false);
            fragSettings.gpsOff = gpsOff;
            btnGPSOff.setChecked(gpsOff);
            btnGPSOn.setChecked(!gpsOff);

            btnGPSOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    getActivity().getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).edit().putBoolean(GPS_OFF,btnGPSOff.isChecked()).apply();
                    fragSettings.gpsOff = btnGPSOff.isChecked();
                    enableSpnEntf();
                    if (btnGPSOff.isChecked() && _main!=null && _main.mService!=null)
                    {
                        try
                        {
                            _main.mService.stopLocationUpdates();
                        }
                        catch (Throwable throwable)
                        {
                            throwable.printStackTrace();
                        }
                    }
                    else if (!btnGPSOff.isChecked() && _main!=null && _main.mService!=null)
                    {
                        try
                        {
                            _main.mService.setupLocClient();
                        }
                        catch (Throwable throwable)
                        {
                            throwable.printStackTrace();
                        }
                    }
                }
            });

            return v;
        }
        catch (Throwable ex)
        {
            return null;
        }
    }

    private void initLayoutServer(View v) {
        View llServer = v.findViewById(R.id.llServer);
        if (_main.clsHTTPS.AdminID!=null && _main.clsHTTPS.AdminID>0)
        {
            llServer.setVisibility(View.VISIBLE);
            txtServer = (EditText) v.findViewById(R.id.txtAdresse);
            txtServer.setText(clsHTTPS.MYURLROOT);
            txtServer.setImeOptions(EditorInfo.IME_ACTION_DONE);
            txtServer.setOnEditorActionListener(new TextView.OnEditorActionListener()
            {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
                {
                    if (i == EditorInfo.IME_ACTION_DONE)
                    {
                        clsHTTPS.MYURLROOT = txtServer.getText().toString();
                        _main.user = null;
                        _main.mService.user = null;
                        _main.clsHTTPS.reset();
                        //_main.mPager.setCurrentItem(fragLogin.fragID);
                        _main.fragLogin.mode = dlgLogin.LoginMode.login;
                        if (_main.fragLogin.isAdded()) _main.fragLogin.dismiss();
                        _main.fragLogin.show(_main.fm, FRAG_LOGIN);
                    }
                    return true;
                }
            });
            btnSet = (Button) v.findViewById(R.id.btnSetAdresse);
            btnSet.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    clsHTTPS.MYURLROOT = txtServer.getText().toString();
                    _main.user = null;
                    _main.mService.user = null;
                    _main.clsHTTPS.reset();
                    _main.fragLogin.mode = dlgLogin.LoginMode.login;
                    if (_main.fragLogin.isAdded()) _main.fragLogin.dismiss();
                    _main.fragLogin.show(_main.fm, FRAG_LOGIN);
                }
            });
        }
        else
        {
            llServer.setVisibility(View.GONE);
        }

    }

    private int enableSpnEntf() {
        int Entf = getActivity().getApplicationContext().getSharedPreferences(ENTF,Context.MODE_PRIVATE).getInt(ENTF,1000);
        if (fragSettings.gpsOff || _main.mService.locCoarse == null) {
            Entf = 1000;
            spnEntf.setEnabled(false);
        }
        else
        {
            spnEntf.setEnabled(true);
        }
        String E = (Entf == 1000 ? getString(R.string.beliebig) : Entf + getString(R.string._km));
        int pos = Arrays.asList(getActivity().getResources().getStringArray(R.array.radius)).indexOf(E);
        spnEntf.setSelection(pos);
        return pos;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {

        menuInflater.inflate(R.menu.home_menu, menu);
        //return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnuCredits:
                break;
            }
        return super.onOptionsItemSelected(item);
    }

    private String getSelectedNodes() {
        StringBuilder stringBuilder = new StringBuilder(getString(R.string.you_have_selected) + ": ");
        return stringBuilder.toString();
    }


}

