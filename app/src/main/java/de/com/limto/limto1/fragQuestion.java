package de.com.limto.limto1;

import android.app.Activity;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.com.limto.limto1.Controls.NoSelectionAutoComplete;
import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;
import de.com.limto.limto1.logger.LogWrapper;

import static de.com.limto.limto1.MainActivity.SPN_LAUFZ;
import static de.com.limto.limto1.fragSettings.ENTF;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fragQuestion.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fragQuestion#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragQuestion extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "FragQuestion";
    public static final String BENUTZER_ID = "BenutzerID";
    public static final String SEARCH = "search";
    public static final String FRAG_QUESTION_GET_FRAGEN2_TALK = "fragQuestion.getFragen2Talk";
    public static final String SERVICE_NOT_BOUND_OR_NOT_INITIALIZED = "Service not bound or not initialized!";
    public static final String QUESTION = "Question";
    //public static final String DT_END = "dtEnd";
    public static final String SPN_ENTF = "spnEntf";
    public static final String SPN_FEHLERGRAD = "spnFehlergrad";
    public static final String BTN_OEFFENTLICH = "btnOeffentlich";
    public static final String SPN_GRADES = "spnGrades";
    public static final String DEAKTIVIERT = "deaktiviert";
    public static final String HTTP_MAPS_GOOGLE_DE_MAPS_Q_S_S_S_OM_0 = "http://maps.google.de/maps?q=%s,%s(%s)&om=0";
    public static final String BENUTZERNAME = "benutzername";
    public static final String UTF_8 = "UTF-8";
    public static final String A_HREF = "<a href=\'";
    public static final String GOOGLE_MAPS_A = "\'>Google Maps</a>";
    public static final String BENUTZERFRAGEID = "benutzerfrageid";
    public static final String FRAGEID = "frageid";
    public static final String BEWERTUNGEN = "bewertungen";
    public static final String ANTWORTEN = "antworten";
    public static final String FRAGEN = "fragen";
    public static final String ANTWORTZEIT = "antwortzeit";
    public static final String DENSITY = "density: ";
    public static final String DPI = " dpi: ";
    public static final String SCALEDDENSITY = " scaleddensity: ";
    public static final String WIDTH = " width: ";
    public static final String HEIGHT = " height: ";
    public static final String EMAIL = "email";
    public static final String DUPLICATE = "duplicate";
    public static final String VOTE = "\\vote\\";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public MainActivity _main;
    public static final int fragID = 2;
    public NoSelectionAutoComplete txtQuestion;
    private ImageButton btnOK;
    //public DatePicker dtEnd;
    public Spinner spnGrades;
    public Spinner spnEntf;
    public Spinner spnLaufzeit;
    public Long ID = -1l;
    public Long FrageID = -1l;
    private Question qold;
    private Calendar cal;
    public RadioButton btnOeffentlich;
    private RadioButton btnPrivat;
    public Spinner spnFehlergrad;
    private boolean initalized;
    public CheckBox chkEmergency;
    private MaterialButton chkEmergencyBlind;
    private MaterialButton chkEmergencBlind;
    private MaterialCheckBox chkVote;

    public fragQuestion() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragQuestion.
     */
    // TODO: Rename and change types and number of parameters
    public static fragQuestion newInstance(String param1, String param2) {
        fragQuestion fragment = new fragQuestion();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (_main == null) _main = (MainActivity) getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(android.view.View view, android.os.Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtQuestion = (NoSelectionAutoComplete) view.findViewById(R.id.edtxtQuestion);
        txtQuestion.setImeOptions(EditorInfo.IME_ACTION_DONE);
        btnOK = (ImageButton) view.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addQuestion();
            }
        });
        btnOeffentlich = (RadioButton) view.findViewById(R.id.btnOeffentlich);
        btnPrivat = (RadioButton) view.findViewById(R.id.btnPrivat);
        //btnPrivat.setChecked(true);
        btnOeffentlich.setChecked(true);
        try {
            //dtEnd = (DatePicker) view.findViewById(R.id.dtTo);
            chkEmergency = (CheckBox) view.findViewById(R.id.chkEmergency);
            setDate();
            this.spnLaufzeit = view.findViewById(R.id.spnLaufzeit);
            spnLaufzeit.setSelection(4);
            this.spnGrades = (Spinner) view.findViewById(R.id.spinnerGrad);
            this.spnGrades.setSelection(4);
            this.spnEntf = (Spinner) view.findViewById(R.id.spinnerEntfernung);
            this.spnEntf.setSelection(5);
            spnEntf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i != 5 && _main.locCoarse() == null) {
                        spnEntf.setSelection(5);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            enableSpnEntf();
            this.spnFehlergrad = (Spinner) view.findViewById(R.id.spinnerFehlergrad);
            this.spnFehlergrad.setSelection(0);
            if (_main != null && _main.user != null && !_main.user.getBoolean(Constants.administrator)) {
                //view.findViewById(R.id.tvFehlergrad).setVisibility(View.GONE);
                //spnFehlergrad.setVisibility(View.GONE);
            }
            spnFehlergrad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i > 0) btnOeffentlich.setChecked(true);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            chkEmergency.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        try {
                            AlertDialog dlg = lib.ShowMessageYesNo(fragQuestion.this.getContext(), getString(R.string.reallyemergency), getString(R.string.SOS), true, null, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    chkEmergency.setChecked(false);
                                }
                            }, null);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                }
            });

            txtQuestion.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if (i != EditorInfo.IME_ACTION_DONE) return false;
                    addQuestion();
                    return true;

                }
            });
            txtQuestion.addTextChangedListener(new TextWatcher() {
                private CharSequence txtChanged;
                private int sel;
                String lastText;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    lastText = txtQuestion.getText().toString();
                    sel = txtQuestion.getSelectionStart() - 1;
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    txtChanged = s;
                    System.out.println(txtChanged);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (_main == null || _main.user == null || _main.clsHTTPS == null) return;
                    SpannableStringBuilder s = (SpannableStringBuilder) txtQuestion.getText();
                    if (txtQuestion.getText().length() > 240) {
                        lib.ShowToast(getContext(), getString(R.string.left) + (255 - txtQuestion.getText().length()) + getString(R.string.leftover));
                    }
                    String txt = s.toString();
                    String[] l = txt.split("\\n", -1);
                    int len = txt.length();
                    boolean multipleInstancesOfSameChar = fragAnswer.findMultipleInstancesOfSameChar(getContext(),txt, false);

                    if ((l.length > 3 && l[l.length - 3].length() < 10 && l[l.length - 2].length() < 10) || multipleInstancesOfSameChar) {
                        lib.ShowToast(getContext(), getString(R.string.spam));
                        if (lastText.endsWith("\n"))
                            lastText = lastText.substring(0, lastText.length() - 1);
                        txtQuestion.setText(lastText);
                        txtQuestion.setSelection(txtQuestion.getText().length());
                    }

                    int selength = StringEscapeUtils.escapeJava(s.toString()).length();
                    if (selength > 1024 || s.toString().split("\\n").length > 10 || txtQuestion.getLineCount() > 10) {
                        lib.ShowToast(getContext(), getString(R.string.maxlength));
                        txtQuestion.setText(lastText);
                        txtQuestion.setSelection(txtQuestion.getText().length());
                    }

                    if (s.length() >= 3) {
                        txt = StringEscapeUtils.escapeJava(s.toString());
                        txt = txt.replace("\\", "\\\\");
                        txt = txt.replace("'", "\\'");
                        HashMap<String, String> p = new java.util.HashMap<String, String>();
                        Long BenutzerID = null;
                        try {
                            BenutzerID = _main.user.getLong(Constants.id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        p.put(BENUTZER_ID, "" + BenutzerID);
                        p.put(SEARCH, "" + txt);
                        if (_main.mService == null || _main.mService.mHandler != _main.mHandler) {
                            Log.e(FRAG_QUESTION_GET_FRAGEN2_TALK, SERVICE_NOT_BOUND_OR_NOT_INITIALIZED);
                            _main.bindService0();
                        }

                        _main.clsHTTPS.getFragen2Talk(p, _main);
                    }
                }
            });
            txtQuestion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    chkVote.setChecked((Boolean) view.findViewById(R.id.lbl_name).getTag());
                }
            });
            fragQuestion fQ = this;
            if (fQ != null && savedInstanceState != null) {
                String Question = savedInstanceState.getString(QUESTION, "");
                fQ.txtQuestion.setText(Question);
                try {
                    /*
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(clsHTTPS.fmtDateTime.parse(savedInstanceState.getString(DT_END)));
                    int mYear = cal.get(Calendar.YEAR);
                    int mMonth = cal.get(Calendar.MONTH);
                    int mDay = cal.get(Calendar.DAY_OF_MONTH);
                    fQ.dtEnd.init(mYear, mMonth, mDay, null);
                    */
                    int pos = savedInstanceState.getInt(SPN_LAUFZ, 4);
                    spnLaufzeit.setSelection(pos);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fQ.spnEntf.setSelection(savedInstanceState.getInt(SPN_ENTF));
                fQ.spnFehlergrad.setSelection(savedInstanceState.getInt(SPN_FEHLERGRAD));
                fQ.btnOeffentlich.setChecked(savedInstanceState.getBoolean(BTN_OEFFENTLICH));
                fQ.spnGrades.setSelection(savedInstanceState.getInt(SPN_GRADES));
            }

            chkEmergencBlind = (MaterialButton) view.findViewById(R.id.chkEmergencyBlind);
            chkEmergencBlind.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        startNewActivity(_main, "com.bemyeyes.bemyeyes");
                    } catch (Throwable throwable) {
                        lib.ShowException(TAG, _main, throwable, false);
                        throwable.printStackTrace();
                    }

                }
            });

            chkVote = (MaterialCheckBox) view.findViewById(R.id.chkVote);

        } catch (Throwable ex) {
            Log.e(TAG, null, ex);
            ex.printStackTrace();
        }

        this.initalized = true;

    }

    private int enableSpnEntf() {
        int Entf = getActivity().getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);
        if (fragSettings.gpsOff || _main.mService.locCoarse == null) {
            Entf = 1000;
            spnEntf.setEnabled(false);
        } else {
            spnEntf.setEnabled(true);
        }
        String E = (Entf == 1000 ? getString(R.string.beliebig) : Entf + getString(R.string._km));
        int pos = Arrays.asList(getActivity().getResources().getStringArray(R.array.radius)).indexOf(E);
        spnEntf.setSelection(pos);
        return pos;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            View v = inflater.inflate(lib.getLayoutResFromTheme(getContext(), R.attr.fragquestion), container, false);
            return v;
        } catch (Throwable ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void startNewActivity(Activity context, String packageName) throws Throwable {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainActivity.dontStop++;
        context.startActivityForResult(intent, MainActivity.RGLOBAL);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        try {
            if (savedInstanceState == null || !initalized) {
                super.onSaveInstanceState(savedInstanceState);
                return;
            }
            fragQuestion f = this;
            savedInstanceState.putString(QUESTION, f.txtQuestion.getText().toString());
        /*Calendar cal = Calendar.getInstance();
        cal.set(f.dtEnd.getYear(), f.dtEnd.getMonth(), f.dtEnd.getDayOfMonth());
        savedInstanceState.putString(DT_END,clsHTTPS.fmtDateTime.format(cal.getTime()));
        */
            savedInstanceState.putInt(SPN_LAUFZ, f.spnLaufzeit.getSelectedItemPosition());
            savedInstanceState.putInt(SPN_ENTF, f.spnEntf.getSelectedItemPosition());
            savedInstanceState.putInt(SPN_FEHLERGRAD, f.spnFehlergrad.getSelectedItemPosition());
            savedInstanceState.putBoolean(BTN_OEFFENTLICH, f.btnOeffentlich.isChecked());
            savedInstanceState.putInt(SPN_GRADES, f.spnGrades.getSelectedItemPosition());

        } catch (Throwable ex) {
            lib.ShowException(TAG, getContext(), ex, true);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    public void setDate() throws Exception {
        cal = Calendar.getInstance();
        Date dtServer = null;
        if (_main.user != null) {
            dtServer = _main.clsHTTPS.getServerDate(_main.user.getLong(Constants.id));
            if (dtServer != null) cal.setTime(dtServer);
        }
        cal.add(Calendar.DATE, 1);
        Date d1 = cal.getTime();
        cal.add(Calendar.DATE, 5);
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);
        //dtEnd.init(mYear, mMonth, mDay, null);
        cal.add(Calendar.DATE, 300);
        //dtEnd.setMaxDate(cal.getTime().getTime());
        //dtEnd.setMinDate(d1.getTime());
        //dtEnd.updateDate(mYear, mMonth, mDay);
        cal.add(Calendar.DATE, -306);
        if (dtServer != null) cal.setTime(dtServer);

    }

    private void addQuestion() {
        if (chkEmergency.isChecked()) {
            try {
                AlertDialog dlg = lib.ShowMessageYesNo(fragQuestion.this.getContext(), getString(R.string.reallyemergency), getString(R.string.SOS), true, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addQuestion2();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        chkEmergency.setChecked(false);
                        //addQuestion2();
                    }
                }, null);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        } else {
            addQuestion2();
        }
    }

    private boolean addQuestion2() {

        String txt = txtQuestion.getText().toString();
        int lines = txtQuestion.getLineCount();
        if (checkLineCount(txt, lines) || fragAnswer.findMultipleInstancesOfSameChar(getContext(),txt,true)) {
            lib.ShowToast(getContext(), getString(R.string.spam));
            return false;
        }
        Question q;
        ProgressDialog p = null;
        try {
            p = ProgressDialog.show(getContext(), getString(R.string.question), getString(R.string.saving));
            if (_main == null) throw new Exception("_main is null");
            if (_main.user == null) throw new Exception(getString(R.string.nichtAngemeldet));
            if (_main.clsHTTPS == null) throw new Exception("_main.clshttps is null");
            if (_main.user.getBoolean(DEAKTIVIERT)) {
                lib.ShowMessage(getContext(), getString(R.string.Accountdeactivated), "");
                return false;
            }

            cal = Calendar.getInstance();
            Date dtServer = null;
            if (_main.user != null) {
                dtServer = _main.clsHTTPS.getServerDate(_main.user.getLong(Constants.id));
                if (dtServer != null) cal.setTime(dtServer);
            }
            Date d1 = cal.getTime();
            cal.setTime(getSelectedLaufzeitDate());
            //cal.set(dtEnd.getYear(), dtEnd.getMonth(), dtEnd.getDayOfMonth());
            int radius = 1000;
            if (spnEntf.getSelectedItemPosition() >= 0) {
                String entf = spnEntf.getSelectedItem().toString();
                if (!entf.equalsIgnoreCase(getString(R.string.beliebig))) {
                    radius = Integer.parseInt(entf.replace(getString(R.string._km), ""));
                }
            }
            Integer Fehlergrad = null;
            if (spnFehlergrad.getSelectedItemPosition() - 1 >= 0) {
                Fehlergrad = spnFehlergrad.getSelectedItemPosition();
                spnFehlergrad.setSelection(0);
            }
            Double breit = null;
            Double lang = null;
            if (_main.locCoarse() != null) {
                breit = _main.locCoarse().getLatitude();
                lang = _main.locCoarse().getLongitude();
            } else {
                breit = null; //50d;
                lang = null; //8d;
            }

            if (chkEmergency.isChecked() && breit != null && lang != null) {
                txt += "\n" + getString(R.string.Breitengrad) + ": " + breit;
                txt += "\n" + getString(R.string.Laengengrad) + ": " + lang;
                String url = String.format(Locale.ENGLISH, HTTP_MAPS_GOOGLE_DE_MAPS_Q_S_S_S_OM_0, breit, lang, URLEncoder.encode(_main.user.getString(BENUTZERNAME), UTF_8));
                url = A_HREF + url + GOOGLE_MAPS_A;
                txt += "\n" + url;
            }
            boolean oeffentlich = btnOeffentlich.isChecked();
            if (chkVote.isChecked()) {
                txt = VOTE + txt;
            }
            q = new Question(_main, ID, FrageID, txt, _main.user.getLong(Constants.id), _main.user.getString(BENUTZERNAME), _main.user.getString(EMAIL), spnGrades.getSelectedItemPosition() + 1, d1, cal.getTime(), radius, lang, breit, true, oeffentlich, Fehlergrad, chkEmergency.isChecked(), _main.onlineState);
            JSONObject IDs = _main.clsHTTPS.question(q.ID, q.FrageID, q.BenutzerID, q.EMail, q.Benutzername, q.Frage, q.DatumStart, q.DatumEnde, q.Kontaktgrad, q.RadiusKM, q.Laengengrad, q.Breitengrad, q.oeffentlich, q.Fehlergrad, q.emergency);
            cal.setTime(d1);
            q.ID = IDs.getLong(BENUTZERFRAGEID);
            q.FrageID = IDs.getLong(FRAGEID);
            q.Bewertungen = IDs.getInt(BEWERTUNGEN);
            q.Antworten = IDs.getInt(ANTWORTEN);
            q.Fragen = IDs.getInt(FRAGEN);
            q.Antwortzeit = IDs.getInt(ANTWORTZEIT);
            q.updateUser(_main.user);
            if (Fehlergrad != null) {
                try {
                    String Answer = getFehlerInfo(getActivity(), q.BenutzerID);
                    JSONObject o = _main.clsHTTPS.answer(q.ID, q.BenutzerID, q.oeffentlich, _main.user.getLong(Constants.id), _main.user.getString(EMAIL), _main.user.getString(BENUTZERNAME), Answer, q.FrageID, q.Kontaktgrad, q.Breitengrad, q.Laengengrad, true, true);
                    Answer = ((LogWrapper) Log.getLogNode()).getErrors(10);
                    if (Answer != null && Answer.length() > 0) {
                        int sub = 1024;
                        while ((StringEscapeUtils.escapeJava(Answer).length()) > 1024) {
                            int beginIndex = Answer.length() - sub;
                            Answer = Answer.substring(beginIndex);
                            sub--;
                        }
                        o = _main.clsHTTPS.answer(q.ID, q.BenutzerID, q.oeffentlich, _main.user.getLong(Constants.id), _main.user.getString(EMAIL), _main.user.getString(BENUTZERNAME), Answer, q.FrageID, q.Kontaktgrad, q.Breitengrad, q.Laengengrad, true, true);
                    }
                } catch (Throwable ex) {
                    Log.e(TAG, null, ex);
                }
            }
            this.ID = -1l;
            this.FrageID = -1l;
            txtQuestion.setText("");
            _main.mPager.setCurrentItem(fragQuestions.fragID);
            fragQuestions fragQuestions = (de.com.limto.limto1.fragQuestions) _main.fPA.findFragment(de.com.limto.limto1.fragQuestions.fragID);
            if (fragQuestions != null) {
                if (fragQuestions.btnGeloescht.isChecked() || fragQuestions.btnArchiviert.isChecked() || fragQuestions.btnKontakte.isChecked()) {
                    fragQuestions.btnEigene.performClick();
                } else {
                    if (qold != null && fragQuestions.questions.contains(qold))
                        fragQuestions.questions.remove(qold);
                    q.online = true;
                    int pos = fragQuestions.findPos(q.ID);
                    if (pos > -1) {
                        fragQuestions.questions.remove(pos);
                    }
                    fragQuestions.questions.add(0, q);
                    fragQuestions.adapter.notifyDataSetChanged();
                    fragQuestions.lv.collapseall();
                }
                fragQuestions.lv.setSelection(fragQuestions.findPos(q.ID));
            }
            qold = null;
            chkEmergency.setChecked(false);
            return true;
        } catch (Throwable e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
            String msg = e.getMessage();
            if (msg != null && msg.toLowerCase().contains(DUPLICATE))
                msg = getString(R.string.duplicate);
            if (txtQuestion != null) txtQuestion.setError(msg);
            return false;
        } finally {
            if (p != null) {
                p.dismiss();
                p.cancel();
            }
        }
    }

    private boolean checkLineCount(String txt, int lines) {
        int length = txt.length();
        if (length < 10) return true;
        int linebreaks = txt.split("\\n").length;
        if (lines > 1 && length / linebreaks < 5) return true;
        return  false;
    }

    public static String getFehlerInfo(Activity activity, Long BenutzerID) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        String Answer = BENUTZER_ID + ":" + BenutzerID + "_" + BuildConfig.VERSION_NAME + "_" + BuildConfig.VERSION_CODE + "_" + Build.VERSION.CODENAME + "_" + Build.VERSION.SDK_INT + "_" + Build.MANUFACTURER + "_" + Build.MODEL + "\n";
        Answer += DENSITY + metrics.density + DPI + metrics.densityDpi;
        Answer += SCALEDDENSITY + metrics.scaledDensity;
        Answer += WIDTH + metrics.widthPixels;
        Answer += HEIGHT + metrics.heightPixels;
        String lastError = ((LogWrapper) Log.getLogNode()).lastError;
        if (lastError != null) Answer += "\n" + lastError;
        return Answer;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void init(Question q) {
        String txt = q.getFrage(true);
        if (txt.startsWith(VOTE)) {
            chkVote.setChecked(true);
            txt = txt.substring(VOTE.length());
        } else {
            chkVote.setChecked(false);
        }
        this.txtQuestion.setText(txt);
        this.spnEntf.setSelection(lib.getIndex(spnEntf, q.RadiusKM + getString(R.string._km), this.getContext()));
        this.spnGrades.setSelection(lib.getIndex(spnGrades, getString(R.string.Grad_) + q.Kontaktgrad, this.getContext()));

        Date date = q.DatumEnde; // your date
        setSelectedLaufzeitDate(date);
        /*Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        try
        {
            cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
            cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
            cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
            if (dtEnd.getMinDate()>cal.getTimeInMillis()) dtEnd.setMinDate(cal.getTimeInMillis());

            cal.add(Calendar.YEAR,1);
            cal.add(Calendar.MONTH,1);
            cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
            cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
            cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
            cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
            this.dtEnd.setMaxDate(cal.getTimeInMillis());
            this.dtEnd.updateDate(year, month, day);

        }
        catch (Throwable ex)
        {
            Log.e(TAG, null, ex);
            ex.printStackTrace();
        }
         */
        this.ID = q.ID;
        this.FrageID = q.FrageID;
        this.qold = q;
        if (q.oeffentlich != null && q.oeffentlich) {
            this.btnOeffentlich.setChecked(true);
        } else {
            this.btnPrivat.setChecked(true);
        }
        if (q.items.size() > 0) {
            lib.ShowMessage(getContext(), getString(R.string.newquestion), getString(R.string.question));
        }

        if (q.Fehlergrad != null) spnFehlergrad.setSelection(q.Fehlergrad);
        else spnFehlergrad.setSelection(0);
    }

    public Date getSelectedLaufzeitDate() {

        int[] arr = getResources().getIntArray(R.array.laufzeitvalues);
        cal.add(Calendar.DAY_OF_YEAR, arr[spnLaufzeit.getSelectedItemPosition()]);
        return cal.getTime();
    }

    public void setSelectedLaufzeitDate(Date d) {
        //Calendar cal = Calendar.getInstance();
        Date startDate = cal.getTime();
        long startTime = startDate.getTime();
        long endTime = d.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);

        int[] arr = getResources().getIntArray(R.array.laufzeitvalues);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= diffDays) {
                spnLaufzeit.setSelection(i);
                break;
            }
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
