package de.com.limto.limto1.Chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.DialogFragment;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Debug;
import android.os.Handler;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.UUID;

import de.com.limto.limto1.BuildConfig;
import de.com.limto.limto1.Constants;
import de.com.limto.limto1.ContactVO;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;
import de.com.limto.limto1.dlgLogin;
import de.com.limto.limto1.lib.LetterTileProvider;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.MainActivity.BENUTZERNAME;
import static de.com.limto.limto1.MainActivity.GRADE;
import static de.com.limto.limto1.MainActivity.blnAccessibility;
import static de.com.limto.limto1.MainActivity.nodialogs;
import static de.com.limto.limto1.clsHTTPS.TRUE;
import static de.com.limto.limto1.lib.lib.*;

public class dlgChat extends DialogFragment {

    private static final String TAG = "dlgChat";
    public static ArrayList<JSONObject> pendingRead = new ArrayList<>();
    public EditText editText;
    public MessageAdapter messageAdapter;
    public MainActivity _main;
    public Context context;
    public Long BenutzerID = -1l;
    public MemberData dataSelf;
    public LinkedHashMap<Long, MemberData> MemberData;
    public boolean isvisible = false;
    public ArrayList<Long> BlockedUsers = new ArrayList<Long>();
    String NameEmpfaenger;
    ArrayList<Message> messagesbak = new ArrayList<>();
    // replace this with a real channelID from Scaledrone dashboard
    private String channelID = "CHANNEL_ID_FROM_YOUR_SCALEDRONE_DASHBOARD";
    private String roomName = "observable-room";
    private ListView messagesView;
    private Long BenutzerIDEmpfaenger = -1l;
    private String Name;
    private FloatingActionButton btnSend;
    private Message pendingMessage;
    private RadioGroup radioGroupChat;
    private HorizontalScrollView svChat;
    private TextView txtUser;
    private FloatingActionButton ivClose;
    private FloatingActionButton btnAttach;
    private String mimetype;
    private String path;
    private String fname;
    private long size;
    private UUID uuid;
    private String thumb = null;
    private Bitmap thumbBitmap = null;
    private boolean sendAttachmentMessage;
    private boolean lastWasThumb;
    private CharSequence message;
    private Message messagewrite;
    private AlertDialog dlg;
    private FloatingActionButton btnoption;
    private RadioButton selectedRadioButton;
    private boolean loaded;
    private boolean rbtnTouched;
    private Runnable runnableRemoveMessageWrite = (new Runnable() {
        @Override
        public void run() {
            if (messagewrite != null) {
                try {
                    removeMessageWrite();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    });
    private ArrayList<Long> shownUsers = new ArrayList<>();
    private ArrayList<UUID> shownUUIDs = new ArrayList<>();
    private AlertDialog dlgOther;
    private long lastOther;
    private boolean rbtnTouched2;

    public dlgChat() {
        getMainActivity();
        messageAdapter = new MessageAdapter(this);
        MemberData = new LinkedHashMap<Long, MemberData>();
        dataSelf = null;
    }

    public static String getRandomName() {
        String[] adjs = {"autumn", "hidden", "bitter", "misty", "silent", "empty", "dry", "dark", "summer", "icy", "delicate", "quiet", "white", "cool", "spring", "winter", "patient", "twilight", "dawn", "crimson", "wispy", "weathered", "blue", "billowing", "broken", "cold", "damp", "falling", "frosty", "green", "long", "late", "lingering", "bold", "little", "morning", "muddy", "old", "red", "rough", "still", "small", "sparkling", "throbbing", "shy", "wandering", "withered", "wild", "black", "young", "holy", "solitary", "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine", "polished", "ancient", "purple", "lively", "nameless"};
        String[] nouns = {"waterfall", "river", "breeze", "moon", "rain", "wind", "sea", "morning", "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn", "glitter", "forest", "hill", "cloud", "meadow", "sun", "glade", "bird", "brook", "butterfly", "bush", "dew", "dust", "field", "fire", "flower", "firefly", "feather", "grass", "haze", "mountain", "night", "pond", "darkness", "snowflake", "silence", "sound", "sky", "shape", "surf", "thunder", "violet", "water", "wildflower", "wave", "water", "resonance", "sun", "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper", "frog", "smoke", "star"};
        return (
                adjs[(int) Math.floor(Math.random() * adjs.length)] +
                        "_" +
                        nouns[(int) Math.floor(Math.random() * nouns.length)]
        );
    }

    public static String getRandomColor() {
        Random r = new Random();
        StringBuffer sb = new StringBuffer("#");
        while (sb.length() < 7) {
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, 7);
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        try {
            saveMessages(false, _main.getBenutzerID());
        } catch (Throwable e) {
           lib.ShowException(TAG, getContext(),e, true);
        }
        isvisible = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        isvisible = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isvisible = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_Dialog_dlgchat);
        getMainActivity();
    }

    private void getMainActivity() {
        if (_main == null) {
            _main = (MainActivity) getActivity();
            if (_main != null) context = _main;
        }
        if (_main == null || _main.user == null) {
            if (this.isAdded()) {
                ShowMessage(this.getContext(), getString(R.string.notloggedin), getString(R.string.message));
                this.dismiss();
            }
            return;
        }
        messageAdapter._main = _main;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(getLayoutResFromTheme(_main, R.attr.dlgchat), container, false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View v, @Nullable final Bundle savedInstanceState) {

        super.onViewCreated(v, savedInstanceState);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getMainActivity();
        findViews(v);
        try {
            initMessagesAndUserData();
            setListeners();
        } catch (Throwable e) {
            e.printStackTrace();
            lib.ShowException(TAG, getContext(), e, false);
        }

        _main.messagesShown();

        addPendingMessage();

        isvisible = true;
    }

    private void addPendingMessage() {
        if (messagesView != null && pendingMessage != null) {
            messageAdapter.add(false, pendingMessage, true);
            messageAdapter._main = _main;
            // messagesView.setSelection(messagesView.getCount() - 1);
            pendingMessage = null;
        }
    }

    private void initMessagesAndUserData() throws Throwable {
        try {
            loadMessages(false, _main.user.getLong(Constants.id));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        if ((dataSelf == null || dataSelf.getBenutzerid() == null) && _main != null && _main.user != null) {
            initUserDataSelf(_main.user);
        } else if (dataSelf != null) {
            initUserDataSelfFromData(dataSelf);
        } else {
            throw new Exception("UserData not found!");
        }
        initMemberDataEmpfaenger(BenutzerIDEmpfaenger, NameEmpfaenger);
        setBenutzerIDEmpfaenger(BenutzerIDEmpfaenger, false);

    }

    private void setListeners() {

        setClickListenerOptions(btnoption);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendMessage(BenutzerIDEmpfaenger);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SelectFile(_main, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        addListenersEditText(editText);

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dlgChat.this.dismiss();
                } catch (Throwable ex) {
                    lib.ShowException(TAG, getContext(), ex, true);
                }
            }
        });

        setCheckedChangedListenerRadioGroupChat(radioGroupChat);


    }

    private void setCheckedChangedListenerRadioGroupChat(RadioGroup rgChat) {
        rgChat.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Object o = group.findViewById(checkedId);
                RadioButton btn = (RadioButton) o;
                if (btn == null) return;
                if (btn.isChecked()) {
                    btn.setPaintFlags(btn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG | Paint.FAKE_BOLD_TEXT_FLAG);
                }
                for (int i = 0; i < group.getChildCount(); i++) {
                    RadioButton b = (RadioButton) group.getChildAt(i);
                    if (!(b == btn))
                        b.setPaintFlags(b.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG & ~Paint.FAKE_BOLD_TEXT_FLAG);
                }

            }
        });
    }

    private void addListenersEditText(EditText editText) {

        editText.addTextChangedListener(new TextWatcher() {
            private int ccount;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //here is your code
                ccount++;
                if (ccount % 5 == 0) {
                    try {
                        uuid = UUID.randomUUID();
                        String res = _main.clsHTTPS.sendMessage(BenutzerIDEmpfaenger, "⸙", uuid, false, _main);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

    }

    private void initMemberDataEmpfaenger(Long benutzerIDEmpfaenger, String nameEmpfaenger) throws Throwable {
        if (!MemberData.containsKey(BenutzerIDEmpfaenger) && BenutzerIDEmpfaenger > -1) {
            MemberData d = new MemberData(NameEmpfaenger, Name, getRandomColor(), BenutzerIDEmpfaenger, BenutzerID, null);
            MemberData.put(BenutzerIDEmpfaenger, d);
            insertMember(d);
        }
    }

    private void initUserDataSelfFromData(MemberData data) throws Throwable {
        Name = data.getName();
        NameEmpfaenger = data.getNameEmpfaenger();
        BenutzerID = data.getBenutzerid();
        if (BenutzerID != null && BenutzerID > -1 && !MemberData.containsKey(BenutzerID)) {
            MemberData.put(BenutzerID, data);
            insertMember(data);
        }
    }

    private void initUserDataSelf(JSONObject user) throws Throwable {
        Name = user.getString(BENUTZERNAME);
        BenutzerID = user.getLong(Constants.id);
        if (BenutzerID > -1) {
            dataSelf = new MemberData(Name, NameEmpfaenger, getRandomColor(), BenutzerID, BenutzerIDEmpfaenger, null);
            if (!MemberData.containsKey(BenutzerID)) {
                MemberData.put(BenutzerID, dataSelf);
                insertMember(dataSelf);
            }
        }
    }

    private void setClickListenerOptions(FloatingActionButton btnoption) {
        btnoption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PopupMenu popup = new PopupMenu(context, btnoption);
                    //Inflating the Popup using xml file
                    popup.getMenuInflater()
                            .inflate(R.menu.mnupopupchat, popup.getMenu());
                    MenuItem mnuAdd = popup.getMenu().findItem(R.id.mnuAdd);
                    MemberData m = (MemberData) selectedRadioButton.getTag();
                    if (m.getKontaktgrad() > 1 || m.getKontaktgrad() < 0) mnuAdd.setVisible(true);
                    else mnuAdd.setVisible(false);
                    setPopupMenuItemClickListener(popup);
                    popup.show();
                } catch (Throwable ex) {
                    lib.ShowException(TAG, getContext(), ex, true);
                }
            }

            private void setPopupMenuItemClickListener(PopupMenu popup) {
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        try {
                            MemberData m = (MemberData) selectedRadioButton.getTag();
                            if (item.getItemId() == R.id.mnuAdd) {
                                addContact(m);
                            } else if (item.getItemId() == R.id.menu_delhist) {
                                deleteHistory(m);
                            } else if (item.getItemId() == R.id.mnu_close) {
                                dlgChat.this.dismiss();
                            } else {
                                blockUser(m, item);
                            }
                        } catch (Throwable ex) {
                            lib.ShowException(TAG, getContext(), ex, true);
                        }
                        return true;
                    }
                });
            }
        });

    }

    private void blockUser(MemberData m, MenuItem item) {
        if (item.getItemId() == R.id.mnuBlockChat) {
            BlockedUsers.add(m.getBenutzerid());
        } else if (item.getItemId() == R.id.mnuBlockAll) {
            try {
                _main.clsHTTPS.addSperre(_main.getBenutzerID(), m.getBenutzerid());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        radioGroupChat.removeView(selectedRadioButton);
        MemberData.remove(m.getBenutzerid());
        try {
            saveMessages(false, _main.getBenutzerID());
        } catch (Throwable throwable) {
            lib.ShowException(TAG, getContext(), throwable,true);
        }
    }

    private void deleteHistory(MemberData m) {
        messageAdapter.clearmessages(BenutzerIDEmpfaenger);
        messageAdapter.notifyDataSetChanged();
        try {
            dlg = lib.ShowMessageYesNo(context, _main.getString(R.string.alsodeletememberhistory),
                    _main.getString(R.string.Chat), false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                dlg = null;
                                MemberData.clear();
                                messageAdapter.clearmessages(null);
                                radioGroupChat.removeViews(0, radioGroupChat.getChildCount());
                                dataSelf.setBenutzeridEmpfaenger(dataSelf.getBenutzerid());
                                initUserDataSelfFromData(dataSelf);
                                insertMember(dataSelf);
                                selectRadioButton(dataSelf.getBenutzerid());
                                //dlgChat.this.saveMessages(true, BenutzerID);
                            } catch (Throwable throwable) {
                                lib.ShowException(TAG, getContext(), throwable, true);
                            }
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dlg = null;
                        }
                    }, null);
            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    dlg = null;
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private void addContact(MemberData m) {
        try {
            _main.clsHTTPS.addContact(_main.getBenutzerID(), m.getBenutzerid(), false);
            m.setKontaktgrad(1);
            m.getContactVO().setKontaktgrad(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViews(View v) {
        editText = (EditText) v.findViewById(R.id.editText);
        btnSend = (FloatingActionButton) v.findViewById(R.id.btnSend);
        btnAttach = (FloatingActionButton) v.findViewById(R.id.btnAttach);
        radioGroupChat = (RadioGroup) v.findViewById(R.id.rg_chat_members);
        svChat = (HorizontalScrollView) v.findViewById(R.id.chat_scrollview);
        messagesView = (ListView) v.findViewById(R.id.messages_view);
        txtUser = (TextView) v.findViewById(R.id.txtUser);
        ivClose = (FloatingActionButton) v.findViewById(R.id.ivCloseButton);
        ivClose.setVisibility(View.GONE);
        ivClose = (FloatingActionButton) v.findViewById(R.id.ivClose);
        ivClose.setVisibility(View.VISIBLE);
        messagesView.setAdapter(messageAdapter);
        btnoption = (FloatingActionButton) v.findViewById(R.id.btnoption);
    }

    public void setEmpfaenger(Long benutzerid, String name) {
        setEmpfaenger(benutzerid, name, false);
    }

    public void setEmpfaenger(Long BenutzerID, String Name, boolean checkOnly) {
        this.setBenutzerIDEmpfaenger(BenutzerID, checkOnly);
        this.NameEmpfaenger = Name;
        if (dataSelf != null) dataSelf.setNameEmpfaenger(Name);
    }

    public void sendMessage(Long BenutzerIDEmpfaenger) throws Exception {
        SpannableStringBuilder message = (SpannableStringBuilder) editText.getText();
        if (message.length() > 0) {
            Date time = Calendar.getInstance().getTime();
            UUID uuid = UUID.randomUUID();
            String res = _main.clsHTTPS.sendMessage(BenutzerIDEmpfaenger, message.toString(), uuid, false, _main);
            if (res.equals(TRUE)) {
                //editText.getText().clear();
                Message Message = onMessage(null, uuid, BenutzerIDEmpfaenger, Name, new SpannableString(message), getRandomColor(), time, true, true);
                if (Message != null) {
                    Message.setUUID(uuid);
                    Message.setSentToServer(true);
                } else {
                    lib.ShowMessage(getContext(), getString(R.string.couldnotinsertmessage), getString(R.string.Error));
                }
                messageAdapter.notifyDataSetChanged();
            } else {
                ShowToast(_main, res);
            }
        }
    }

    public void sendAttachment(Long BenutzerIDEmpfaenger, CharSequence _message, final String mimetype, String path, final String strfile, long size, UUID uuid, String thumb) throws Throwable {
        CharSequence m = new File(path).getName();
        CharSequence message = "";
        if (_message != null && _message.length() > 0)
            message = _message;
        if (m.length() > 0) {
            Date time = Calendar.getInstance().getTime();

            JSONObject o = createJSONObjectAttachment(message, path, strfile, mimetype, size, uuid);

            lib.Ref<Bitmap> ThumbImage = new lib.Ref<>(null);

            InputStream inputStream = getInputStream(o, thumb, ThumbImage);

            sendAttachmentMessage = uploadAttachment(inputStream, strfile, this);

            if (sendAttachmentMessage) {
                sendAttachmentMessage(o, time, ThumbImage);
            }
        }
    }

    private void sendAttachmentMessage(JSONObject o, Date time, Ref<Bitmap> ThumbImage) throws Exception {
        String res = _main.clsHTTPS.sendMessage(BenutzerIDEmpfaenger, "(JSONObject)" + o.toString(), uuid, false, _main);
        if (res == TRUE) {
            Message Message = onMessage(uuid, BenutzerIDEmpfaenger, Name, "(JSONObject)" + o.toString(), getRandomColor(), time, true, true);
            if (Message != null) {
                Message.setUUID(uuid);
                Message.setSentToServer(true);
                Message.setThumb(ThumbImage.get() != null ? ThumbImage.get() : this.thumbBitmap);
            }
            messageAdapter.notifyDataSetChanged();
        } else {
            ShowToast(_main, res);
        }
    }

    private boolean uploadAttachment(InputStream inputStream, String strfile, dlgChat recallDlgChat) throws Throwable {
        if (!lastWasThumb && inputStream != null) {
            sendAttachmentMessage = false;
            lastWasThumb = true;
            _main.clsHTTPS.upload(context, inputStream, "thumb_" + strfile, _main.getBenutzerID(), recallDlgChat);
        } else {
            lastWasThumb = false;
            sendAttachmentMessage = true;
        }
        return sendAttachmentMessage;
    }

    private InputStream getInputStream(JSONObject o, String thumb, Ref<Bitmap> ThumbImage) throws Throwable {
        InputStream inputStream = null;
        if (thumb != null) {
            o.put("thumb", thumb);
            inputStream = _main.getContentResolver().openInputStream(Uri.parse(thumb));
            ThumbImage.set(BitmapFactory.decodeStream(inputStream));
            inputStream.close();
            inputStream = _main.getContentResolver().openInputStream(Uri.parse(thumb));
        } else if (this.thumbBitmap != null) {
            o.put("thumb", "bitmap");
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final Bitmap bmp = this.thumbBitmap;
            dlgLogin.UserLoginTask.myRunnableCompressBM myRunnable = new dlgLogin.UserLoginTask.myRunnableCompressBM(out, bmp);
            _main.runOnUiThread(myRunnable);
            synchronized (myRunnable) {
                while (myRunnable.needWait) myRunnable.wait();
            }
            inputStream = new ByteArrayInputStream(out.toByteArray());
        }
        return inputStream;
    }

    private JSONObject createJSONObjectAttachment(CharSequence message, String path, String strfile, String mimetype, long size, UUID uuid) throws JSONException {
        JSONObject o = new JSONObject();
        o.put("message", message);
        o.put("path", path);
        o.put("file", strfile);
        o.put("mimetype", mimetype);
        o.put("size", size);
        o.put("uuid", uuid);
        return o;
    }

    public void backupMessages() {
        messageAdapter.backupMessages();
    }

    public void restoreMessagesFromBackup() throws Exception {
        messageAdapter.restoreMessagesFromBackup();
    }

    public Message insertObject(JSONObject o) throws Throwable {
        return messageAdapter.insertObject(o);

    }

    public Message onMessage(final UUID uuid, final Long BenutzerID, final String name, final String Message, final String color, final Date time, final boolean self, boolean sentToServer) {
        return onMessage(null, uuid, BenutzerID, name, new SpannableString(Message), color, time, self, sentToServer);
    }

    public Message onMessage(Message mm) {
        return onMessage(mm, mm.getUUID(), mm.getBenutzerID(), mm.getBenutzerName(), new SpannableString(mm.getText()), dlgChat.getRandomColor(), mm.getTime(), mm.getSelf(), mm.getSentToServer());
    }

    //ToDo überarbeiten
    public Message onMessage(Message mm, final UUID uuid, final Long BenutzerID, final String name, final SpannableString Message, final String color, final Date time, final boolean self, final boolean sentToServer) {

        getMainActivity();


        if (checkNull(BenutzerID, uuid, name, Message, color, time, self, sentToServer))
            return null;

        try {
            boolean unequal = BenutzerID.longValue() != BenutzerIDEmpfaenger.longValue();

            //if (Message.startsWith("selfxx") && !self) belongsToCurrentUser = false;
            MemberData data;
            if (mm != null && mm.getMemberData() != null) {
                data = mm.getMemberData();
                if (getContext() != null && unequal) {
                    newMessageFromOtherUser(Message, name, data, BenutzerID);
                } else {
                    setBenutzerIDEmpfaenger(BenutzerID, false);
                    setNameEmpfaenger(name);
                }
            } else {
                data = getMemberData(mm, uuid, BenutzerID, name, Message, color, time, self, sentToServer);
            }

            if (Message.toString().equals("⸙")) {
                setMessageWrite(mm, data, uuid, BenutzerID, name, color, time, self);
                removeMessageWriteAfter5000ms();
            } else if (data != null) {
                removeMessageWrite();
                mm = addNewMessageAndSave(mm, data, uuid, BenutzerID, Message, time, self);
                if (sentToServer) mm.setSentToServer(sentToServer);
                return mm;
            } else {
                throw new Exception("MemberData null!");
            }

            return null;
        } catch (Exception e) {
            e.printStackTrace();
            ShowException(TAG, context, e, false);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    private boolean checkNull(Long BenutzerID, UUID uuid, String name, SpannableString Message, String color, Date time, boolean self, boolean sentToServer) {

        if (BenutzerID < 0) {
            ShowMessage(_main, _main.getString(R.string.invalid_userid), _main.getString(R.string.Error));
            return true;
        }
        if (BlockedUsers.contains(BenutzerID)) return true;
        //call again if dialog is not initialized
        if ((getContext() == null && isAdded()) || dlg != null) {
            if (_main != null) {
                callAgainOnMessageInOneSecond(null, uuid, BenutzerID, name, Message, color, time, self, sentToServer);
            }
            return true;
        }
        return false;
    }

    private void removeMessageWriteAfter5000ms() {
        try {
            _main.mHandler.removeCallbacks(runnableRemoveMessageWrite);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        _main.mHandler.postDelayed(runnableRemoveMessageWrite, 5000);
    }

    private void removeMessageWrite() throws Exception {
        if (messagewrite != null) {
            messageAdapter.remove(messagewrite);
            messagewrite = null;
            messageAdapter.notifyDataSetChanged();
        }

    }

    private Message addNewMessageAndSave(Message mm, dlgChat.MemberData data, UUID uuid, Long BenutzerID, SpannableString message, Date time, boolean self) throws Exception {
        if (data == null || (mm != null && mm.getMemberData() == null)) {
            System.out.println("Data null");
            return mm;
        }
        if (mm == null) mm = new Message(_main, this, message, data, time, uuid, self);

        MemberData memberDataRadiobutton = null;
        if (selectedRadioButton != null) {
            memberDataRadiobutton = (MemberData) selectedRadioButton.getTag();
        }

        boolean radiobuttonSelected = memberDataRadiobutton != null
                && memberDataRadiobutton.getBenutzerid().longValue()
                == mm.getMemberData().getBenutzerid().longValue();

        if (!(isAdded() && isvisible && this.isVisible() && !isHidden())) {
            mm.getMemberData().incCountMessages();
        } else {
            if (!radiobuttonSelected) {
                // mm.getMemberData().incCountMessages();
                try {
                    insertMember(mm.getMemberData());
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }

        boolean sentToMySelf = BenutzerID.longValue() == _main.getBenutzerID().longValue();
        if (messagesView != null) {
            messageAdapter.add(sentToMySelf, mm, true);
            // messagesView.setSelection(messagesView.getCount() - 1);
        } else {
            messageAdapter.add(sentToMySelf, mm, false);
        }
        mm.setUUID(uuid);
        try {
            saveMessages(false, _main.getBenutzerID());
        } catch (Throwable e) {
            lib.ShowException(TAG, getContext(), e, true);
        }
        return mm;
    }

    private void setMessageWrite(Message mm, MemberData data, UUID uuid, Long BenutzerID, String name, String color, Date time, boolean self) {
        if (messagewrite == null) {
            if (data == null)
                data = new MemberData(name, name, color, BenutzerID, BenutzerID, null);
            messagewrite = new Message(_main, this, new SpannableString("."), data, time, uuid, self);
            if (messagesView != null) {
                messageAdapter.add(self, messagewrite, true);
                // messagesView.setSelection(messagesView.getCount() - 1);
            } else {
                messageAdapter.add(self, messagewrite, false);
            }
        } else {
            messagewrite.addMessage(new SpannableString("."));
            messagewrite.setMemberData(data);
            messagewrite.setTime(time);
            // messagewrite.setUUID(uuid);
            messagewrite.setSelf(self);
            messageAdapter.notifyDataSetChanged();

        }
    }

    private dlgChat.MemberData getMemberData(final Message mm, final UUID uuid, final Long BenutzerID, final String name, final SpannableString Message, final String color, final Date time, final boolean self, final boolean sentToServer) throws Throwable {
        MemberData data;
        if (!self) {
            {
                if ((!MemberData.containsKey(BenutzerID))) {
                    data = newChatMember(mm, uuid, BenutzerID, name, Message, color, time, self, sentToServer);
                    if (data == null) return null; //Grade > 1
                } else {
                    data = MemberData.get(BenutzerID);
                    data.setNameEmpfaenger(name);
                    data.setBenutzeridEmpfaenger(BenutzerID);
                }
            }
            if (getContext() != null && this.BenutzerIDEmpfaenger.longValue() != BenutzerID.longValue()) {
                newMessageFromOtherUser(Message, name, data, BenutzerID);
            } else {
                setBenutzerIDEmpfaenger(BenutzerID, false);
                setNameEmpfaenger(name);
            }
        } else {
            if (!MemberData.containsKey(BenutzerID)) {
                data = new MemberData(name, NameEmpfaenger, color, BenutzerID, BenutzerIDEmpfaenger, null);
                MemberData.put(BenutzerID, data);
                insertMember(data);
            } else {
                data = new MemberData(name, NameEmpfaenger, color, BenutzerID, BenutzerIDEmpfaenger, null);
                data.setNameEmpfaenger(NameEmpfaenger);
                data.setBenutzeridEmpfaenger(BenutzerIDEmpfaenger);
            }
            setBenutzerIDEmpfaenger(BenutzerIDEmpfaenger, false);

        }
        return data;
    }

    MemberData newChatMember(Message message, final UUID uuid, Long BenutzerID, String name, SpannableString Message, String color, Date time, boolean self, boolean sentToServer) throws Throwable {
        final int Grad = _main.getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);
        int k = _main.clsHTTPS.getKontaktgrad(Grad, _main.getBenutzerID(), BenutzerID);
        if (k > 1 || k < 0) {
            UserWithGradeGreaterOneWantsToChat(message, uuid, BenutzerID, name, Message, color, time, self, k, sentToServer);
            return null;
        }
        MemberData data = new MemberData(name, name, color, BenutzerID, BenutzerID, null);
        MemberData.put(BenutzerID, data);
        insertMember(data);
        if (message != null) message.setMemberData(data);
        return data;
    }

    private void newMessageFromOtherUser(SpannableString message, String name, MemberData data, Long BenutzerID) throws Throwable {
        if (getContext() == null || !isVisible())
        {
            _main.mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        newMessageFromOtherUser(message, name, data, BenutzerID);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                }
            }, 500);
        }
        else {
            if (!(message.toString().startsWith("⸙"))) {
                data.incCountMessages();
                if (!nodialogs && getContext() != null && dlgOther == null || lastOther != BenutzerID) {
                    lastOther = BenutzerID;
                }
                dlgOther = lib.ShowMessageYesNo(getContext(), getString(R.string.newmessage, name, name), getString(R.string.message), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setBenutzerIDEmpfaenger(BenutzerID, false);
                        setNameEmpfaenger(name);
                        dlgOther = null;
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dlgOther = null;
                    }
                }, null);
                dlgOther.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        dlgOther = null;
                    }
                });

            }
            insertMember(data);
        }
    }

    private void UserWithGradeGreaterOneWantsToChat(Message message, final UUID uuid, Long BenutzerID, String name, SpannableString Message, String color, Date time, boolean self, int k, boolean sentToServer) throws Throwable {
        if (Message.toString().equals("⸙")) {
            return;
        }
        if (!BlockedUsers.contains(BenutzerID) && !shownUsers.contains(uuid) && (this.isVisible() || isvisible) && !nodialogs && dlg == null && getContext() != null) {
            {
                shownUsers.add(BenutzerID);
                dlg = lib.ShowMessageYesNo(getContext(), _main.getString(R.string.ChatGradGreater1user, name, "" + k), _main.getString(R.string.Chat), false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        createNewMemberAndShowMessage(message, uuid, BenutzerID, name, Message, color, time, self, k, sentToServer);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (message != null) message.setMemberData(null);
                        dlg = null;
                        try {
                            lib.ShowMessageYesNo(getContext(), _main.getString(R.string.BlockUser, name), _main.getString(R.string.Chat), false, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    BlockedUsers.add(BenutzerID);
                                    if (message.getMemberData() != null)
                                        message.getMemberData().getContactVO().setBlocked(true);
                                }
                            }, null, null);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                }, null);
                dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dlg = null;
                        if (message != null) message.setMemberData(null);
                    }
                });
            }
            // dlg.show();
        } else if (message == null) {
            // callAgainUserWGInOneSecond(message, uuid, BenutzerID, name, Message, color, time, self, k, sentToServer);
            callAgainOnMessageInOneSecond(message, uuid, BenutzerID, name, Message, color, time, self, sentToServer);
        } else {
            shownUUIDs.add(uuid);
            //selectRadioButton(BenutzerID);
            if (!shownUsers.contains(BenutzerID)) {
                callAgainUserWGInOneSecond(message, uuid, BenutzerID, name, Message, color, time, self, k, sentToServer);
            } else {
                callAgainOnMessageInOneSecond(message, uuid, BenutzerID, name, Message, color, time, self, sentToServer);
            }
        }
        return;
    }

    private void callAgainUserWGInOneSecond(final Message message, final UUID uuid, final Long BenutzerID, final String name, final SpannableString Message, final String color, final Date time, final boolean self, int k, final boolean sentToServer) {
        _main.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    UserWithGradeGreaterOneWantsToChat(message, uuid, BenutzerID, name, Message, color, time, self, k, sentToServer);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }, 1000);
    }

    private void createNewMemberAndShowMessage(Message message, UUID uuid, Long BenutzerID, String name, SpannableString Message, String color, Date time, boolean self, int k, boolean sentToServer) {
        MemberData d = new MemberData(name, name, color, BenutzerID, BenutzerID, null);
        try {
            BlockedUsers.remove(BenutzerID);
            MemberData.put(BenutzerID, d);
            insertMember(d);
            if (message != null) message.setMemberData(d);
            selectRadioButton(BenutzerID);
        } catch (Throwable ex) {
            ex.printStackTrace();
        }

        dlg = null;

        onMessage(message, uuid, BenutzerID, name, Message, color, time, self, false);

    }

    private void callAgainOnMessageInOneSecond(Message mm, final UUID uuid, final Long BenutzerID, final String name, final SpannableString Message, final String color, final Date time, final boolean self, final boolean sentToServer) {
        _main.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onMessage(mm, uuid, BenutzerID, name, Message, color, time, self, sentToServer);
            }
        }, 1000);

    }

    public void saveMessages(boolean clear, long userid)   {
        if (_main == null || messageAdapter._main == null) return;

        if (clear) {
            MemberData.clear();
            dataSelf = null;
        }

        try {
            messageAdapter.saveMessages(clear, userid);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(_main,TAG,"+++saveMessages", throwable);
        }


        try {
            saveMemberData(userid);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(_main,TAG,"+++saveMemberData", throwable);
        }

        try {
            saveSelf(userid);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(_main,TAG,"+++saveSelf", throwable);
        }

        try {
            saveBlocked(userid);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(_main,TAG,"+++saveBlocked", throwable);
        }

        //if (pendingRead.size()>0) {
        try {
            savePendingRead(userid);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(_main,TAG,"+++savePendingRead", throwable);
        }
        //}


    }

    private void saveSelf(long userid) throws Throwable {
        if (dataSelf != null) {
            String strO = new Gson().toJson(dataSelf);
            _main.getPreferences(Context.MODE_PRIVATE).edit().putString("Data" + userid, strO).apply();
        }
    }

    private void saveMemberData(long userid) throws Throwable {
        if (MemberData.size() > 0) {
            String strO = new Gson().toJson(MemberData);
            _main.getPreferences(Context.MODE_PRIVATE).edit().putString("MemberData" + userid, strO).apply();
        }
        else
        {
            _main.getPreferences(Context.MODE_PRIVATE).edit().remove("MemberData" + userid).apply();
        }
    }

    public void saveBlocked(long userid) throws Throwable {
        {
            String strO = new Gson().toJson(BlockedUsers);
            _main.getPreferences(Context.MODE_PRIVATE).edit().putString("BlockedUsers" + userid, strO).apply();
        }
    }

    public void savePendingRead(long userid) throws Throwable {
        String strO = new Gson().toJson(pendingRead);
        _main.getPreferences(Context.MODE_PRIVATE).edit().putString("pendingread" + userid, strO).apply();

    }

    public void loadMessages(boolean clear, long userid) {
        try {
            boolean clear2 = false;

            if (BenutzerID > -1 && this.BenutzerID != userid) {
                saveMessages(false, BenutzerID);
                clear2 = true;
                this.loaded = false;
            }
            {
                clear = clear || clear2;
                this.BenutzerID = userid;

                messageAdapter.loadMessages(clear, userid);

                if (clear2) {
                    MemberData.clear();
                    dataSelf = null;
                }

                loadMemberData(userid);

                loadData(userid);

                loadBlockedUsers(userid);

                loadPendingRead(userid);
            }
            //MemberData.clear();

            initRadioGroupChat();

        } catch (Throwable ex) {
            ShowException(TAG, getContext(), ex, true);
        }
        this.loaded = true;
    }

    private void loadPendingRead(long userid) {
        if (dlgChat.pendingRead == null || dlgChat.pendingRead.size() == 0) {
            String r = _main.getPreferences(Context.MODE_PRIVATE).getString("pendingread" + userid, "");
            if (r.length() > 0) {
                Object o = new Gson().fromJson(r, new TypeToken<ArrayList<JSONObject>>() {
                }.getType());
                dlgChat.pendingRead = (ArrayList<JSONObject>) o;
            }
        }
    }

    private void loadBlockedUsers(long userid) {
        String strO = _main.getPreferences(Context.MODE_PRIVATE).getString("BlockedUsers" + userid, null);
        if (strO != null && BlockedUsers.size() == 0) {
            Object o = new Gson().fromJson(strO, new TypeToken<ArrayList<Long>>() {
            }.getType());
            BlockedUsers = (ArrayList<Long>) o;
        }

    }

    private void loadData(long userid) {
        String strO = _main.getPreferences(Context.MODE_PRIVATE).getString("Data" + userid, null);
        if (strO != null && dataSelf == null) {
            Object o = new Gson().fromJson(strO, new TypeToken<MemberData>() {
            }.getType());
            MemberData data = (dlgChat.MemberData) o;
            if (data.getBenutzerid().longValue() == userid)
            {
                dataSelf = data;
            }
        }
    }

    private void loadMemberData(long userid) {
        String strO = _main.getPreferences(Context.MODE_PRIVATE).getString("MemberData" + userid, null);
        if (strO != null && MemberData.size() == 0) {
            Object o = new Gson().fromJson(strO, new TypeToken<LinkedHashMap<Long, MemberData>>() {
            }.getType());
            MemberData = (LinkedHashMap<Long, MemberData>) o;

        }
    }

    private void initRadioGroupChat() throws Throwable {
        if (radioGroupChat != null && radioGroupChat.getChildCount() == 0) {
            MemberData[] arr = MemberData.values().toArray(new MemberData[0]);
            for (int i = 0; i < arr.length; i++) {

                MemberData m = arr[i];
                boolean foundother = false;

                for (int ii = i + 1; ii < arr.length; ii++) {
                    MemberData mm = arr[ii];
                    if (mm.getBenutzerid().longValue() == m.getBenutzerid().longValue()) {
                        foundother = true;
                        //MemberData.remove(mm);
                        //i = ii;
                        if (foundother) {
                            ShowMessage(_main, "Found double BenutzerID for " + m.toString() + "\n" + mm.toString(), "MemberData");
                        }
                    }
                }

                if (!(m.getBenutzerid() < 0)) {
                    insertMember(m);
                } else {
                    MemberData.remove(m.getBenutzerid());
                }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    RadioButton insertMember(final MemberData m) throws Throwable {

        if (repeatInsertMemberIfNotInitialized(m)) return null;

        boolean isOnline = getOnlineFromContactVO(m);

        int dp40 = dpToPx(40);
        if (blnAccessibility) dp40 *= 1.5f;
        //View v = getLayoutInflater().inflate(R.layout.radiobuttonchat,rgChat, false);
        int pos = 0;
        pos = findRadioButtonChatPos(m);
        AppCompatRadioButton radioButtonForMember = findRadioButtonChat(m);
        if (radioButtonForMember == null) {
            radioButtonForMember = new AppCompatRadioButton(_main);
        } else {

        }
        final AppCompatRadioButton radioButton = radioButtonForMember;

        //TextView txtUser = v.findViewById(R.id.txtUser);
        //b.setWidth(dp40);
        //b.setHeight(dp40);
        //b.setBackgroundColor(Color.parseColor(m.getColor()));
        Bitmap dNumber = null;

        Drawable drNumber = getNumberTileNewMessages(m, dp40);

        Drawable drOnline = getDrawableOnline(dp40, m.getBenutzerid().longValue() == _main.getBenutzerID());

        //drNumber.setBounds((int)(dp40 * (2/3f)), 0, dp40, (int)(dp40 * (2/3f)));

        Drawable drawableCircle = getDrawableCircle(dp40, m);

        //d1.setColorFilter(new PorterDuffColorFilter(Color.parseColor(m.getColor()), PorterDuff.Mode.DST_ATOP));
        //if (drawable != null) d1 = drawable;
        //b.setBackground(drawable);
        Drawable drawableShapeRoundBorder = getDrawableShapeRoundBorder(isOnline, drOnline);


        //d2.setBounds(0,0, dp40, dp40);
        //d2.setAlpha(128);
        Drawable finalDrawable = new LayerDrawable(new Drawable[]{drawableCircle, drawableShapeRoundBorder});
        Drawable finalDrawableOnline = new LayerDrawable(new Drawable[]{drawableCircle, drOnline});
        if (drNumber != null) {
            finalDrawable = new LayerDrawable(new Drawable[]{finalDrawable, drNumber});
            finalDrawableOnline = new LayerDrawable(new Drawable[]{finalDrawableOnline, drNumber});
            drawableCircle = new LayerDrawable(new Drawable[]{drawableCircle, drNumber});
        }

        /*
        s.addState(new int[]{android.R.attr.state_checked}, finalDrawable);
        if (!isOnline) {
            s.addState(new int[]{}, d1);
        }
        else
        {
            s.addState(new int[]{}, finalDrawableOnline);
        }
         */
        if (_main != null) {
            lib.Ref<Drawable> refFinalDrawable = new lib.Ref<>(finalDrawable);
            lib.Ref<Drawable> refFinalDrawableOnline = new lib.Ref<>(finalDrawableOnline);
            lib.Ref<Drawable> refDrawableCircle = new lib.Ref<>(drawableCircle);
            getDrawablesAvatar(dp40, m, drawableShapeRoundBorder, drOnline, drNumber, refFinalDrawable, refFinalDrawableOnline, refDrawableCircle);
            finalDrawable = refFinalDrawable.get();
            finalDrawableOnline = refFinalDrawableOnline.get();
            drawableCircle = refDrawableCircle.get();
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) { //Convert Drawables to Bitmap because of Error
            Bitmap II = drawableToBitmap(finalDrawable, dp40, dp40);
            finalDrawable = new BitmapDrawable(context.getResources(), II);
            II = drawableToBitmap(finalDrawableOnline, dp40, dp40);
            finalDrawableOnline = new BitmapDrawable(context.getResources(), II);
            II = drawableToBitmap(drawableCircle, dp40, dp40);
            drawableCircle = new BitmapDrawable(context.getResources(), II);
            // finalDrawable.setBounds(0,0,dp40,dp40);
        }
        StateListDrawable s = creatStateListDrawable(isOnline, finalDrawable, finalDrawableOnline, drawableCircle);

        setRadioButton(dp40, m, radioButton, s, pos);

        return radioButton;
    }

    private void setRadioButton(int dp40, dlgChat.MemberData m, AppCompatRadioButton radioButton, StateListDrawable s, int pos) {
        radioButton.setGravity(Gravity.CENTER);
        s.setBounds(0, 0, dp40, dp40);
        radioButton.setButtonDrawable(null);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            radioButton.setButtonDrawable(android.R.color.transparent);
        }
        radioButton.setBackground(null);
        final StateListDrawable originalSLD = s;

        radioButton.setCompoundDrawables(null, s, null, null);
        m.setOriginalSLD(originalSLD);
        radioButton.setTag(m);
        setListenersRadioButton(radioButton, originalSLD);
        String ttText = m.getName();
        radioButton.setTextSize(12); //lib.spToPx(8.0f, _main));
        String ttText2 = ttText;
        if (ttText.length() > 10) ttText2 = ttText.substring(0, 7) + "...";
        radioButton.setText(ttText2);
        if (debugMode()) {
            ttText += ":" + m.getBenutzerid() + ":" + m.getBenutzeridEmpfaenger();
        }
        //TooltipCompat.setTooltipText(b, ttText);
        //txtUser.setText(ttText);
        RadioGroup.LayoutParams l = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int mm = dpToPx(4);
        l.setMargins(mm, mm, mm, 0);
        radioGroupChat.setOrientation(RadioGroup.HORIZONTAL);
        //b.setChecked(true);
        if (pos == -1) pos = 0;
        radioGroupChat.addView(radioButton, pos, l);
        if (m.getBenutzerid().longValue() == this.BenutzerID.longValue()) {
            radioButton.setOnLongClickListener(null);
        }

    }

    private void setListenersRadioButton(AppCompatRadioButton radioButton, Drawable originalSLD) {
        radioButton.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                dlgChat.this.rbtnTouched = true;
                dlgChat.this.rbtnTouched2 = true;
                return false;
            }


        });
        radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    processClickRBChat(v, radioButton, originalSLD);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                rbtnTouched = false;
                rbtnTouched2 = false;
            }
        });
        radioButton.setLongClickable(true);
        radioButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                rbtnTouched = false;
                rbtnTouched2 = false;
                PopupMenu popup = new PopupMenu(dlgChat.this._main, radioButton);
                //Inflating the Popup using xml file
                popup.getMenuInflater()
                        .inflate(R.menu.mnupopupchat, popup.getMenu());
                MenuItem mnuAdd = popup.getMenu().findItem(R.id.mnuAdd);
                MenuItem mnuDelHist = popup.getMenu().findItem(R.id.menu_delhist);
                popup.getMenu().findItem(R.id.mnu_close).setVisible(false);
                MemberData m = (MemberData) radioButton.getTag();
                if (m.getKontaktgrad() > 1 || m.getKontaktgrad() < 0) mnuAdd.setVisible(true);
                else mnuAdd.setVisible(false);

                //registering popup with OnMenuItemClickListener
                setPopupMenuListener(radioButton, popup);
                popup.show();
                return true;
            }

            private void setPopupMenuListener(AppCompatRadioButton radioButton, PopupMenu popup) {
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        MemberData m = (MemberData) radioButton.getTag();
                        if (item.getItemId() == R.id.mnuAdd) {
                            try {
                                _main.clsHTTPS.addContact(_main.getBenutzerID(), m.getBenutzerid(), false);
                                m.setKontaktgrad(1);
                                m.getContactVO().setKontaktgrad(1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.menu_delhist) {
                            deleteHistoryOfSelectedMember();
                        } else {
                            if (item.getItemId() == R.id.mnuBlockChat) {
                                BlockedUsers.add(m.getBenutzerid());
                                if (m.getContactVO() != null) m.getContactVO().setBlocked(true);
                            } else if (item.getItemId() == R.id.mnuBlockAll) {
                                try {
                                    _main.clsHTTPS.addSperre(_main.getBenutzerID(), m.getBenutzerid());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            radioGroupChat.removeView(radioButton);
                            MemberData.remove(m.getBenutzerid());
                            try {
                                saveMessages(false, _main.getBenutzerID());
                            } catch (Throwable throwable) {
                                lib.ShowException(TAG, getContext(), throwable,true);
                            }
                        }
                        return true;
                    }

                    private void deleteHistoryOfSelectedMember() {
                        messageAdapter.clearmessages(BenutzerIDEmpfaenger);
                        messageAdapter.notifyDataSetChanged();
                        try {
                            dlg = lib.ShowMessageYesNo(context, _main.getString(R.string.alsodeletememberhistory),
                                    _main.getString(R.string.Chat), false, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dlg = null;
                                            MemberData.clear();
                                            radioGroupChat.removeViews(0, radioGroupChat.getChildCount());
                                            try {
                                                insertMember(dataSelf);
                                            } catch (Throwable throwable) {
                                                throwable.printStackTrace();
                                                Log.e(TAG, null, throwable);
                                            }
                                        }
                                    }, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dlg = null;
                                        }
                                    }, null);
                            dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    dlg = null;
                                }
                            });
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                });

            }
        });

    }

    private StateListDrawable creatStateListDrawable(boolean isOnline, Drawable finalDrawable, Drawable finalDrawableOnline, Drawable drawableCircle) {
        try {

            StateListDrawable s = new StateListDrawable();
            try {
                finalDrawable.mutate();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            s.addState(new int[]{android.R.attr.state_checked}, finalDrawable);
            if (isOnline) {
                finalDrawableOnline.mutate();
                s.addState(new int[]{-android.R.attr.state_checked}, finalDrawableOnline);
                finalDrawableOnline.mutate();
                s.addState(new int[]{android.R.attr.state_empty}, finalDrawableOnline);
                finalDrawableOnline.mutate();
                s.addState(new int[]{}, finalDrawableOnline);
            } else {
                s.addState(new int[]{-android.R.attr.state_checked}, drawableCircle);
                s.addState(new int[]{android.R.attr.state_empty}, drawableCircle);
                s.addState(new int[]{}, drawableCircle);
            }
            return s;
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }

    }

    private void getDrawablesAvatar(float dp40, dlgChat.MemberData m, Drawable drawableShapeRoundBorder, Drawable drOnline, Drawable drNumber, Ref<Drawable> finalDrawable, Ref<Drawable> finalDrawableOnline, Ref<Drawable> drawableCircle) {
        try {
            Bitmap I = (_main.clsHTTPS.downloadProfileImage(_main, m.getBenutzerid(), _main.user.getLong(Constants.id), m.getName()));
            if (I != null && I.getByteCount() != _main.VNLOGO.getByteCount()) {
                float fact = (float) dpToPx(40) / (float) Math.min(I.getHeight(), I.getWidth());
                I = resizeBM(I, fact, fact);
                RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), I);
                roundDrawable.setCircular(true);
                roundDrawable.setCornerRadius(dp40 / 2.0f);
                //b.setBackground(roundDrawable);
                //Drawable r2 = getResources().getDrawable(R.drawable.shproundborder);
                //r2.setBounds(0, 0, lib.dpToPx(36), lib.dpToPx(36));
                //r2.setAlpha(128);
                //int horizontalInset = (roundDrawable.getIntrinsicWidth() - r2.getIntrinsicWidth()) / 2;
                drawableShapeRoundBorder.mutate();
                drOnline.mutate();
                finalDrawable.set(new LayerDrawable(new Drawable[]{roundDrawable, drawableShapeRoundBorder}));
                finalDrawableOnline.set(new LayerDrawable(new Drawable[]{roundDrawable, drOnline}));

                //finalDrawable.setLayerInset(0, 0, 0, 0, r2.getIntrinsicHeight());
                //finalDrawable.setLayerInset(1, horizontalInset, roundDrawable.getIntrinsicHeight(), horizontalInset, 0);
                drawableCircle.set(roundDrawable);
                if (drNumber != null) {
                    finalDrawable.set(new LayerDrawable(new Drawable[]{finalDrawable.get(), drNumber}));
                    finalDrawableOnline.set(new LayerDrawable(new Drawable[]{finalDrawableOnline.get(), drNumber}));
                    drawableCircle.set(new LayerDrawable(new Drawable[]{drawableCircle.get(), drNumber}));
                }
                //b.setBackground(roundDrawable);

            }

        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private Drawable getDrawableShapeRoundBorder(boolean isOnline, Drawable drOnline) {
        Drawable drawableShapeRoundBorder = null;
        if (!isOnline) {
            drawableShapeRoundBorder = getResources().getDrawable(getDrawableResFromTheme(_main, R.attr.shproundborderselectedonline));
        } else {
            drawableShapeRoundBorder = getResources().getDrawable(getDrawableResFromTheme(_main, R.attr.shproundborderselectedonline));
            drawableShapeRoundBorder = new LayerDrawable(new Drawable[]{drawableShapeRoundBorder, drOnline});
            //d2 = getResources().getDrawable(getDrawableResFromTheme(_main, R.attr.shproundborderselectedonline));
        }
        return drawableShapeRoundBorder;
    }

    private Drawable getDrawableCircle(int dp40, dlgChat.MemberData m) {
        GradientDrawable drawable = (GradientDrawable) getResources().getDrawable(R.drawable.circle);
        drawable.setColor(Color.parseColor(m.getColor()));
        //drawable.setBounds(0, 0, lib.dpToPx(40), lib.dpToPx(40));
        drawable.setSize(dp40, dp40);
        //b.setBackground(drawable);
        return drawable;
    }

    private Drawable getDrawableOnline(int dp40, boolean self) throws Throwable {
        Bitmap dOnline = new LetterTileProvider(context).getOnlineTile((int) (dp40 / 3f), (int) (dp40 / 3f), true, self);

        BitmapDrawable dr = new BitmapDrawable(getResources(), dOnline);
        dr.setGravity(Gravity.BOTTOM | Gravity.LEFT);

        return dr;
    }

    private boolean repeatInsertMemberIfNotInitialized(dlgChat.MemberData m) {
        if (_main == null || getContext() == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        insertMember(m);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                }
            }, 1000);
            return true;
        }
        return false;
    }

    private Drawable getNumberTileNewMessages(MemberData m, int dp40) throws Throwable {
        if (m.getCountMessages() > 0) {
            Bitmap dNumber = new LetterTileProvider(context).getNumberTile(m.getCountMessages(), m.getName(), (int) (dp40 / 2f), (int) (dp40 / 2f), true);
            BitmapDrawable dr = new BitmapDrawable(getResources(), dNumber);
            dr.setGravity(Gravity.TOP | Gravity.RIGHT);
            return dr;
            //drNumber.setBounds((int)(dp40 * (2/3f)), 0, dp40, (int)(dp40 * (2/3f)));
        }
        return null;

    }

    private AppCompatRadioButton findRadioButtonChat(MemberData m) {
        boolean isOld = false;
        AppCompatRadioButton bb = null;
        if (radioGroupChat != null) {
            for (int i = 0; i < radioGroupChat.getChildCount(); i++) {
                final RadioButton r = (RadioButton) radioGroupChat.getChildAt(i);
                if (r.getTag().equals(m)) {
                    bb = (AppCompatRadioButton) r;
                    radioGroupChat.removeView(bb);
                    isOld = true;
                    break;
                }
            }
        }
        return bb;
    }

    private int findRadioButtonChatPos(MemberData m) {
        boolean isOld = false;
        AppCompatRadioButton bb = null;
        if (radioGroupChat != null) {
            for (int i = 0; i < radioGroupChat.getChildCount(); i++) {
                final RadioButton r = (RadioButton) radioGroupChat.getChildAt(i);
                if (r.getTag().equals(m)) {
                    bb = (AppCompatRadioButton) r;
                    //radioGroupChat.removeView(bb);
                    isOld = true;
                    return i;
                }
            }
        }
        return -1;
    }

    private boolean getOnlineFromContactVO(MemberData m) {
        boolean isOnline = false;
        try {
            ContactVO vo = m.getContactVO();
            if (vo == null) {
                vo = new ContactVO(_main.clsHTTPS.getUser(_main.getBenutzerID(), m.getBenutzerid()));
                m.setContactVO(vo);
            }
            isOnline = vo.isOnline();
            m.setKontaktgrad(vo.getKontaktgrad());
            m.setName(vo.getContactName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isOnline;
    }

    private void processClickRBChat(View v, RadioButton b, Drawable originalSLD) throws Throwable {
        MemberData m = (MemberData) v.getTag();
        dlgChat.this.setEmpfaenger(m.getBenutzerid(), m.getName(), true);
        dlgChat.this.messageAdapter.notifyDataSetChanged();
        if (rbtnTouched2) m.setCountMessages(0);
        b.setCompoundDrawables(null, originalSLD, null, null);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (radioGroupChat != null) {
                for (int i = 0; i < radioGroupChat.getChildCount(); i++) {
                    final RadioButton r = (RadioButton) radioGroupChat.getChildAt(i);
                    if (r != b) r.setChecked(false);
                }
            }
        }
        dlgChat.this.selectedRadioButton = b;
        if (rbtnTouched2) m.setCountMessages(0);
        insertMember(m);
        rbtnTouched2 = false;

    }

    private void updateRadiobutton(RadioButton b, Drawable originalSLD) throws Throwable {
        MemberData m = (MemberData) b.getTag();
        b.setCompoundDrawables(null, originalSLD, null, null);
        insertMember(m);
    }

    public Long getBenutzerIDEmpfaenger() {
        return BenutzerIDEmpfaenger;
    }

    public void setBenutzerIDEmpfaenger(Long benutzerIDEmpfaenger, boolean checkOnly) {
        if (benutzerIDEmpfaenger < 0) return;
        if (benutzerIDEmpfaenger.longValue() != BenutzerIDEmpfaenger.longValue()) {
            this.BenutzerIDEmpfaenger = benutzerIDEmpfaenger;
            try {
                selectRadioButton(benutzerIDEmpfaenger, checkOnly);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                ShowException(TAG, context, throwable, "selectRadioButton", false);
            }
        } else {
            try {
                selectRadioButton(benutzerIDEmpfaenger, true);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

    }

    void selectRadioButton(Long benutzerIDEmpfaenger) throws Throwable {
        selectRadioButton(benutzerIDEmpfaenger, false);
    }

    private void selectRadioButton(final Long benutzerIDEmpfaenger, boolean checkonly) throws Throwable {
        if (benutzerIDEmpfaenger < 0) return;
        if (radioGroupChat != null) {
            for (int i = 0; i < radioGroupChat.getChildCount(); i++) {
                final RadioButton r = (RadioButton) radioGroupChat.getChildAt(i); //.findViewById(R.id.rbChat);
                MemberData m = (MemberData) r.getTag();
                if (m.getBenutzerid().longValue() == benutzerIDEmpfaenger.longValue()) {
                    setRadioButtonSelected(r, m, checkonly);
                    break;
                }
            }

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        selectRadioButton(benutzerIDEmpfaenger);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            }, 500);
        }
    }

    private void setRadioButtonSelected(RadioButton r, MemberData m, boolean checkonly) throws Throwable {
        r.setPaintFlags(r.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG | Paint.FAKE_BOLD_TEXT_FLAG);
        if (selectedRadioButton != null && rbtnTouched2){
            if (selectedRadioButton.getTag() != null)
            {
                MemberData mm = (dlgChat.MemberData) selectedRadioButton.getTag();
                mm.setCountMessages(0);
                updateRadiobutton(selectedRadioButton, mm.getOriginalSLD());
            }
        }
        if (!checkonly) {
            r.performClick();
        } else {
            r.setChecked(true);
            selectedRadioButton = r;
        }
        String ttText = m.getName();
                    /*
                    if (Debug.isDebuggerConnected() || BuildConfig.DEBUG) {
                        ttText += ":" + m.toString();
                    }
                     */
        txtUser.setTypeface(txtUser.getTypeface(), Typeface.BOLD);
        txtUser.setText(ttText);
        //rgChat.removeView(r);
        RadioGroup.LayoutParams l = new RadioGroup.LayoutParams(dpToPx(36), dpToPx(36));
        int mm = dpToPx(6);
        l.setMargins(mm, mm, mm, mm);
        radioGroupChat.setOrientation(RadioGroup.HORIZONTAL);
        //rgChat.addView(r, 0, l);
        MemberData.remove(m.getBenutzerid());
        MemberData.put(m.getBenutzerid(), m);
        if (!dlgChat.this.rbtnTouched) {
            svChat.post(new Runnable() {
                @Override
                public void run() {
                    int right = r.getRight() + dpToPx(4);
                    int left = r.getLeft() - dpToPx(4);
                    int sWidth = svChat.getWidth();
                    int svscrollx = svChat.getScrollX();
                    svChat.smoothScrollTo(((left + right - sWidth) / 2), 0);
                }
            });
        } else if (checkonly) {
            rbtnTouched = false;
        }

    }

    public String getNameEmpfaenger() {
        return NameEmpfaenger;
    }

    public void setNameEmpfaenger(String nameEmfaenger) {
        this.NameEmpfaenger = nameEmfaenger;
    }

    public Message hasuuid(UUID uuid, Long BenutzerID) {
        return messageAdapter.hasuuid(uuid, BenutzerID);
    }

    public void sendAttachmentMessage() throws Throwable {
        sendAttachment(getBenutzerIDEmpfaenger(), message, mimetype, path, fname, size, uuid, thumb);
    }

    public void setAttachment(CharSequence message, String mimeType, String path, String fname, long size, UUID uuid, Uri uri) {
        this.mimetype = mimeType;
        this.path = path;
        this.fname = fname;
        this.size = size;
        this.uuid = uuid;
        this.message = message;

        try {
            this.thumb = getThumbnailPath(uri, context);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            try {
                InputStream inputStream = context.getContentResolver().openInputStream(uri);
                Bitmap I = BitmapFactory.decodeStream(inputStream);
                Bitmap thumbBitmap = ThumbnailUtils.extractThumbnail(I, 256, 256);
                this.thumbBitmap = thumbBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    public static class MemberData {
        private Long benutzeridempfaenger;
        private Bitmap image;
        private String nameEmpfaenger;
        private String name;
        private String color;
        private Long benutzerid;
        private int countMessages;
        private int kontaktgrad = -1;
        private transient ContactVO contactVO;
        private transient StateListDrawable originalSLD;

        public MemberData(String name, String nameEmpfaenger, String color, Long BenutzerID, Long BenutzerIDEmpfaenger, Bitmap image) {
            this.name = name;
            this.color = color;
            this.benutzerid = BenutzerID;
            this.benutzeridempfaenger = BenutzerIDEmpfaenger;
            this.nameEmpfaenger = nameEmpfaenger;
            this.image = image;
        }

        public MemberData() {
        }

        public String getName() {
            return name;
        }

        public void setName(String Name) {
            name = Name;
        }

        public String getColor() {
            if (color == null || color.length() <= 0) {
                color = getRandomColor();
            }
            return color;
        }

        ;

        public Long getBenutzerid() {
            return benutzerid;
        }

        public String getNameEmpfaenger() {
            return nameEmpfaenger;
        }

        public void setNameEmpfaenger(String nameEmpfaenger) {
            if (nameEmpfaenger.equals("empty name")) return;
            this.nameEmpfaenger = nameEmpfaenger;
        }

        @Override
        public String toString() {
            return "MemberData{" +
                    "name='" + name + '\'' +
                    ", nameEmpfaenger='" + nameEmpfaenger + '\'' +
                    ", color='" + color + '\'' +
                    ", BenutzerID='" + benutzerid + '\'' +
                    ", BenutzerIDEmpfaenger='" + benutzeridempfaenger + '\'' +
                    '}';
        }

        public Bitmap getImage() {
            return image;
        }

        public void setImage(Bitmap image) {
            this.image = image;
        }

        public Long getBenutzeridEmpfaenger() {
            return benutzeridempfaenger;
        }

        public void setBenutzeridEmpfaenger(Long benutzeridEmpfaenger) {
            this.benutzeridempfaenger = benutzeridEmpfaenger;
        }

        public void incCountMessages() {
            countMessages++;
        }

        public int getCountMessages() {
            return countMessages;
        }

        public void setCountMessages(int pcountMessages) {
            countMessages = pcountMessages;
        }

        public int getKontaktgrad() {
            return kontaktgrad;
        }

        public void setKontaktgrad(int kontaktgrad) {
            this.kontaktgrad = kontaktgrad;
        }

        public ContactVO getContactVO() {
            return contactVO;
        }

        public void setContactVO(ContactVO contactVO) {
            this.contactVO = contactVO;
        }

        public StateListDrawable getOriginalSLD() {
            return originalSLD;
        }

        public void setOriginalSLD(StateListDrawable originalSLD) {
            this.originalSLD = originalSLD;
        }
    }

}

