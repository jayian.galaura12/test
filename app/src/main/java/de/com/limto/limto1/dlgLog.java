package de.com.limto.limto1;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

public class dlgLog extends DialogFragment {
    private static final String DLGLOG = "dlgLog";
    private TextView tvLog;
    public static dlgLog dlg;
    private StringBuilder strBldrLog = new StringBuilder();

    public static dlgLog show(FragmentManager fm)
    {
        fm.executePendingTransactions();
        boolean isNew = false;
        if (dlg == null) {
            dlg = new dlgLog();
            isNew = true;
        }
        try
        {
            //dlg.dismiss();
            boolean isVisible = dlg.isAdded() &&  dlg.isVisible() && !dlg.isHidden();
            boolean added = dlg.isVisible() || dlg.isAdded();
            if (isNew) {
                dlg.show(fm, DLGLOG);
            }
            else if (!isVisible)
            {
                try {
                    dlg.dismiss();
                } catch (Throwable ex)
                {
                    System.out.println("could not dismiss dlgLog!");
                }
                dlg.show(fm, DLGLOG);
            }
            fm.executePendingTransactions();
        }
        catch (Throwable ex)
        {
            System.out.println("dlgLog not added");
        }

        return dlg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dlglog, container, false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.tvLog = (TextView) view.findViewById(R.id.tvLog);
        tvLog.setMovementMethod(new ScrollingMovementMethod());
        appendlog(strBldrLog.toString());
    }


    public void appendlog(String txt)
    {
        if (tvLog != null) {
            tvLog.append(txt + "\n");
        }
        else
        {
            strBldrLog.append(txt);
        }
    }
}

