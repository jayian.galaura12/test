/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.com.limto.limto1;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.work.BackoffPolicy;
import androidx.work.Configuration;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.Operation;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.os.Process;
import android.text.SpannableString;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.Errors.dbSqlite;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;
import de.com.limto.limto1.logger.LogWrapper;
import de.com.limto.limto1.logger.MessageOnlyLogFilter;
import de.com.limto.limto1.pedometer.StepDetector;
import de.com.limto.limto1.pedometer.StepListener;
import tech.gusavila92.websocketclient.WebSocketClient;

import static de.com.limto.limto1.Constants.MESSAGE_EXCEPTION;
import static de.com.limto.limto1.Constants.MESSAGE_STATE_CHANGE;
import static de.com.limto.limto1.Controls.Question.sdftime;
import static de.com.limto.limto1.LimindoService.SendType.Upload;
import static de.com.limto.limto1.LimtoWorker.AUTOKILL;
import static de.com.limto.limto1.LimtoWorker.IAUTOKILL;
import static de.com.limto.limto1.LimtoWorker.RESTARTINTERVALL;
import static de.com.limto.limto1.MainActivity.BENUTZERID;
import static de.com.limto.limto1.MainActivity.BENUTZERNAME;
import static de.com.limto.limto1.MainActivity.KONTAKTGRAD;
import static de.com.limto.limto1.MainActivity.TIME;
import static de.com.limto.limto1.clsHTTPS.MYURLROOT;
import static de.com.limto.limto1.clsHTTPS.MYURLTALK;
import static de.com.limto.limto1.clsHTTPS.getSSLFactory;
import static de.com.limto.limto1.fragQuestion.VOTE;
import static de.com.limto.limto1.fragQuestions.ENTF;
import static de.com.limto.limto1.fragQuestions.ID;
import static de.com.limto.limto1.fragQuestions.SEARCH;
import static de.com.limto.limto1.fragSettings.MENGE;
import static de.com.limto.limto1.lib.lib.SHOWEXCEPTIONSDEBUG;
import static de.com.limto.limto1.lib.lib.appendLog;
import static de.com.limto.limto1.lib.lib.debugMode;
import static de.com.limto.limto1.lib.lib.gStatus;
import static de.com.limto.limto1.lib.lib.setStatusAndLog;


public class LimindoService extends android.app.Service implements Executor, SensorEventListener, StepListener {
    //Constants
    public static final Integer ONGOING_NOTIFICATION_ID_ANSWER = 304854;
    public static final Integer ONGOING_NOTIFICATION_ID_QUESTION = 304855;
    public static final Integer ONGOING_NOTIFICATION_ID_MESSAGES = 304900;
    public static final int ONGOING_NOTIFICATION_ID_DEBUG = 305000;

    public static final int ONGOING_NOTIFICATION_ID_NEW = 304857;
    public static final Integer SERVICE_NOTIFICATION_ID = 304856;
    public static final String ACCESSKEY = "Accesskey";
    public static final String ERROR = "Error: ";
    public static final String BENUTZER_ID = "BenutzerID";
    public static final String GRADE = "Grade";
    public static final String SERVICE_ON_CREATE = "Service onCreate";
    public static final String NOT_ONLINE = "not online";
    public static final String STARTED_RECONNECT_AFTER_SECS = "Started reconnect after secs ";
    public static final String FAILED_TO_CONNECT = "failed to connect";
    public static final String CONNECTION_CLOSED_BY_PEER = "connection closed by peer";
    public static final String NOT_CONNECTED_AFTER_SECS = "not connected after secs ";
    public static final String IS_ONLINE = "isOnline";
    public final static String DatePattern = "yyyy.MM.dd HH:mm:ss";
    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    public static final int STATE_ERROR = 4;
    // Debugging
    private static final String ACTION_CLOSE = "action_close";
    private static final String ACTION_RECONNECT = "action_reconnect";
    private static final int REQUEST_CODE_CLOSE = 9500;
    private static final int REQUEST_CODE_RECONNECT = 9501;
    public static final String LIMINDO_SERVICE = "LimindoService";
    private static final String TAG = LIMINDO_SERVICE;
    private static final String NOTIFICATION_CHANNEL_ID = LIMINDO_SERVICE;
    public static final String UNCAUGHT_EXCEPTION_HANDLER = "UncaughtExceptionHandler";
    public static final int DELAY_STOP_SERVICE = 10000;
    public static int MaximumRestartInvervallMins = 1;
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    //statische Variablen
    public static Context ApplicationContext;
    static boolean dontStop = lib.debugMode();
    public Context ccontext = null;
    public static int alivecount = 0;
    static Ping r = null;
    static Throwable pingEx = null;
    // Class Variables
    /**
     * The Handler that gets information back from the LimindoService
     */
    final Handler mHandlerUnbound = new HandlerUnbound(this);
    // Member fields
    public Handler mHandler;
    //private ConnectThread mConnectThread;
    //private ConnectedThread mConnectedThread;
    public int mState;
    public Context context;
    public JSONObject user;
    public String mErrMsg = "";
    public dbSqlite db;
    //public fragLogin fragLogin;
    public int Grad;
    public boolean showAnswers;
    public boolean blnNotOff;
    public WebSocketClient webSocketClient;
    public boolean isWebSocketConnected;
    public int countAnswers;
    public int countQuestions;
    public long NotificatonPopupIntervall = 60 * 1000;
    public int neueFragen;
    public int neueAntworten;
    public ServiceConnection serviceConnection;
    public Notification notification;
    public int gpsUpdateInterval = 2;
    public clsHTTPS clsHTTPS;
    public boolean blnKill;
    ArrayList<String> MessageCacheChatService = new ArrayList<>();
    /**
     * Stop all threads
     */
    public boolean mblnStop = false;
    public boolean blnArduinoLoop; //Flag für testbetrieb
    Location locCoarse = null;
    Long timeOffsetGPS = null;
    Long timeOffsetNTP = null;
    @SuppressLint("SimpleDateFormat")
    static SimpleDateFormat DateTimeParser = new SimpleDateFormat(DatePattern);
    boolean isBound = false;
    String mConnectedDeviceName;
    String mConnectedAddress;
    int ConnectionRetries = 0;
    boolean lastWasNetworkError;
    int postdelayed;
    int connectionretries;
    boolean notificationChannelRegistered = false;

    int count = 0;
    Thread tReconnect = null;
    private long TimeOffset = 0;
    private int mNewState;
    AppCompatActivity activity = null;
    private FusedLocationProviderClient mFusedLocationClient;
    private UserLoginTask mAuthTask;
    private SSLSocketFactory sslSocketFactory;
    private Date dtLastNot = new Date(0L);
    private boolean boot;
    private boolean isRunning = false;
    private boolean activityStopped;

    private SensorManager sensorManager;
    private StepDetector simpleStepDetector;
    private int numSteps;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private long userID = -1;
    private int grad = -1;
    boolean blnWasDestroyed;
    private boolean quiet;
    private BroadcastReceiver receiver;
    byte[] Pong;
    private PeriodicWorkRequest LimtoWorkRequest;
    private boolean WSisBusy;
    private String startCommand = null;
    //private Thread ANRThread;
    private boolean servicestopped;

    // Defined Runnables
    private Runnable runnableinterrupt = createRunnableInterrupt();
    private Runnable runnableStopService = createRunnableStopService();
    int DebugMessages;
    private boolean blnDontLogoff = false;
    public boolean runnableStopServiceEnabled = true;


    public LimindoService() {
        initializeLoggingOnEmptyDB();
    }


    public LimindoService(AppCompatActivity a, Context context, Handler handler, dbSqlite db) throws Throwable {
        init(a, context, handler, null, db, true);
    }

    private void initializeLoggingOnEmptyDB() {
        if (db == null && !(Log.getLogNode() instanceof LogWrapper)) {
            try {
                Context c = getContext();
                initializeDB(c);
                initializeLogging(0, null, db);
                lib.setStatusAndLog(context, TAG, "initializeLogging finished LimindoService()");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

    private Context getContext() {
        return (ccontext == null ? getApplicationContext() : ccontext);
    }


    private Runnable createRunnableInterrupt() {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    if (tReconnect != null) tReconnect.interrupt();
                    tReconnect = null;
                } catch (Throwable ex) {
                    tReconnect = null;
                }
            }
        };
    }

    private Runnable createRunnableStopService() {
        return new Runnable() {
            @Override
            public void run() {
                if (webSocketClient == null || (getState() != STATE_CONNECTED && getState() != STATE_CONNECTING) || (!WSisBusy && getState() != STATE_CONNECTING)) {
                    try {
                        //createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES,getString(R.string.service), getString(R.string.servicewastoppedat, sdftime.format(Calendar.getInstance().getTime())), null, false, false);
                        if (runnableStopServiceEnabled)
                        {
                            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "Stopping Service at " + Calendar.getInstance().getTime().toString(), null, false, false);
                            blnDontLogoff = true;
                            stopService(true, true);
                            closeDatabase();
                            db = null;
                        }
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                } else {
                    createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "rescheduling stop Service at " + Calendar.getInstance().getTime().toString(), null, false, false);
                    mHandler.postDelayed(this, DELAY_STOP_SERVICE);
                }
            }
        };

    }

    private void initializeDB(Context c) throws Throwable {
        if (db != null) return;
        db = new dbSqlite(c);
        db.createDataBase();
    }

    public void enqueueWorkAndPostKill(long autokill, boolean keep)  {

        try {
            Context c = (context == null ? getApplicationContext() : context);

            boolean isWorkScheduled = LimtoWorker.isWorkScheduled("LimtoWorker", c);

            if (autokill > 0 && (!keep || !isWorkScheduled)) {
                removeRunnableStop();
            }

            try {

                Constraints constraints = new Constraints.Builder()
                        //.setRequiresDeviceIdle(true)
                        .setRequiresBatteryNotLow(true)
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build();

                int restartInterval = getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).getInt("restartInterval", RESTARTINTERVALL);

                LimtoWorkRequest =
                        new PeriodicWorkRequest.Builder(LimtoWorker.class, restartInterval, TimeUnit.MILLISECONDS)
                                .setInitialDelay(restartInterval, TimeUnit.MILLISECONDS)
                                .addTag("LimtoWorker")
                                .setConstraints(constraints)
                                .setBackoffCriteria(
                                        BackoffPolicy.LINEAR,
                                        restartInterval,
                                        TimeUnit.MILLISECONDS)
                                .build();

                if (restartInterval < 60000 * MaximumRestartInvervallMins)
                    getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).edit().putInt("restartInterval", restartInterval * 2).apply();

                ExistingPeriodicWorkPolicy policy = ExistingPeriodicWorkPolicy.REPLACE;
                if (keep) policy = ExistingPeriodicWorkPolicy.KEEP;

                getWorkManager(c, false)
                        .enqueueUniquePeriodicWork("LimtoWorker", policy, LimtoWorkRequest);

                boolean rr = LimtoWorker.isWorkScheduled("LimtoWorker", c);
                System.out.println(rr);

                lib.setStatusAndLog(context, TAG, "enqueuework LimtoWorker");

                if (rr && autokill > 0 && (!keep || !isWorkScheduled))
                {
                    mHandlerUnbound.postDelayed(runnableStopService, autokill);
                    lib.setStatusAndLog(context, TAG, "enqueuework runnableStopService");
                }

                if (!keep && isWorkScheduled) {
                    createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "Worker started at " + Calendar.getInstance().getTime().toString(), null, false, false);
                }

            } catch (Throwable ex) {
                lib.setStatusAndLog(context, TAG, "enqueuework error" + ex.getMessage());
            }



        }
        catch (Throwable ex)
        {
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "enqueue Work exception " + ex.getMessage(), null, false, false);
        }

    }

    public void removeRunnableStop()
    {
        if (runnableStopService == null) return;
        try {
            mHandler.removeCallbacks(runnableStopService);
        } catch (Throwable ex)
        {
            ex.printStackTrace();
        }
    }

    public void removeWorkRequests() {
        try {

                Context c = (context == null ? getApplicationContext() : context);
                Operation res = getWorkManager(c, false)
                        .cancelAllWorkByTag("LimtoWorker");
                System.out.println(res.getResult().isDone());
        } catch (Throwable ex) {
            Log.e(TAG, null, ex);
            ex.printStackTrace();
        } finally {
            LimtoWorkRequest = null;
        }
    }

    public void removeWorkRequestsAndRunnableStop()
    {
        removeRunnableStop();
        removeWorkRequests();
    }

    public static String LogCatFileRoot;

    public static void setLogCatFileRoot(Context context) throws IOException {
        String fileName = "logcatLimindo_";
        File outputFile = new File(context.getExternalCacheDir(), fileName);
        LogCatFileRoot = outputFile.getAbsolutePath();
    }

    public static void saveLogcatToFile() throws IOException {
        if (LogCatFileRoot == null) return;
        String fileName = LogCatFileRoot + System.currentTimeMillis() + ".txt";
        java.lang.Process process = Runtime.getRuntime().exec("logcat -df " + fileName + " *:E");
    }

    public static String createRandomString(int MAX_LENGTH) {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public static Ping ping(final URL url, final Context ctx) throws Throwable {
        r = new Ping();
        r.dns = Integer.MAX_VALUE;
        pingEx = null;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if (isNetworkConnected(ctx)) {
                    r.net = getNetworkType(ctx);
                    try {
                        String hostAddress;
                        long start = System.currentTimeMillis();
                        hostAddress = InetAddress.getByName(url.getHost()).getHostAddress();
                        long dnsResolved = System.currentTimeMillis();
                        Socket socket = new Socket(hostAddress, url.getPort());
                        socket.close();
                        long probeFinish = System.currentTimeMillis();
                        r.dns = (int) (dnsResolved - start);
                        r.cnt = (int) (probeFinish - dnsResolved);
                        r.host = url.getHost();
                        r.ip = hostAddress;
                    } catch (Exception ex) {
                        Log.e(TAG, "Unable to ping");
                        pingEx = ex;
                    }
                }

            }
        });
        t.start();
        t.join(5000);
        if (pingEx != null) throw pingEx;
        return r;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetwork != null && activeNetwork.isConnected();
    }

    @Nullable
    public static String getNetworkType(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        if (activeNetwork != null) {
            return activeNetwork.getTypeName();
        }
        return null;
    }

    static double round(double wert, int stellen) {
        return Math.round(wert * Math.pow(10, stellen)) / Math.pow(10, stellen);
    }

    @Override
    public void onCreate() {
        lib.blnDebugOn = getSharedPreferences("lib", Context.MODE_PRIVATE).getBoolean("DebugOn", false);
        if (context == null) context = getApplicationContext();

        lib.setStatusAndLog(context, TAG, "Service.onCreate");

        try {
            initService1();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            lib.setStatusAndLog(context, TAG, throwable.getMessage());
        }

        try {
            startForegroundWithNotification();

            LoadMessageCacheChatService();

            reconnectIfWasConnectedBefore();

            if (isRunning) {
                return;
            } else {
                isRunning = true;
            }

            setUncaughtExceptionHandler();



            startSensorManager();

        } catch (Throwable ex) {
            lib.setStatusAndLog(context, TAG, ex.getMessage());
        }

        checkStartCommand();

        lib.setStatusAndLog(context, TAG, "Service.onCreate.finished");

    }

    private void reconnectIfWasConnectedBefore() {
        if (this.userID >= 0 && this.grad >= 0) {

            try {
                appendLog("Service.onCreate.reconnect", context);
                this.reconnect(false, false);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, null, e);
            }
        } else {
            mState = STATE_NONE;
            mNewState = mState;
        }
    }

    private void initService1() throws Throwable {
        // runANRThread();

        LimindoService.ApplicationContext = context;

        blnWasDestroyed = false;

        MaximumRestartInvervallMins = (getContext().getSharedPreferences("LimindoService", Context.MODE_PRIVATE).getInt(fragSettings.RESTARTINTERVALL, LimindoService.MaximumRestartInvervallMins));

        broadcastReceiver.service = this;
    }

    // uncaught exception handler variable
    private Thread.UncaughtExceptionHandler defaultUEH;
    // handler listener
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    // re-throw critical exception further to the os (important)

                    try {
                        Log.e(LIMINDO_SERVICE, UNCAUGHT_EXCEPTION_HANDLER, ex);
                        try {
                            saveLogcatToFile();
                        } catch (Exception e) {
                            //e.printStackTrace();
                        }
                    } catch (Throwable throwable) {
                        android.util.Log.e(TAG, LIMINDO_SERVICE, throwable);
                        throwable.printStackTrace();
                    } finally {
                        defaultUEH.uncaughtException(thread, ex);
                    }
                }
            };

    private void setUncaughtExceptionHandler() {
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        // setup handler for uncaught exception
        Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
    }

    private void checkStartCommand() {
        startCommand = null;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (startCommand == null) {
                    String msg = "No start command received!" + gStatus + ":" + getStateString(getState());
                    appendLog(msg, context);
                    Log.e(TAG, null, new Exception(msg));
                    try {
                        createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES + 1, getString(R.string.service), msg, null, false, false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                    if (LimtoWorkRequest == null) {
                        try {
                            enqueueWorkAndPostKill(60000, false);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                } else {
                    Log.i(TAG, startCommand);
                }
            }
        }, 10000);
    }

    private void startSensorManager() {
        lib.setgstatus("");
        setStatusAndLog(context, TAG, "SensorManager start");
        try {
            sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            Sensor accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            simpleStepDetector = new StepDetector();
            simpleStepDetector.registerListener(this);
            numSteps = 0;
            sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_FASTEST);
            setStatusAndLog(context, TAG,"SensorManager start complete");
        } catch (Throwable ex) {
            Log.e("Service", "SensorManager", ex);
        }
    }

    private void startForegroundWithNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            registerNotificationChannel();
            //sendMessageToNotification("boot",false,false);
            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            Intent serviceIntent = new Intent(getApplicationContext(), LimindoService.class);
            serviceIntent.putExtra("action", "actionclose");
            serviceIntent.putExtra("ServiceIntent", true);
            serviceIntent.setAction(ACTION_CLOSE);
            Intent serviceIntent2 = new Intent(getApplicationContext(), LimindoService.class);
            serviceIntent2.putExtra("action", "actionreconnect");
            serviceIntent2.putExtra("ServiceIntent", true);
            serviceIntent2.setAction(ACTION_RECONNECT);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(notificationIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent servicePendingIntent = PendingIntent.getService(getApplicationContext(), REQUEST_CODE_CLOSE, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent servicePendingIntent2 = PendingIntent.getService(getApplicationContext(), REQUEST_CODE_RECONNECT, serviceIntent2, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Action action = new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.close), servicePendingIntent).build();
            NotificationCompat.Action action2 = new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_rotate, getString(R.string.mnuReconnect), servicePendingIntent2).build();
            notification = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(getText(R.string.notification_title_service_created))
                    //.setContentText(getText(R.string.notification_message))
                    .setSmallIcon(R.drawable.vn_logo_c02)
                    .setContentIntent(resultPendingIntent)
                    .setTicker(getText(R.string.ticker_text))
                    .setChannelId(NOTIFICATION_CHANNEL_ID)
                    .addAction(action)
                    .addAction(action2)
                    //.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                    //        .setShowActionsInCompactView(0,1))
                    .setWhen(0)
                    .setOngoing(true)
                    .build();
            appendLog("StartForeground start", context);
            startForeground(SERVICE_NOTIFICATION_ID, notification);
            appendLog("StartForeground finished", context);
            //NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            //notificationManager.notify(SERVICE_NOTIFICATION_ID, notification);
        } else {
                /*
                notification = new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle(getText(R.string.notification_title))
                        .setContentText(getText(R.string.notification_message))
                        .setSmallIcon(R.drawable.vn_logo_c02)
                        .setContentIntent(resultPendingIntent)
                        .setTicker(getText(R.string.ticker_text))
                        .setOngoing(false)
                        .build();
                //
                */
        }

    }

    public void LoadMessageCacheChatService()  throws Throwable{
        if (this.MessageCacheChatService.size() == 0) {
            String str0 = getSharedPreferences("LimindoService", Context.MODE_PRIVATE).getString("MessageCacheChatService", "");
            if (str0 != null && str0.length() > 0) {
                Object o = new Gson().fromJson(str0, new TypeToken<ArrayList<String>>() {
                }.getType());
                ArrayList<String> M = (ArrayList<String>) o;
                if (M != null && M.size() > 0) {
                    this.MessageCacheChatService = M;
                }
                //Debug
                setStatusAndLog(context, TAG, "*** " + MessageCacheChatService.size() + " Messages MessageCacheChatService loaded!");
            } else {
                //Debug
                setStatusAndLog(context, TAG, "*** Load MessageCacheChatService empty!");
            }
        }
        else
        {
            //Debug
            setStatusAndLog(context, TAG, "*** Load MessageCacheChatService not empty!");
        }
    }

    /* private void runANRThread() {
        if (ANRThread != null && ANRThread.isAlive()) return;
        ANRThread = new Thread(new Runnable() {
            AtomicBoolean anr = new AtomicBoolean(true);

            @Override
            public void run() {
                try {
                    do {
                        {
                            long timer = Calendar.getInstance().getTimeInMillis();
                            anr.set(true);
                            Handler handler = new Handler(context.getMainLooper());
                            Runnable runnableMain = new Runnable() {
                                @Override
                                public void run() {
                                    anr.set(false);

                                }
                            };
                            handler.post(runnableMain);
                            while (anr.get()) {
                                if (Calendar.getInstance().getTimeInMillis() - timer > 5000) {
                                    try {
                                        handler.removeCallbacks(runnableMain);
                                    } catch (Throwable ex) {
                                        ex.printStackTrace();
                                    }
                                    anr.set(true);
                                    break;
                                }
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                    throw e;
                                }
                            }
                            if (anr.get()) {
                                Exception ex = new Exception("ANR in Mainactivity!");
                                StringWriter errors = new StringWriter();
                                ex.printStackTrace(new PrintWriter(errors));
                                String err = errors.toString();
                                lib.setStatusAndLog(context, TAG, err);
                            }
                        }
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            throw e;
                        }
                    } while (true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                System.out.println("ANR FINISHED");
            }
        });
        ANRThread.start();
    }

     */

    public void initializeLogging(long userID, clsHTTPS clsHTTPS, dbSqlite db) {
        // Wraps Android's native log framework.
        LogWrapper logWrapper = null;
        lib.setStatusAndLog(context, TAG, "create new LogWrapper");
        if (Log.getLogNode() != null && Log.getLogNode() instanceof LogWrapper) {
            logWrapper = (LogWrapper) Log.getLogNode();
            if (logWrapper.getBenutzerID() != userID
                    || logWrapper.getClsHTTPS() != clsHTTPS
                    || logWrapper.getDBSQLite() != db)
                logWrapper = new LogWrapper(userID, clsHTTPS, db);
        } else {
            logWrapper = new LogWrapper(userID, clsHTTPS, db);
        }
        // Using Log, front-end to the logging chain, emulates android.util.log method signatures.
        Log.setLogNode(logWrapper);
        lib.setStatusAndLog(context, TAG, "set msgFilter");
        // Filter strips out everything except the message text.
        MessageOnlyLogFilter msgFilter = new MessageOnlyLogFilter();
        lib.setStatusAndLog(context, TAG, "setNext msgFilter");
        logWrapper.setNext(msgFilter);

        // On screen logging via a fragment with a TextView.lib.setStatusAndLog( context, TAG, "Ready");
        //Log.e(TAG,"Start",new RuntimeException("Start"));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        blnWasDestroyed = false;
        try {
            try {
                setLogCatFileRoot(getApplicationContext());
            } catch (IOException e) {
                e.printStackTrace();
            }

            startCommand = intent != null ? "intent " + intent.getAction() : "nointent";

            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "Service onStartCommand " + startCommand, null, false, false);

            lib.setStatusAndLog(context, TAG, "Service onStartCommand" + (intent != null ? ":" + intent.getAction() : ""));

            // runANRThread(); //doppelt



            String action = (intent != null && intent.getAction() != null ? intent.getAction() : "noaction");

            processAction(intent, action);

        }
        catch (Throwable ex)
        {
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Error", "Service onStartCommand " + ex.getMessage(), null, false, false);
        }

        return Service.START_STICKY;

    }

    private void processAction(Intent intent, String action) {
        if (action.equals(ACTION_CLOSE) && intent.getBooleanExtra("ServiceIntent", false)) {
            processActionClose(intent);
        } else if (action.equals(ACTION_RECONNECT) && intent.getBooleanExtra("ServiceIntent", false)) {
            processActionReconnect(intent);
        } else { // undefined action
            processActionUndefined(intent, action);
        }

    }

    private void processActionUndefined(Intent intent, String action) {
        createNewNotificationDebug("Debug", "processActionUndefined " + startCommand);
        if (mHandler != null && this.getState() == LimindoService.STATE_NONE) { //reconnect or start if not connected
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "actionUndefinedReconnectOrStartOnStateNone(); " + startCommand, null, false, false);
            actionUndefinedReconnectOrStartOnStateNone();
        } else {
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "actionUndefined state = " + getStateString(getState()), null, false, false);
        }
        if (intent != null && intent.hasExtra("boot") && !this.boot && !this.isBound && this.activity == null) {
            //start boot if service has not booted before
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "actionUndefined start boot; " + startCommand, null, false, false);
            startBoot(intent);
        } else { //Workrequest entfernen, wenn kein boot commando ansonsten nichts tun.
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "actionUndefined Remove Workrequest", null, false, false);
            actionUndefinedRemoveWorkRequestOnNoBoot(intent, action);
        }
    }

    private void actionUndefinedRemoveWorkRequestOnNoBoot(Intent intent, String action) {
        if (!(intent != null && intent.hasExtra("boot"))) {
            removeRunnableStop();
            // removeWorkRequestsAndRunnableStop(false);
            broadcastReceiver.service = this;
            this.boot = false;
            getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).edit().putInt("restartInterval", RESTARTINTERVALL).apply();
        }
        this.quiet = false;
        lib.setStatusAndLog(context, TAG, "unknown intent " + action);
    }

    private void actionUndefinedReconnectOrStartOnStateNone() {
        lib.setStatusAndLog(context, TAG, "action undefined State = NONE");
        // Start the Bluetooth chat services
        //this.start();
        try {
            if (this.userID >= 0 && this.grad >= 0) this.reconnect(false, false);
            else this.start();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, null, e);
        }
    }

    private void processActionClose(Intent intent) {
        try {
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "action Close ", null, false, false);
            if (intent.getStringExtra("action").equals("actionclose")) {
                removeWorkRequestsAndRunnableStop();
                blnKill = true;
                if (isBound && activity != null && mHandler != mHandlerUnbound) {
                    ((MainActivity) activity).stopserviceandfinish(true);
                } else {
                    this.stopService(false, false);
                }
            }
        } catch (Throwable throwable) {
          throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
    }

    private void processActionReconnect(Intent intent) {
        try {
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "action Reconnect " , null, false, false);
            if (intent.getStringExtra("action").equals("actionreconnect")) {
                lib.setStatusAndLog(context, TAG, "actionreconnect");
                if (db == null || clsHTTPS == null) {
                    createStandAloneService();
                } else {
                    this.relogin(false);
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
    }

    private void startBoot(Intent intent) {
        try {
            appendLog("Extra boot", context);
            this.boot = true;
            this.quiet = false;
            createStandAloneService();
            int autokill = intent.getIntExtra(AUTOKILL, 0);
            if (autokill == 0) {
               startBroadCastBoot(autokill);
            } else {
                startWorkerBoot(autokill);
            }

            //reconnectwhenonline(null,true,false,false);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
            gStatus = throwable.getMessage();
            appendLog(gStatus, context);
        }
    }

    private void startWorkerBoot(int autokill) throws Throwable {
        int restartInterval = getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).getInt("restartInterval", RESTARTINTERVALL);
        enqueueWorkAndPostKill(autokill, false);
        LimtoWorker.count = getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).getInt("restartCount", 1);
        if (lib.debugMode()) createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES, getString(R.string.service), lib.fromHtml((getString(R.string.servicestartedworker, LimtoWorker.count) + "<br>" + getString(R.string.nextStart, restartInterval / 60000))), null, false, false);
        setRestartCount(++LimtoWorker.count);
    }

    private void startBroadCastBoot(int autokill) throws Throwable {
        getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).edit().putInt("restartInterval", RESTARTINTERVALL).apply();
        autokill = IAUTOKILL;
        enqueueWorkAndPostKill(autokill, false);
        if (lib.debugMode()) createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES, getString(R.string.service), getString(R.string.servicestartedboot), null, false, false);
    }

    void setRestartCount(int count) {
        getApplicationContext().getSharedPreferences("LimtoService", MODE_PRIVATE).edit().putInt("restartCount", count).apply();
    }

    private void createStandAloneService() throws Throwable {
        if (db == null && !(Log.getLogNode() instanceof LogWrapper)) {
            try {
                lib.setStatusAndLog(context, TAG, "boot init db");
                Context c = getApplicationContext();
                initializeDB(c);
                initializeLogging(0, null, db);
                lib.setStatusAndLog(context, TAG, "Logging started createStandalone");

            } catch (Throwable throwable) {
                throwable.printStackTrace();
                lib.setStatusAndLog(context, TAG, "error " + throwable.getMessage());
            }
        }
        Log.e(TAG, "BOOT");
        //sendMessageToNotification("boot",false,false);
        lib.setStatusAndLog(context, TAG, "init boot");
        this.init(null, getApplicationContext(), mHandlerUnbound, null, null, false);
        //sendMessageToNotification("relogin",false,false);
        lib.setStatusAndLog(context, TAG, "reLogin boot");
        relogin(false);
        lib.setStatusAndLog(context, TAG, "relogin boot successbul");

    }

    @Override
    public IBinder onBind(Intent intent) {
        isBound = true;
        blnWasDestroyed = false;
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {

        isBound = false;
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);

        isBound = true;
    }

    @Override
    public void onDestroy() {
        lib.setStatusAndLog(context, TAG, "Service.onDestroy");
        try {
            Log.e(TAG, "onDestroy");

            boolean tmpDontLogof = shutdownService();

            unBind(false);

            closeDatabase();

            if (!servicestopped) stopService(true, tmpDontLogof);
        } catch (Throwable t) {
            lib.setStatusAndLog(context, TAG, "Service.onDestroy.error :" + t.getMessage());
        }
        //broadcastReceiver.service = null;

        // saveMessageCacheChatService();

        super.onDestroy();
        blnWasDestroyed = true;

        try {
            enqueueWorkIfNotScheduled();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        dontStop = true;
        if (!LimindoService.dontStop) {
            Process.killProcess(Process.myPid());
            System.exit(1);
        }
        //dontStop = false;

    }

    private void enqueueWorkIfNotScheduled() throws Throwable {
        if (!blnKill)
        {
            Context c = (context == null ? getApplicationContext() : context);


            ListenableFuture<List<WorkInfo>> res = getWorkManager(c, false)
                    .getWorkInfosByTag("LimtoWorker");

            try {
                int rr = res.get().size();
                {
                    try {
                        enqueueWorkAndPostKill(IAUTOKILL, true);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public WorkManager getWorkManager(Context c, boolean retry) throws Throwable
    {
        try {
            WorkManager w = WorkManager.getInstance(c);
            return w;
        }
        catch (Throwable e)
        {
            if (retry) {
                createNewNotificationDebug("Debug", "getWorkmanager " + e.getMessage(), true);
                throw e;
            }
            WorkManager.initialize(c, new Configuration.Builder().build());
            return getWorkManager(c, true);
        }
    }

    private boolean shutdownService() {
        if (activity != null) ((MainActivity) activity).blnServicWasDestroyed = true;
        try {
            stop(false);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        if (mFusedLocationClient != null) {
            try {
                stopLocationUpdates();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, "stoplocationupdates", throwable);
            }
        }
        if (sensorManager != null) sensorManager.unregisterListener(this);
        boolean tmpDontLogof = blnDontLogoff;
        if (!blnDontLogoff) {
            if (clsHTTPS != null && userID > -1) {
                try {
                    clsHTTPS.logoff(userID, this.boot);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                userID = -1;
            }
        }
        else
        {
            blnDontLogoff = false;
        }
        return tmpDontLogof;
    }

    private void closeDatabase() throws Throwable {
        if (db != null) {
            if (db.DataBase != null) {
                db.DataBase.close();
                db.DataBase = null;
            }
            db.close();
        }
    }

    public void saveMessageCacheChatService() {
        String strO = new Gson().toJson(MessageCacheChatService);
        getSharedPreferences("LimindoService", Context.MODE_PRIVATE).edit().putString("MessageCacheChatService", strO).apply();
        //Debug
        setStatusAndLog(context, TAG, "*** " + MessageCacheChatService.size() + " Messages of MessageCacheChatService saved!");
    }

    @Override
    public void execute(@NonNull Runnable runnable) {
        runnable.run();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    sensorEvent.timestamp, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void step(long timeNs) {
        numSteps++;
    }

    public void setActivityStopped(MainActivity mainActivity, boolean b) {
        this.activityStopped = b;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        if (this.userID != userID) {
            this.userID = userID;
            initializeLogging(userID, clsHTTPS, db);
            lib.setStatusAndLog(context, TAG, "Logging started setUserID");
        }
    }

    public int getGrad() {
        return grad;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public synchronized void relogin(boolean showProgress) throws Throwable {
        if (tReconnect != null && tReconnect.isAlive()) tReconnect.interrupt();
        user = null;
        setUserID(-1);
        SharedPreferences prefs = this.context.getSharedPreferences("secret", MODE_PRIVATE);
        String AccessKey = prefs.getString("KEY", null);
        String Email = prefs.getString("Email", null);
        String Benutzername = prefs.getString("Benutzername", null);
        if (AccessKey != null && Email != null && Benutzername != null && clsHTTPS != null) {
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "relogin", null, false, false);
            if (activity != null && ((MainActivity) activity).fragLogin != null) {
                dlgLogin fragLogin = ((MainActivity) activity).fragLogin;
                if (fragLogin.mAuthTask != null || fragLogin.isVisible() || fragLogin.isvisible()) {
                    createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "relogin cancelled fragLogin ", null, false, false);
                    return;
                }
            }
            Log.e(TAG, "relogin: " + Benutzername);
            if (mAuthTask != null) mAuthTask.cancel(true);
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "relogin started", null, false, false);
            mAuthTask = new UserLoginTask(showProgress, false, Email, Benutzername, null, null, null, null, null, null, AccessKey, false, null);
            if (! showProgress) {
                mAuthTask.execute((Void) null);
            }
            else
            {
                String res = "";
                mAuthTask.execute((Void) null);
                System.out.println(res);
            }
        } else {
            throw new Exception(getString(R.string.NoReloginPossible));
        }
        /*
        //fragLogin f = fragLogin;
        if (f!=null) {
            if (f.isAdded()) f.dismiss();
            fragLogin.mode = de.org.Limto.limindo2.fragLogin.LoginMode.none;
            f.logon(null,false,null);
        }
        */
    }

    public void unBind(boolean enqueue) throws Throwable {
        mHandler = mHandlerUnbound;
        clsHTTPS.setHandler(mHandler);
        //isBound = context.bindService( new Intent(context, BluetoothChatFragment.class), serviceConnection, Context.BIND_AUTO_CREATE );
        {
            try {
                if (this.activity != null) {
                    Bundle b = new Bundle();
                    ((MainActivity) activity).SaveInstanceState(b);
                    SharedPreferences prefs = getSharedPreferences("Service", MODE_PRIVATE);
                    lib.savePreferencesBundle(prefs, "MainActivity", b);
                    showAnswers = ((MainActivity) activity).showAnswers;
                    this.activity.unbindService(serviceConnection);
                    ((MainActivity) activity).ServiceStarted = 0;
                    ((MainActivity) activity).mServiceInitialized = false;
                    ((MainActivity) activity).mServiceRequested = false;
                    ((MainActivity) activity).mService = null;
                    ((MainActivity) activity).setStatus(getString(R.string.disconnectingservice), false);
                }
            } catch (Throwable ex) {
                Log.e("service", "unbind", ex);
            }
            isBound = false;
        }
        context = getApplicationContext();
        this.activity = null;
        clsHTTPS.context = context;
        if (db != null) db.mContext = context;
        broadcastReceiver.service = this;
        sendMessageToNotification(getString(R.string.serviceunbound), false, false, true);
        //this.fragLogin._main = null;
        //this.fragLogin.context = context;
        //this.fragLogin.mService = this;
        //this.fragLogin.mode = de.org.Limto.limindo2.fragLogin.LoginMode.none;
        //relogin();
        saveMessageCacheChatService();
        if (enqueue) enqueueWorkAndPostKill(IAUTOKILL, false);
    }

    /**
     * Constructor. Prepares a new BluetoothChat session.
     *
     * @param context   The UI Activity Context
     * @param handler   A Handler to send messages back to the UI Activity
     * @param fragLogin
     */
    public void init(AppCompatActivity a, Context context, Handler handler, dlgLogin fragLogin, dbSqlite db, boolean quiet) throws Throwable {
        setActivityHandlerHttps(handler, context, a);
        // runANRThread();
        Context c;

        //init count and Context
        if (a != null) {
            c = a;
            if (mHandler != mHandlerUnbound) {
                countAnswers = 0;
                countQuestions = 0;
            }
            ((MainActivity) a).blnServicWasDestroyed = false;
            this.quiet = false;
        } else {
            c = context;
        }
        //this.fragLogin = fragLogin;

        lib.setStatusAndLog(context, TAG, "setup loc client");
        if (mFusedLocationClient == null) {
            setupLocClient();
        }

        this.activityStopped = false;

        lib.setStatusAndLog(context, TAG, "init db");
        initializeDBInit(c);

        lib.setStatusAndLog(context, TAG, "init clsHTTPS");
        createHTTPS(c, context, a);

        lib.setStatusAndLog(context, TAG, "init Activity");
        initActivity();
        //lib.OkCancelStringResult res = lib.InputBox(a,"StepSensitivity","Sensitivity","50",false);
        String res = null;
        lib.setStatusAndLog(context, TAG, "init StepDetector");
        initStepDetector(res);

        //this.relogin();
        updateUserInterfaceTitle(false, quiet);


        lib.setStatusAndLog(context, TAG, "init finished");


    }

    private void initStepDetector(String res) {
        try {
            res = "12"; //lib.getSetting(context, "step sensitivity", "12");
            if (!lib.libString.IsNullOrEmpty(res)) {
                float f = Float.parseFloat(res);
                if (f > 0) StepDetector.STEP_THRESHOLD = f;
            } else {
                //StepDetector.STEP_THRESHOLD = 12f; //Float.parseFloat(res.input);
            }
        } catch (Throwable throwable) {
            Log.e("Service", "Step Threshhold", throwable);
        }
    }

    private void initActivity() throws JSONException {
        if (activity != null) {
            MainActivity main = (MainActivity) activity;
            main.clsHTTPS = clsHTTPS;
            this.blnNotOff = main.blnNotOff;
            JSONObject u = main.user;
            if (u == null) u = user;
            if (u != null) {
                if (u.getBoolean(Constants.administrator)) {
                    clsHTTPS.AdminID = u.getLong(Constants.id);
                }
            }
            if (showAnswers) ((MainActivity) activity).showAnswers = showAnswers;
        }
    }

    private void initializeDBInit(Context c) throws Throwable {
        if (db == null && this.db == null) {
            initializeDB(c);
        } else {
            this.db.mContext = c;
        }
        if (db != null) this.db = db;
    }

    private void createHTTPS(Context c, Context context, AppCompatActivity a) {
        if (clsHTTPS == null) {
            clsHTTPS = new clsHTTPS(c, mHandler, null);
            lib.setStatusAndLog(context, TAG, "init Logging");
            if (userID > -1) {
                initializeLogging(userID, clsHTTPS, db);
                lib.setStatusAndLog(context, TAG, "Logging started service.init");
            }
        }
        lib.setStatusAndLog(context, TAG, "init clsHTTPS");
        if (clsHTTPS != null) {
            if (a != null) clsHTTPS.context = a;
            else clsHTTPS.context = context;
            clsHTTPS.main = (MainActivity) a;
            clsHTTPS.setHandler(mHandler);
        }
    }

    private void setActivityHandlerHttps(Handler handler, Context pcontext, AppCompatActivity pa) {
        mHandler = handler;
        this.context = pcontext;
        this.activity = pa;
    }

    /*public synchronized void connected(final WebSocketClient conn, final boolean isalive) {
        Log.d(TAG, "connected, Socket Type:" + conn);

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }


        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(conn, isalive);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.DEVICE_NAME, MYURL);
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        updateUserInterfaceTitle(isalive);
    }
*/
    /*public synchronized void stopConnection() throws Throwable {
        if (webSocketClient != null) webSocketClient.close();
        if (clsHTTPS != null && userID > -1) try {
            clsHTTPS.logoff(userID);
            setUserID(-1);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }


    }
    */

    public void setupLocClient() {
        boolean gpsOff = context.getSharedPreferences("Settings", Context.MODE_PRIVATE).getBoolean("GPSOff", false);
        fragSettings.gpsOff = gpsOff;
        if (gpsOff || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            locCoarse = location;
                        }
                    }
                });
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(600000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    if (locCoarse == null || locCoarse.getTime() < location.getTime() || (location.getAccuracy() > 0.0f && location.getAccuracy() < locCoarse.getAccuracy())) {
                        locCoarse = location;
                    }
                }
            }

            ;
        };

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null /* Looper */);

    }

    public void stopLocationUpdates() throws Throwable {
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        mFusedLocationClient = null;
        locCoarse = null;
    }

    /**
     * Update UI title according to the current state of the chat connection
     */
    public synchronized void updateUserInterfaceTitle(boolean isalive, boolean quiet) {
        updateUserInterfaceTitle(isalive, quiet, false);
    }

    public synchronized void updateUserInterfaceTitle(boolean isalive, boolean quiet, boolean dontreconnect) {
        try {
            mState = getState();
            lib.setStatusAndLog(context, TAG, "updateUserInterfaceTitle() " + getStateString(mNewState) + " -> " + getStateString(mState));
            mNewState = mState;

            // Give the new state to the Handler so the UI Activity can update
            Message msg = mHandler.obtainMessage(MESSAGE_STATE_CHANGE, mNewState, -1);
            Bundle b = new Bundle();
            if (mNewState == STATE_ERROR || isalive) {
                if (mNewState == STATE_ERROR) {
                    if (mErrMsg.startsWith("Failed to connect to"))
                        mErrMsg = getString(R.string.not_connectiontoserver);
                    b.putString(Constants.ERR_MSG, mErrMsg);
                }
                if (isalive) b.putBoolean(Constants.ISALIVE, isalive);
            }
            b.putBoolean(Constants.QUIET, quiet);
            msg.setData(b);
            msg.sendToTarget();
        } catch (Throwable ex) {
            ex.printStackTrace();
            Log.e(TAG, null, ex);
        }

    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
        Log.d(TAG, "start");
        if (tReconnect != null && tReconnect.isAlive()) tReconnect.interrupt();
        mblnStop = false;
        // Cancel any thread attempting to make a connection
        if (clsHTTPS != null && userID > -1) try {
            clsHTTPS.logoff(userID, this.boot);
            user = null;
            setUserID(-1);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }

        broadcastReceiver.service = this;
        mState = STATE_NONE;
        // Update UI title
        updateUserInterfaceTitle(false, true);
    }

    public synchronized String initReceiverAndConnect(final int jksFile, final Map<String, String> params, final String startparam, final boolean isalive, final boolean quiet) throws Throwable {
        try {
            setStatusAndLog(context, TAG, "connect to: " + startparam);
            initBroadCastReceiverifNull(isalive);
            connectIfNotConnectedOrError(jksFile, params, startparam, isalive, quiet);
        } catch (Throwable ex) {
            sendErrorToHandlerSetStateAndErrMsg(ex, "WebsocketError");
            throw ex;
        }
        return "";
    }

    private void sendErrorToHandlerSetStateAndErrMsg(Throwable ex, String title) {
        mState = STATE_ERROR;
        mErrMsg = ex.getMessage();
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_EXCEPTION);
        msg.obj = ex;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.MESSAGE, title);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    private void initBroadCastReceiverifNull(boolean isalive) {
        if (receiver == null) {
            initBroadCastReceiver(isalive);
        }
    }

    private void connectIfNotConnectedOrError(int jksFile, Map<String, String> params, String startparam, boolean isalive, boolean quiet) throws Throwable {
        // Cancel reconnection in progress
        if (mState == STATE_NONE || mState == STATE_ERROR) {
            cancelThreadReconnect();
            connectToNewWebsocket(jksFile, params, startparam, isalive, this.quiet);
        }
    }

    private void cancelThreadReconnect() {
        if (tReconnect != null) tReconnect.interrupt();
    }

    private void connectToNewWebsocket(final int jksFile, final Map<String, String> params, final String startparam, final boolean isalive, final boolean quiet) throws Throwable {
        mState = STATE_CONNECTING;
        lib.setStatusAndLog(context, TAG, "create websocket");
        //mState = STATE_NONE;
        updateUserInterfaceTitle(isalive, quiet);
        closeWebSocket();


        try {
            URI uri = buildURIWebsocket(params);

            //HttpsURLConnection.setDefaultHostnameVerifier(new clsHTTPS.NullHostNameVerifier());
            //conn = (HttpsURLConnection) url.openConnection();
            createSSLSocketFactory(jksFile);
            {
                mState = STATE_CONNECTING;
                webSocketClient = createNewWebsocket(uri, sslSocketFactory, isalive);
                initWebSocketClient(webSocketClient);
            }

            //mConnectThread = new ConnectThread(jksFile, params, startparam,isalive);
            //mConnectThread.start();

        } catch (Throwable ex) {
            lib.setStatusAndLog(context, TAG, ex.getMessage());
            relogin(false);
        }

    }

    private void closeWebSocket() throws Exception {
        if (webSocketClient != null) {
            isWebSocketConnected = false;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (webSocketClient.getisRunning()) {
                        webSocketClient.close();
                    }
                    //webSocketClient = null;
                }
            });
            t.start();
            try {
                t.join(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (t.isAlive()) {
                t.interrupt();
                t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        webSocketClient.kill();
                    }
                });
                t.start();
                try {
                    t.join(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (t.isAlive()) {
                    throw new Exception("Could not close Websocket!");
                }
            }
            mState = STATE_NONE;
        }
    }

    private void initWebSocketClient(WebSocketClient webSocketClient) {
        webSocketClient.setConnectTimeout(10000);
        webSocketClient.addHeader("Origin", "https://v15849.1blu.de");
        lib.setStatusAndLog(context, TAG, "webSocketClient.connect");
        updateUserInterfaceTitle(true, true, true);
        webSocketClient.connect();
        lib.setStatusAndLog(context, TAG, "webSocketClient.connect started");
    }

    private void createSSLSocketFactory(int jksFile) {
        TrustManager[] trustManagers = null;
        if (sslSocketFactory == null) {
            try {
                Object[] o = getSSLFactory(jksFile, context);
                trustManagers = (TrustManager[]) o[1];
                sslSocketFactory = (SSLSocketFactory) o[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private URI buildURIWebsocket(Map<String, String> params) throws Exception {
        if (!params.containsKey(ACCESSKEY) || params.get(ACCESSKEY) == null || params.get(ACCESSKEY).length() < 5) {
            if (clsHTTPS.accesskey != null) {
                params.put(ACCESSKEY, clsHTTPS.accesskey);
            } else {
                lib.setStatusAndLog(context, TAG, "Accesskey NULL!");
                throw new Exception("Accesskey null");
            }
        }
        Uri.Builder builder = Uri.parse(MYURLTALK).buildUpon();
        builder.appendPath(params.get(BENUTZER_ID));
        builder.appendPath(params.get("Grad"));
        MainActivity main = activity != null ? (MainActivity) activity : null;
        MainActivity.OnlineState onlineState = main != null ? main.onlineState : MainActivity.getOnlineState(context);
        ;
        builder.appendPath("" + onlineState.state);
        String key = params.get(ACCESSKEY);
        if (key == null) key = clsHTTPS.accesskey;
        key = key != null ? key.replace("/", "|") : null;
        if (key == null) throw new Exception("Accesskey null");
        builder.appendPath(key);
        String u = builder.toString();
        URI ur = null;
        try {
            ur = new URI(u);
            //uri = new URI(uri.toASCIIString());
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return ur;
    }

    private void initBroadCastReceiver(boolean isalive) {
        setStatusAndLog(context, TAG, "Init receiver ");
        receiver = createBroadcastReceiver(isalive);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.supplicant.CONNECTION_CHANGE");
        setStatusAndLog(context, TAG, "register receiver ");
        LimindoService.this.registerReceiver(receiver, filter);
        setStatusAndLog(context, TAG, "register receiver finished");
    }

    private BroadcastReceiver createBroadcastReceiver(boolean isalive) {
        return new BroadcastReceiver() {
            private long lastReceived = 0;

            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    int res = -1;
                    long received;
                    if ((received = Calendar.getInstance().getTimeInMillis()) - lastReceived < 10000) {
                        lastReceived = received;
                        // return;
                    }
                    boolean isnotconnecting = webSocketClient != null && (tReconnect == null);
                    boolean notconnectedAndOnline = isnotconnecting
                            && (mState == STATE_NONE || mState == STATE_ERROR)
                            && (res = isisonline() ? 1 : 0) == 1;
                    boolean notnotconnectedAndLonlineAndConnectedAndOffline =
                            isnotconnecting && !notconnectedAndOnline
                                    && (mState == STATE_CONNECTED || mState == STATE_CONNECTING)
                                    && (res = isisonline() ? 1 : 0) == 0;
                    if (isnotconnecting && (notconnectedAndOnline || notnotconnectedAndLonlineAndConnectedAndOffline)) {
                        reconnectwhenonline(new Exception(intent.getAction()), true, false, isalive, false, res);
                    } else {
                        StringBuilder err = new StringBuilder();
                        err.append("isnotconnecting");
                        err.append(isnotconnecting);
                        err.append("\n");
                        err.append("websocketClient != null");
                        err.append(webSocketClient != null);
                        err.append("\n");
                        err.append("tReconnect == null");
                        err.append(tReconnect == null);
                        err.append("STATE");
                        err.append(mState);
                        err.append(getStateString(mState));
                        err.append("\n");
                        err.append("\n");
                        err.append("notconnectedandonline");
                        err.append(notconnectedAndOnline);
                        err.append("\n");
                        err.append("notconnectedandonlineAndConnectedAndOffline");
                        err.append(notnotconnectedAndLonlineAndConnectedAndOffline);
                        err.append("\n");
                        gStatus = err.toString();
                        Log.i(TAG, err.toString());
                        /*Exception e = new Exception(err.toString());
                        Message msg = mHandler.obtainMessage(Constants.MESSAGE_EXCEPTION);
                        msg.obj = e;
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.MESSAGE, "BroadcastReceiverError");
                        msg.setData(bundle);
                        mHandler.sendMessage(msg);
                         */
                    }
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Message msg = mHandler.obtainMessage(Constants.MESSAGE_EXCEPTION);
                    msg.obj = throwable;
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.MESSAGE, "BroadCastReceiverError");
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                }
            }
        };
    }

    private String getStateString(int mState) {
        String strState = "";
        switch (mState) {
            case STATE_CONNECTED:
                strState = "CONNECTED";
                break;
            case STATE_CONNECTING:
                strState = "CONNECTING";
                break;
            case STATE_ERROR:
                strState = "ERROR";
                break;
            case STATE_LISTEN:
                strState = "LISTEN";
                break;
            case STATE_NONE:
                strState = "NONE";
                break;
        }
        return strState;
    }

    private WebSocketClient createNewWebsocket(URI uri, SSLSocketFactory sslSocketFactory, boolean isalive) {
        return new WebSocketClient(uri, sslSocketFactory) {
            boolean cancelled = false;
            Throwable ex = null;

            @Override
            public void onOpen() {
                lib.setStatusAndLog(context, TAG, "webSocketClient.onOpen ");
                mState = STATE_CONNECTED;
                connectionretries = 0;
                updateUserInterfaceTitle(isalive, true);
                isWebSocketConnected = true;

            }

            @Override
            public void onTextReceived(String message) {
                synchronized (LimindoService.this) {
                    WSisBusy = true;
                }
                System.out.println(message);
                String result = message;
                if (result.length() > 0 && !result.equals("onOpen")) {
                    String readMessage = result;
                    byte[] buffer = readMessage.getBytes();
                    int bytes = buffer.length;
                    if (bytes > 0) {
                        mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();
                    } else {
                        mState = STATE_ERROR;
                        mErrMsg = getString(R.string.emptyanswer);
                        updateUserInterfaceTitle(isalive, false);
                    }
                    //ir.close();
                    //mmInStream.close();
                    //mmInStream = conn.getInputStream();
                    //ir = new InputStreamReader(mmInStream);
                    if (!readMessage.startsWith("Error:")) {
                        if (!readMessage.startsWith("listen stopped for ")) {
                            Log.i(TAG, "go on after receiving " + readMessage);
                            //reconnect(false,isalive);
                        } else {
                            Log.e(TAG, ERROR + readMessage);
                        }

                    } else {
                        if (!readMessage.contains("The remote endpoint was in state [BINARY_FULL_WRITING] which is an invalid state for called method")
                                && !readMessage.contains("The remote endpoint was in state [TEXT_FULL_WRITING] which is an invalid state for called method")) {
                            mState = STATE_ERROR;
                            mErrMsg = readMessage.replace("Error:", "");
                            Log.e(TAG, ERROR + readMessage);
                            updateUserInterfaceTitle(isalive, false);
                        } else {
                            Log.i(TAG, readMessage);
                        }
                    }
                } else if (result.length() > 0) {
                    this.send("received");
                }
                synchronized (LimindoService.this) {
                    WSisBusy = false;
                }
            }

            @Override
            public void onBinaryReceived(byte[] data) {
                synchronized (LimindoService.this) {
                    WSisBusy = true;
                }
                System.out.println("onBinary");
                if (data.length > 0) {
                    byte[] buffer = data;
                    int bytes = buffer.length;
                    if (bytes > 0) {
                        mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();
                    } else {
                        mState = STATE_ERROR;
                        mErrMsg = getString(R.string.emptyanswer);
                        updateUserInterfaceTitle(isalive, false);
                    }
                    //ir.close();
                    //mmInStream.close();
                    //mmInStream = conn.getInputStream();
                    //ir = new InputStreamReader(mmInStream);

                    this.send("received");
                    synchronized (LimindoService.this) {
                        WSisBusy = false;
                    }
                }
            }

            @Override
            public void onPingReceived(byte[] data) {
                System.out.println("onPing");
            }

            @Override
            public void onPongReceived(byte[] data) {
                LimindoService.this.Pong = data;
            }

            @Override
            public void onException(Exception e) {
                synchronized (LimindoService.this) {
                    WSisBusy = true;
                }
                System.out.println(e.getMessage());
                lib.setStatusAndLog(context, TAG, "websocket error " + e.getMessage());
                isWebSocketConnected = false;
                sendErrorToHandlerSetStateAndErrMsg(e, "WebsocketError");
                try {
                    if (e instanceof java.net.ConnectException) {
                        if (cancelled) return;
                        ex = e;
                        Log.e(TAG, "WSERROR ConnectException", e);
                        //ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        reconnectwhenonline(e, true, true, isalive);

                    } else if (e instanceof SSLException) {
                        if (cancelled) return;
                        ex = e;
                        Log.e(TAG, "WSERROR SSLException", e);
                        reconnectwhenonline(e, true, true, isalive);

                    } else if (e instanceof java.net.UnknownHostException) {
                        if (cancelled) return;
                        ex = e;
                        Log.e(TAG, "WSERROR UnknownHostException", e);
                        reconnectwhenonline(e, true, true, isalive);

                    } else {
                        if (cancelled) return;
                        ex = e;
                        Log.e(TAG, "WSERROR", e);
                        updateUserInterfaceTitle(isalive, false);
                        if (activity == null) {
                            reconnectwhenonline(e, true, true, isalive);
                        } else {
                            connectionLost(e);
                            return;
                        }

                    }
                } catch (Throwable ex) {
                    ex.printStackTrace();
                    Log.e(TAG, null, ex);
                }
                updateUserInterfaceTitle(isalive, quiet);
                synchronized (LimindoService.this) {
                    WSisBusy = false;
                }

            }

            @Override
            public void onCloseReceived() {
                synchronized (LimindoService.this) {
                    WSisBusy = true;
                }
                cancelled = true;
                if (mState != STATE_ERROR) {
                    mState = STATE_NONE;
                }
                updateUserInterfaceTitle(isalive, false);
                reconnectIfConnectedTrue();
                showMessageClosedIfNotConnectedAfter5Secs();
                synchronized (LimindoService.this) {
                    WSisBusy = true;
                }
            }

            private void showMessageClosedIfNotConnectedAfter5Secs() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (mState != STATE_CONNECTED)
                                sendMessageToNotification(getString(R.string.connectionlostwebsocket), false, false, true);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                }, 5000);

            }

            private void reconnectIfConnectedTrue() {
                if (isWebSocketConnected) {
                    isWebSocketConnected = false;
                    try {
                        reconnectwhenonline(null, true, false, false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }

            }
        };

    }


    private boolean isisonline() {
        boolean res;
        try {
            res = isOnline();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            res = false;
        }
        return res;
    }

    void reconnectwhenonline(Throwable e, boolean always, boolean networkError, boolean isalive) throws Throwable {
        reconnectwhenonline(e, always, networkError, isalive, false);
    }

    public void connectService(boolean restart, boolean isalive, boolean quiet) throws Throwable {
        Log.i(TAG, "connectService");
        if (user != null && (this.getUserID() != user.getLong(Constants.id) || restart)) {
            //this.stop();
            //mService.stopConnection();
            lib.setStatusAndLog(context, TAG, "connectService start user != null");
            connectToWebsocketIfNotConnected(isalive);
            {
                showNewQuestionsAndNewAnswers();
            }
            Log.i(TAG, "connectservice Completed ");
        }

    }

    private void showNewQuestionsAndNewAnswers() throws Throwable {
        neueFragen = user.getInt("neuefragen");
        neueAntworten = user.getInt("neueantworten");
        if (neueAntworten > 0 || neueFragen > 0) {
            String msg = "";
            if (neueFragen > 0) {
                msg = neueFragen + " " + getString(R.string.newQuestions);
                if (neueAntworten > 0) msg += "\n";
            }
            if (neueAntworten > 0) {
                msg += neueAntworten + " " + getString(R.string.newAnswers);
            }
            if (msg.length() > 0) {
                createNewNotification(ONGOING_NOTIFICATION_ID_NEW, getString(R.string.service), msg, null, false, false);
                //sendMessageToNotification(msg, false, false, true);
            }
        }
        if (activity != null) {
            ((MainActivity) activity).user = user;
            neueFragen = 0;
            neueAntworten = 0;
        }
    }

    private void connectToWebsocketIfNotConnected(boolean isalive) throws Throwable {
        Map<String, String> p = new java.util.HashMap<String, String>();
        String res = null;
        long BenutzerID = user.getLong(Constants.id);
        p.put(BENUTZER_ID, String.valueOf(BenutzerID));
        Grad = setKontaktGrad();
        p.put("Grad", "" + Grad);

        res = this.initReceiverAndConnect(0, p, null, isalive, quiet);

        this.setUserID(BenutzerID);
        this.setGrad(Grad);
    }

    private int setKontaktGrad() throws Exception {
        if (Grad == 0) Grad = 5;
        if (Grad < 0) {
            if (activity != null) {
                Grad = ((MainActivity) activity).getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);
            } else {
                throw new Exception("Grad < 0!");
            }
        }
        return Grad;
    }

    public void reconnect(final boolean networkError, final boolean isalive) throws Exception {
        mState = STATE_NONE;
        updateUserInterfaceTitle(isalive, true);
        if (tReconnect != null) tReconnect.interrupt();
        if (!mblnStop) {
            Message msg = mHandler.obtainMessage(Constants.MESSAGE_RECONNECT);
            Bundle b = new Bundle();
            b.putBoolean(Constants.SHOWUSERINTERFACE, false);
            b.putBoolean(Constants.ISNETWORKERROR, networkError);
            b.putBoolean(Constants.ISALIVE, isalive);
            b.putBoolean(Constants.QUIET, true);
            msg.setData(b);
            mHandler.sendMessage(msg);
            createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, "Debug", "reconnect", null, false, false);
        } else {
            Log.e(TAG, "reconnect mblnStop");
        }
    }

    public void reconnectMainThread(final boolean networkError, final boolean isalive, final boolean quiet) throws
            Throwable {
        if (tReconnect != null) tReconnect.interrupt();
        Log.e(TAG, "reconnectMainThread started retries: " + connectionretries + " networkError: " + networkError + " lastWasNetworkError: " + lastWasNetworkError);
        if (lastWasNetworkError) {
            if (!(postdelayed++ > 1) && connectionretries < 3) {

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            postdelayed = 0;
                            lastWasNetworkError = false;
                            reconnectMainThread(networkError, isalive, quiet);
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                }, 2000);
                return;
            } else {
                Log.e(TAG, "no postDelayed reconnect");
            }

        } else {
            postdelayed = 0;
        }
        if (userID < 0 && user != null) userID = user.getLong(Constants.id);
        if (userID < 0 && activity != null && ((MainActivity) (activity)).user != null)
            userID = ((MainActivity) (activity)).user.getLong(Constants.id);
        if (grad < 0 && activity != null) {
            grad = activity.getPreferences(Context.MODE_PRIVATE).getInt(GRADE, 5);
        }
        if (!(userID < 0 || grad < 0) && connectionretries < 4 && (networkError || mState == STATE_NONE || mState == STATE_ERROR)) {
            mState = STATE_NONE;
            updateUserInterfaceTitle(isalive, quiet);
            Map<String, String> p = new java.util.HashMap<String, String>();
            String res = null;
            p.put(BENUTZER_ID, String.valueOf(this.userID));
            p.put("Grad", "" + grad);
            res = initReceiverAndConnect(0, p, null, isalive, quiet);
            connectionretries++;
            lastWasNetworkError = networkError;
            if (networkError) mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lastWasNetworkError = false;
                }
            }, 1500);
        } else {
            try {
                mState = STATE_NONE;
                updateUserInterfaceTitle(isalive, quiet);
                Log.e(TAG, "reconnectMainThread relogin retries: " + connectionretries + " networkError: " + networkError + " lastWasNetworkError: " + lastWasNetworkError);
                relogin(false);
            } catch (Throwable throwable) {
                Log.e(TAG, null, throwable);
                throwable.printStackTrace();
                lib.ShowException(TAG, context, throwable, false);
                throw throwable;
            }
        }

    }

    public synchronized void stop(boolean dontLogoff) throws Throwable {
        Log.d(TAG, "stop");
        //mblnStop = true;
        /*if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
         */
        //stopConnection();
        if (tReconnect != null && tReconnect.isAlive()) tReconnect.interrupt();
        if (!dontLogoff && clsHTTPS != null && userID > -1) try {
            clsHTTPS.logoff(userID, this.boot);
            user = null;
            setUserID(-1);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }

        closeWebSocket();


        // Update UI title
        try {
            updateUserInterfaceTitle(false, true);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }

    }

    public void stopService(boolean dontRemoveNotifications, boolean dontLogoff) throws Throwable {
        try {
            mblnStop = true;
            unBind(false);
            stop(dontLogoff);
            stopForeground(true);
            this.stopSelf();
            if (!dontRemoveNotifications) {
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancelAll();
            }
            broadcastReceiver.service = null;
            if (receiver != null) {
                unregisterReceiver(receiver);
                receiver = null;
            }
            // stopANRThread();
        } catch (Throwable ex) {
            Log.e("Service", "Stopservice", ex);
        }
        // in unbind: saveMessageCacheChatService();
        servicestopped = true;

    }
/*
    private void stopANRThread() {
        if (ANRThread != null) {
            try {
                ANRThread.interrupt();
                ANRThread = null;
            } catch (Throwable ex) {
                ex.printStackTrace();
            }
        }
    }

 */

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     */
    public void write(byte[] out) {
        // Create temporary object
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
        }
        synchronized (LimindoService.this) {
            WSisBusy = true;
        }
        // Perform the write unsynchronized
        webSocketClient.send(out);
        synchronized (LimindoService.this) {
            WSisBusy = false;
        }
    }

    public boolean interrupted;

    private boolean write(String message) throws Exception {
        synchronized (LimindoService.this) {
            WSisBusy = true;
        }
        synchronized (this) {
            if (mState != STATE_CONNECTED) throw new Exception("Not Connected!");
        }
        // Perform the write unsynchronized
        try {
            if (webSocketClient.getisRunning()) {
                LimindoService.this.Pong = null;
                interrupted = false;
            /*
            webSocketClient.sendPing(new byte[]{5,4,3,2,1});
            Thread t = new Thread(new Runnable() {

                @Override
                public void run() {
                    do {
                        if (LimindoService.this.Pong != null && Arrays.equals(LimindoService.this.Pong, new byte[]{5, 4, 3, 2, 1})) break;;
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            interrupted = true;
                            break;
                        }
                    } while (true);
                }
            });
            t.start();
            t.join(maxNetworkTimeout);
            if (interrupted) return false;
            */
                webSocketClient.send(message);
                return true;
            }

            return false;
        } finally {
            synchronized (LimindoService.this) {
                WSisBusy = false;
            }
        }
    }
    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    /*
    private class ConnectThread extends Thread {


        private final int jksFile;
        private final Map<String, String> params;
        private final String startparam;
        private final boolean isalive;
        private SSLSocketFactory sslSocketFactory;
        private HttpsURLConnection conn;
        private boolean cancelled;

        public ConnectThread(int jksFile, Map<String, String> params, String startparam, boolean isalive) {

            this.jksFile = jksFile;
            this.params = params;
            this.startparam = startparam;
            mState = STATE_CONNECTING;
            this.isalive = isalive;
            updateUserInterfaceTitle(isalive);

        }

        public void run() {
            String url;
            try {

                url = MYURLTALK + "/" + params.get("BenutzerID") + "/" + params.get("Grad") + "/" + params.get("Accesskey");
                URI uri = new URI(url);
                //HttpsURLConnection.setDefaultHostnameVerifier(new clsHTTPS.NullHostNameVerifier());
                //conn = (HttpsURLConnection) url.openConnection();
                if (sslSocketFactory == null) sslSocketFactory = getSSLFactory(jksFile);
                WebSocketClient w = new WebSocketClient(uri,sslSocketFactory) {
                    @Override
                    public void onOpen() {

                    }

                    @Override
                    public void onTextReceived(String message) {
                        System.out.println(message);
                    }

                    @Override
                    public void onBinaryReceived(byte[] data) {

                    }

                    @Override
                    public void onPingReceived(byte[] data) {

                    }

                    @Override
                    public void onPongReceived(byte[] data) {

                    }

                    @Override
                    public void onException(Exception e) {
                        System.out.println(e.getMessage();)
                    }

                    @Override
                    public void onCloseReceived() {

                    }
                };
                //conn.setSSLSocketFactory(sslSocketFactory);

                //conn.setRequestMethod("POST");
                //Map<String, List<String>> headers = conn.getHeaderFields();
                //conn.setDoOutput(true);
                //conn.setUseCaches(true);

                //synchronized (LimindoService.this) {
                //mConnectThread = null;
                //}

                connected(w, isalive);
                // Start the connected thread
            } catch (Exception e) {
                try {
                    conn = null;
                } catch (Exception ex) {
                    Log.e(TAG, "unable to close() " +
                            " conn during connection failure", ex);
                }
                if (!cancelled) connectionFailed(e);
            }

        }

        public void cancel() {
            try {
                this.cancelled = true;
                conn = null;
            } catch (Exception e) {
                Log.e(TAG, "close() of connect " + conn.getURL().toString() + "  failed", e);
            }
        }
    }
    */


    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    /*private class ConnectedThread extends Thread {
        private final boolean isalive;
        private InputStream mmInStream;
        private OutputStream mmOutStream;
        private WebSocketClient conn;
        private boolean cancelled;

        public ConnectedThread(WebSocketClient conn, boolean isalive) {
            Log.d(TAG, "create ConnectedThread: " + conn.toString());
            this.conn = conn;
            //mState = STATE_CONNECTED;
            this.isalive = isalive;
            //updateUserInterfaceTitle(isalive);
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");

            InputStream tmpIn = null;
            OutputStream tmpOut = null;



            *//*
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace(); Log.e(TAG,null,e);
            }
            *//*
            // Keep listening to the InputStream while connected
            try {
                Throwable ex = null;
                try {
                    mState = STATE_CONNECTED;
                    updateUserInterfaceTitle(isalive);
                    //tmpIn = conn.getInputStream();
                    lastWasNetworkError = false;
                    connectionretries = 0;
                    //tmpOut = conn.getOutputStream();
                } catch (java.net.ConnectException e) {
                    if (cancelled) return;
                    ex = e;
                    Log.e(TAG, "temp streams not created! ConnectException", e);
                    //ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    if (reconnectwhenonline(e, true, true, isalive)) return;
                } catch (SSLException e) {
                    if (cancelled) return;
                    ex = e;
                    Log.e(TAG, "SSLException", e);
                    mState = STATE_ERROR;
                    mErrMsg = e.getMessage();
                    if (reconnectwhenonline(e, true, true, isalive)) return;
                } catch (java.net.UnknownHostException e) {
                    if (cancelled) return;
                    ex = e;
                    Log.e(TAG, "temp streams not created UnknownHostException", e);
                    mState = STATE_ERROR;
                    mErrMsg = e.getMessage();
                    if (reconnectwhenonline(e, false, true, isalive)) return;
                } catch (Throwable e) {
                    if (cancelled) return;
                    ex = e;
                    Log.e(TAG, "temp streams not created2", e);
                    mState = STATE_ERROR;
                    mErrMsg = e.getMessage();
                    if (a == null) {
                        if (reconnectwhenonline(e, true, true, isalive)) return;
                    } else {
                        connectionLost(e);
                        return;
                    }

                }
                if (tmpIn != null) {
                    mmInStream = tmpIn;
                    mmOutStream = tmpOut;

                    //ir = new InputStreamReader(mmInStream);


                    while (mState == STATE_CONNECTED) {
                        try {
                            Scanner s = new Scanner(mmInStream, "UTF-8").useDelimiter("\\A");
                            String result = s.hasNext() ? s.next() : "";

                            int i;
                            if (result.length() > 0) {
                                String readMessage = result;
                                boolean isalive = readMessage.equals("isalive");
                                if (isalive == false) {
                                    byte[] buffer = readMessage.getBytes();
                                    int bytes = buffer.length;
                                    if (bytes > 0) {
                                        mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)
                                                .sendToTarget();
                                    } else {
                                        throw new Exception("leere Antwort");
                                    }
                                    //ir.close();
                                    //mmInStream.close();
                                    //mmInStream = conn.getInputStream();
                                    //ir = new InputStreamReader(mmInStream);
                                } else {
                                    LimindoService.alivecount++;
                                }
                                if (!readMessage.startsWith("Error:") && !readMessage.startsWith("listen stopped for ")) {
                                    Log.i(TAG, "reconnect after receiving " + readMessage);
                                    //reconnect(false,isalive);

                                } else {
                                    mState = STATE_ERROR;
                                    mErrMsg = readMessage.replace("Error:", "");
                                    Log.e(TAG, "Connected Thread: " + readMessage);
                                }

                            }
                        } catch (Exception e) {
                            if (cancelled) return;
                            Log.e(TAG, "disconnected", e);
                            connectionLost(e);
                            break;
                        }

                    }
                } else if (!mblnStop) {
                    if (cancelled) return;
                    Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TOAST, "Die Verbindung konnte nicht hergestellt werden!");
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                    connectionFailed(ex);

                }

            } catch (Throwable ex) {
                if (cancelled) return;
                Log.e(TAG, "disconnected", ex);
                try {
                    mState = STATE_ERROR;
                    mErrMsg = ex.getMessage();
                    connectionLost(ex);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, null, throwable);
                }
            } finally {
                if (tmpIn != null) try {
                    tmpIn.close();
                    tmpIn = null;
                    mmInStream = null;
                } catch (Throwable e) {
                    Log.e(TAG, null, e);
                    e.printStackTrace();
                }
            }

        }
*//*
     */

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed(Throwable ex) {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, getString(R.string.connectionfailed));
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        mState = STATE_NONE;
        if (ex != null) {
            mState = STATE_ERROR;
            mErrMsg = ex.getMessage();
            Log.e(TAG, "connectionFailed", ex);
        }
        if (mErrMsg.startsWith(FAILED_TO_CONNECT)) {
            mErrMsg = getString(R.string.failedtoconnect);
            Log.e(TAG, mErrMsg);
        }
        // Update UI title
        updateUserInterfaceTitle(false, false);

        // Start the service over to restart listening mode
        if (!mblnStop) mHandler.sendMessage(mHandler.obtainMessage(Constants.MESSAGE_RELOGIN));
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost(Throwable ex) throws Throwable {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, getString(R.string.connectioninterrupted));
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        mState = STATE_NONE;
        if (ex != null) {
            mState = STATE_ERROR;
            mErrMsg = ex.getMessage();
        }
        if (mErrMsg == null || mErrMsg.startsWith("Read error"))
            mErrMsg = getString(R.string.connectionint);
        else if (mErrMsg.startsWith(FAILED_TO_CONNECT)) {
            mErrMsg = getString(R.string.failedtoconnect);
        }
        else if (mErrMsg.startsWith("v1589.1blu.de") && ex instanceof UnknownHostException)
        {
            mErrMsg = getString(R.string.unknownServer);
        }
        else if (mErrMsg.startsWith("Failed to connect to v1589.1blu.de") || mErrMsg.startsWith("Unable to resolve host \"v1589.1blu.de\"") && ex instanceof UnknownHostException)
        {
            mErrMsg = getString(R.string.cantconnectToServer);
        }
        else
        {
            mErrMsg = getString(R.string.unknownErrorMessage) + "\n" + mErrMsg;
        }

        // Update UI title
        try {
            sendMessageToNotification(getString(R.string.connectioninterrupted), true, false);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
        try {
            SharedPreferences prefs = getSharedPreferences("Limto", MODE_PRIVATE);
            if (prefs.getBoolean("Alarm", false) == true) {
                lib.play_sound(context, R.raw.zymbel);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Log.e(TAG, null, throwable);
        }
        // Start the service over to restart listening mode
        if (!mblnStop) {
            reconnectwhenonline(ex, true, true, false);
            /*
            msg = mHandler.obtainMessage(Constants.MESSAGE_RECONNECT);
            Bundle b = new Bundle();
            b.putBoolean(Constants.SHOWUSERINTERFACE, true);
            if (ex != null && ex instanceof ConnectException)
                b.putBoolean(Constants.ISNETWORKERROR, true);
            msg.setData(b);
            mHandler.sendMessage(msg);
            */
        }
        updateUserInterfaceTitle(false, true);
        //mblnStop = false;
    }

    public void sendMessageToNotification(String msg, boolean nosound, boolean alarm) throws
            Throwable {
        sendMessageToNotification(msg, nosound, alarm, false);
    }

    public void sendMessageToNotification(String msg, boolean nosound, boolean alarm,
                                          boolean popup) throws Throwable {
        lib.setStatusAndLog(context, TAG, "sendMessageToNotification " + msg);
        if (mblnStop) return;
        registerNotificationChannel();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// Sets an ID for the notification, so it can be updated
        int notifyID = SERVICE_NOTIFICATION_ID;
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (alarm)
            soundUri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + R.raw.alarm);
        if (nosound) soundUri = null;
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        //mBuilder.setContentIntent(resultPendingIntent);
        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationCompat.Builder mBuilder;
            Intent serviceIntent = new Intent(getApplicationContext(), LimindoService.class);
            serviceIntent.putExtra("action", "actionclose");
            serviceIntent.putExtra("ServiceIntent", true);
            serviceIntent.setAction(ACTION_CLOSE);
            Intent serviceIntent2 = new Intent(getApplicationContext(), LimindoService.class);
            serviceIntent2.putExtra("action", "actionreconnect");
            serviceIntent2.putExtra("ServiceIntent", true);
            serviceIntent2.setAction(ACTION_RECONNECT);
            PendingIntent servicePendingIntent = PendingIntent.getService(getApplicationContext(), REQUEST_CODE_CLOSE, serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent servicePendingIntent2 = PendingIntent.getService(getApplicationContext(), REQUEST_CODE_RECONNECT, serviceIntent2, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Action action = new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.close), servicePendingIntent).build();
            NotificationCompat.Action action2 = new NotificationCompat.Action.Builder(android.R.drawable.ic_menu_rotate, getString(R.string.mnuReconnect), servicePendingIntent2).build();
            mBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                    .setContentTitle(msg)
                    //.setContentText(msg)
                    .setSmallIcon(R.drawable.vn_logo_c02)
                    .setContentIntent(resultPendingIntent)
                    .setTicker(msg)
                    .setChannelId(NOTIFICATION_CHANNEL_ID)
                    .addAction(action)
                    .addAction(action2)
                    //.setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                    //        .setShowActionsInCompactView(0,1))
                    .setWhen(0)
                    .setOngoing(false)
            //.setSound(soundUri, AudioManager.STREAM_MUSIC)
            ;


            ;
            if (popup) {
                mBuilder.setPriority(Notification.PRIORITY_MAX);
                mBuilder.setDefaults(Notification.DEFAULT_ALL);
            } else if (nosound) {
                mBuilder.setDefaults(0)
                        .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_SUMMARY)
                        .setGroup(this.getString(R.string.Limto))
                        .setGroupSummary(false);
            }
            mNotificationManager.notify(
                    notifyID,
                    mBuilder.build());

        } else {
            androidx.core.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(getText(R.string.notification_title))
                    .setContentText(msg)
                    .setContentIntent(resultPendingIntent)
                    .setSmallIcon(R.drawable.vn_logo_c02)
                    .setTicker(msg)
                    .setOngoing(false)
                    //.setSound(soundUri)
                    ;
            ;
            if (popup) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mBuilder.setPriority(Notification.PRIORITY_MAX);
                }
                mBuilder.setDefaults(Notification.DEFAULT_ALL);
            } else if (nosound) {
                mBuilder.setDefaults(0);
            }

            mNotificationManager.notify(
                    notifyID,
                    mBuilder.build());

            //

        }

    }

    void createNewNotification(int notification_id, String Title, String msg, String readMessage, boolean emergency, boolean nosound) {
        createNewNotification(notification_id, Title, new SpannableString(msg), readMessage, emergency, nosound);
    }

    void createNewNotificationDebug(String Title, String msg) {
        createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, Title, msg, null, false, false, false);
    }

    void createNewNotificationDebug(String Title, String msg, boolean showalways) {
        createNewNotificationDebug(ONGOING_NOTIFICATION_ID_DEBUG + ++DebugMessages, Title, msg, null, false, false, showalways);
    }
    void createNewNotificationDebug(int notification_id, String Title, String msg, String readMessage, boolean emergency, boolean nosound) {
        createNewNotificationDebug(notification_id, Title, msg, readMessage, emergency, nosound, false);
    }


        void createNewNotificationDebug(int notification_id, String Title, String msg, String readMessage, boolean emergency, boolean nosound, boolean showalways) {
        if (showalways || (SHOWEXCEPTIONSDEBUG && (lib.debugMode()))) {
            if (this.activity == null) {
                createNewNotification(notification_id, Title, new SpannableString(msg), readMessage, emergency, nosound);
            }
            else
            {
                lib.ShowMessageDebug(this.activity,msg,Title);
            }
        }
    }

    void createNewNotification(int notification_id, String Title, CharSequence msg, String readMessage, boolean emergency, boolean nosound) {
        if (blnNotOff) return;
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        //notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("readMessage", readMessage);
        String xId = createRandomString(30);
        notificationIntent.setAction(xId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (emergency) {
            //soundUri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + R.raw.alarm);
            soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Title = Title.replace("Frage", "Notruf");
        }

        if (nosound) soundUri = null;
        boolean popup;
        Date dt = Calendar.getInstance().getTime();
        if (!emergency && dt.getTime() - this.dtLastNot.getTime() < this.NotificatonPopupIntervall) {
            dtLastNot = dt;
            popup = false;
        } else if (!emergency) {
            NotificatonPopupIntervall *= 2;
            dtLastNot = dt;
            popup = true;
        } else {
            popup = true;
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            registerNotificationChannel();
            Notification.Builder mBuilder;
            String msgShow = msg.toString();
            msgShow = msgShow.replace("THUMB\n", "");
            msg = msgShow;
            mBuilder = new Notification.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.vn_logo_c02)
                    .setContentTitle(Title)
                    .setContentText(msg)
                    .setStyle(new Notification.BigTextStyle().bigText(msg).setBigContentTitle(Title))
                    .setContentIntent(resultPendingIntent)
                    .setTicker(msg)
            //.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
            //.setSound(soundUri)
            ;
            if (popup) {
                mBuilder.setPriority(Notification.PRIORITY_MAX);
                mBuilder.setDefaults(Notification.DEFAULT_ALL);
            } else {
                mBuilder.setDefaults(0);
                mBuilder.setPriority(Notification.PRIORITY_MIN);
                mBuilder.setTicker(null);
            }
            notification = mBuilder.build();
            //if ((notification.flags & Notification.FLAG_ONLY_ALERT_ONCE) != 0) notification.flags ^= Notification.FLAG_ONLY_ALERT_ONCE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            if (!popup) {
                notification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
            } else {
                notification.flags &= ~Notification.FLAG_ONLY_ALERT_ONCE;
            }
            notificationManager.notify(notification_id, notification);

        } else {
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.drawable.vn_logo_c02)
                            .setContentTitle(Title)
                            .setContentText(msg)
                            .setTicker(msg)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(msg).setBigContentTitle(Title))
                            .setContentIntent(resultPendingIntent);
            //.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
            //.setSound(soundUri);
            if (popup) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mBuilder.setPriority(Notification.PRIORITY_MAX);
                }
                mBuilder.setDefaults(Notification.DEFAULT_ALL);
            } else {
                mBuilder.setDefaults(0);
                mBuilder.setPriority(Notification.PRIORITY_MIN);
                mBuilder.setTicker(null);
            }
            notification = mBuilder.build();
            //if ((notification.flags & Notification.FLAG_ONLY_ALERT_ONCE) != 0) notification.flags ^= Notification.FLAG_ONLY_ALERT_ONCE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            if (!popup) {
                notification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
            } else {
                notification.flags &= ~Notification.FLAG_ONLY_ALERT_ONCE;
            }
            notificationManager.notify(notification_id, notification);
        }
        //NOTIFICATION_ID++;
    }

    private void registerNotificationChannel() {
        if (notificationChannelRegistered) return;
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.notification_title), NotificationManager.IMPORTANCE_LOW);

            // Configure the notification channel.
            notificationChannel.setDescription(getString(R.string.LimindoNews));
            notificationChannel.enableLights(false);
            notificationChannel.setLightColor(getResources().getColor(R.color.LightColor));//(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(false);
            notificationManager.createNotificationChannel(notificationChannel);
            notificationChannelRegistered = true;
        }

    }

    Semaphore semReconnect = new Semaphore(1);

    public boolean reconnectwhenonline(Throwable e, final boolean always, final boolean networkError,
                                       final boolean isalive, final boolean dontwait) throws Throwable {
        return reconnectwhenonline(e, always, networkError, isalive, dontwait, -1);
    }

    public boolean reconnectwhenonline(Throwable e, final boolean always, final boolean networkError,
                                       final boolean isalive, final boolean dontwait, int rres) {

        count = 0;
        setErrorReconnect(e);
        updateUserInterfaceTitle(isalive, false, true);
        boolean blnOnline = setOnline(rres);
        if (always || !blnOnline) {

            Log.e(TAG, NOT_ONLINE);
            if (tReconnect == null) { //&& tReconnect.isAlive()) tReconnect.interrupt();
                final Throwable finalE = e;
                // mState = STATE_CONNECTING;
                try {
                    mHandler.removeCallbacks(runnableinterrupt);
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
                mHandler.postDelayed(runnableinterrupt, 70000);
                tReconnect = createThreadReconnect(dontwait, isalive, finalE); 
                tReconnect.start();
            }
            // tReconnect.join();
            //tReconnect = null;

        } else {
            Log.e(TAG, IS_ONLINE);
            mState = STATE_ERROR;
            mErrMsg = e != null ? e.getMessage() : "";
            updateUserInterfaceTitle(isalive, false);
        }
        return false;
    }

    private Thread createThreadReconnect(boolean dontwait, boolean isalive, Throwable finalE) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                boolean semaquired = false;
                try {
                    semReconnect.acquire();
                    semaquired = true;
                    boolean blnOnline;
                    int sleep = 1000;
                    int maxWait = 40000;
                    blnOnline = isisonline();
                    if (dontwait) maxWait = 20000;
                    
                    checkOnlineStatusUntilTimeout(blnOnline, isalive, sleep, maxWait, isalive);
                    
                    
                    lib.Ref<Boolean> err = new lib.Ref<>(false);

                    blnOnline = checkOnlineAndSetErrorMessage(isalive, err);

                    try {
                        if (blnOnline) {
                            reloginDependingOnException(finalE);
                            // return true;
                        } else if (!err.get()) {
                            ShowErrorNoInternet(count, isalive);
                        }
                    } catch (Throwable ex) {
                        Log.e(TAG, IS_ONLINE, ex);
                        mState = STATE_ERROR;
                        mErrMsg = finalE != null ? finalE.getMessage() : "";
                        updateUserInterfaceTitle(isalive, false);
                    }
                } catch (Throwable ex) {
                    Log.e(TAG, IS_ONLINE, ex);
                    mState = STATE_ERROR;
                    mErrMsg = ex.getMessage();
                    updateUserInterfaceTitle(isalive, false);
                } finally {
                    if (semaquired) semReconnect.release();
                    tReconnect = null;
                }

            }
        });
    }

    private void ShowErrorNoInternet(int count, boolean isalive) {
        Log.e(TAG, NOT_CONNECTED_AFTER_SECS + count);
        mState = STATE_ERROR;
        //this.connectService(true, false);
        mErrMsg = getString(R.string.nointernet);
        updateUserInterfaceTitle(isalive, false);
    }

    private void reloginDependingOnException(Throwable finalE) {
        Log.e(TAG, STARTED_RECONNECT_AFTER_SECS + count);
        if (finalE instanceof ConnectException && finalE.getMessage().toLowerCase().contains(FAILED_TO_CONNECT)) {
            //connectionLost(e);
            reloginhandler();
            //return true;
        }
        if (finalE instanceof SSLException && finalE.getMessage().toLowerCase().contains(CONNECTION_CLOSED_BY_PEER)) {
            if (!mblnStop)
                reloginhandler();
            //mHandler.sendMessage(mHandler.obtainMessage(Constants.MESSAGE_RELOGIN));
        } else {
            reloginhandler();
            //reconnect(networkError, isalive);
        }

    }

    private boolean checkOnlineAndSetErrorMessage(boolean isalive, lib.Ref<Boolean> err) {
        boolean blnOnline = false;
        try {
            blnOnline = isOnline();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            blnOnline = false;
            mState = STATE_ERROR;
            //this.connectService(true, false);
            mErrMsg = throwable.getMessage();
            updateUserInterfaceTitle(isalive, false);
            err.set(true);
        }
        return blnOnline;
    }

    private void checkOnlineStatusUntilTimeout(boolean online, boolean blnOnline, int sleep, int maxWait, boolean isalive) {
        while (!blnOnline) {
            setErrorNotOnline(isalive, sleep);
            count += sleep;
            sleep += 500;
            if (count > maxWait || Thread.interrupted() || mblnStop) break;
            blnOnline = checkOnlineAndShowStatus(isalive,sleep);
        }
    }

    private boolean checkOnlineAndShowStatus(boolean isalive, int sleep) {
        boolean blnOnline;
        try {
            mErrMsg = getString(R.string.checkingconnectivity);
            updateUserInterfaceTitle(isalive, true, true);
            ;
            blnOnline = isOnline();
            if (!blnOnline) Thread.sleep(sleep / 2);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            blnOnline = false;
        }
        return blnOnline;
    }

    private void setErrorNotOnline(boolean isalive, int sleep) {
        try {
            mErrMsg = getString(R.string.notonline);
            updateUserInterfaceTitle(isalive, true, true);
            ;
            Thread.sleep(sleep / 2);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    private boolean setOnline(int rres) {
        boolean blnOnline;
        if (rres > -1) {
            blnOnline = rres != 0;
        } else {
            try {
                blnOnline = isOnline();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                blnOnline = false;
            }
        }
        
        return blnOnline;

    }

    private void setErrorReconnect(Throwable e) {
        mState = STATE_ERROR;
        if (e != null) {
            mErrMsg = e.getMessage();
        } else {
            mErrMsg = getString(R.string.notonline);
            e = new Exception("reconnect");
        }
        if (mErrMsg.startsWith("Read error")) mErrMsg = getString(R.string.notonline);
        if (mErrMsg.contains("CONNECTIVITY_CHANGE"))
            mErrMsg = getString(R.string.ConnectivityChange);
    }

    private void reloginhandler() {
        if (mHandler != null) {
            // mState = STATE_CONNECTING;
            updateUserInterfaceTitle(true, true, true);
            if (!mblnStop) mHandlerUnbound.sendMessage(mHandler.obtainMessage(Constants.MESSAGE_RELOGIN));
        }
    }

    private boolean isOnline() throws Throwable {
        if (context == null) throw new IllegalArgumentException("context is nothing!");
        if (!isNetworkConnected(context)) {
            return false;
        }
        try {
            Ping r = ping(new URL(MYURLROOT), context);
            if (r.dns < 1000) {
                return true;
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            mState = STATE_ERROR;
            mErrMsg = ex.getMessage();
            updateUserInterfaceTitle(false, false);
            return false;
        }
        return false;
    }

    private void setStatus(int resId, boolean nosound) throws Throwable {
        setStatus(resId, null, nosound);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId, String msg, boolean nosound) throws Throwable {
        if (quiet) return;
        sendMessageToNotification(getString(resId) + (msg != null ? " " + msg : ""), nosound, false);
        AppCompatActivity activity = this.activity;
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(CharSequence subTitle, boolean nosound) throws Throwable {
        sendMessageToNotification(subTitle.toString(), nosound, false);
        FragmentActivity activity = this.activity;
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    public boolean sendMessage(String message) throws Exception {
        // Check that we're actually connected before trying anything
        if (this.getState() != LimindoService.STATE_CONNECTED) {
            if (activity != null) {
                Toast.makeText(activity, R.string.not_connected, Toast.LENGTH_SHORT).show();
            }
            return false;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the LimindoService to write
            return this.write(message);

            // Reset out string buffer to zero and clear the edit text field
            //mOutStringBuffer.setLength(0);
            //mOutEditText.setText(mOutStringBuffer);
        }
        return true;
    }

    public enum SendType {
        Answer,
        Question, User,
        DelAnswer, DelQuestion, Logon, Logoff, ValueAnswer, Upload, Message
    }

    public static class Ping {
        public String net = "NO_CONNECTION";
        public String host = "";
        public String ip = "";
        public int dns = Integer.MAX_VALUE;
        public int cnt = Integer.MAX_VALUE;
    }

    int countMessages;

    public static class HandlerUnbound extends Handler {
        public static final String FAILED_TO_CONNECT = LimindoService.FAILED_TO_CONNECT;
        public static final String EMERGENCY = "emergency";
        public static final String ERROR = "Error:";
        public static final String BENUTZERIDFRAGE = "benutzeridfrage";
        public static final String BENUTZERIDEMPFAENGER = "benutzeridempfaenger";
        public static final String SENDTYPE = "sendtype";
        public static final String BENUTZERNAME = "benutzername";
        public static final String ANTWORT = "antwort";
        public static final String GRAD = "grad";
        public static final String READMESSAGE = "readmessage: ";
        public static final String BENUTZER_NICHT_ANGEMELDET = "Benutzer nicht angemeldet!";
        public static final String DIE_IDENTITÄT_KONNTE_NICHT_ÜBERPRÜFT_WERDEN = "Die Identität konnte nicht überprüft werden!";
        public static final String RELOGIN = "relogin";
        public static final String M_HANDLER_UNBOUND = "mHandlerUnbound";

        private final LimindoService service;
        private long dtLastQuestion;
        private long lastBenutzerID;
        private int lastCountMessages;
        private CharSequence lastSS;


        public HandlerUnbound(LimindoService service) {
            this.service = service;
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                boolean isalive = false;
                boolean lquiet = false;
                boolean networkError = false;
                Bundle bundle = msg.peekData();
                if (bundle != null && bundle.containsKey(Constants.ISALIVE)) {
                    isalive = bundle.getBoolean(Constants.ISALIVE);
                }
                if (bundle != null && bundle.containsKey(Constants.ISNETWORKERROR)) {
                    networkError = bundle.getBoolean(Constants.ISNETWORKERROR);
                }
                if (bundle != null && bundle.containsKey(Constants.QUIET)) {
                    lquiet = bundle.getBoolean(Constants.QUIET);
                }


                switch (msg.what) {
                    case MESSAGE_EXCEPTION:

                        showException(msg);

                        break;
                    case MESSAGE_STATE_CHANGE:
                        showState(msg, isalive, bundle);
                        break;
                    case Constants.MESSAGE_WRITE:
                        byte[] writeBuf = (byte[]) msg.obj;
                        // construct a string from the buffer
                        String writeMessage = new String(writeBuf);
                        //mConversationArrayAdapter.add("Me:  " + writeMessage);

                        break;
                    case Constants.MESSAGE_READ:
                        processreadmessage(msg);
                        break;
                    case Constants.MESSAGE_DEVICE_NAME:
                        // save the connected device's name
                        service.mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                        /*
                        sendMessageToNotification(getString(R.string.connectedto) + " "
                                + mConnectedDeviceName, false, false);
                        if (null != context) {
                            try {
                                Toast.makeText(context, getString(R.string.connectedto) + " "
                                        + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                            } catch (Throwable ex) {
                                Log.i("HandlerUnbound", ex.getMessage());
                            }
                        }
                        */
                        break;
                    case Constants.MESSAGE_DEVICE_ADDRESS:
                        // save the connected device's name
                        service.mConnectedAddress = msg.getData().getString(Constants.DEVICE_ADDRESS);
                        /*
                        if (null != context) {
                            sendMessageToNotification(getString(R.string.connectedto), false, false);
                            Toast.makeText(context, getString(R.string.connectedto)
                                    + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                        }
                        */
                        break;
                    case Constants.MESSAGE_TOAST:
                        service.sendMessageToNotification(msg.getData().getString(Constants.TOAST), false, false);
                        if (null != service.context) {
                            Toast.makeText(service.context, msg.getData().getString(Constants.TOAST),
                                    Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Constants.MESSAGE_RECONNECT:
                        //LimindoService.this.start();
                        try {
                            service.reconnectMainThread(networkError, isalive, lquiet);
                            //LimindoService.this.reconnectwhenonline(null,true,networkError,isalive);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, null, e);
                        }
                        break;
                    case Constants.MESSAGE_RELOGIN:
                        service.start();
                        try {
                            service.relogin(false);
                            //LimindoService.this.reconnect(false,false);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, null, e);
                        }
                        break;
                    default:
                        break;

                }
            } catch (
                    Throwable ex) {
                Log.e(M_HANDLER_UNBOUND, "", ex);
            }
        }

        private void processreadmessage(Message msg) throws Throwable {
            byte[] readBuf = (byte[]) msg.obj;
            // construct a string from the valid bytes in the buffer
            String readMessage = new String(readBuf, 0, msg.arg1);
            if (readMessage.startsWith("54321")) {
                processChatMessage(readMessage);
            } else if (readMessage.startsWith("123456")) {
                System.out.println(service.getString(R.string.AnswersReceived));
            } else if (!readMessage.startsWith(ERROR)) {

                processAnswerOrQuestion(readMessage);

            } else {
                Log.e(TAG, READMESSAGE + readMessage);
                if (readMessage.contains(BENUTZER_NICHT_ANGEMELDET) || readMessage.contains(DIE_IDENTITÄT_KONNTE_NICHT_ÜBERPRÜFT_WERDEN)) {
                    Log.e(TAG, RELOGIN);
                    service.relogin(false);
                } else {
                    service.sendMessageToNotification(service.getString(R.string.Error_) + " " + readMessage.replace(ERROR, ""), false, false);
                }
            }
            //mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
            //reconnect();

        }

        private void processAnswerOrQuestion(String readMessage) {
            if (!service.blnNotOff) {
                JSONObject o;
                try {
                    JSONArray oo = new JSONArray();
                    try {
                        o = new JSONObject(readMessage);
                        oo.put(o);
                    } catch (Throwable eex) {
                        oo = new JSONArray(readMessage);

                    }
                    for (int i = 0; i < oo.length(); i++) {
                        o = oo.getJSONObject(i);

                        LimindoService.SendType t = LimindoService.SendType.valueOf(o.getString(SENDTYPE));

                        if (t == LimindoService.SendType.Answer) {
                            MainActivity.MessageCache.add(readMessage);
                            Long BenutzerIDFrage = o.getLong(BENUTZERIDFRAGE);
                            Long BenutzerIDEmpfaenger = null;
                            Boolean isEmpfaenger = false;
                            if (!o.isNull(BENUTZERIDEMPFAENGER)) {
                                BenutzerIDEmpfaenger = o.getLong(BENUTZERIDEMPFAENGER);
                                if (BenutzerIDEmpfaenger == service.user.getLong(Constants.id))
                                    isEmpfaenger = true;
                            }
                            if (service.showAnswers || BenutzerIDFrage == service.user.getLong(Constants.id) || isEmpfaenger) {
                                service.countAnswers++;
                                service.createNewNotification(ONGOING_NOTIFICATION_ID_ANSWER, service.getString(R.string.answer)
                                                + " " + o.getString(BENUTZERNAME)
                                                + " (" + service.countAnswers + " "
                                                + service.getString(R.string.answers2) + ")"
                                        , StringEscapeUtils.unescapeJava(o.getString(ANTWORT))
                                        , readMessage
                                        , !o.isNull(EMERGENCY) && o.getInt(EMERGENCY) != 0
                                        , false);
                            }

                        } else if (t == LimindoService.SendType.Question) {
                            Long BenutzerID = o.getLong(BENUTZERID);
                            String search = service.getApplicationContext().getSharedPreferences(SEARCH, MODE_PRIVATE).getString(SEARCH, "");
                            int _grad = 0;
                            _grad = (!o.isNull(LimindoService.HandlerUnbound.GRAD) ? o.getInt(LimindoService.HandlerUnbound.GRAD) : -2);
                            if (_grad == -2)
                                _grad = service.clsHTTPS.getKontaktgrad(o.getInt(KONTAKTGRAD), service.user.getLong(Constants.id), BenutzerID);
                            final int grad = _grad;
                            int Grad = service.getApplicationContext().getSharedPreferences(GRADE, Context.MODE_PRIVATE).getInt(GRADE, 5);
                            if (grad > -1 && grad <= Grad) {
                                String Frage = StringEscapeUtils.unescapeJava(o.getString("frage")).replace(VOTE, "");
                                boolean matches = true;
                                if (search.length() > 0) {
                                    matches = false;
                                    String[] s = search.split("\\s");
                                    for (String ss : s) {
                                        ss = ss.trim();
                                        if (ss.length() > 0 && Frage.toUpperCase().contains(ss.toUpperCase())) {
                                            matches = true;
                                            break;
                                        }
                                    }
                                }
                                if (matches) {
                                    Double e = null;
                                    Question q = new Question(service.context, o);
                                    if (service.locCoarse != null && q.Laengengrad != null && q.Breitengrad != null) {
                                        e = lib.distanceInKm(q.Breitengrad, q.Laengengrad, service.locCoarse.getLatitude(), service.locCoarse.getLongitude());
                                    }
                                    int Entf = service.getApplicationContext().getSharedPreferences(ENTF, Context.MODE_PRIVATE).getInt(ENTF, 1000);
                                    int Menge = service.getApplicationContext().getSharedPreferences(MENGE, Context.MODE_PRIVATE).getInt(MENGE, 0);
                                    if ((Entf == 1000 || (e != null && e <= Entf && (q.RadiusKM == 1000 || e < q.RadiusKM))) && dtLastQuestion < Calendar.getInstance().getTimeInMillis() - Menge * 1000) {
                                        MainActivity.MessageCache.add(readMessage);
                                        dtLastQuestion = Calendar.getInstance().getTimeInMillis();
                                        service.countQuestions++;
                                        service.createNewNotification(ONGOING_NOTIFICATION_ID_QUESTION, service.getString(R.string.question) + " "
                                                        + o.getString(BENUTZERNAME)
                                                        + " (" + service.countQuestions + " "
                                                        + service.getString(R.string.questions2) + ")"
                                                , Frage
                                                , readMessage
                                                , !o.isNull(EMERGENCY) && o.getInt(EMERGENCY) != 0
                                                , false);
                                    }
                                }
                            }
                        } else if (t == Upload) {
                            String FileName = o.getString("filename");
                            if (service.clsHTTPS.hashImages.containsKey(FileName))
                                service.clsHTTPS.hashImages.remove(FileName);
                        } else {
                            MainActivity.MessageCache.add(readMessage);
                            //createNewNotification("Event" , t.name(),readMessage);
                        }

                    }
                } catch (Throwable eex) {
                    if (!readMessage.startsWith("listen stopped for")) {
                        Log.e(TAG, readMessage, eex);
                    } else {
                        Log.i(TAG, readMessage, eex);
                        gStatus = readMessage;
                    }
                }
            }
        }

        private void processChatMessage(String readMessage) throws Exception {
            JSONObject o = new JSONObject(readMessage.substring(5));
            o.put(SENDTYPE, SendType.Message);
            long BenutzerIDEmpfaenger = o.getLong(BENUTZERIDEMPFAENGER);
            long BenutzerID = o.getLong(BENUTZER_ID);
            Date time = Calendar.getInstance().getTime();
            if (o.has(TIME)) {
                time = new Date(o.getLong(TIME));
            }
            Long id = service.user.getLong(ID);
            JSONObject user = service.clsHTTPS.getUser(id, BenutzerID);
            String u = user.getString(BENUTZERNAME);
            String chatMessage = o.optString("Message", "");
            boolean receivedbyserver = chatMessage.equalsIgnoreCase("*received*");
            boolean senttoreceiver = o.optBoolean("sent", false);
            boolean readbyreceiver = o.optBoolean("read", false);
            boolean statuschanged = receivedbyserver || senttoreceiver || readbyreceiver;
            boolean typing = chatMessage.equalsIgnoreCase("⸙");
            if (!statuschanged) {
                if (!typing) service.countMessages++;
                CharSequence ss = null;
                if (chatMessage.startsWith("(JSONObject)")) {
                    SpannableString m = new SpannableString(sdftime.format(time) + " " + user.getString(BENUTZERNAME) + ": ");
                    ss = lib.getClickableSpanFromMessage(this.service.clsHTTPS, this.service.getUserID(), (service.activity != null ? service.activity : service.context), chatMessage, true);
                    ss = TextUtils.concat(m, ss);
                    service.createNewNotification(service.countMessages + ONGOING_NOTIFICATION_ID_MESSAGES, service.getString(R.string.message), ss, o.toString(), false, false);
                    service.MessageCacheChatService.add(o.toString());
                    //Debug
                    setStatusAndLog(service.context, TAG, "*** Message Object added to MessageCacheChatService! " + m);
                } else {
                    int countMessages = service.countMessages;
                    ss = chatMessage;
                    if (typing) {
                        ss = ".";
                        if (BenutzerID == lastBenutzerID) {
                            countMessages = lastCountMessages;
                            ss = TextUtils.concat(lastSS, ss);
                        }
                        lastBenutzerID = BenutzerID;
                        lastCountMessages = countMessages;
                        lastSS = ss;
                    } else {
                        if (BenutzerID == lastBenutzerID) {
                            countMessages = lastCountMessages;
                        }
                        lastBenutzerID = -1;
                        lastCountMessages = countMessages;
                        lastSS = null;
                    }
                    boolean noSound = lastBenutzerID == BenutzerID;
                    if (!typing) {
                        service.createNewNotification(countMessages + ONGOING_NOTIFICATION_ID_MESSAGES, service.getString(R.string.message), sdftime.format(time) + " " + u + ": " + ss, o.toString(), false, noSound);
                        service.MessageCacheChatService.add(o.toString());
                        //Debug
                        setStatusAndLog(service.context, TAG, "*** Message added to MessageCacheChatService! " + ss);
                    }
                }

            } else {
                service.MessageCacheChatService.add(o.toString());
                //Debug
                setStatusAndLog(service.context, TAG, "*** Service Status Message added to MessagecacheChatService! Status changed " + chatMessage);
            }
        }

        private void showState(Message msg, boolean isalive, Bundle bundle) throws Throwable {
            switch (msg.arg1) {
                case LimindoService.STATE_CONNECTED:
                    if (!service.quiet) {
                        if (isalive) {
                            service.setStatus(service.getString(R.string.title_connected_to) + service.getString(R.string.limindoservicerenewed) + LimindoService.alivecount, true); // + MYURL);
                        } else {
                            service.setStatus(service.getString(R.string.title_connected_to) + " " + service.getString(R.string.LimindoService), true); // + MYURL);
                        }
                    }
                    if (service.neueAntworten > 0 || service.neueFragen > 0) {
                        String m = "";
                        if (service.neueFragen > 0) {
                            m = service.neueFragen + " " + service.getString(R.string.newQuestions);
                            if (service.neueAntworten > 0) m += "\n";
                        }
                        if (service.neueAntworten > 0) {
                            m += service.neueAntworten + " " + service.getString(R.string.newAnswers);
                        }
                        if (m.length() > 0)
                            service.createNewNotification(ONGOING_NOTIFICATION_ID_NEW, service.getString(R.string.service), m, null, false, false);
                    }

                    //mConversationArrayAdapter.clear();
                    break;
                case LimindoService.STATE_CONNECTING:
                    if (!service.quiet)
                        service.setStatus(R.string.title_connecting, true);
                    break;
                case LimindoService.STATE_LISTEN:
                case LimindoService.STATE_NONE:
                    if (!service.quiet) {
                        if (service.blnWasDestroyed) {
                            service.setStatus(R.string.ServiceDestroyed, false);
                        } else if (isalive) {
                            service.setStatus(R.string.title_connection_renewing, true);
                        } else {
                            service.setStatus(R.string.title_not_connected, true);
                        }
                    }
                    break;
                case LimindoService.STATE_ERROR:
                    if (!service.quiet) {
                        String mErrMsgtmp = (bundle != null ? bundle.getString(Constants.ERR_MSG) : "");
                        if (mErrMsgtmp != null) service.mErrMsg = mErrMsgtmp;
                        if (service.mErrMsg != null && service.mErrMsg.startsWith(FAILED_TO_CONNECT)) {
                            service.mErrMsg = service.getString(R.string.failedtoconnect);
                        }
                        if (service.blnWasDestroyed) {
                            service.setStatus(R.string.ServiceDestroyed, false);
                        } else {
                            service.setStatus(R.string.error, service.mErrMsg, true);
                        }
                    }
                    break;
            }
        }

        private void showException(Message msg) {
            Exception ex = (Exception) msg.obj;
            String message = msg.getData().getString(Constants.MESSAGE);
            if ((debugMode())) {
                service.countMessages++;
                try {
                    service.createNewNotification(ONGOING_NOTIFICATION_ID_MESSAGES + service.countMessages, message, ex.getMessage(), "", false, true);
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private final String mEmail;
        private final boolean mUpdate;
        private final boolean mCallback;
        private final Bundle mSavedInstanceState;
        private final String mPassword;
        private final String mName;
        private final String mVorname;
        private final String mPLZ;
        private final String mAccesskey;
        private final String mNeuerBenutzername;
        private final boolean mShowProgress;
        private String mBenutzername;
        private String mPhoneNumber;
        private ProgressDialog p;


        UserLoginTask(boolean showProgress, boolean update, String email, String benutzername, String neuerbenutzername, String password, String Name, String Vorname, String Rufnummer, String PLZ, String AccessKey, boolean callback, Bundle savedinstancestate) {
            mShowProgress = showProgress;
            mUpdate = update;
            mEmail = email;
            mBenutzername = benutzername;
            mNeuerBenutzername = neuerbenutzername;
            mPassword = password;
            mAccesskey = AccessKey;
            mName = Name;
            mVorname = Vorname;
            mPhoneNumber = Rufnummer;
            mPLZ = PLZ;
            if (mPhoneNumber != null && mPhoneNumber.length() > 3) {
                mPhoneNumber = lib.formatPhoneNumber(mPhoneNumber);
            }
            this.mCallback = callback;
            this.mSavedInstanceState = savedinstancestate;
            Log.i(TAG, "UserLoginTask");

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mShowProgress && LimindoService.this.activity != null) p = ProgressDialog.show(activity, getString(R.string.loginservice), getString(R.string.login));
            try {
                LimindoService.this.stop(false);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                Log.e(TAG, null, throwable);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {

                clsHTTPS https = clsHTTPS;
                if (clsHTTPS == null) throw new Exception("clshttps null");
                Map<String, String> p = new java.util.HashMap<String, String>();
                String res = null;
                if (mAccesskey != null) {
                    lib.setStatusAndLog(context, TAG, "Logon " + mBenutzername);
                    res = https.Logon(mAccesskey, mBenutzername, mEmail, activity != null ? ((MainActivity) activity).onlineState : MainActivity.getOnlineState(context));
                } else {
                    p.put("update", "" + mUpdate);
                    p.put("Name", mName);
                    p.put("Vorname", mVorname);
                    p.put("Nick", "test");
                    p.put("Benutzername", mBenutzername);
                    p.put("NeuerBenutzername", mNeuerBenutzername);
                    p.put("EMail", mEmail);
                    p.put("NeueEMail", mEmail);
                    if (mPhoneNumber == null || mPhoneNumber.length() == 0)
                        mPhoneNumber = context.getString(R.string.leer);
                    p.put("Rufnummer", mPhoneNumber);
                    p.put("PLZ", mPLZ);
                    p.put("Kennwort", mPassword);
                    if (mPassword.length() < 8)
                        throw new RuntimeException("Kennwort zu kurz!");
                    double breit = 0.0d;
                    double lang = 0.0d;
                    Date dt = Calendar.getInstance().getTime();
                    p.put("Breitengrad", "" + breit);
                    p.put("Laengengrad", "" + lang);
                    p.put("DatumPositionAktualisierung", https.fmtDateTime.format(dt));
                    //p.put("login", "test");
                    Log.i(TAG, "Login");
                    res = https.Login(p);
                }
                return res;
                //Thread.sleep(2000);
            } catch (Throwable e) {
                lib.setStatusAndLog(context, TAG, "Logon " + e.getMessage());
                return "Error:" + e.getMessage();
            }
/*
            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return "" + pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return "" + true; */
        }

        @Override
        protected void onPostExecute(final String success) {
            Log.e(TAG, "UserLoginTask complete " + success);
            {
                if (success != null && !success.startsWith("Error:")) {
                    if (mAccesskey == null) {
                        try {
                            lib.setStatusAndLog(context, TAG, "Logon " + mErrMsg);
                            sendMessageToNotification(getString(R.string.nosecretkey), false, false);
                            setErrorState(getString(R.string.nosecretkey));
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            Log.e(TAG, null, throwable);
                            setErrorState(throwable.getMessage());
                        }
                    } else {
                        try {
                            user = new JSONArray(success).getJSONObject(0);
                            clsHTTPS.setAccesskey(mAccesskey);
                            if (user != null) {
                                if (user.getBoolean(Constants.administrator)) {
                                    clsHTTPS.AdminID = user.getLong(Constants.id);
                                }
                                String fname;
                                fname = ".JPG";
                                Long BenutzerID = user.getLong(Constants.id);
                                String BenutzerName = user.getString(BENUTZERNAME);
                                try {
                                    Bitmap in = clsHTTPS.downloadProfileImage(context, BenutzerID, BenutzerID, BenutzerName);
                                    if (in != null) {
                                        user.put("Image", in);
                                    }
                                } catch (Throwable e) {
                                    Log.i(TAG, null, e);
                                    e.printStackTrace();
                                }
                            }

                            lib.setStatusAndLog(context, TAG, "Logon connectService");
                            connectService(true, false, true);
                            Log.e(TAG, "Relogin Service successful!");
                        } catch (Throwable e) {
                            e.printStackTrace();
                            Log.e(TAG, null, e);
                            setErrorState(e.getMessage());
                        }
                    }
                } else {
                    lib.setStatusAndLog(context, TAG, "Logon Error" + success);
                    String result = success.replace("Error:", "");
                    if (result.contains("Duplicate entry")) {
                        if (result.contains("Benutzername")) {
                            result = getString(R.string.BenNaSchonVorh);
                        } else {
                            result = getString(R.string.doubleentry);
                        }
                    }
                    try {
                        sendMessageToNotification(result, false, false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, null, throwable);
                    }
                    setErrorState(result);
                }
            }
            mAuthTask = null;
            if (p != null) p.dismiss();
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    private void setErrorState(String msg) {
        mState = LimindoService.STATE_ERROR;
        mErrMsg = msg;
        updateUserInterfaceTitle(false, true);
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        LimindoService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LimindoService.this;
        }
    }
}
