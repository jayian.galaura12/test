package de.com.limto.limto1.Errors;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.Semaphore;

import de.com.limto.limto1.clsHTTPS;
import de.com.limto.limto1.lib.lib;

public class dbSqlite extends SQLiteOpenHelper {
    private static final String TAG = "DBSQLITE";
    //The Android's default system path of your application database.
    private String DB_PATH = "/data/data/de.org.Limto/databases/";
    private String original_path = null;
    private static String DB_NAMEERR = "errors.sqlite";
    private String dbname = DB_NAMEERR;
    public SQLiteDatabase DataBase;

    public Context mContext;

    private String original_name;
    public String lastError;

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int P1, int P2) {

    }

    public dbSqlite(Context context) throws Throwable {
        super(context, (DB_NAMEERR), null, 1);
        if (context == null) {
            throw new RuntimeException("context is null!");
        }
        lib.gStatus = "getFilesDir";
        File FilesDir = context.getFilesDir();
        DB_PATH = Path.combine(FilesDir.getPath(), "databases");
        this.mContext = context;
        lib.gStatus = "getExternalStorageDirectory";
        String extPath = Environment.getExternalStorageDirectory().getPath();
        File F = new File(extPath);
        File F2 = context.getExternalFilesDir(null);
        if (F2 != null) F = F2;
        extPath = F.getPath();
        if (!F.isDirectory() && !F.exists()) {
            boolean res = F.mkdirs();
        }
        if (F.isDirectory() && F.exists()) {
            String JMGDataDirectory = Path.combine(extPath, "Limto", "database");
            File F1 = new File(JMGDataDirectory);
            if (!F1.isDirectory() && !F1.exists()) {
                boolean res = F1.mkdirs();
                if (checkDataBase() && F1.exists()) {
                    try {
                        lib.copyFile(DB_PATH + dbname, Path.combine(JMGDataDirectory, dbname));
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        lib.ShowException(TAG, context, e, false);
                    }
                }
            }
            if (F1.exists()) DB_PATH = JMGDataDirectory;
        }
        if (DB_PATH.endsWith("/") == false) {
            DB_PATH = DB_PATH + "/";
        }
        original_path = DB_PATH;
        original_name = dbname;
        if (mContext != null) {
            //DB_PATH = mContext.getSharedPreferences("sqlite", Context.MODE_PRIVATE).getString("dbpath", DB_PATH);
            //dbname = mContext.getSharedPreferences("sqlite", Context.MODE_PRIVATE).getString("dbname", dbname);
        }

    }

    public final String createDataBase() {

        boolean dbExist = checkDataBase();
        if (dbExist) // && !isNewVersion)
        {
            return null;
        } else {
            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                return copyDataBase();

            } catch (IOException e) {
                System.out.println(e.getMessage());
                if (mContext != null) lib.ShowException(TAG, mContext, e, false);
                return e.getMessage();
                //throw new RuntimeException("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            if (DB_PATH.endsWith("/") == false) {
                DB_PATH = DB_PATH + "/";
            }
            String myPath = DB_PATH + dbname;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        } catch (Throwable e) {
            System.out.println(e.getMessage());
            //database does't exist yet.

        }

        if (checkDB != null) {

            checkDB.close();

        }

        return checkDB != null;
    }

    private String copyDataBase() throws IOException {

        //Open your local db as the input stream
        if (mContext != null) {
            lib.gStatus = "Copy Assets:" + DB_NAMEERR;
            AssetManager A = mContext.getAssets();
            //int rID = mContext.getResources().getIdentifier("fortyonepost.com.lfas:raw/"+fileName, null, null);
            //InputStream myInput = A.open(DB_NAME);
            for (int ii = 0; ii < 1; ii++) {
                // Path to the just created empty db
                String outFileName = original_path + original_name;
                if ((new java.io.File(DB_PATH)).isDirectory() == false) {
                    (new java.io.File(DB_PATH)).mkdirs();
                }
                //Open the empty db as the output stream

                File file = new File(outFileName);

                if (file.exists()) {
                    file.delete();
                }
                // if file doesnt exists, then create it
                if (!file.exists()) {
                    file.createNewFile();
                }
                OutputStream myOutput = new FileOutputStream(file);

                //transfer bytes from the inputfile to the outputfile
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: byte[] buffer = new byte[1024];
                String[] assetfiles;
                Locale L = Locale.getDefault();
                String folder;
                folder = "db";
                assetfiles = A.list(folder);
                Arrays.sort(assetfiles);
                byte[] buffer = new byte[1024];
                int length;
                try {
                    for (int i = 0; i < assetfiles.length; i++) //I have definitely less than 10 files; you might have more
                    {
                        String partname = assetfiles[i];
                        if (partname.startsWith("errors"))// && partname.length() == 4)
                        {
                            InputStream instream = A.open(folder + "/" + partname);
                            while ((length = instream.read(buffer, 0, 1024)) > 0) {
                                myOutput.write(buffer, 0, length);
                            }
                            instream.close();
                        }
                    }

                } catch (Throwable ex) {
                    Log.e("copyDatabase", ex.getMessage(), ex);
                    if (file.exists()) {
                        file.delete();
                    }
                    return ex.getMessage();
                } finally {
                    myOutput.flush();
                    myOutput.close();
                    //myInput.close();
                }
            }
        }
        return null;
        //Close the streams


    }

    Semaphore semopeninsert = new Semaphore(1);

    public final boolean openDataBase(final boolean sync) {

        if (DataBase != null) return true;
        //Open the database
        if (mContext != null) {
            mContext.getSharedPreferences("sqlite", Context.MODE_PRIVATE).edit().putString("dbpath", DB_PATH).commit();
            mContext.getSharedPreferences("sqlite", Context.MODE_PRIVATE).edit().putString("dbname", dbname).commit();
        }
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!sync) {
                        semopeninsert.acquire();
                    }
                    String myPath = DB_PATH + dbname;
                    try {
                        DataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
                        isClosed = false;
                        try {
                            String sql = "CREATE TABLE IF NOT EXISTS \"Errors\" (\n" +
                                    "\t`ID`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,\n" +
                                    "\t`date`\tDATETIME DEFAULT CURRENT_TIMESTAMP,\n" +
                                    "\t`ex`\tTEXT NOT NULL,\n" +
                                    "\t`CodeLoc`\tTEXT,\n" +
                                    "\t`comment`\tTEXT,\n" +
                                    "\t`exported`\tINTEGER DEFAULT 0\n" +
                                    ")";
                            DataBase.execSQL(sql);
                            DataBase.execSQL("PRAGMA foreign_keys = ON;");
                        } catch (Throwable ex) {
                            try {
                                InsertError(ex, "OpenDatabase", "Create Table Errors", 0);
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }
                        try {
                            InsertError(null, "OpenDatabase", "Logging started", 1);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    } catch (Throwable ex) {
                        Log.e(TAG, ex.getMessage(), ex);
                    } finally {
                        if (!sync) {
                            semopeninsert.release();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });


        if (sync) {
            t.run();
            return DataBase != null;
        } else {
            t.start();
            return true;
        }

    }

    public boolean isClosed = true;

    @Override
    public void close() {
        boolean blnSem = false;
        try {
            if (!lib.isOnMainThread()) {
                semopeninsert.acquire();
                blnSem = true;
            }
            try {
                if (stInsertError != null) {
                    try {
                        stInsertError.close();
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                    }
                    stInsertError = null;
                }
                if (DataBase != null) {

                    try {
                        DataBase.close();
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                    }
                    isClosed = true;
                    DataBase = null;
                }

                super.close();
            } finally {
                if (blnSem) semopeninsert.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public final android.database.Cursor query(String SQL) {
        if (DataBase == null) {
            openDataBase(true);
        }
        return DataBase.rawQuery(SQL, null);

    }

    public final android.database.Cursor query(String SQL, String[] params) {
        if (DataBase == null) {
            openDataBase(true);
        }
        return DataBase.rawQuery(SQL, params);

    }


    private SQLiteStatement stInsertError;

    public boolean InsertError(final Throwable ex, final String CodeLoc, final String comment,
                               final long exported) throws Throwable {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    semopeninsert.acquire();
                    try {

                        String err = "";
                        if (ex == null) {
                            err = "null";
                        } else {
                            StringWriter errors = new StringWriter();
                            ex.printStackTrace(new PrintWriter(errors));
                            err = errors.toString();
                            lastError = err;
                        }
                        String lcomment = comment;
                        if (lib.libString.IsNullOrEmpty(comment))
                            lcomment = ex.getMessage();

                        if (!openDataBaseSync()) return;

                        if (stInsertError != null) {
                            try {
                                stInsertError.close();
                            } catch (Throwable ex) {
                                ex.printStackTrace();
                            }
                            stInsertError = null;
                        }

                        if (stInsertError == null) {
                            stInsertError = DataBase.compileStatement("INSERT INTO Errors (ex, CodeLoc, comment, exported, date) VALUES(?,?,?,?,?)");
                        }
                        DataBase.beginTransaction();
                        try {
                            SQLiteStatement st = stInsertError;
                            //ContentValues values = new ContentValues();
                            st.clearBindings();
                            st.bindString(1, err);
                            st.bindString(2, CodeLoc);
                            st.bindString(3, lcomment);
                            st.bindLong(4, exported);
                            st.bindString(5, clsHTTPS.fmtDateTime.format(Calendar.getInstance().getTime()));
                            st.executeInsert();
                            //this.DataBase.insert("Data", null, values);
                            DataBase.setTransactionSuccessful();
                        } catch (Throwable eex) {
                            Log.e(TAG, null, eex);
                        } finally {
                            try {
                                DataBase.endTransaction();
                            } catch (Throwable eex) {
                                Log.e(TAG, null, eex);
                            }
                        }
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                    } finally {
                        semopeninsert.release();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();
        return true;
    }

    private boolean openDataBaseSync() {
        if (DataBase == null || !DataBase.isOpen()) {
            if (DataBase != null) {
                try {
                    DataBase.close();
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
                DataBase = null;
            }
            if (!openDataBase(true)) return false;
        }
        return true;
    }


}