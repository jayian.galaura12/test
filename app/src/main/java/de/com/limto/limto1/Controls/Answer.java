package de.com.limto.limto1.Controls;


import android.content.Context;
import android.view.View;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import de.com.limto.limto1.Constants;
import de.com.limto.limto1.MainActivity;
import de.com.limto.limto1.R;

import static de.com.limto.limto1.clsHTTPS.ONLINE_STATE;
import static de.com.limto.limto1.fragAnswer.NO;
import static de.com.limto.limto1.fragAnswer.UNDEFINED;
import static de.com.limto.limto1.fragAnswer.YES;

public class Answer extends Object
{

    public static final String ANTWORT = "antwort";
    public static final String BENUTZERID = "benutzerid";
    public static final String BENUTZERNAME = "benutzername";
    public static final String EMAIL = "email";
    public static final String DATUM = "datum";
    public static final String KONTAKTGRAD = "kontaktgrad";
    public static final String ANTWORTID = "antwortid";
    public static final String PARENTFRAGEANTWORTENID = "parentfrageantwortenid";
    public static final String REANTWORTID = "reantwortid";
    public static final String BREITENGRAD = "breitengrad";
    public static final String LAENGENGRAD = "laengengrad";
    public static final String ONLINE = "online";
    public static final String BEWERTUNG = "bewertung";
    public static final String BEWERTUNGEN = "bewertungen";
    public static final String FRAGEN = "fragen";
    public static final String ANTWORTEN = "antworten";
    public static final String ANTWORTZEIT = "antwortzeit";
    public static final String AKTIVITAETSINDEX = "aktivitaetsindex";
    public static final String BESTAENDIGKEIT = "bestaendigkeit";
    public static final String WERTUNG = "wertung";
    public static final String GEMELDET = "gemeldet";
    public static final String OEFFENTLICH = "oeffentlich";
    public static final String ANTWORT_2 = "antwort2";
    public static final String BENUTZERID_2 = "benutzerid2";
    public static final String BENUTZERNAME_2 = "benutzername2";
    public static final String EMAIL_2 = "email2";
    public static final String DATUM_2 = "datum2";
    public static final String ID_2 = "id2";
    public static final String ANTWORTID_2 = "antwortid2";
    public static final String BREITENGRAD_2 = "breitengrad2";
    public static final String LAENGENGRAD_2 = "laengengrad2";
    public static final String ONLINE_2 = "online2";
    public static final String PARENTFRAGEANTWORTENID_2 = "parentfrageantwortenid2";
    public static final String REANTWORTID_2 = "reantwortid2";
    public static final String BENUTZERIDSENDER_2 = "benutzeridsender2";
    public static final String BENUTZERIDEMPFAENGER_2 = "benutzeridempfaenger2";
    public static final String BEWERTUNGEN_2 = "bewertungen2";
    public static final String FRAGEN_2 = "fragen2";
    public static final String ANTWORTEN_2 = "antworten2";
    public static final String ANTWORTZEIT_2 = "antwortzeit2";
    public static final String AKTIVITAETSINDEX_2 = "aktivitaetsindex2";
    public static final String BESTAENDIGKEIT_2 = "bestaendigkeit2";
    public static final String WERTUNG_2 = "wertung2";
    public static final String OEFFENTLICH_2 = "oeffentlich2";
    private final Context context;
    public Integer Wertung = null;
    public Integer Bestaendigkeit = null;
    public  Integer Aktivitaetsindex = null;
    public long reAntwortID = 0;
    public Integer Antwortzeit = null;
    public boolean gemeldet = false;
    public  int Grad;
    public  Long ParentFrageAntwortenID;
    public  String Antwort2;
    public String EMail;
    public String Antwort;
    public  Long BenutzerID;
    public  String BenutzerName;
    public Date Datum;
    public Double Breitengrad;
    public Double Laengengrad;
    public  Question Frage;
    public Long AntwortID;
    public Long ID;
    public boolean online;
    public MainActivity.OnlineState onlineState = MainActivity.OnlineState.offline;
    public Long BenutzerIDSender;
    public Long BenutzerIDEmpfaenger;
    public int Bewertung;
    public Integer Bewertungen = null;
    public Integer Fragen = null;
    public Integer Antworten = null;
    public Boolean oeffentlich = null;
    public int padding;
    public int level;
    public Boolean emergency;
    public boolean selected;
    private View view;

    public Answer(Context context, Long ID, Long AntwortID, int Bewertung, String Antwort, Question Frage, int Grad, Long BenutzerID, String BenutzerName, String EMail, Date Datum, Double Breitengrad, Double Laengengrad, boolean online, boolean oeffentlich, MainActivity.OnlineState onlineState)
    {
        this.context = context;
        this.ParentFrageAntwortenID = null;
        this.reAntwortID = 0l;
        this.BenutzerIDEmpfaenger = null;
        this.BenutzerIDSender = null;
        this.Antwort = Antwort;
        this.Antwort2 = null;
        this.AntwortID = AntwortID;
        this.BenutzerID = BenutzerID;
        this.BenutzerName = BenutzerName;
        this.EMail = EMail;
        this.Datum = Datum;
        this.Grad = Grad;
        this.Frage = Frage;
        this.ID = ID;
        this.Bewertung = Bewertung;
        this.Breitengrad = Breitengrad;
        this.Laengengrad = Laengengrad;
        this.online = online;
        this.oeffentlich = oeffentlich;
        this.onlineState = onlineState;
    }
    public Answer(Context context, Long ID, Long ParentFrageAntwortenID, long reAntwortID, Long AntwortID, int Bewertung, String Antwort, String Antwort2, Question Frage, int Grad, Long BenutzerID, Long BenutzerIDSender, Long BenutzerIDEmpfaenger, String BenutzerName, String EMail, Date Datum, Double Breitengrad, Double Laengengrad,boolean online, boolean oeffentlich, MainActivity.OnlineState onlineState)
    {
        this.context = context;
        this.ParentFrageAntwortenID = ParentFrageAntwortenID;
        this.reAntwortID = reAntwortID;
        this.BenutzerIDSender = BenutzerIDSender;
        this.BenutzerIDEmpfaenger = BenutzerIDEmpfaenger;
        this.Antwort = Antwort;
        this.Antwort2 = Antwort2;
        this.AntwortID = AntwortID;
        this.BenutzerID = BenutzerID;
        this.BenutzerName = BenutzerName;
        this.EMail = EMail;
        this.Datum = Datum;
        this.Grad = Grad;
        this.Frage = Frage;
        this.ID = ID;
        this.Breitengrad = Breitengrad;
        this.Laengengrad = Laengengrad;
        this.online = online;
        this.Bewertung = Bewertung;
        this.oeffentlich = oeffentlich;
        this.onlineState = onlineState;
    }

    public Answer(Context context, Question Frage, JSONObject j) throws JSONException, ParseException {
        this.context = context;
        this.Antwort = StringEscapeUtils.unescapeJava(j.getString(ANTWORT));
        this.BenutzerID = j.getLong(BENUTZERID);
        this.BenutzerName = j.getString(BENUTZERNAME);
        this.EMail = j.getString(EMAIL);
        this.Datum = Question.sdf.parse(j.getString(DATUM));
        this.Grad = j.getInt(KONTAKTGRAD);
        this.Frage = Frage;
        this.ID = j.getLong(Constants.id);
        this.AntwortID = j.getLong(ANTWORTID);
        if (j.isNull(PARENTFRAGEANTWORTENID)) this.ParentFrageAntwortenID = null; else this.ParentFrageAntwortenID = j.getLong(PARENTFRAGEANTWORTENID);
        if (j.isNull(REANTWORTID)) this.reAntwortID = 0; else this.reAntwortID = j.getLong(REANTWORTID);
        if (j.isNull(BREITENGRAD)) this.Breitengrad = null; else this.Breitengrad = j.getDouble(BREITENGRAD);
        if (j.isNull(LAENGENGRAD)) this.Laengengrad = null; else this.Laengengrad = j.getDouble(LAENGENGRAD);
        if (j.isNull(ONLINE)) this.online = false; else this.online = j.getBoolean(ONLINE);
        if (j.isNull(BEWERTUNG)) this.Bewertung = 0; else this.Bewertung = j.getInt(BEWERTUNG);
        if (j.isNull(BEWERTUNGEN)) this.Bewertungen = null; else this.Bewertungen = j.getInt(BEWERTUNGEN);
        if (j.isNull(FRAGEN)) this.Fragen = null; else this.Fragen = j.getInt(FRAGEN);
        if (j.isNull(ANTWORTEN)) this.Antworten = null; else this.Antworten = j.getInt(ANTWORTEN);
        if (j.isNull(ANTWORTZEIT)) this.Antwortzeit = null; else this.Antwortzeit = j.getInt(ANTWORTZEIT);
        if (j.isNull(AKTIVITAETSINDEX)) this.Aktivitaetsindex = null; else this.Aktivitaetsindex = j.getInt(AKTIVITAETSINDEX);
        if (j.isNull(BESTAENDIGKEIT)) this.Bestaendigkeit = null; else this.Bestaendigkeit = j.getInt(BESTAENDIGKEIT);
        if (j.isNull(WERTUNG)) this.Wertung = null; else this.Wertung = j.getInt(WERTUNG);

        try
        {
            if (j.isNull(GEMELDET)) this.gemeldet = false; else this.gemeldet = j.getInt(GEMELDET)!=0;
        }
        catch (JSONException ex)
        {
            if (j.isNull(GEMELDET)) this.gemeldet = false; else this.gemeldet = j.getBoolean(GEMELDET);
        }
        try
        {
            if (j.isNull(OEFFENTLICH)) this.oeffentlich = false; else this.oeffentlich = j.getInt(OEFFENTLICH)!=0;
        }
        catch (JSONException ex)
        {
            if (j.isNull(OEFFENTLICH)) this.oeffentlich = false; else this.oeffentlich = j.getBoolean(OEFFENTLICH);
        }

        //if (j.isNull("parentfrageantwortenid2")) this.ParentFrageAntwortenID = null; else this.ParentFrageAntwortenID = j.getLong("parentfrageantwortenid2");
        //if (j.isNull("antwort2")) this.Antwort2 = null; else this.Antwort2 = j.getString("antwort2");
        if (j.isNull(ONLINE_STATE)) this.onlineState = this.online ? MainActivity.OnlineState.visible : MainActivity.OnlineState.offline;
        else this.onlineState = MainActivity.OnlineState.getOnlineState(j.getInt(ONLINE_STATE));
    }

    public String getAnswer()
    {
        if (!Frage.isVOTE()) return Antwort;
        if (Antwort.startsWith(YES)) return context.getString(R.string.yes);
        if (Antwort.startsWith(NO)) return context.getString(R.string.no);
        if (Antwort.startsWith(UNDEFINED)) return context.getString(R.string.undefined);
        return Antwort;
    }
    
    public void update2(Question Frage, JSONObject j) throws Throwable
    {
        this.Antwort = StringEscapeUtils.unescapeJava(j.getString(ANTWORT_2));
        this.BenutzerID = j.getLong(BENUTZERID_2);
        this.BenutzerName = j.optString(BENUTZERNAME_2, "");
        this.EMail = j.optString(EMAIL_2,"");
        this.Datum = Question.sdf.parse(j.optString(DATUM_2, Question.sdf.format(Calendar.getInstance().getTime())));
        this.Grad = -10;
        this.Frage = Frage;
        this.ID = j.getLong(ID_2);
        this.AntwortID = j.optLong(ANTWORTID_2, -1);
        if (j.isNull(BREITENGRAD_2)) this.Breitengrad = null; else this.Breitengrad = j.getDouble(BREITENGRAD_2);
        if (j.isNull(LAENGENGRAD_2)) this.Laengengrad = null; else this.Laengengrad = j.getDouble(LAENGENGRAD_2);
        if (j.isNull(ONLINE_2)) this.online = false; else this.online = j.getBoolean(ONLINE_2);
        if (j.isNull(PARENTFRAGEANTWORTENID_2)) this.ParentFrageAntwortenID = null; else this.ParentFrageAntwortenID = j.getLong(PARENTFRAGEANTWORTENID_2);
        if (j.isNull(REANTWORTID_2)) this.reAntwortID = 0; else this.reAntwortID = j.getLong(REANTWORTID_2);
        if (j.isNull(BENUTZERIDSENDER_2)) this.BenutzerIDSender = null; else this.BenutzerIDSender = j.getLong(BENUTZERIDSENDER_2);
        if (j.isNull(BENUTZERIDEMPFAENGER_2)) this.BenutzerIDEmpfaenger = null; else this.BenutzerIDEmpfaenger = j.getLong(BENUTZERIDEMPFAENGER_2);
        if (j.isNull(ANTWORT_2)) this.Antwort2 = null; else this.Antwort2 = j.getString(ANTWORT_2);
        if (j.isNull(BEWERTUNGEN_2)) this.Bewertungen = null; else this.Bewertungen = j.getInt(BEWERTUNGEN_2);
        if (j.isNull(FRAGEN_2)) this.Fragen = null; else this.Fragen = j.getInt(FRAGEN_2);
        if (j.isNull(ANTWORTEN_2)) this.Antworten = null; else this.Antworten = j.getInt(ANTWORTEN_2);
        if (j.isNull(ANTWORTZEIT_2)) this.Antwortzeit = null; else this.Antwortzeit = j.getInt(ANTWORTZEIT_2);
        if (j.isNull(AKTIVITAETSINDEX_2)) this.Aktivitaetsindex = null; else this.Aktivitaetsindex = j.getInt(AKTIVITAETSINDEX_2);
        if (j.isNull(BESTAENDIGKEIT_2)) this.Bestaendigkeit = null; else this.Bestaendigkeit = j.getInt(BESTAENDIGKEIT_2);
        if (j.isNull(WERTUNG_2)) this.Wertung = null; else this.Wertung = j.getInt(WERTUNG_2);
        try
        {
            if (j.isNull(GEMELDET)) this.gemeldet = false; else this.gemeldet = j.getInt(GEMELDET)!=0;
        }
        catch (JSONException ex)
        {
            if (j.isNull(GEMELDET)) this.gemeldet = false; else this.gemeldet = j.getBoolean(GEMELDET);
        }
        try
        {
            if (j.isNull(OEFFENTLICH_2)) this.oeffentlich = false; else this.oeffentlich = j.getInt(OEFFENTLICH_2)!=0;
        }
        catch (JSONException ex)
        {
            if (j.isNull(OEFFENTLICH_2)) this.oeffentlich = false; else this.oeffentlich = j.getBoolean(OEFFENTLICH_2);
        }

        this.Bewertung = -1;
    }
    public void updateUser(JSONObject user) throws JSONException {
        if (user.isNull(ANTWORTZEIT)) this.Antwortzeit = null; else this.Antwortzeit = user.getInt(ANTWORTZEIT);
        if (user.isNull(AKTIVITAETSINDEX)) this.Aktivitaetsindex = null; else this.Aktivitaetsindex = user.getInt(AKTIVITAETSINDEX);
        if (user.isNull(BESTAENDIGKEIT)) this.Bestaendigkeit = null; else this.Bestaendigkeit = user.getInt(BESTAENDIGKEIT);
        if (user.isNull(WERTUNG)) this.Wertung = null; else this.Wertung = user.getInt(WERTUNG);
        if (user.isNull(BEWERTUNGEN)) this.Bewertungen = null; else this.Bewertungen = user.getInt(BEWERTUNGEN);
        if (user.isNull(FRAGEN)) this.Fragen = null; else this.Fragen = user.getInt(FRAGEN);
        if (user.isNull(ANTWORTEN)) this.Antworten = null; else this.Antworten = user.getInt(ANTWORTEN);
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}