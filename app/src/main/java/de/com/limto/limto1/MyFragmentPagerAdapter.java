/*
 * Copyright (c) 2015 GPL by J.M.Goebel. Distributed under the GNU GPL v3.
 * 
 * 08.06.2015
 * 
 * This file is part of learnforandroid.
 *
 * learnforandroid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  learnforandroid is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.com.limto.limto1;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import android.os.Debug;
import android.view.ViewGroup;

import org.json.JSONException;

import de.com.limto.limto1.Errors.SQLiteDataActivity;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.lib.lib.debugMode;


public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private static final String TAG = "MyFragmentPagerAdapter";
    private final FragmentManager fm;
    int PAGE_COUNT = 8;
    /*
    public fragQuestions fragQuestions;
    public fragQuestion fragQuestion;
    public fragAnswer fragAnswer;
    public fragContacts fragContacts;
    public fragContacts2 fragContacts2;
    public fragSettings fragSettings;
    public fragLogin fragLogin;
    */
    //public org.liberty.android.fantastischmemo.downloader.quizlet.QuizletOAuth2AccessCodeRetrievalFragment fragAuthQuizlet;
    public MainActivity main;
    private String[] Tags = new String[PAGE_COUNT];

    /**
     * Constructor of the class
     */
    public MyFragmentPagerAdapter(FragmentManager fm, MainActivity main, boolean restart) {
        super(fm);
        this.fm = fm;
        this.main = main;
        if (restart) {
            this.notifyDataSetChanged();

        }

    }
/*
    public void clear() {
        fragQuestions = null;
        fragAnswer = null;
        fragSettings = null;
        fragQuestion = null;
        fragContacts = null;
        fragLogin = null;
        fragContacts2 = null;
    }
*/
    /**
     * This method will be invoked when a page is requested to create
     */
    public Fragment LastItem;
    public Fragment findFragment(int position)
    {
        try {
            Fragment f = fm.findFragmentByTag(Tags[position]);
            return f;
        }
        catch (Throwable ex)
        {
            return null;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        // get the tags set by FragmentPagerAdapter
        /*int pos = -1;
        if (createdFragment instanceof fragLogin)
        {
            pos = fragLogin.fragID;
        }
        else if (createdFragment instanceof  fragQuestions)
        {
            pos = fragQuestions.fragID;
        }
        else if (createdFragment instanceof  fragQuestion)
        {
            pos = fragQuestion.fragID;
        }
        else if (createdFragment instanceof fragAnswer)
        {
            pos = fragAnswer.fragID;
        }
        else if (createdFragment instanceof fragContacts)
        {
            pos = fragContacts.fragID;
        }
        else if (createdFragment instanceof  fragContacts2)
        {
            pos = fragContacts2.fragID;
        }
        else if (createdFragment instanceof  fragSettings)
        {
            pos = fragSettings.fragID;
        }

        if (pos > -1)*/
        Tags[position] = createdFragment.getTag();
        return createdFragment;
    }
    @Override
    public Fragment getItem(int arg0) {
        switch (arg0) {

            /** tab1 is selected */
            case fragHome.fragID:
                fragHome fragHome = new fragHome();
                fragHome._main = main;
                LastItem = fragHome;
                return fragHome;
            case fragAnswer.fragID:
                fragAnswer fragAnswer = null;
                if (fragAnswer == null) {
                    fragAnswer = new fragAnswer();

                }
                fragAnswer._main = main;
                LastItem = fragAnswer;
                return fragAnswer;

            case fragQuestion.fragID:
                try {
                    fragQuestion fragQuestion = null;
                    if (fragQuestion == null) {
                        fragQuestion = new fragQuestion();
                        //if (main!=null) fragChooser.init(main.getFileChooserIntent(true),main);
                    }
                    fragQuestion._main = main;
                    LastItem = fragQuestion;
                    return fragQuestion;

                } catch (Exception e) {

                    lib.ShowException(TAG, main, e, false);
                }

            case fragSettings.fragID:
                fragSettings fragSettings = null;
                if (fragSettings == null) {
                    fragSettings = new fragSettings();
                    //fragSettings.init(main.getSettingsIntent(),main);
                }
                fragSettings._main = main;
                LastItem = fragSettings;
                return fragSettings;
            case fragQuestions.fragID:
                fragQuestions fragQuestions = null;
                if (fragQuestions == null) {
                    fragQuestions = new fragQuestions();
                    //fragSettings.init(main.getSettingsIntent(),main);
                }
                LastItem = fragQuestions;
                fragQuestions._main = main;
                try {
                    //fragQuestions.init(false);
                } catch (Throwable throwable) {
                    throwable.printStackTrace(); Log.e(TAG,null,throwable);
                }
                return fragQuestions;
            case fragContacts.fragID:
                fragContacts fragContacts = null;
                if (fragContacts == null)
                {
                    fragContacts = new fragContacts();
                }
                fragContacts._main = main;
                LastItem = fragContacts;
                return fragContacts;
            case fragContacts2.fragID:
                fragContacts2 fragContacts2 = null;
                if (fragContacts2 == null)
                {
                    fragContacts2 = new fragContacts2();
                }
                fragContacts2._main = main;
                LastItem = fragContacts2;
                return fragContacts2;
            /*case de.org.Limto.limindo2.fragLogin.fragID:
                fragLogin fragLogin = null;
                if (fragLogin == null)
                {
                    fragLogin = new fragLogin();
                }
                fragLogin._main = main;
                LastItem = fragLogin;
                return fragLogin;*/
            case SQLiteDataActivity.fragID:
                SQLiteDataActivity frag = null;
                if (frag == null)
                {
                    frag = new SQLiteDataActivity();
                }
                frag._main = main;
                LastItem = frag;
                return frag;
            case fragPreferences.fragID:
                fragPreferences fragP = null;
                if (fragP == null)
                {
                    fragP = new fragPreferences();
                }
                fragP._main = main;
                LastItem = fragP;
                return fragP;
            default:
                return null;




        }

        //return null;
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount() {

        try
        {
            if(PAGE_COUNT == 8 && !((debugMode())) && !(main.user != null && main.user.optBoolean(Constants.administrator))) {
                PAGE_COUNT = 7;
                notifyDataSetChanged();
            }
            else if (PAGE_COUNT == 7 && main.user != null && main.user.optBoolean(Constants.administrator)) {
                PAGE_COUNT = 8;
                notifyDataSetChanged();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return PAGE_COUNT;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        if (object == LastItem) LastItem = null;
    }


}
