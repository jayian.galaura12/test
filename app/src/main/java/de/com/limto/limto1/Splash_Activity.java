package de.com.limto.limto1;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;

import de.com.limto.limto1.lib.lib;


/**
 * Created by hmnatalie on 11.09.17.
 */

public class Splash_Activity extends Activity {

    private static final String TAG = "Splash_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PackageInfo p;
        try {
            p = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = p.versionName;
            int versionCode = p.versionCode;
            Bitmap bmOverlay = BitmapFactory.decodeResource(getResources(),R.drawable.vn_logo_c02);
            bmOverlay = bmOverlay.copy(Bitmap.Config.ARGB_8888, true);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            bmOverlay = Bitmap.createScaledBitmap(bmOverlay,size.x,size.y,false);
            Canvas canvas = new Canvas(bmOverlay);
            //canvas.drawColor(Color.LTGRAY);
            //Paint paint = new Paint();

            Paint mPaintText = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaintText.setStrokeWidth(3);
            mPaintText.setStyle(Paint.Style.FILL_AND_STROKE);
            mPaintText.setColor(Color.GREEN);
            mPaintText.setTextSize(lib.convertFromDp(this,30f));
            final String txt = "LimTo Version " + version;
            mPaintText.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
            Rect bounds = new Rect();
            mPaintText.getTextBounds(txt, 0, txt.length(), bounds);
            int left = (bmOverlay.getWidth() - bounds.width()) / 2;
            int top = (bmOverlay.getHeight() - bounds.height()) / 2;
            canvas.drawText(txt, left , top, mPaintText);
            BitmapDrawable drawable = new BitmapDrawable(bmOverlay);
            getWindow().setBackgroundDrawable(drawable);
            /*
            ImageView vw = new ImageView(this);
            vw.setScaleType(ImageView.ScaleType.FIT_XY);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                vw.setBackground(drawable);
            }
            else
            {
                //noinspection deprecation
                vw.setBackgroundDrawable(drawable);
            }

            this.setContentView(vw);
            */
            //Thread.sleep(2000);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        //setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.splash_activity);
            //setContentView(R.layout.activity_main);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(Splash_Activity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    catch (Throwable ex)
                    {
                        lib.ShowException(Splash_Activity.TAG,Splash_Activity.this, ex, true);
                    }
                }
            },5000);

        }
        catch(Throwable e)
        {
            e.printStackTrace();
        }
    }
}
