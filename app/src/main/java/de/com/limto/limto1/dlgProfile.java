package de.com.limto.limto1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import de.com.limto.limto1.Controls.Answer;
import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

import static de.com.limto.limto1.fragContacts2.EMAIL;
import static de.com.limto.limto1.fragQuestions.*;

public class dlgProfile extends DialogFragment {

    MainActivity _main;
    private MainActivity context;
    private TextView txtBenutzerName;
    private TextView txtName;
    private TextView txtAdresse;
    private TextView txtTaetigkeit;
    private ImageView ivProfile;
    private String TAG = "fragprofile";
    private Button btnSchliessen;
    private Question q;
    private Answer a;
    private TextView txtID;
    private Long BenutzerID;
    private String BenutzerName;
    private Button btnPhone;
    private Button btnEMail;
    private Button btnWhatsApp;
    private MainActivity.OnlineState onlineState = MainActivity.OnlineState.offline;
    private TextView txtOnlineState;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (_main == null) _main = (MainActivity) getActivity();
        context = _main;
        View v = inflater.inflate(R.layout.fragprofile, container, false);
        txtID = v.findViewById(R.id.txtID);
        txtBenutzerName = v.findViewById(R.id.txtBenutzername);
        txtName = v.findViewById(R.id.txtName);
        txtAdresse = v.findViewById(R.id.txtAdresse);
        txtTaetigkeit = v.findViewById(R.id.txtTaetigkeitsschwerpunkte);
        txtOnlineState = v.findViewById(R.id.txtOnlineState);
        ivProfile = v.findViewById(R.id.imgProfile);
        btnSchliessen = v.findViewById(R.id.btnClose);
        btnSchliessen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlgProfile.this.dismiss();
            }
        });

        btnPhone = (Button) v.findViewById(R.id.btnPhone);
        btnEMail = (Button) v.findViewById(R.id.btnEMail);
        btnWhatsApp = (Button) v.findViewById(R.id.btnWhatsApp);

        if (this.BenutzerName != null)
        {
            try {
                setValues(this.BenutzerID,this.BenutzerName);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                lib.ShowException(TAG,context,throwable, false);
            }
        }


        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return v;
    }

    void setValues(final Question q, final Answer a) throws Throwable {
        JSONObject N;
        if (a == null) {
            this.onlineState = q.onlineState;
            setValues(q.BenutzerID, q.Benutzername);
            /*
            String msg = getString(R.string.ID) + q.BenutzerID
                    + "\n" + getString(R.string.Benutzername) + q.Benutzername
                    + "\n" + getString(R.string.name2) + (N.isNull(VORNAME) ? "" : N.getString(VORNAME)) + " " + (N.isNull(NAME) ? "" : N.getString(NAME))
                    + (N.getBoolean("gewerblich") ? "\n" + getString(R.string.Adresse) + ":" + (N.isNull("adresse") ? "" : N.getString("adresse")) : "")
                    + (N.isNull(TAETIGKEITSSCHWERPUNKTE) || N.getString(TAETIGKEITSSCHWERPUNKTE).length() == 0 ? "" : "\n" + getString(R.string.Taetigkeitsschwerpunkte) + ":" + N.getString(TAETIGKEITSSCHWERPUNKTE))
                    + "\n" + getString(R.string.Grad) + ":" + q.Grad
                    + "\n" + getString(R.string.cmnuFragen) + q.Fragen
                    + "\n" + getString(R.string.Antworten) + q.Antworten
                    + "\n" + getString(R.string.Bewertungen) + q.Bewertungen
                    + "\n" + getString(R.string.Antwortzeit) + (q.Antwortzeit != null ? DateUtils.formatElapsedTime(q.Antwortzeit) : "null")
                    + "\n" + getString(R.string.Aktivitaetsindex) + ":" + q.Aktivitaetsindex
                    + "\n" + getString(R.string.Bestaendigkeit) + ":" + q.Bestaendigkeit
                    + "\n" + getString(R.string.Wertung) + ":" + q.Wertung;
                    */
        } else {
            this.onlineState = a.onlineState;
            setValues(a.BenutzerID, a.BenutzerName);
            /*
            msg = getString(R.string.ID) + a.BenutzerID
                    + "\n" + getString(R.string.Benutzername) + a.BenutzerName
                    + "\n" + getString(R.string.name2) + (N.isNull(VORNAME) ? "" : N.getString(VORNAME)) + " " + (N.isNull(NAME) ? "" : N.getString(NAME))
                    + (N.getBoolean("gewerblich") ? "\n" + getString(R.string.Adresse) + ":" + (N.isNull("adresse") ? "" : N.getString("adresse")) : "")
                    + (N.isNull(TAETIGKEITSSCHWERPUNKTE) || N.getString(TAETIGKEITSSCHWERPUNKTE).length() == 0 ? "" : "\n" + getString(R.string.Taetigkeitsschwerpunkte) + ":" + N.getString(TAETIGKEITSSCHWERPUNKTE))
                    + "\n" + getString(R.string.Grad) + ":" + a.Grad
                    + "\n" + getString(R.string.cmnuFragen) + a.Fragen
                    + "\n" + getString(R.string.Antworten) + a.Antworten
                    + "\n" + getString(R.string.Bewertungen) + a.Bewertungen
                    + "\n" + getString(R.string.Antwortzeit) + (a.Antwortzeit != null ? DateUtils.formatElapsedTime(a.Antwortzeit) : "null")
                    + "\n" + getString(R.string.Aktivitaetsindex) + ":" + a.Aktivitaetsindex
                    + "\n" + getString(R.string.Bestaendigkeit) + ":" + a.Bestaendigkeit
                    + "\n" + getString(R.string.Wertung) + ":" + a.Wertung;
            */
        }

    }

    void setValues(Long BenutzerID, String Benutzername) throws Exception {
        if (txtID == null)
        {
            this.BenutzerID = BenutzerID;
            this.BenutzerName = Benutzername;
            return;
        }
        JSONObject N = _main.clsHTTPS.getUser(_main.getBenutzerID(), BenutzerID);
        txtID.setText("" + BenutzerID);
        txtBenutzerName.setText(Benutzername);
        txtName.setText((N.isNull(VORNAME) ? "" : N.getString(VORNAME)) + " " + (N.isNull(NAME) ? "" : N.getString(NAME)));
        txtAdresse.setVisibility(N.getBoolean("gewerblich") ? View.VISIBLE : View.GONE);
        txtAdresse.setText((N.isNull("adresse") ? "" : N.getString("adresse")));
        txtTaetigkeit.setVisibility(N.getBoolean("gewerblich") ? View.VISIBLE : View.GONE);
        txtTaetigkeit.setText((N.isNull(TAETIGKEITSSCHWERPUNKTE) ? "" : N.getString(TAETIGKEITSSCHWERPUNKTE)));
        txtOnlineState.setText(this.onlineState.getStateString());
        Bitmap profile = null;
        try {
            String fname;
            fname = ".JPG";
            Bitmap in = _main.clsHTTPS.downloadProfileImage(this.getContext(), BenutzerID, _main.user.getLong(Constants.id), Benutzername);
            if (in != null) {
                profile = in;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            Log.i(TAG, null, e);
        }

        if (profile != null) {
            ivProfile.setImageBitmap(profile);
        }
        final ContactVO contactVO = new ContactVO(N);
        if (!contactVO.isBestaetigt() && contactVO.isLimindoUser() && (_main.clsHTTPS.AdminID == null || _main.clsHTTPS.AdminID == 0)) {
            btnEMail.setVisibility(View.GONE);
            btnPhone.setVisibility(View.GONE);
            btnWhatsApp.setVisibility(View.GONE);
        }
        else
        {
            if (contactVO.getContactEmail().length()>3) {
                btnEMail.setTag(contactVO.getContactEmail());
                btnEMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                        String aEmailList[] = {contactVO.getContactEmail()};
                        //String aEmailCCList[] = { "user3@fakehost.com","user4@fakehost.com"};
                        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
                        //emailIntent.putExtra(android.content.Intent.EXTRA_CC, aEmailCCList);
                        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                        MainActivity.dontStop++;
                        context.startActivityForResult(emailIntent,MainActivity.RGLOBAL);
                    }
                });
            }
            else
            {
                btnEMail.setVisibility(View.GONE);
            }
            if (contactVO.getContactNumber().length()>3) {
                btnPhone.setTag(contactVO.getContactNumber());
                btnPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String phone = contactVO.getContactNumber();
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        MainActivity.dontStop++;
                        startActivityForResult(intent,MainActivity.RGLOBAL);
                    }
                });
                btnWhatsApp.setTag(contactVO.getContactNumber());
                btnWhatsApp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (contactVO.getContactNumber().length() > 0) {
                            String number = contactVO.getContactNumber();
                            number = number.replaceFirst("\\+", "");
                            String message = "";
                            try {
                                /*Intent sendIntent = new Intent("android.intent.action.MAIN");
                                //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.setType("text/plain");
                                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                                sendIntent.putExtra("jid", number + "@s.whatsapp.net"); //phone number without "+" prefix
                                sendIntent.setPackage("com.whatsapp");
                                startActivityForResult(sendIntent,MainActivity.RGLOBAL);
                                */
                                String url = "https://api.whatsapp.com/send?phone="+number;
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(url));
                                MainActivity.dontStop++;
                                startActivityForResult(i,MainActivity.RGLOBAL);
                            } catch (Exception e) {

                            }

                        }
                    }
                });
            }
            else
            {
                btnPhone.setVisibility(View.GONE);
                btnWhatsApp.setVisibility(View.GONE);
            }

        }


    }
}
