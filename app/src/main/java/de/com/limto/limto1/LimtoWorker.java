package de.com.limto.limto1;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.List;
import java.util.concurrent.ExecutionException;

import de.com.limto.limto1.lib.lib;

import static de.com.limto.limto1.broadcastReceiver.BOOT;
import static de.com.limto.limto1.lib.lib.gStatus;

public class LimtoWorker extends Worker {
    static final String AUTOKILL = "autokill";
    static final int RESTARTINTERVALL = 60000;
    static final int IAUTOKILL = 10000;
    static int count = 0;
    private static final String TAG = "LimtoWorker";

    public LimtoWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override
    public Result doWork() {


        if (broadcastReceiver.service != null && !broadcastReceiver.service.blnWasDestroyed) {
            android.util.Log.i(TAG, "LimtoWorkerRetry");
            return Result.retry();
        }

        android.util.Log.i(TAG, "LimtoWorkerStarted");

        lib.setStatusAndLog( getApplicationContext(), TAG, "startBootWorker");

        Intent serviceIntent;
        serviceIntent = new Intent(getApplicationContext(), LimindoService.class);
        serviceIntent.putExtra(BOOT, true);
        serviceIntent.putExtra(AUTOKILL, IAUTOKILL);
        serviceIntent.setAction(BOOT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.getApplicationContext().startForegroundService(serviceIntent);
        } else {
            this.getApplicationContext().startService(serviceIntent);
        }

        // Indicate whether the work finished successfully with the Result
        return Result.success();
    }

    public static boolean isWorkScheduled(String tag, Context c) {
        WorkManager instance = WorkManager.getInstance(c);
        ListenableFuture<List<WorkInfo>> statuses = instance.getWorkInfosByTag(tag);
        try {
            boolean running = false;
            List<WorkInfo> workInfoList = statuses.get();
            for (WorkInfo workInfo : workInfoList) {
                WorkInfo.State state = workInfo.getState();
                running = state == WorkInfo.State.RUNNING | state == WorkInfo.State.ENQUEUED;
                if (running) break;
            }
            return running;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }
}
