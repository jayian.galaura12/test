package de.com.limto.limto1;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import de.com.limto.limto1.lib.lib;
import de.com.limto.limto1.logger.Log;

public class broadcastReceiver extends BroadcastReceiver {
    public static final String BOOT = "boot";
    public static final String BROADCAST_RECEIVER = "LIMTO broadcastReceiver";
    public static final String ANDROID_INTENT_ACTION_QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON";
    public static final String COM_HTC_INTENT_ACTION_QUICKBOOT_POWERON = "com.htc.intent.action.QUICKBOOT_POWERON";
    public static final String BROADCAST = "Broadcast";
    private static final String TAG = "BroadcastReceiver";
    public static LimindoService service;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            lib.setStatusAndLog(context, TAG, "onReceive");
            lib.setStatusAndLog(context, TAG, intent.getAction());
            //lib.ShowToast(context, BROADCAST);
            if (Intent.ACTION_SHUTDOWN.equals(intent.getAction())) {
                if (service != null) {
                    try {
                        service.stop(false);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(BROADCAST_RECEIVER,throwable.getMessage(), throwable);
                    }
                }
            } else {

                if (service != null) return;
                //Intent serviceIntent;
                //serviceIntent = new Intent(context, LimindoService.class);
                //serviceIntent.putExtra(BOOT, true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    //context.startForegroundService(serviceIntent);
                    lib.scheduleJob(context);
                } else {
                    //context.startService(serviceIntent);
                    startServiceByAlarm(context);
                }


            }
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
            lib.setStatusAndLog( context, TAG, ex.getMessage());
            android.util.Log.e(BROADCAST_RECEIVER,null,ex);
            //lib.ShowToast(context, ex.getMessage());
        }
    }

    private static final String TAG_BOOT_BROADCAST_RECEIVER = "BOOT_BROADCAST_RECEIVER";

    /* Create an repeat Alarm that will invoke the background service for each execution time.
     * The interval time can be specified by your self.  */
    private void startServiceByAlarm(Context context)
    {
        // Get alarm manager.
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        lib.setStatusAndLog( context, TAG, "startServiceByAlarm");

        // Create intent to invoke the background service.
        Intent intent = new Intent(context, LimindoService.class);
        intent.setAction(BOOT);
        intent.putExtra(BOOT, true);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long startTime = System.currentTimeMillis();
        long intervalTime = 60 *60 *1000;

        String message = "Start service use repeat alarm. ";

        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

        Log.d(TAG_BOOT_BROADCAST_RECEIVER, message);

        // Create repeat alarm.
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startTime, intervalTime, pendingIntent);
    }
}
