package de.com.limto.limto1;

import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.test.espresso.AmbiguousViewMatcherException;
import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import de.com.limto.limto1.Controls.Question;
import de.com.limto.limto1.Controls.ZoomExpandableListview;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withHint;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSubstring;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.startsWith;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule
            = new ActivityTestRule<>(
            MainActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False to customize the intent

    @Before
    public void run() {
        try {
            Intent intent = new Intent();
            intent.putExtra("nodialogs", "true");
            intent.putExtra("autoanswer", "true");
            activityRule.launchActivity(intent);
            MainActivity main = activityRule.getActivity();
            int count = 0;
            do {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    count = 100;
                }
                count++;
            } while (count < 100 && (main.clsHTTPS.accesskey == null || main.user == null));

            count = 0;
            ZoomExpandableListview list = null;
            fragQuestions f = null;
            do {
                if (f == null) f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                if (f != null && list == null) {
                    list = (ZoomExpandableListview) f.getView().findViewById(R.id.lvQuestions);
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    count = 100;
                }
                count++;

            } while (count < 100 && (list == null || list.getAdapter() == null));
        } catch (Throwable ex)
        {
            ex.printStackTrace();
        }

    }
//    @UiThreadTest


    //@Test
    public void _4_DeleteTestDataQuestions() {
        MainActivity main = activityRule.getActivity();

        if (main.clsHTTPS.accesskey != null && main.user != null) {
            ArrayList<Question> res = null;
            try {
                // main.mPager.setCurrentItem(fragQuestions.fragID);
                //fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                //res = f.getQuestions(true, false, 0, 5, 1000, false, false, false, 0, false, "", null);
                ViewInteraction v = onView(withId(R.id.btnNewAnswersForOwnQuestions));
                v.check(matches(isDisplayed()));
                v.perform(click());

                /*onView(withId(R.id.btnKontakte))
                        .perform(click())
                        .check(matches(isDisplayed()));
*/
                fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                final ZoomExpandableListview list = (ZoomExpandableListview) f.getView().findViewById(R.id.lvQuestions);

                assertNotNull("The list was not loaded", list);
                assertNotNull("The listadapter was not loaded", list.getAdapter());
                //QuestionsAdapter g = (QuestionsAdapter) list.getAdapter();
                int i = 0;
                boolean lastItem = false;
                do {
                    int position = i;
                    //list.performItemClick(list.getAdapter().getView(position, null, null),
                    //        position, list.getAdapter().getItemId(position));
                    boolean err = false;
                    boolean mnuQuestionShown = false;
                    do {
                        try {
                            DataInteraction view = onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i);
                            view.check(matches(isDisplayed()));
                            try {
                                view.onChildView(withText(startsWith("espressotest"))).check(matches(isDisplayed()));
                                view.onChildView(withId(R.id.imgMenu)).perform(click());
                                onView(withText(R.string.Archivieren)).perform(click());
                            } catch (Throwable ex) {
                                i++;
                            }


                        } catch (Throwable ex) {

                            err = true;
                        }
                    } while (!err);

                } while (false);


                getInstrumentation().waitForIdleSync();
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            }
            if (res != null) {
                System.out.println(res.size());
            } else {
                //fail();
            }


        } else {
            fail();
        }

        // Continue with your test
    }

    //@Test
    public void _3_DeleteTestDataAnswers() {
        MainActivity main = activityRule.getActivity();

        if (main.clsHTTPS.accesskey != null && main.user != null) {
            ArrayList<Question> res = null;
            try {
                // main.mPager.setCurrentItem(fragQuestions.fragID);
                //fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                //res = f.getQuestions(true, false, 0, 5, 1000, false, false, false, 0, false, "", null);
                ViewInteraction v = selectButtonNewQuestionsOfContacts();
                /*onView(withId(R.id.btnKontakte))
                        .perform(click())
                        .check(matches(isDisplayed()));
*/
                fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                final ZoomExpandableListview list = (ZoomExpandableListview) f.getView().findViewById(R.id.lvQuestions);

                assertNotNull("The list was not loaded", list);
                assertNotNull("The listadapter was not loaded", list.getAdapter());
                //QuestionsAdapter g = (QuestionsAdapter) list.getAdapter();
                int i = 0;
                boolean lastItem = false;
                do {
                    int position = i;
                    //list.performItemClick(list.getAdapter().getView(position, null, null),
                    //        position, list.getAdapter().getItemId(position));
                    boolean err = false;
                    boolean mnuQuestionShown = false;
                    do {
                        try { //click on indicator to show answers
                            onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i).onChildView(withId(R.id.imgIndicator)).perform(click());
                            i++;
                            try {
                                onView(withText(main.getString(R.string.Antwort))).check(matches(isDisplayed()));
                                Espresso.pressBack();
                                mnuQuestionShown = true;
                            } catch (Throwable ex) {

                            }

                        } catch (Throwable ex) {

                            err = true;
                        }
                    } while (!err);
                    boolean done = false;

                    do {
                        try {
                            onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i).onChildView(withSubstring("espressotest")).check(matches(isDisplayed()));
                            onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i).onChildView(withId(R.id.imgMenu)).perform(click());
                        } catch (Throwable ex) {
                            done = true;
                        }
                        if (!done) {
                            try {
                                onView(withText(main.getString(R.string.Delete)))
                                        .check(matches(isDisplayed()))
                                        .perform(click());
                            } catch (Throwable ex) {
                                i++;
                                Espresso.pressBack();
                            }
                        }
                        err = false;
                        do {
                            try {
                                onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i).onChildView(withId(R.id.imgIndicator)).perform(click());
                                i++;
                                onView(withText(main.getString(R.string.Antwort))).check(matches(isDisplayed()));
                                Espresso.pressBack();
                            } catch (Throwable ex) {
                                err = true;
                            }
                        } while (!err);


                    } while (!done);
                    i++;
                    try {
                        onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i).check(matches(isDisplayed()));
                    } catch (Throwable ex) {
                        lastItem = true;
                    }
                } while (!lastItem);


                getInstrumentation().waitForIdleSync();
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            if (res != null) {
                System.out.println(res.size());
            } else {
                //fail();
            }


        } else {
            fail();
        }

        // Continue with your test
    }

    //@Test
    public void _2_enterTestDataAnswes() {
        MainActivity main = activityRule.getActivity();

        if (main.clsHTTPS.accesskey != null && main.user != null) {
            ArrayList<Question> res = null;
            try {
                // main.mPager.setCurrentItem(fragQuestions.fragID);
                //fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                //res = f.getQuestions(true, false, 0, 5, 1000, false, false, false, 0, false, "", null);
                ViewInteraction v = selectButtonNewQuestionsOfContacts();

                /*onView(withId(R.id.btnKontakte))
                        .perform(click())
                        .check(matches(isDisplayed()));
*/
                fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                final ZoomExpandableListview list = (ZoomExpandableListview) f.getView().findViewById(R.id.lvQuestions);

                checkIfListLoaded (list);
                //QuestionsAdapter g = (QuestionsAdapter) list.getAdapter();
                int i = 0;
                boolean lastItem = false;
                DataInteraction parentview = null;

                do {
                    int position = i;
                    DataInteraction view = null;
                    boolean blnVote = false;
                    boolean blnVote2 = false;
                    //list.performItemClick(list.getAdapter().getView(position, null, null),
                    //        position, list.getAdapter().getItemId(position));
                    DataInteraction p = null;
                    try {
                        view = onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i);
                        try {
                            p = view.onChildView(withId(R.id.imgIndicator));
                            p.check(matches(isDisplayed()));
                            parentview = p;
                        } catch (Throwable ex) {
                            ex.printStackTrace();
                        }
                        view.onChildView(withId(R.id.txtGrad)).perform(click());
                        try {
                            try {
                                ViewInteraction btnOeffentlich = onView(allOf(withId(R.id.btnOeffentlich), withText(main.getString(R.string.yes))));
                                btnOeffentlich.check(matches(isDisplayed()));
                                blnVote = true;
                            } catch (Throwable ex) {
                                ViewInteraction btnOeffentlich = onView(withContentDescription(main.getString(R.string.publicAnswer)));
                                btnOeffentlich.check(matches(isDisplayed()));
                            }
                        } catch (Throwable eex) {
                            try {
                                onView(withText(R.string.OK))
                                        .inRoot(isDialog()) // <---
                                        .check(matches(isDisplayed()))
                                        .perform(click());
                                i++;
                                continue;
                            } catch (Throwable eeex) {
                                eeex.printStackTrace();
                                i++;
                                continue;
                            }
                        }

                    } catch (Throwable ex) {
                        break;
                    }


                    try {
                        view.check(matches(isDisplayed()));
                        blnVote2 = true;
                        i++;
                        continue;
                    } catch (Throwable eeex) {
                        eeex.printStackTrace();
                    }
                    /*onView(withId(R.id.imgQuestion))
                            .perform(click())
                            .check(matches(isDisplayed()));
                            */
                    String text = "espressotest" + i + ": " + UUID.randomUUID().toString();
                    String t = main.getString(R.string.OK);

                    try {
                        boolean vote = blnVote;
                        ViewInteraction btnOeffentlich = onView(withId(R.id.btnOeffentlich));
                        ViewInteraction btnPrivat = onView(withId(R.id.btnPrivat));
                        try {
                            btnOeffentlich = onView(allOf(withId(R.id.btnOeffentlich), withText(main.getString(R.string.yes))));
                            btnOeffentlich.check(matches(isDisplayed()));
                            btnPrivat = onView(allOf(withId(R.id.btnPrivat), withText(main.getString(R.string.no))));
                            vote = true;
                            int random = new Random().nextInt(99);
                            if (random > 66) {
                                btnOeffentlich.perform(click());
                                text = main.getString(R.string.yes);
                            } else if (random > 33) {
                                btnPrivat.perform(click());
                                text = main.getString(R.string.no);
                            } else {
                                text = main.getString(R.string.undefined);
                            }
                        } catch (Throwable ex) {
                            try {
                                btnOeffentlich = onView(withContentDescription(main.getString(R.string.publicAnswer)));
                                btnOeffentlich.check(matches(isDisplayed()));
                                btnOeffentlich.perform(click());
                            } catch (Throwable eex) {
                                eex.printStackTrace();
                            }
                        }
                        if (!vote)
                            onView(withId(R.id.txtAnswer)).perform(typeText(text), closeSoftKeyboard());
                    } catch (Throwable ex) {
                        Throwable e = null;
                        int counter = 0;
                        counter++;
                        e = null;
                        try {
                            onView(withText(t))
                                    .inRoot(isDialog()) // <---
                                    .check(matches(isDisplayed()))
                                    .perform(click());
                            i++;
                            continue;
                        } catch (Throwable eex) {
                            eex.printStackTrace();
                            e = eex;
                        }

                    }
                    boolean enterverified = false;
                    if (!blnVote2) {
                        onView((withContentDescription(main.getString(R.string.enternewanswer)))).perform(click());
                        try {
                            try {
                                view.check(matches(isDisplayed()));
                            } catch (Throwable ex) {
                                ex.printStackTrace();
                            }

                            for (int l = 0; l < 10; l++) {
                                try {
                                    ViewInteraction vv = onView(withText(text));
                                    vv.check(matches(isDisplayed()));
                                    enterverified = true;
                                } catch (AmbiguousViewMatcherException a) {
                                    a.printStackTrace();
                                } catch (Throwable ll) {
                                    ll.printStackTrace();
                                    if (l == 0) {
                                        try {
                                            onView(withText(t))
                                                    .inRoot(isDialog()) // <---
                                                    .check(matches(isDisplayed()))
                                                    .perform(click());
                                            onView(withText(t))
                                                    .inRoot(isDialog()) // <---
                                                    .check(matches(isDisplayed()))
                                                    .perform(click());
                                        } catch (Throwable eex) {
                                            eex.printStackTrace();
                                        }
                                    }
                                    if (l == 9) throw ll;
                                    try {
                                        if (parentview != null) parentview.perform(click());
                                    } catch (Throwable ex) {
                                        ex.printStackTrace();
                                        if (!blnVote) {
                                            onView(withId(R.id.txtAnswer)).perform(clearText());
                                            onView(withId(R.id.txtAnswer)).perform(typeText(text), closeSoftKeyboard());
                                            onView((withContentDescription(main.getString(R.string.enternewanswer)))).perform(click());
                                        }
                                        onView(withId(R.id.mnuQuestions)).perform(click());
                                    }
                                    continue;
                                }
                                break;
                            }
                            //vv = vv.onChildView(withText(text));
                            //vv.check(matches(isDisplayed()));
                            //vv.check(matches(withText(text)));
                        } catch (AmbiguousViewMatcherException a) {
                            a.printStackTrace();
                        } catch (Throwable x) {
                            /*
                            try {
                                view.perform(ViewActions.)
                                view.onChildView(withId(R.id.imgIndicator)).perform(click());
                                ViewInteraction vv = onView(withText(text));
                                vv.check(matches(isDisplayed()));
                            } catch (AmbiguousViewMatcherException a) {
                                a.printStackTrace();
                            } catch (Throwable ex) { */
                            //if (i == 0) fail();
                            try {
                                onView(withText(t))
                                        .inRoot(isDialog()) // <---
                                        .check(matches(isDisplayed()))
                                        .perform(click());
                                pressBack();
                                pressBack();
                            } catch (Throwable ex) {
                                ex.printStackTrace();
                            }

                        }
                        //}
                        if (enterverified) i++;
                    }
                    for (int k = 0; k < 10; k++) {
                        try {
                            onView(withId(R.id.mnuQuestions)).perform(click());
                        } catch (Throwable ex) {
                            ex.printStackTrace();
                            Thread.sleep(1000);
                            continue;
                        }
                        break;
                    }
                    i++;
                }
                while (!lastItem);


                getInstrumentation().waitForIdleSync();
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                fail();
            }
            if (res != null) {
                System.out.println(res.size());
            } else {
                //fail();
            }


        } else {
            fail();
        }

        // Continue with your test
    }

    private void checkIfListLoaded(ZoomExpandableListview list) {
        assertNotNull("The list was not loaded", list);
        assertNotNull("The listadapter was not loaded", list.getAdapter());
    }

    private ViewInteraction selectButtonNewQuestionsOfContacts() throws Throwable {
        ViewInteraction v = onView(withId(R.id.btnNewQuestionsOfContacts));
        v.check(matches(isDisplayed()));
        v.perform(click());
        return v;
    }

    public static Matcher<View> nthChildOf(final Matcher<View> parentMatcher, final int childPosition) {
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("position " + childPosition + " of parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                if (!(view.getParent() instanceof ViewGroup)) return false;
                ViewGroup parent = (ViewGroup) view.getParent();

                return parentMatcher.matches(parent)
                        && parent.getChildCount() > childPosition
                        && parent.getChildAt(childPosition).equals(view);
            }
        };
    }
    ViewInteraction vvStart = null;
    @Test
    public void _0_sendChatMessages() {
        MainActivity main = activityRule.getActivity();
        vvStart = null;
        if (main.clsHTTPS.accesskey != null && main.user != null) {
            ArrayList<Question> res = null;
            try {
                // main.mPager.setCurrentItem(fragQuestions.fragID);
                //fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                //res = f.getQuestions(true, false, 0, 5, 1000, false, false, false, 0, false, "", null);
                boolean rres = ClickMenuChat();
                SelectFirstContactIfVisible();
                ViewInteraction v = onView(withId(R.id.rg_chat_members));
                int ii = 0;
                int loops = 0;

                do {
                    try {
                        String txt = "testmessage" + ii + "loop" + loops;
                        if (!SelectContactAndSendChatMessage(ii, txt)) break;
                        ValidateSentText(txt);
                    } catch (NoMatchingViewException ee) {
                        loops++;
                        ii = -1;
                        if (vvStart != null) {
                            vvStart.perform(scrollTo(), click());
                        }
                        if (loops > 5) break;
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                        fail();
                    }
                    ii++;
                } while (true);


            } catch (Throwable ex) {
                ex.printStackTrace();
                fail();
            }
        }
    }

    private void ValidateSentText(String txt) {
        DataInteraction a = onData(anything()).inAdapterView(withId(R.id.messages_view));
        int i = 0;
        do {
            try {
                DataInteraction vvv = a.atPosition(i);
                //vvv.check(matches(isDisplayed()));
                try {
                    DataInteraction vvvv = vvv.onChildView(withId(R.id.rlmy_message)).onChildView(withId(R.id.message_body));
                    vvvv.perform(scrollTo());
                    ViewInteraction vspan = vvvv.check(matches(new withSpannable(txt)));
                    if (vspan != null) vspan.check(matches(isDisplayed())); else continue;
                    break;
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }
            } catch (Throwable ex) {
                ex.printStackTrace();
                fail();
            }
            i++;
        } while (true);
    }

    class withSpannable extends BoundedMatcher<View, TextView> {

        private final String text;

        public withSpannable(String text) {
            super(TextView.class);
            this.text = text;
        }

        @Override
        protected boolean matchesSafely(TextView item) {
            try {
                String txt = item.getText().toString();
                System.out.println(txt);
                return txt.equals(text);
            }
            catch (Throwable ex)
            {
                return false;
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("with spannable")
                    .appendValue(text);
        }
    }

    private boolean SelectContactAndSendChatMessage(int ii, String txt) {
        ViewInteraction vv = onView(nthChildOf(withId(R.id.rg_chat_members), ii));
        if (vv == null) return false;
        vv.check(matches(isDisplayed()));
        vv.perform(scrollTo(), click());
        if (ii == 0) vvStart = vv;
        vv = onView(withId(R.id.editText));
        vv.check(matches(isDisplayed()));
        vv.perform(typeText(txt));
        vv = onView(withId(R.id.btnSend));
        vv.perform(click());

        return true;
    }

    private void SelectFirstContactIfVisible() {
        try {
            ViewInteraction vv = onView(withId(R.id.rvContacts));
            vv.check(matches(isDisplayed()));
            vv.perform(actionOnItemAtPosition(0, click()));
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    private boolean ClickMenuChat() throws Throwable {
        try {
            ViewInteraction v = onView(withId(R.id.mnuChat));
            v.check(matches(isDisplayed()));
            v.perform(click());
            return true;
        } catch (Throwable ex)
        {
            ex.printStackTrace();
            return false;
        }
    }

    //@Test
    public void _1_enterTestDataQuestions() {
        MainActivity main = activityRule.getActivity();

        if (main.clsHTTPS.accesskey != null && main.user != null) {
            ArrayList<Question> res = null;
            try {
                // main.mPager.setCurrentItem(fragQuestions.fragID);
                //fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
                //res = f.getQuestions(true, false, 0, 5, 1000, false, false, false, 0, false, "", null);
                ViewInteraction v = selectButtonNewQuestionsOfContacts();

                /*onView(withId(R.id.btnKontakte))
                        .perform(click())
                        .check(matches(isDisplayed()));
*/
                ZoomExpandableListview list = getListView(main);
                //QuestionsAdapter g = (QuestionsAdapter) list.getAdapter();
                int i = 0;
                boolean lastItem = false;
                boolean r;
                do {
                    int position = i;
                    ViewInteraction view = null;
                    boolean blnVote = false;
                    System.out.println("Test " + i);
                    //list.performItemClick(list.getAdapter().getView(position, null, null),
                    //        position, list.getAdapter().getItemId(position));
                    r = enterquestion(view, lastItem, main, i, blnVote);
                    i++;

                } while (r && i < 100);


                getInstrumentation().waitForIdleSync();
            } catch (Exception e) {
                e.printStackTrace();
                fail();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                fail();
            }
            if (res != null) {
                System.out.println(res.size());
            } else {
                //fail();
            }


        } else {
            fail();
        }

        try {
            ViewInteraction view = onView(withId(R.id.mnuHome));
            view.perform(click());
        } catch (Throwable ignored) {
        }
        //enterTestDataAnswes();

        // Continue with your test
    }

    private ZoomExpandableListview getListView(MainActivity main) {
        fragQuestions f = (fragQuestions) main.fPA.findFragment(fragQuestions.fragID);
        final ZoomExpandableListview list = (ZoomExpandableListview) f.getView().findViewById(R.id.lvQuestions);

        assertNotNull("The list was not loaded", list);
        assertNotNull("The listadapter was not loaded", list.getAdapter());
        return list;
    }

    private boolean enterquestion(ViewInteraction view, boolean lastItem, MainActivity main,
                                  int i, boolean blnVote) {

        // select bottom menu question;
        try {
            view = onView((withId(R.id.mnuQuestion)));
            view.perform(click());
        } catch (Throwable ex) {
            lastItem = true;
            return false;
        }
                    /*onView(withId(R.id.imgQuestion))
                            .perform(click())
                            .check(matches(isDisplayed()));
                            */
        String text = "espressotest" + i + ": " + UUID.randomUUID().toString();
        String t = main.getString(R.string.OK);

        try {
            setQuestionToPublicAndEnterSampleText(i, text);
        } catch (Throwable ex) {
            Throwable e = null;
            int counter = 0;
            Inte inte = new Inte(i);
            counter = checkIfDialogIsDisplayedAndEnterTextAgain(counter, e, t, text, inte);
            i = inte.x;
            if (counter == 99) return true;
        }
        if (!blnVote) {
            try {
                enterNewQuestionAndCheckIfTextIsDisplayed(main, text, i, t);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                fail();
            }

        }
        return true;
    }

    private boolean enterNewQuestionAndCheckIfTextIsDisplayed(MainActivity main, String text,
                                                           int i, String t) throws Throwable {
        onView((withContentDescription(main.getString(R.string.enternewquestion)))).perform(click());
        try {
            onData(hasToString(startsWith(text)))
                    .inAdapterView(withId(R.id.lvQuestions))
                    .perform(scrollTo());
            ViewInteraction vv = onView(withText(text));
            vv.check(matches(isDisplayed()));
            return true;
            //vv = vv.onChildView(withText(text));
            //vv.check(matches(isDisplayed()));
            //vv.check(matches(withText(text)));
        } catch (AmbiguousViewMatcherException a) {
            a.printStackTrace();
        } catch (Throwable ex) {
            ex.printStackTrace();
            if (i == 0) return false;
            onView(withText(t))
                    .inRoot(isDialog()) // <---
                    .check(matches(isDisplayed()))
                    .perform(click());
            pressBack();
        }
        return false;
    }

    public class Inte {
        Inte(int x) {
            this.x = x;
        }

        public int x = 0;
    }

    private Integer checkIfDialogIsDisplayedAndEnterTextAgain(int counter, Throwable e, String
            t, String text, Inte i) {
        do {
            counter++;
            e = null;
            try {
                onView(withText(t))
                        .inRoot(isDialog()) // <---
                        .check(matches(isDisplayed()))
                        .perform(click());
                i.x++;
            } catch (Throwable eex) {
                eex.printStackTrace();
                e = eex;
                counter = 99;
                break;
            }
            //onData(anything()).inAdapterView(withId(R.id.lvQuestions)).atPosition(i).perform(click());
            e = null;
            try {
                onView(withId(R.id.edtxtQuestion)).perform(clearText());
                onView(withId(R.id.edtxtQuestion)).perform(typeText(text), closeSoftKeyboard());
            } catch (Throwable eex) {
                eex.printStackTrace();
                e = eex;
            }
            if (counter > 5) {
                fail();
            }
        } while (e != null);
        return counter;
    }

    private void setQuestionToPublicAndEnterSampleText(int i, String text) {
        boolean vote = false;
        ViewInteraction btnOeffentlich = onView(withId(R.id.btnOeffentlich));
        ViewInteraction btnPrivat = onView(withId(R.id.btnPrivat));
        try {
            btnOeffentlich = onView(withContentDescription(R.string.publicQuestion));
            btnOeffentlich.check(matches(isDisplayed()));
            btnOeffentlich.perform(click());
        } catch (Throwable eex) {
            eex.printStackTrace();
        }
        System.out.println("Test " + i + " " + text);

        if (!vote)
            onView(withId(R.id.edtxtQuestion)).perform(clearText());
        onView(withId(R.id.edtxtQuestion)).perform(typeText(text), closeSoftKeyboard());
    }
}
