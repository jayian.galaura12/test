package de.com.limto.limto1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import de.com.limto.limto1.lib.*;

public class LimtoTest {

    @Test
    public void testlib() {
        int actual = lib.dpToPx(50);
        // expected value is 212
        actual = lib.pxToDp(actual);
        int expected = 50;
        // use this method because float is not precise
        assertEquals("Conversion dpToPX failed", expected, actual, 1);
    }

    @Test
    public void testlib2() {
        float actual = lib.pxToDp(100);
        // expected value is 100
        float expected = actual;
        // use this method because float is not precise
        assertEquals("Conversion from px to dp failed", expected, actual, 0.001);
    }

}